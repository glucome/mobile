package com.glucomeandroidapp.Fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.MEDICION_MAXIMA_ACEPTABLE;
import static com.glucomeandroidapp.Utils.Util.MEDICION_MINIMA_ACEPTABLE;


/**
 * Created by MartinArtime on 14 de mayo del 2019
 **/
public class ResumenPacienteFragment extends Fragment {

    @BindView(R.id.sv_grafico) ScrollView svGrafico;
    @BindView(R.id.sv_grafico_vacio) ScrollView svGraficoVacio;
    @BindView(R.id.filtro_cantidad_de_registros) Spinner spFiltro;
    @BindView(R.id.grafico_ultimas_mediciones) LineChart grUltimasMediciones;

    Activity padre;
    Context context;

    public LoginResponse loginResponse;

    public static ResumenPacienteFragment newInstance() {return new ResumenPacienteFragment();}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_resumen, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        padre = getActivity();
        context = padre.getApplicationContext();


        // Construyo el grafico de ultimas mediciones obteniendo todas las mediciones de la BD
        grUltimasMediciones.setNoDataText("Usted no ha cargado ninguna medición aún.");
        grUltimasMediciones.setNoDataTextColor(R.color.colorNegro);
        List<Medicion> mediciones = DataBaseManager.getDaoSession().getMedicionDao().loadAll();
        if(!mediciones.isEmpty()){
            construirGraficoDeUltimasMediciones(mediciones);
            svGrafico.setVisibility(View.VISIBLE);
        }else{
            svGraficoVacio.setVisibility(View.VISIBLE);
        }


        // Obtengo el usuario logueado
        loginResponse = AccountDataManager.obtenerDatosUsuario();

    }



    /**
     * Construye el gráfico de ultimas mediciones con valores dummy.
     */
    private void construirGraficoDeUltimasMediciones(List<Medicion> mediciones) {

        ArrayList<String> xVals = new ArrayList<>();
        ArrayList<Entry> yVals = new ArrayList<>();

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int i=0; i<mediciones.size(); i++){
            int value = mediciones.get(i).getValorMedicion().intValue();

            if(value <= MEDICION_MINIMA_ACEPTABLE){
                colors.add(ContextCompat.getColor(context, R.color.colorDanger));
            } else if(value >= MEDICION_MAXIMA_ACEPTABLE){
                colors.add(ContextCompat.getColor(context, R.color.colorDanger));
            } else {
                colors.add(Color.BLACK);
            }

            xVals.add(mediciones.get(i).getFechaDeCreacion().substring(11, 16));
            yVals.add(new Entry(i, value));
        }

        // Creo el dataset y lo edito
        LineDataSet dataset = new LineDataSet(yVals, "Ultimas mediciones");

        dataset.setFillAlpha(100);
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.gradient_graph);
        dataset.setFillDrawable(drawable);

//        dataset.setFillColor(ContextCompat.getColor(context, R.color.colorPrimary));
//


        dataset.setColor(Color.BLACK);
        dataset.setCircleColors(colors);
        dataset.setLineWidth(1.5f);
        dataset.setCircleRadius(4f);
        dataset.setDrawCircleHole(false);
        dataset.setValueTextSize(12f);
        dataset.setValueTextColors(colors);
        dataset.setDrawFilled(true);
        dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);


        // Agrego los datos al grafico
        LineData data = new LineData(dataset);
        grUltimasMediciones.setData(data);
        grUltimasMediciones.getDescription().setEnabled(false);

        // Tratando los ejes
        XAxis xAxis = grUltimasMediciones.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.TOP);
        IndexAxisValueFormatter formatter = new IndexAxisValueFormatter(xVals);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);

        YAxis yAxisRight = grUltimasMediciones.getAxisRight();
        yAxisRight.setEnabled(false);

        YAxis yAxisLeft = grUltimasMediciones.getAxisLeft();
        yAxisLeft.setMaxWidth(30);
        yAxisLeft.setGranularity(1f);

        // Seteo los limites superior e inferior
        LimitLine upper_limit = new LimitLine(MEDICION_MAXIMA_ACEPTABLE, "Limite superior");
        upper_limit.setLineWidth(1.5f);
        upper_limit.enableDashedLine(10f, 10f, 0f);
        upper_limit.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        upper_limit.setTextSize(12f);

        LimitLine lower_limit = new LimitLine(MEDICION_MINIMA_ACEPTABLE, "Limite inferior");
        lower_limit.setLineWidth(1.5f);
        lower_limit.enableDashedLine(10f, 10f, 0f);
        lower_limit.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        lower_limit.setTextSize(12f);

        YAxis leftAxis = grUltimasMediciones.getAxisLeft();
        leftAxis.removeAllLimitLines();
        leftAxis.addLimitLine(upper_limit);
        leftAxis.addLimitLine(lower_limit);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawLimitLinesBehindData(false);

        // Dibujo el grafico
        grUltimasMediciones.getAxisRight().setEnabled(false);
        grUltimasMediciones.animateXY(2000, 2000);
        grUltimasMediciones.invalidate();

    }

}
