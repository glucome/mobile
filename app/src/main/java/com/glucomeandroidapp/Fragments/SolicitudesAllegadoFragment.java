package com.glucomeandroidapp.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Adapters.SolicitudesAdapter;
import com.glucomeandroidapp.POJO.SolicitudVinculacionAllegadoPaciente;
import com.glucomeandroidapp.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.SOLICITUDES_BUNDLE;


/**
 * Created by MartinArtime on 14 de mayo del 2019
 **/
public class SolicitudesAllegadoFragment extends Fragment {

    @BindView(R.id.rc_menu_principal) RecyclerView recyclerView;

    Activity activity;
    Context context;

    public static SolicitudesAllegadoFragment newInstance() {
        return new SolicitudesAllegadoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_solicitudes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        activity = getActivity();
        context = Objects.requireNonNull(activity).getApplicationContext();
        Bundle bundle = this.getArguments();


        Type listType = new TypeToken<List<SolicitudVinculacionAllegadoPaciente>>(){}.getType();
        String solicitudesString = bundle.getString(SOLICITUDES_BUNDLE);
        List<SolicitudVinculacionAllegadoPaciente> solicitudes = new Gson().fromJson(solicitudesString, listType);


        SolicitudesAdapter solicitudesAdapter = new SolicitudesAdapter(solicitudes,activity);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(solicitudesAdapter);

    }
}
