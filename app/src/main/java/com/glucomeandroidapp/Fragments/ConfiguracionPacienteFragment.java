package com.glucomeandroidapp.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Calibracion.MenuCalibracionActivity;
import com.glucomeandroidapp.Activities.Insulina.FactoresCorreccionInsulinaActivity;
import com.glucomeandroidapp.Activities.Pacientes.MenuAlertasMedicacionActivity;
import com.glucomeandroidapp.Activities.Pacientes.MenuAlertasMedicionActivity;
import com.glucomeandroidapp.Activities.Perfil.MiPerfilActivity;
import com.glucomeandroidapp.Activities.Suscripcion.CambiarSuscripcionActivity;
import com.glucomeandroidapp.Activities.TarjetaAumentada.MiTarjetaAumentadaActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.ContactoControlador;
import com.glucomeandroidapp.POJO.Enumeraciones.Permisos;
import com.glucomeandroidapp.POJO.Enumeraciones.Rol;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by MartinArtime on 14 de mayo del 2019
 **/
public class ConfiguracionPacienteFragment extends Fragment {

    @BindView(R.id.rc_menu_configuracion)
    RecyclerView rcMenuConfiguracion;

    Activity activity;
    Context context;

    public static ConfiguracionPacienteFragment newInstance() {
        return new ConfiguracionPacienteFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_configuracion, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        activity = getActivity();
        context = Objects.requireNonNull(activity).getApplicationContext();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rcMenuConfiguracion.setLayoutManager(layoutManager);
        rcMenuConfiguracion.setItemAnimator(new DefaultItemAnimator());

        ArrayList<MenuAdapterModel> menuAdapterModelArrayList = new ArrayList<>();

        int[] iconos = {
                R.drawable.ic_calibracion,
                R.drawable.ic_calculadora,
                R.drawable.ic_notificaciones,
                R.drawable.ic_notificaciones_2,
                R.drawable.ic_contactos,
                R.drawable.ic_suscripcion,
                R.drawable.ic_lector_tarjeta,
                R.drawable.ic_perfil
        };

        Permisos[] permisos = {
                Permisos.CALIBRAR_DISPOSITIVO,
                Permisos.INSULINA,
                Permisos.NOTIFICACIONES,
                Permisos.NOTIFICACIONES,
                Permisos.CONTACTOS,
                Permisos.VER_SUSCRIPCION,
                Permisos.TARJETA_AUMENTADA,
                Permisos.VER_PERFIL
        };

        String[] titulos = getResources().getStringArray(R.array.menu_configuracion_paciente);

        Rol rolActual = AccountDataManager.obtenerRolEnum();
        for (int i = 0; i < iconos.length; i++) {
            if (permisos[i].aceptaRol(rolActual)) {
                MenuAdapterModel view = new MenuAdapterModel(
                        titulos[i],
                        iconos[i]);
                menuAdapterModelArrayList.add(view);
            }
        }

        MenuAdapter menuConfiguracionAdapter = new MenuAdapter(activity, menuAdapterModelArrayList) {
            @Override
            public void ejecutarAccion(int position) {
                int icono = menuAdapterModelArrayList.get(position).getIcono();
                switch (icono) {
                    case R.drawable.ic_calibracion:
                        App.getInstance().iniciarActividad(MenuCalibracionActivity.class);
                        break;
                    case R.drawable.ic_calculadora:
                        App.getInstance().iniciarActividad(FactoresCorreccionInsulinaActivity.class);
                        break;
                    case R.drawable.ic_notificaciones:
                        App.getInstance().iniciarActividad(MenuAlertasMedicionActivity.class);
                        break;
                    case R.drawable.ic_notificaciones_2:
                        App.getInstance().iniciarActividad(MenuAlertasMedicacionActivity.class);
                        break;
                    case R.drawable.ic_contactos:
                        ContactoControlador.abrirListadoContactos();
                        break;
                    case R.drawable.ic_suscripcion:
                        App.getInstance().iniciarActividad(CambiarSuscripcionActivity.class);
                        break;
                    case R.drawable.ic_lector_tarjeta:
                        App.getInstance().iniciarActividad(MiTarjetaAumentadaActivity.class);
                        break;
                    case R.drawable.ic_perfil:
                        App.getInstance().iniciarActividad(MiPerfilActivity.class);
                        break;
                    default:
                        break;
                }
            }
        };

        rcMenuConfiguracion.setAdapter(menuConfiguracionAdapter);

    }


}
