package com.glucomeandroidapp.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Allegados.MenuAllegadosActivity;
import com.glucomeandroidapp.Activities.Comidas.MenuComidasActivity;
import com.glucomeandroidapp.Activities.Ejercicios.MenuEjerciciosActivity;
import com.glucomeandroidapp.Activities.Informacion.MenuInformacionActivity;
import com.glucomeandroidapp.Activities.Insulina.MenuInsulinaActivity;
import com.glucomeandroidapp.Activities.Medicamentos.MenuMedicamentosActivity;
import com.glucomeandroidapp.Activities.Mediciones.MenuMedicionesActivity;
import com.glucomeandroidapp.Activities.Medicos.MenuMedicosActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.VariosControlador;
import com.glucomeandroidapp.POJO.Enumeraciones.Permisos;
import com.glucomeandroidapp.POJO.Enumeraciones.Rol;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by MartinArtime on 14 de mayo del 2019
 **/
public class MenuPrincipalPacienteFragment extends Fragment {

    @BindView(R.id.rc_menu_principal)
    RecyclerView rcMenuPrincipal;

    Activity activity;
    Context context;

    public static MenuPrincipalPacienteFragment newInstance() {
        return new MenuPrincipalPacienteFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu_principal, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        activity = getActivity();
        context = Objects.requireNonNull(activity).getApplicationContext();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rcMenuPrincipal.setLayoutManager(layoutManager);
        rcMenuPrincipal.setItemAnimator(new DefaultItemAnimator());

        ArrayList<MenuAdapterModel> menuAdapterModelArrayList = new ArrayList<>();



        int[] iconos = {
                R.drawable.ic_medicion,
                R.drawable.ic_medicamento,
                R.drawable.ic_medico,
                R.drawable.ic_allegado,
                R.drawable.ic_inyeccion,
                R.drawable.ic_comida,
                R.drawable.ic_ejercicio,
                R.drawable.ic_sync,
                R.drawable.ic_info};

        Permisos[] permisos = {
                Permisos.MEDICIONES,
                Permisos.MEDICAMENTOS,
                Permisos.MEDICOS,
                Permisos.ALLEGADOS,
                Permisos.INSULINA,
                Permisos.ALIMENTOS,
                Permisos.EJERCICIOS,
                Permisos.SINCRONIZAR,
                Permisos.INFORMACION
        };

        String[] titulos = getResources().getStringArray(R.array.menu_principal_paciente);

        Rol rolActual = AccountDataManager.obtenerRolEnum();
        for (int i = 0; i < iconos.length; i++) {
            if (permisos[i].aceptaRol(rolActual)) {
                MenuAdapterModel view = new MenuAdapterModel(
                        titulos[i],
                        iconos[i]);
                menuAdapterModelArrayList.add(view);
            }
        }

        MenuAdapter menuPrincipalPacienteAdapter = new MenuAdapter(activity, menuAdapterModelArrayList) {
            @Override
            public void ejecutarAccion(int position) {
                int icono = menuAdapterModelArrayList.get(position).getIcono();
                switch (icono) {
                    case R.drawable.ic_medicion:
                        App.getInstance().iniciarActividad(MenuMedicionesActivity.class);
                        break;
                    case R.drawable.ic_medicamento:
                        App.getInstance().iniciarActividad(MenuMedicamentosActivity.class);
                        break;
                    case R.drawable.ic_medico:
                        App.getInstance().iniciarActividad(MenuMedicosActivity.class);
                        break;
                    case R.drawable.ic_allegado:
                        App.getInstance().iniciarActividad(MenuAllegadosActivity.class);
                        break;
                    case R.drawable.ic_inyeccion:
                        App.getInstance().iniciarActividad(MenuInsulinaActivity.class);
                        break;
                    case R.drawable.ic_comida:
                        App.getInstance().iniciarActividad(MenuComidasActivity.class);
                        break;
                    case R.drawable.ic_ejercicio:
                        App.getInstance().iniciarActividad(MenuEjerciciosActivity.class);
                        break;
                    case R.drawable.ic_sync:
                        VariosControlador.sincronizar(activity);
                        break;
                    case R.drawable.ic_info:
                        App.getInstance().iniciarActividad(MenuInformacionActivity.class);
                        break;
                    default:
                        break;
                }
            }
        };

        rcMenuPrincipal.setAdapter(menuPrincipalPacienteAdapter);

    }

}
