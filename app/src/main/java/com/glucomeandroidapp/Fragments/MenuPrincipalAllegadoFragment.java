package com.glucomeandroidapp.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Comidas.BuscarAlimentoActivity;
import com.glucomeandroidapp.Activities.Informacion.MenuInformacionActivity;
import com.glucomeandroidapp.Activities.Perfil.MiPerfilActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.R;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by MartinArtime on 14 de mayo del 2019
 **/
public class MenuPrincipalAllegadoFragment extends Fragment {

    @BindView(R.id.rc_menu_principal) RecyclerView rcMenuPrincipal;

    Activity activity;
    Context context;

    public static MenuPrincipalAllegadoFragment newInstance() {
        return new MenuPrincipalAllegadoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu_principal, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        activity = getActivity();
        context = Objects.requireNonNull(activity).getApplicationContext();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rcMenuPrincipal.setLayoutManager(layoutManager);
        rcMenuPrincipal.setItemAnimator(new DefaultItemAnimator());

        ArrayList<MenuAdapterModel> menuAdapterModelArrayList = new ArrayList<>();

        int[] iconos = {R.drawable.ic_info, R.drawable.ic_comida ,R.drawable.ic_perfil};

        String[] titulos = getResources().getStringArray(R.array.menu_principal_allegado);

        for (int i = 0; i < iconos.length; i++) {
            MenuAdapterModel view = new MenuAdapterModel(
                    titulos[i],
                    iconos[i]);
            menuAdapterModelArrayList.add(view);
        }

        MenuAdapter menuPrincipalAllegadoAdapter =
                new MenuAdapter(activity, menuAdapterModelArrayList) {
                    @Override
                    public void ejecutarAccion(int position) {
                        switch (position) {
                            case 0:
                                App.getInstance().iniciarActividad(MenuInformacionActivity.class);
                                break;
                            case 1:
                                App.getInstance().iniciarActividad(BuscarAlimentoActivity.class);
                                break;
                            case 2:
                                App.getInstance().iniciarActividad(MiPerfilActivity.class);
                                break;
                            default:
                                break;
                        }
                    }
                };

        rcMenuPrincipal.setAdapter(menuPrincipalAllegadoAdapter);

    }

}
