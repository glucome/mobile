package com.glucomeandroidapp.Receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.glucomeandroidapp.POJO.APIClasses.Requests.LoginRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.Crypto;
import com.glucomeandroidapp.Utils.UtilesFecha;
import com.glucomeandroidapp.Utils.UtilesFunciones;

import java.time.LocalDateTime;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BAD_REQUEST;
import static com.glucomeandroidapp.Utils.Util.OK;
import static com.glucomeandroidapp.Utils.Util.OK_REQUEST;
import static com.glucomeandroidapp.Utils.Util.REFRESH_TOKEN;
import static com.glucomeandroidapp.Utils.Util.REFRESH_TOKEN_MILLIS;
import static com.glucomeandroidapp.Utils.Util.TAG;
import static com.glucomeandroidapp.Utils.Util.apiService;

/**
 * Creado por MartinArtime el 27 de mayo del 2019
 * <p>
 * Clase encargada de crear la alarma para renovar el token y recibir dicha alarma y ejecutar la acción
 * de pedirle al backend renovarlo.
 */
public class RefreshTokenReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        LoginResponse loginResponse = AccountDataManager.obtenerDatosUsuario();

        if (UtilesFunciones.hayConexionAInternet() && loginResponse != null) {

            LoginRequest req = new LoginRequest(loginResponse.getUsuario(), Crypto.decrypt(loginResponse.getPassword()));

            apiService.loginPaciente(APP_JSON, req)
                    .enqueue(new Callback<RespuestaApi<LoginResponse>>() {
                        @Override
                        public void onResponse(@NonNull Call<RespuestaApi<LoginResponse>> call,
                                               @NonNull Response<RespuestaApi<LoginResponse>> response) {

                            // Si el código de respuesta está entre 200 y 300
                            if (response.isSuccessful()) {
                                // Si el código es 200
                                if (response.code() == OK_REQUEST) {
                                    RespuestaApi res = response.body();
                                    if (res != null) {
                                        if (res.getMensaje().equals(OK)) {

                                            // Obtengo la respuesta
                                            LoginResponse<PacienteDataResponse> loginResp =
                                                    (LoginResponse<PacienteDataResponse>) res.getRespuesta();

                                            // Vacio el paciente del cambiarPass response para almacenarlo
                                            loginResp.setAsociado(null);

                                            // Guardo la contraseña encriptada para futuros refresh del token
                                            loginResp.setPassword(Crypto.encrypt(req.getPassword()));

                                            //Log.e(TAG, "TOKEN SETEADO a las: "+LocalDateTime.now().toString());

                                            LocalDateTime local = LocalDateTime.now().plusMinutes(30);

                                            // Seteo la fecha de expiración de token
                                            loginResp.setTokenFechaExpiracion(
                                                    UtilesFecha.getStringFromLocalDateTime(local));

                                            //Log.e(TAG, "TOKEN RENOVADO hasta las: "+loginResp.getTokenFechaExpiracion());
                                            AccountDataManager.loguearse(loginResp);
                                        }
                                    }
                                }
                            } else {
                                if (response.code() == BAD_REQUEST) {
                                    Log.e(TAG, "BAD REQUEST");
                                } else {
                                    Log.e(TAG, "Otro código de error");
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<RespuestaApi<LoginResponse>> call, @NonNull Throwable t) {
                            Log.e(TAG, t.getMessage());
                        }
                    });
        } else {
            Log.e(TAG, "No hay conexion a internet");
        }
    }

    public static void setAlarm(Context context, Long horaDeAlarma) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, RefreshTokenReceiver.class);
        i.putExtra(REFRESH_TOKEN_MILLIS, horaDeAlarma);
        PendingIntent pi = PendingIntent.getBroadcast(context, REFRESH_TOKEN, i, PendingIntent.FLAG_UPDATE_CURRENT);
        am.setRepeating(AlarmManager.RTC, horaDeAlarma, AlarmManager.INTERVAL_HALF_HOUR, pi);
    }

    public static void cancelAlarm(Context context) {
        Intent intent = new Intent(context, RefreshTokenReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, REFRESH_TOKEN, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}

