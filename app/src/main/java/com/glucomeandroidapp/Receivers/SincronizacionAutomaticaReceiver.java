package com.glucomeandroidapp.Receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.glucomeandroidapp.Controladores.AlimentosControlador;
import com.glucomeandroidapp.Controladores.ContactoControlador;
import com.glucomeandroidapp.Controladores.EjerciciosControlador;
import com.glucomeandroidapp.Controladores.InyeccionesControlador;
import com.glucomeandroidapp.Controladores.MedicamentosControlador;
import com.glucomeandroidapp.Controladores.MedicionesControlador;
import com.glucomeandroidapp.DAO.ContactoDAO;
import com.glucomeandroidapp.DAO.InyeccionesDAO;
import com.glucomeandroidapp.DAO.MedicionesDAO;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ContactoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.InyeccionRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.MedicionRequest;
import com.glucomeandroidapp.POJO.Contacto;
import com.glucomeandroidapp.POJO.Inyeccion;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.Utils.LocalStorageManager;

import java.time.LocalDateTime;
import java.util.List;

import static com.glucomeandroidapp.Utils.Util.ENVIO_AUTOMATICO;
import static com.glucomeandroidapp.Utils.Util.ENVIO_AUTOMATICO_MILLIS;
import static com.glucomeandroidapp.Utils.Util.REFRESH_TOKEN;
import static com.glucomeandroidapp.Utils.Util.ULTIMO_ENVIO_A_BACKEND;

/**
 * Creado por MartinArtime el 27 de mayo del 2019
 *
 * Clase encargada de crear la alarma para renovar el token y recibir dicha alarma y ejecutar la acción
 * de pedirle al backend renovarlo.
 */
public class SincronizacionAutomaticaReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        //Log.e(TAG, "Enviando datos a backend a las "+ LocalDateTime.now().toString());

        // Inicialmente envío todas las mediciones que tengan el atributo 'idBackend' vacío
        // (No se enviaron aún)
        List<Medicion> medicionesSinEnviar = MedicionesDAO.getMedicionesNoEnviadasAlBackend();
        if (!medicionesSinEnviar.isEmpty()) {
            for (int i = 0; i < medicionesSinEnviar.size(); i++) {
                Medicion medicion = medicionesSinEnviar.get(i);
                MedicionRequest medicionRequest = new MedicionRequest(
                        medicion.getFechaDeCreacion(), medicion.getValorMedicion().intValue(),
                        medicion.getMomento().ordinal(), medicion.getTipoMedicion().ordinal());

                // Llamo al método que envia la medición al backend
                MedicionesControlador.enviarMedicionBackground(medicionRequest, medicion);
            }
        }

        // Inicialmente envío todas las inyecciones que tengan el atributo 'idBackend' vacío
        // (No se enviaron aún)
        List<Inyeccion> inyeccionsSinEnviar = InyeccionesDAO.getInyeccionesNoEnviadasAlBackend();
        if (!inyeccionsSinEnviar.isEmpty()) {
            for (int i = 0; i < inyeccionsSinEnviar.size(); i++) {
                Inyeccion inyeccion = inyeccionsSinEnviar.get(i);
                InyeccionRequest inyeccionRequest = new InyeccionRequest(
                        inyeccion.getFechaDeCreacion(), inyeccion.getValorInyeccion().intValue(),
                        inyeccion.getTipoInsulina().ordinal());

                // Llamo al método que envia la intyección al backend
                InyeccionesControlador.enviarInyeccionBackground(inyeccionRequest, inyeccion);
            }
        }


        // Inicialmente envío todas los contactos que tengan el atributo 'idBackend' vacío
        // (No se enviaron aún)
        List<Contacto> contactosSinEnviar = ContactoDAO.getContactosNoEnviadossAlBackend();
        if (!contactosSinEnviar.isEmpty()) {
            for (int i = 0; i < contactosSinEnviar.size(); i++) {
                Contacto contacto = contactosSinEnviar.get(i);
                ContactoRequest contactoRequest = new ContactoRequest(contacto.getNombre(),contacto.getTelefono());
                ContactoControlador.enviarContacto(contacto,contactoRequest);
            }
        }

        ContactoControlador.getContactosBackground();

        //Descargo los medicamentos que esten grabados en el backend del paciente
        //Puede haber diferencias entre lo cargado por el paciente en la app porque el medico las carga en el front
        MedicamentosControlador.getMedicamentosBackground();

        MedicionesControlador.getMedicionesBackground();

        AlimentosControlador.getAlimentosBackground();

        InyeccionesControlador.getInyeccionesBackground();

        EjerciciosControlador.getEjerciciosBackground();



        //Log.e(TAG, "Enviar datos a backend, proxima seteada a las "+ LocalDateTime.now().plusHours(6).toString());

        // Guardo el momento de último envío al backend
        LocalStorageManager.guardar(ULTIMO_ENVIO_A_BACKEND, LocalDateTime.now());

    }


    public static void setAlarm(Context context, Long horaDeAlarma) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, SincronizacionAutomaticaReceiver.class);
        i.putExtra(ENVIO_AUTOMATICO_MILLIS, horaDeAlarma);
        PendingIntent pi = PendingIntent.getBroadcast(context, ENVIO_AUTOMATICO, i, PendingIntent.FLAG_UPDATE_CURRENT);
        am.setRepeating(AlarmManager.RTC, horaDeAlarma, AlarmManager.INTERVAL_HOUR * 6, pi);
    }

    public static void cancelAlarm(Context context) {
        Intent intent = new Intent(context, RefreshTokenReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, REFRESH_TOKEN, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}

