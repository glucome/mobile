package com.glucomeandroidapp.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.glucomeandroidapp.Utils.UtilesFunciones;

import static com.glucomeandroidapp.Utils.Util.HORA;
import static com.glucomeandroidapp.Utils.Util.ID_MEDICION;
import static com.glucomeandroidapp.Utils.Util.ID_NOTIFICACION;
import static com.glucomeandroidapp.Utils.Util.MINUTO;
import static com.glucomeandroidapp.Utils.Util.TAG;
import static com.glucomeandroidapp.Utils.Util.TEXTO_NOTIFICACION;
import static com.glucomeandroidapp.Utils.Util.TIPO_NOTIFICACION;
import static com.glucomeandroidapp.Utils.Util.TITULO_NOTIFICACION;

/**
 * Creado por MartinArtime el 09 de junio del 2019
 */
public class NotificacionesReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        int idNotificacion;
        int idMedicion;
        int hora;
        int minuto;
        String titulo;
        String texto;
        String tipo;
        Bundle b = intent.getExtras();
        if (b != null) {
            idNotificacion = b.getInt(ID_NOTIFICACION);
            idMedicion = b.getInt(ID_MEDICION);
            hora = b.getInt(HORA);
            minuto = b.getInt(MINUTO);
            titulo = b.getString(TITULO_NOTIFICACION);
            texto = b.getString(TEXTO_NOTIFICACION);
            tipo = b.getString(TIPO_NOTIFICACION);

            UtilesFunciones.agregarNotificacion(context, titulo, texto, idNotificacion, idMedicion, hora, minuto, tipo);
        }
    }
}
