package com.glucomeandroidapp.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.Medicacion.HorarioMedicacion;
import com.glucomeandroidapp.POJO.Medicion.MedicionAutomatica;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.LocalStorageManager;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static com.glucomeandroidapp.Utils.Util.MEDICIONES_AUTOMATICAS;
import static com.glucomeandroidapp.Utils.Util.TAG;
import static com.glucomeandroidapp.Utils.Util.TEXTO_MEDICION_AUTOMATICA;
import static com.glucomeandroidapp.Utils.Util.TITULO_MEDICACION;
import static com.glucomeandroidapp.Utils.Util.TITULO_MEDICION_AUTOMATICA;
import static com.glucomeandroidapp.Utils.Util.TOMAR_MEDICAMENTOS;
import static com.glucomeandroidapp.Utils.Util.ULTIMO_ENVIO_A_BACKEND;
import static com.glucomeandroidapp.Utils.UtilesFunciones.programarNotificacion;
import static java.lang.Math.toIntExact;

/**
 * Creado por MartinArtime el 10 de junio del 2019
 */
public class RebootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.e(TAG, "onReceive: Receiver");
        if(intent.getAction()!= null && intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){

            //Log.e(TAG, "REBOOT");
            //Log.e(TAG, "COUNT MEDICIONES AUTOMATICAS: "+DataBaseManager.getDaoSession().getMedicionAutomaticaDao().count());

            // Reprogramo todas las mediciones automáticas guardadas en la BD
            for (MedicionAutomatica m : DataBaseManager.getDaoSession().getMedicionAutomaticaDao().loadAll()) {
                programarNotificacion(context, m.getId().intValue(), TITULO_MEDICION_AUTOMATICA,
                        TEXTO_MEDICION_AUTOMATICA + m.getNombre(),
                        m.getHora(), m.getMinuto(), (int) m.getNotificacionId(), false, MEDICIONES_AUTOMATICAS);
            }

            // Refresheo el token si hace falta
            LoginResponse usuarioLogueado = AccountDataManager.obtenerDatosUsuario();

            if(usuarioLogueado != null){

                // Seteo la alarma para renovar el token
                LocalDateTime ultimoToken = UtilesFecha.getLocalDateTimeFromString(usuarioLogueado.getTokenFechaExpiracion());

                if(ultimoToken == null || ultimoToken.isBefore(LocalDateTime.now())) {
                    //Log.e(TAG, "Refresheo el token en reboot");
                    RefreshTokenReceiver.setAlarm(App.getContext(), LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
                }

                // Envío ahora novedades y ya seteo para que envíe novedades cada 6 horas al backend
                LocalDateTime ultimoEnvioABackend = LocalStorageManager.obtener(ULTIMO_ENVIO_A_BACKEND, LocalDateTime.class);

                if(ultimoEnvioABackend==null || ultimoEnvioABackend.isBefore(LocalDateTime.now())) {
                    //Log.e(TAG, "Envío novedades datos en reboot");
                    SincronizacionAutomaticaReceiver.setAlarm(App.getContext(), LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
                }
            }

        } else {
            Log.e(TAG, "NOT REBOOT");
        }
    }
}
