package com.glucomeandroidapp.Receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.VariosControlador;
import com.glucomeandroidapp.DAO.TomasMedicamentosDAO;
import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Medicacion.Medicamento;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LocalStorageManager;
import com.glucomeandroidapp.Utils.UtilesFecha;

import static com.glucomeandroidapp.Utils.Util.ALERTAS_PACIENTE_A_ALLEGADO_MEDICACIONES;
import static com.glucomeandroidapp.Utils.Util.ALERTAS_PACIENTE_A_MEDICO_MEDICACIONES;
import static com.glucomeandroidapp.Utils.Util.MEDICAMENTO;
import static com.glucomeandroidapp.Utils.Util.TAG;
import static com.glucomeandroidapp.Utils.Util.VERIFICAR_PACIENTE_TOMO_MEDICACION;
import static com.glucomeandroidapp.Utils.Util.VERIFICAR_PACIENTE_TOMO_MEDICACION_MILLIS;

/**
 * Creado por MartinArtime el 3 de Julio del 2019
 *
 * Clase encargada de crear la alarma y controlar si el paciente tomo una medicacion programada
 */
public class VerificarSiTomoMedicacionReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        //Log.e(TAG, "Enviando datos a backend a las "+ LocalDateTime.now().toString());
        //Medicamento medicamento = (Medicamento) Objects.requireNonNull(intent.getExtras()).getSerializable(MEDICAMENTO);
        long horaEnMillis = intent.getLongExtra(VERIFICAR_PACIENTE_TOMO_MEDICACION_MILLIS, 0);
        long medicamentoId = intent.getLongExtra(MEDICAMENTO, 0);

        //String hora = UtilesFecha.getStringFromLocalDateTime(UtilesFecha.millsToLocalDateTime(horaEnMillis));

        if(medicamentoId!=0){
            // Si NO tomo el medicamento le aviso a sus asociados autorizados
            if(!TomasMedicamentosDAO.verificarSiTomoElMedicamentoEnLaUltimaHora(medicamentoId, horaEnMillis)){
                Log.e(TAG, "Verificando si tomo la medicación: NO LA TOMO");



                String avisarAAllegado =  LocalStorageManager.obtener(ALERTAS_PACIENTE_A_ALLEGADO_MEDICACIONES, String.class);

                String avisarAMedico =  LocalStorageManager.obtener(ALERTAS_PACIENTE_A_MEDICO_MEDICACIONES, String.class);

                LoginResponse loginResponse =   AccountDataManager.obtenerDatosUsuario();

                PacienteDataResponse usuario = AccountDataManager.obtenerDatosPaciente();

                if(usuario!=null) {
                    if(avisarAMedico!=null && avisarAMedico.equals("true")){
                        if(loginResponse!=null && usuario.getMedicos()!=null){
                            for (MedicoDataResponse medico : usuario.getMedicos()) {

                                App.getInstance().mostrarMensajeFlotante("Enviando alerta de medicamentos a medicos");
                                VariosControlador.enviarNotificacionMedicacionAUsuario(loginResponse, medico.getIdUsuario(), horaEnMillis);
                            }
                        }
                    }
                    if(avisarAAllegado!=null && avisarAAllegado.equals("true")){
                        if(loginResponse!=null && usuario.getAllegados()!=null){
                            for (AllegadoDataResponse allegado : usuario.getAllegados()) {

                                App.getInstance().mostrarMensajeFlotante("Enviando alerta de medicamentos a allegados");
                                VariosControlador.enviarNotificacionMedicacionAUsuario(loginResponse, allegado.getIdUsuario(), horaEnMillis);
                            }
                        }
                    }
                }

            } else {
                Log.e(TAG, "Verificando si tomo la medicación: SI LA TOMO");
            }

        }


    }


    public static void setAlarm(Context context, Long horaDeAlarma, Medicamento medicamento) {
        Log.e(TAG, "Seteando alarma de medicación para las: "+ UtilesFecha.millsToLocalDateTime(horaDeAlarma));
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, VerificarSiTomoMedicacionReceiver.class);
        i.putExtra(MEDICAMENTO, medicamento.getId());
        i.putExtra(VERIFICAR_PACIENTE_TOMO_MEDICACION_MILLIS, horaDeAlarma);
        PendingIntent pi = PendingIntent.getBroadcast(context, VERIFICAR_PACIENTE_TOMO_MEDICACION, i, PendingIntent.FLAG_UPDATE_CURRENT);
        am.set(AlarmManager.RTC, horaDeAlarma, pi);
    }

    public static void cancelAlarm(Context context) {
        Intent intent = new Intent(context, VerificarSiTomoMedicacionReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, VERIFICAR_PACIENTE_TOMO_MEDICACION, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}

