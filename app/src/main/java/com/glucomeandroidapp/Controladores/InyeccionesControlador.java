package com.glucomeandroidapp.Controladores;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.POJO.APIClasses.Requests.InyeccionRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ListadoInyeccionInsulinaRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.IdResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.InyeccionResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoInyeccionInsulinaResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.Inyeccion;
import com.glucomeandroidapp.POJO.InyeccionDao;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.util.List;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.FALLO_INGRESO_INYECCION;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.apiService;

/**
 * Creado por MartinArtime el 28 de mayo del 2019
 */
public class InyeccionesControlador {

    public static void enviarInyeccion(InyeccionRequest InyeccionRequest,
                                       Inyeccion inyeccion) {


        Call<RespuestaApi<IdResponse>> enviarInyeccionRequest =
                apiService.enviarInyeccion(APP_JSON, AccountDataManager.obtenerIdPaciente(), InyeccionRequest,
                        BEARER + AccountDataManager.obtenerToken());


        APIManager.obtenerRespuesta(enviarInyeccionRequest, new CallListener<IdResponse>() {
            @Override
            public void procesarRespuesta(IdResponse respuesta) {
                // Actualizo la Inyeccion con el id que viene en el
                // InyeccionResponse que devuelve el backend.
                if (inyeccion != null) {
                    inyeccion.setIdBackend(respuesta.getId());
                    DataBaseManager.getDaoSession().getInyeccionDao().update(inyeccion);
                }
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_INGRESO_INYECCION);
            }
        });

    }

    public static void enviarInyeccionBackground(InyeccionRequest inyeccionRequest, Inyeccion inyeccion) {

            Call<RespuestaApi<IdResponse>> enviarInyeccionRequest =
                    apiService.enviarInyeccion(APP_JSON, AccountDataManager.obtenerIdPaciente(), inyeccionRequest,
                            BEARER + AccountDataManager.obtenerToken());

            APIManager.obtenerRespuesta(enviarInyeccionRequest, new CallListener<IdResponse>() {
                @Override
                public void procesarRespuesta(IdResponse respuesta) {
                    // Actualizo la Inyeccion con el id que viene en el
                    // InyeccionResponse que devuelve el backend.
                    if (inyeccion != null) {
                        inyeccion.setIdBackend(respuesta.getId());
                        DataBaseManager.getDaoSession().getInyeccionDao().update(inyeccion);
                    }
                }

                @Override
                public void procesarError(int codigo) {
                    //App.getInstance().mostrarMensajeFlotante(FALLO_INGRESO_INYECCION);
                }
            });
    }

    public static void getInyeccionesBackground() {
        InyeccionDao inyeccionDao = DataBaseManager.getDaoSession().getInyeccionDao();
        List<Inyeccion> inyeccionesEnBd = inyeccionDao.loadAll();


        if (inyeccionesEnBd != null && inyeccionesEnBd.isEmpty()) {
            long idPaciente = AccountDataManager.obtenerIdPaciente();
            Call<RespuestaApi<ListadoInyeccionInsulinaResponse>> getInsulinaBackend =
                    apiService.getInsulinaInyectadaPaciente(APP_JSON, idPaciente, BEARER + AccountDataManager.obtenerToken(), new ListadoInyeccionInsulinaRequest());

            APIManager.obtenerRespuesta(getInsulinaBackend, new CallListener<ListadoInyeccionInsulinaResponse>() {
                @Override
                public void procesarRespuesta(ListadoInyeccionInsulinaResponse respuesta) {
                    List<InyeccionResponse> responses = respuesta.getInsulinaInyectadas();
                    List<Inyeccion> inyecciones = InyeccionResponse.toInyeccion(responses);

                    for (Inyeccion inyeccion : inyecciones) {
                        inyeccionDao.save(inyeccion);
                    }

                }

                @Override
                public void procesarError(int codigo) {
                    //App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA);
                }
            });
        }
    }
}
