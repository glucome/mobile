package com.glucomeandroidapp.Controladores;

import android.app.Activity;
import android.content.Intent;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.POJO.APIClasses.Requests.SuscripcionPlanRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.BooleanResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.Enumeraciones.Plan;
import com.glucomeandroidapp.POJO.Enumeraciones.Rol;
import com.glucomeandroidapp.Utils.AccountDataManager;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.EXITO_CAMBIO_SUSCRIPCION;
import static com.glucomeandroidapp.Utils.Util.FALLO_CAMBIO_SUSCRIPCION;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.apiService;

public class SuscripcionControlador {

    public static void enviarCambioSuscripcion(LoginResponse datosUsuario, SuscripcionPlanRequest suscripcionPlanRequest, Activity activity){

        Call<RespuestaApi<BooleanResponse>> enviarCambioSuscripcionRequest = apiService.enviarCambioSuscripcion(APP_JSON,BEARER + AccountDataManager.obtenerToken(), suscripcionPlanRequest, AccountDataManager.obtenerIdPaciente());

        APIManager.obtenerRespuesta(enviarCambioSuscripcionRequest, new CallListener<BooleanResponse>() {
            @Override
            public void procesarRespuesta(BooleanResponse respuesta) {
                if (respuesta != null) {
                    if(respuesta.isValor()){
                        if(datosUsuario.getRoles().contains(Rol.PACIENTE.getNombre())){
                            datosUsuario.getRoles().remove(Rol.PACIENTE.getNombre());
                            datosUsuario.getRoles().add(Rol.PACIENTE_PREMIUM.getNombre());
                        } else {
                            datosUsuario.getRoles().remove(Rol.PACIENTE_PREMIUM.getNombre());
                            datosUsuario.getRoles().add(Rol.PACIENTE.getNombre());
                        }
                        AccountDataManager.actualizarCuentaUsuario(datosUsuario);
                        App.getInstance().mostrarMensajeFlotante(EXITO_CAMBIO_SUSCRIPCION);
                        Intent intent = activity.getIntent();
                        activity.finish();
                        App.getInstance().iniciarActividad(intent);
                    } else {
                        App.getInstance().mostrarMensajeFlotante(FALLO_CAMBIO_SUSCRIPCION);
                    }
                }
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_CAMBIO_SUSCRIPCION);
            }
        });
    }

    public static void cambiarRolUsuario(LoginResponse datosUsuario, Activity activity){
        if(datosUsuario.getRoles().contains(Rol.PACIENTE.getNombre())){
            enviarCambioSuscripcion(datosUsuario, new SuscripcionPlanRequest(Plan.PACIENTE_PREMIUM.ordinal()), activity);
        } else {
            enviarCambioSuscripcion(datosUsuario, new SuscripcionPlanRequest(Plan.PACIENTE_BASICO.ordinal()), activity);
        }
    }
}
