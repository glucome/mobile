package com.glucomeandroidapp.Controladores;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Medicamentos.MisMedicamentosActivity;
import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.Conexiones.CallbackInterface;
import com.glucomeandroidapp.POJO.APIClasses.Requests.MedicamentoPacienteRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.TomaMedicamentoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.BooleanResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.IdResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMedicamentoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.Medicacion.Medicamento;
import com.glucomeandroidapp.POJO.Medicacion.MedicamentoDao;
import com.glucomeandroidapp.POJO.Medicacion.TomaMedicamento;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.LoadingDialogManager.prepararDialogSiNo;
import static com.glucomeandroidapp.Utils.Util.AGREGAR_MEDICAMENTO_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.AGREGAR_MEDICAMENTO_TITULO;
import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_MEDICAMENTO;
import static com.glucomeandroidapp.Utils.Util.FALLO_EN_GRABAR_MEDICAMENTO_BACKEND;
import static com.glucomeandroidapp.Utils.Util.MEDICAMENTO_CORRECTO;
import static com.glucomeandroidapp.Utils.Util.MIS_MEDICAMENTOS_LIST;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.apiService;

public class MedicamentosControlador {

    public static void enviarTomaMedicamento(TomaMedicamentoRequest tomaMedicamentoRequest,
                                             TomaMedicamento tomaMedicamento) {

        Call<RespuestaApi<IdResponse>> enviarTomaMedicamentoRequest =
                apiService.enviarTomaMedicamento(APP_JSON, AccountDataManager.obtenerIdPaciente(), tomaMedicamentoRequest,
                        BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(enviarTomaMedicamentoRequest, new CallListener<IdResponse>() {
            @Override
            public void procesarRespuesta(IdResponse respuesta) {
                // Actualizo la Inyeccion con el id que viene en el
                // InyeccionResponse que devuelve el backend.
                if (tomaMedicamento != null) {
                    tomaMedicamento.setIdBackend(respuesta.getId());
                    DataBaseManager.getDaoSession().getTomaMedicamentoDao().update(tomaMedicamento);
                }
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_EN_GRABAR_MEDICAMENTO_BACKEND);
            }
        });
    }

    public static void mostrarPopupMedicamento(Medicamento medicamento, Activity activity) {

        AlertDialog dialog = prepararDialogSiNo(activity, AGREGAR_MEDICAMENTO_TITULO, AGREGAR_MEDICAMENTO_DESCRIPCION);

        dialog.findViewById(R.id.button_no).setOnClickListener(v -> dialog.dismiss());
        dialog.findViewById(R.id.button_si).setOnClickListener(v -> {
            DataBaseManager.getDaoSession().getMedicamentoDao().insert(medicamento);
            MedicamentosControlador.enviarMedicamentoSeleccionado(medicamento);
        });
    }

    private static void enviarMedicamentoSeleccionado(Medicamento medicamento) {


        MedicamentoPacienteRequest medicamentoPacienteRequest = new MedicamentoPacienteRequest(AccountDataManager.obtenerIdPaciente(), medicamento.getId());

        Call<RespuestaApi<BooleanResponse>> agregarMedicamentoRequest =
                apiService.agregarMedicamento(APP_JSON, medicamentoPacienteRequest, BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(agregarMedicamentoRequest, new CallListener<BooleanResponse>() {
            @Override
            public void procesarRespuesta(BooleanResponse respuesta) {
                if (respuesta.isValor()) {
                    App.getInstance().iniciarActividad(MenuPrincipalActivity.class, null);
                    App.getInstance().mostrarMensajeFlotante(MEDICAMENTO_CORRECTO);
                }
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_EN_GRABAR_MEDICAMENTO_BACKEND);
            }
        });

    }

    @Deprecated
    public static void getMedicamentos() {

        Call<RespuestaApi<ListadoMedicamentoResponse>> obtenerMedicamentosPacienteRequest =
                apiService.getMedicamentosDelPaciente(APP_JSON, AccountDataManager.obtenerIdPaciente(), BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(obtenerMedicamentosPacienteRequest, new CallListener<ListadoMedicamentoResponse>() {
            @Override
            public void procesarRespuesta(ListadoMedicamentoResponse respuesta) {
                // Obtengo la data del paciente
                List<Medicamento> medicamentos = respuesta.getMedicamentos();

                Bundle bundle = new Bundle();
                bundle.putString(MIS_MEDICAMENTOS_LIST, new Gson().toJson(medicamentos));
                App.getInstance().iniciarActividad(
                        MisMedicamentosActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_MEDICAMENTO);
            }
        });

    }

    public static void getMedicamentosPorNombre(String nombre, CallbackInterface<List<Medicamento>> callback) {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("nombre", nombre);
        parametros.put("paciente", String.valueOf(AccountDataManager.obtenerIdPaciente()));

        Call<RespuestaApi<ListadoMedicamentoResponse>> obtenerMedicamentosPorNombreRequest =
                apiService.getListadoMedicamentos(APP_JSON, parametros, BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(obtenerMedicamentosPorNombreRequest, new CallListener<ListadoMedicamentoResponse>() {
            @Override
            public void procesarRespuesta(ListadoMedicamentoResponse respuesta) {
                // Obtengo la data del paciente
                List<Medicamento> medicamentos = respuesta.getMedicamentos();
                callback.onSuccess(medicamentos);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_MEDICAMENTO);
            }
        });


    }

    public static void getMedicamentosBackground() {

            Call<RespuestaApi<ListadoMedicamentoResponse>> obtenerMedicamentosPacienteRequest =
                    apiService.getMedicamentosDelPaciente(APP_JSON, AccountDataManager.obtenerIdPaciente(), BEARER + AccountDataManager.obtenerToken());


            APIManager.obtenerRespuesta(obtenerMedicamentosPacienteRequest, new CallListener<ListadoMedicamentoResponse>() {
                @Override
                public void procesarRespuesta(ListadoMedicamentoResponse respuesta) {
                    // Obtengo la data del paciente
                    List<Medicamento> medicamentos = respuesta.getMedicamentos();
                    MedicamentoDao medicamentoDao = DataBaseManager.getDaoSession().getMedicamentoDao();
                    List<Medicamento> medicamentosEnBd = medicamentoDao.loadAll();

                    for (Medicamento medicamentoBackend : medicamentos) {
                        boolean estaEnAndroid = false;
                        for (Medicamento medicamentoAndroid : medicamentosEnBd) {
                            if (medicamentoBackend.getId() == medicamentoAndroid.getId()) {
                                estaEnAndroid = true;
                                break;
                            }
                        }
                        if (!estaEnAndroid) {
                            medicamentoDao.save(medicamentoBackend);
                        }
                    }

                }

                @Override
                public void procesarError(int codigo) {
                    //App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA);
                }
            });


    }
}

