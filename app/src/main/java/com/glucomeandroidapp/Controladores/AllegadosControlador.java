package com.glucomeandroidapp.Controladores;

import android.os.Bundle;

import com.glucomeandroidapp.Activities.Allegados.AllegadoSeleccionadoActivity;
import com.glucomeandroidapp.Activities.Allegados.MisAllegadosActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.Conexiones.CallbackInterface;
import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoAllegadoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoSolicitudResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.APIClasses.Response.SolicitudResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoSolicitud;
import com.glucomeandroidapp.POJO.SolicitudVinculacion;
import com.glucomeandroidapp.POJO.SolicitudVinculacionAllegadoPaciente;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.ALLEGADO_BUSCADO_BUNDLE;
import static com.glucomeandroidapp.Utils.Util.ALLEGADO_SELECCIONADO;
import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_ALLEGADO;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_SOLICITUDES;
import static com.glucomeandroidapp.Utils.Util.MIS_ALLEGADOS_LIST;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.SOLICITUD_ALLEGADO;
import static com.glucomeandroidapp.Utils.Util.apiService;

/**
 * Creado por MartinArtime el 21 de mayo del 2019
 */
public class AllegadosControlador {


    public static void getAllegadosBackground() {

        Call<RespuestaApi<ListadoAllegadoResponse>> obtenerAllegadosRequest =
                apiService.getAllegadosDelPaciente(APP_JSON, AccountDataManager.obtenerIdPaciente(), BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(obtenerAllegadosRequest, new CallListener<ListadoAllegadoResponse>() {
            @Override
            public void procesarRespuesta(ListadoAllegadoResponse respuesta) {
                // Obtengo la data del paciente
                List<AllegadoDataResponse> allegados = respuesta.getAllegados();
                PacienteDataResponse datosDelPaciente = AccountDataManager.obtenerDatosPaciente();
                datosDelPaciente.setAllegados(allegados);
                AccountDataManager.almacenarDatosUsuario(datosDelPaciente);
            }

            @Override
            public void procesarError(int codigo) {
                //App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_ALLEGADO);
            }
        });
    }

    public static void getAllegados() {

        Call<RespuestaApi<ListadoAllegadoResponse>> obtenerAllegadosRequest =
                apiService.getAllegadosDelPaciente(APP_JSON, AccountDataManager.obtenerIdPaciente(), BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(obtenerAllegadosRequest, new CallListener<ListadoAllegadoResponse>() {
            @Override
            public void procesarRespuesta(ListadoAllegadoResponse respuesta) {
                // Obtengo la data del paciente
                List<AllegadoDataResponse> allegados = respuesta.getAllegados();
                Bundle bundle = new Bundle();
                bundle.putString(MIS_ALLEGADOS_LIST, new Gson().toJson(allegados));
                App.getInstance().iniciarActividad(MisAllegadosActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_ALLEGADO);
            }
        });


    }

    public static void obtenerAllegadosPorNombre(String nombre, CallbackInterface<List<AllegadoDataResponse>> callback) {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("nombre", nombre);

        Call<RespuestaApi<ListadoAllegadoResponse>> obtenerAllegadosPorNombreRequest =
                apiService.getListadoAllegado(APP_JSON, parametros, BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(obtenerAllegadosPorNombreRequest, new CallListener<ListadoAllegadoResponse>() {
            @Override
            public void procesarRespuesta(ListadoAllegadoResponse respuesta) {
                List<AllegadoDataResponse> allegados = respuesta.getAllegados();
                callback.onSuccess(allegados);
            }

            @Override
            public void procesarError(int codigoError) {
                if (codigoError == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_ALLEGADO);
            }
        });

    }

    public static void getSolicitud(AllegadoDataResponse allegado, TipoSolicitud tipo) {

        LoginResponse datosDelusuario = AccountDataManager.obtenerDatosUsuario();
        PacienteDataResponse datosDelPaciente = AccountDataManager.obtenerDatosPaciente();
        Map<String, String> params = new HashMap<>();
        params.put("tipoSolicitud", String.valueOf(tipo.ordinal()));


        Call<RespuestaApi<SolicitudResponse>> obtenerSolicitudRequest =
                apiService.getSolicitud(APP_JSON, datosDelPaciente.getId(), allegado.getId(), params, BEARER + datosDelusuario.getToken());

        APIManager.obtenerRespuesta(obtenerSolicitudRequest, new CallListener<SolicitudResponse>() {
            @Override
            public void procesarRespuesta(SolicitudResponse respuesta) {
                // Obtengo la data del paciente
                SolicitudVinculacion solicitud = respuesta.getSolicitud();
                Bundle bundle = new Bundle();
                bundle.putString(ALLEGADO_SELECCIONADO, new Gson().toJson(allegado));
                bundle.putString(SOLICITUD_ALLEGADO, new Gson().toJson(solicitud));
                bundle.putString(BUNDLE_NAME, ALLEGADO_BUSCADO_BUNDLE);
                App.getInstance().iniciarActividad(AllegadoSeleccionadoActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_SOLICITUDES);
            }
        });

    }

    public static void getSolicitudesDelAllegado(CallbackInterface<List<SolicitudVinculacionAllegadoPaciente>> callback) {

        Call<RespuestaApi<ListadoSolicitudResponse>> obtenerListadoSolicitudRequest =
                apiService.getSolicitudesDelAllegado(APP_JSON, AccountDataManager.obtenerIdAllegado(), BEARER + AccountDataManager.obtenerToken());


        APIManager.obtenerRespuesta(obtenerListadoSolicitudRequest, new CallListener<ListadoSolicitudResponse>() {
            @Override
            public void procesarRespuesta(ListadoSolicitudResponse respuesta) {
                // Obtengo la data del paciente
                List<SolicitudVinculacionAllegadoPaciente> solicitudes = respuesta.getSolicitudes();
                LoadingDialogManager.ocultar();
                callback.onSuccess(solicitudes);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_SOLICITUDES);
            }
        });

    }
}
