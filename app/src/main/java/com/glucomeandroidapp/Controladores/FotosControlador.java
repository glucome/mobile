package com.glucomeandroidapp.Controladores;

import android.graphics.Bitmap;
import android.util.Log;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.Utils.AccountDataManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.FALLO_ENVIO_FOTO;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.TAG;
import static com.glucomeandroidapp.Utils.Util.apiService;

/**
 * Creado por MartinArtime el 21 de mayo del 2019
 */
public class FotosControlador {


    public static void enviarFotoDePerfil(Bitmap bitmap) {

        File f = new File(App.getContext().getCacheDir(), "foto");
        try {
            long idUsuario = AccountDataManager.obtenerIdUsuario();

            // Creo el archivo de imagen a enviar
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), f);
            MultipartBody.Part body = MultipartBody.Part.createFormData("file", "imagen" + idUsuario + ".jpg", reqFile);

            Call<RespuestaApi<LoginResponse>> enviarFotoDePerfilRequest =
                    apiService.enviarFotoDePerfil(idUsuario, body, BEARER + AccountDataManager.obtenerToken());

            APIManager.obtenerRespuesta(enviarFotoDePerfilRequest, new CallListener<LoginResponse>() {
                @Override
                public void procesarRespuesta(LoginResponse respuesta) {
                    Log.e(TAG, "OK");
                }

                @Override
                public void procesarError(int codigo) {
                    if (codigo == SIN_CONEXION)
                        App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                    else
                        App.getInstance().mostrarMensajeFlotante(FALLO_ENVIO_FOTO);
                }
            });


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
