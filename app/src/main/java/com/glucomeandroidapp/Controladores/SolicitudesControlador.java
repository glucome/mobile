package com.glucomeandroidapp.Controladores;

import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.POJO.APIClasses.Requests.SolicitudRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.BooleanResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.APIClasses.Response.SolicitudResponse;
import com.glucomeandroidapp.POJO.SolicitudVinculacion;
import com.glucomeandroidapp.POJO.SolicitudVinculacionAllegadoPaciente;
import com.glucomeandroidapp.Utils.AccountDataManager;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.ALLEGADO_BORRADO_CORRECTAMENTE;
import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.FALLO_ACEPTAR_SOLICITUD;
import static com.glucomeandroidapp.Utils.Util.FALLO_EN_BORRAR_ALLEGADO;
import static com.glucomeandroidapp.Utils.Util.FALLO_EN_BORRAR_MEDICO;
import static com.glucomeandroidapp.Utils.Util.FALLO_EN_CANCELACION_SOLICITUD;
import static com.glucomeandroidapp.Utils.Util.FALLO_EN_SOLICITUD;
import static com.glucomeandroidapp.Utils.Util.FALLO_RECHAZO_SOLICITUD;
import static com.glucomeandroidapp.Utils.Util.MEDICO_BORRADO_CORRECTAMENTE;
import static com.glucomeandroidapp.Utils.Util.SOLICITUD_ACEPTADA_CORRECTA;
import static com.glucomeandroidapp.Utils.Util.SOLICITUD_CANCELACION_CORRECTA;
import static com.glucomeandroidapp.Utils.Util.SOLICITUD_CORRECTA;
import static com.glucomeandroidapp.Utils.Util.SOLICITUD_RECHAZADA_CORRECTA;
import static com.glucomeandroidapp.Utils.Util.apiService;

/**
 * Creado por MartinArtime el 21 de mayo del 2019
 */
public class SolicitudesControlador {

    public static void cancelarSolicitud(SolicitudVinculacion solicitudVinculacion) {

        Call<RespuestaApi<SolicitudResponse>> cancelarSolicitudRequest =
                apiService.cancelarSolicitud(APP_JSON, solicitudVinculacion.getId(),
                        BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(cancelarSolicitudRequest, new CallListener<SolicitudResponse>() {
            @Override
            public void procesarRespuesta(SolicitudResponse respuesta) {
                // Obtengo la data del paciente
                App.getInstance().mostrarMensajeFlotante(SOLICITUD_CANCELACION_CORRECTA);
            }

            @Override
            public void procesarError(int codigo) {
                App.getInstance().mostrarMensajeFlotante(FALLO_EN_CANCELACION_SOLICITUD);
            }
        });

    }

    public static void rechazarSolicitud(SolicitudVinculacionAllegadoPaciente solicitud) {

        Call<RespuestaApi<SolicitudResponse>> cancelarSolicitudRequest =
                apiService.cancelarSolicitud(APP_JSON, solicitud.getId(),
                        BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(cancelarSolicitudRequest, new CallListener<SolicitudResponse>() {
            @Override
            public void procesarRespuesta(SolicitudResponse respuesta) {
                // Obtengo la respuesta
                App.getInstance().iniciarActividad(MenuPrincipalActivity.class);
                App.getInstance().mostrarMensajeFlotante(SOLICITUD_RECHAZADA_CORRECTA);
            }

            @Override
            public void procesarError(int codigo) {
                App.getInstance().mostrarMensajeFlotante(FALLO_RECHAZO_SOLICITUD);
            }
        });

    }

    public static void aceptarSolicitud(SolicitudVinculacionAllegadoPaciente solicitud) {

        Call<RespuestaApi<SolicitudResponse>> aceptarSolicitudRequest =
                apiService.aceptarSolicitud(APP_JSON, solicitud.getId(),
                        BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(aceptarSolicitudRequest, new CallListener<SolicitudResponse>() {
            @Override
            public void procesarRespuesta(SolicitudResponse respuesta) {
                // Obtengo la respuesta
                App.getInstance().iniciarActividad(MenuPrincipalActivity.class);
                App.getInstance().mostrarMensajeFlotante(SOLICITUD_ACEPTADA_CORRECTA);
            }

            @Override
            public void procesarError(int codigo) {
                App.getInstance().mostrarMensajeFlotante(FALLO_ACEPTAR_SOLICITUD);
            }
        });
    }

    public static void agregarMedico(SolicitudRequest solicitud) {

        Call<RespuestaApi<SolicitudResponse>> enviarSolicitudRequest =
                apiService.enviarSolicitud(APP_JSON, solicitud, BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(enviarSolicitudRequest, new CallListener<SolicitudResponse>() {
            @Override
            public void procesarRespuesta(SolicitudResponse respuesta) {
                App.getInstance().mostrarMensajeFlotante(SOLICITUD_CORRECTA);
            }

            @Override
            public void procesarError(int codigo) {
                App.getInstance().mostrarMensajeFlotante(FALLO_EN_SOLICITUD);
            }
        });
    }

    public static void borrarMedico(SolicitudRequest solicitud) {

        Map<String, String> params = new HashMap<>();
        params.put("idPaciente", String.valueOf(solicitud.getIdSolicitante()));
        params.put("idMedico", String.valueOf(solicitud.getIdSolicitado()));

        Call<RespuestaApi<BooleanResponse>> borrarMedicoRequest =
                apiService.borrarMedico(APP_JSON, params,
                        BEARER + AccountDataManager.obtenerToken());


        APIManager.obtenerRespuesta(borrarMedicoRequest, new CallListener<BooleanResponse>() {
            @Override
            public void procesarRespuesta(BooleanResponse respuesta) {
                if (respuesta.isValor()) {
                    App.getInstance().mostrarMensajeFlotante(MEDICO_BORRADO_CORRECTAMENTE);
                } else {
                    App.getInstance().mostrarMensajeFlotante(FALLO_EN_BORRAR_MEDICO);
                }
            }

            @Override
            public void procesarError(int codigo) {
                App.getInstance().mostrarMensajeFlotante(FALLO_EN_BORRAR_MEDICO);
            }
        });

    }

    public static void borrarAllegado(SolicitudRequest solicitud) {

        Map<String, String> params = new HashMap<>();
        params.put("idPaciente", String.valueOf(solicitud.getIdSolicitante()));
        params.put("idAllegado", String.valueOf(solicitud.getIdSolicitado()));

        Call<RespuestaApi<BooleanResponse>> borrarMedicoRequest =
                apiService.borrarAllegado(APP_JSON, params,
                        BEARER + AccountDataManager.obtenerToken());


        APIManager.obtenerRespuesta(borrarMedicoRequest, new CallListener<BooleanResponse>() {
            @Override
            public void procesarRespuesta(BooleanResponse respuesta) {
                if (respuesta.isValor()) {
                    App.getInstance().mostrarMensajeFlotante(ALLEGADO_BORRADO_CORRECTAMENTE);
                } else {
                    App.getInstance().mostrarMensajeFlotante(FALLO_EN_BORRAR_ALLEGADO);
                }
            }

            @Override
            public void procesarError(int codigo) {
                App.getInstance().mostrarMensajeFlotante(FALLO_EN_BORRAR_MEDICO);
            }
        });
    }

    public static void agregarAllegado(SolicitudRequest solicitud) {

        Call<RespuestaApi<SolicitudResponse>> enviarSolicitudAllegadoRequest =
                apiService.enviarSolicitudAllegado(APP_JSON, solicitud, BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(enviarSolicitudAllegadoRequest, new CallListener<SolicitudResponse>() {
            @Override
            public void procesarRespuesta(SolicitudResponse respuesta) {
                App.getInstance().mostrarMensajeFlotante(SOLICITUD_CORRECTA);
            }

            @Override
            public void procesarError(int codigo) {
                App.getInstance().mostrarMensajeFlotante(FALLO_EN_SOLICITUD);
            }
        });

    }
}
