package com.glucomeandroidapp.Controladores;

import android.os.Bundle;

import com.glucomeandroidapp.Activities.Chat.ChatActivity;
import com.glucomeandroidapp.Activities.Chat.ListadoChatActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.POJO.APIClasses.Requests.CambiarEstadoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.MarcarMensajesComoLeidosRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.EmptyResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMensajeWrapperResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoUsuariosChatsResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.APIClasses.Response.UsuarioChatResponse;
import com.glucomeandroidapp.POJO.Chat.MensajeDeChat;
import com.glucomeandroidapp.POJO.Chat.MensajeWrapper;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_MEDICOS;
import static com.glucomeandroidapp.Utils.Util.FALLO_EN_CAMBIO_DE_ESTADO;
import static com.glucomeandroidapp.Utils.Util.FALLO_MARCANDO_COMO_LEIDO;
import static com.glucomeandroidapp.Utils.Util.MENSAJES_LIST;
import static com.glucomeandroidapp.Utils.Util.MIS_CHAT_LIST;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.apiService;

public class ChatControlador {
    public static void irAlChat(UsuarioChatResponse chat) {

        Map<String, String> params = new HashMap<>();
        params.put("idChat", String.valueOf(chat.getIdChat()));

        Call<RespuestaApi<ListadoMensajeWrapperResponse>> mensajesDelChat = apiService.getMensajesDelChat(APP_JSON, BEARER + AccountDataManager.obtenerToken(),params);


        APIManager.obtenerRespuesta(mensajesDelChat, new CallListener<ListadoMensajeWrapperResponse>() {
            @Override
            public void procesarRespuesta(ListadoMensajeWrapperResponse respuesta) {
                // Obtengo la data del paciente
                List<MensajeWrapper> chatsWrapper = respuesta.getMensajes();

                List<MensajeDeChat> mensajes = new ArrayList<>();

                for(MensajeWrapper mensaje : chatsWrapper){
                    mensajes.add(new MensajeDeChat(mensaje,chat.getIdChat()));
                }
                Bundle bundle = new Bundle();
                bundle.putString(MIS_CHAT_LIST, new Gson().toJson(chat));
                bundle.putString(MENSAJES_LIST, new Gson().toJson(mensajes));
                App.getInstance().iniciarActividad(ChatActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_MEDICOS);
            }
        });


    }

    public static void getListadoChats() {
        Map<String, String> params = new HashMap<>();
        params.put("idUsuarioPaciente", String.valueOf(AccountDataManager.obtenerIdUsuario()));

        Call<RespuestaApi<ListadoUsuariosChatsResponse>> chatsDelPaciente = apiService.getChatsDelPaciente(APP_JSON, BEARER + AccountDataManager.obtenerToken(),params);


        APIManager.obtenerRespuesta(chatsDelPaciente, new CallListener<ListadoUsuariosChatsResponse>() {
            @Override
            public void procesarRespuesta(ListadoUsuariosChatsResponse respuesta) {
                // Obtengo la data del paciente
                List<UsuarioChatResponse> chats = respuesta.getUsuariosChats();

                Bundle bundle = new Bundle();
                bundle.putString(MIS_CHAT_LIST, new Gson().toJson(chats));
                App.getInstance().iniciarActividad(ListadoChatActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_MEDICOS);
            }
        });
    }

    public static void marcarMensajesComoLeidos(Long idChat, Long idAutor, String fechaHoraVisto) {
        MarcarMensajesComoLeidosRequest request  = new MarcarMensajesComoLeidosRequest(idChat, idAutor, fechaHoraVisto);

        Call<RespuestaApi<EmptyResponse>> mensajes  = apiService.marcarMensajesComoLeidos(APP_JSON, BEARER + AccountDataManager.obtenerToken(),request);


        APIManager.obtenerRespuesta(mensajes, new CallListener<EmptyResponse>() {
            @Override
            public void procesarRespuesta(EmptyResponse respuesta) { }

            @Override
            public void procesarError(int codigo) {
                App.getInstance().mostrarMensajeFlotante(FALLO_MARCANDO_COMO_LEIDO);
            }
        });
    }

    public static void actualizarEstado(boolean online) {
        CambiarEstadoRequest request  = new CambiarEstadoRequest(AccountDataManager.obtenerIdUsuario(),online);

        Call<RespuestaApi<EmptyResponse>> mensajes  = apiService.actualizarEstado(APP_JSON, BEARER + AccountDataManager.obtenerToken(),request);


        APIManager.obtenerRespuesta(mensajes, new CallListener<EmptyResponse>() {
            @Override
            public void procesarRespuesta(EmptyResponse respuesta) { }

            @Override
            public void procesarError(int codigo) {
                App.getInstance().mostrarMensajeFlotante(FALLO_EN_CAMBIO_DE_ESTADO);
            }
        });
    }
}
