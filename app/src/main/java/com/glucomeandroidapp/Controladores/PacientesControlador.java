package com.glucomeandroidapp.Controladores;

import android.os.Bundle;

import com.glucomeandroidapp.Activities.Comidas.HistorialAlimentosIngresadosActivity;
import com.glucomeandroidapp.Activities.Ejercicios.HistorialEjerciciosActivity;
import com.glucomeandroidapp.Activities.Insulina.HistorialInyeccionesActivity;
import com.glucomeandroidapp.Activities.Medicamentos.HistorialTomaMedicamentosActivity;
import com.glucomeandroidapp.Activities.Mediciones.HistorialDeMedicionesActivity;
import com.glucomeandroidapp.Activities.TarjetaAumentada.TarjetaAumentadaActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.Conexiones.CallbackInterface;
import com.glucomeandroidapp.POJO.APIClasses.Requests.FactorCorreccionInsulinaRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ListadoAlimentoIngeridoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ListadoEjercicioRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ListadoInyeccionInsulinaRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ListadoMedicamentoTomadosRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.AlimentoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.EjercicioResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.IdResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.InfoTarjetaAumentadaResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.InyeccionResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoAlimentoIngeridoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoEjercicioResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoInyeccionInsulinaResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMedicamentosIngeridosResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMedicionesResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoPacienteResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicamentoIngerido;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicionPacienteResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.Alimentos.IngresoAlimento;
import com.glucomeandroidapp.POJO.Ejercicio.Ejercicio;
import com.glucomeandroidapp.POJO.FactoresCorreccionInsulina;
import com.glucomeandroidapp.POJO.Inyeccion;
import com.glucomeandroidapp.POJO.Medicacion.TomaMedicamento;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.Util;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.FALLO_ACTUALIZACION_FACTORES;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_ALIMENTOS;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_INYECCIONES;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_MEDICAMENTO;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_MEDICIONES;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_PACIENTES;
import static com.glucomeandroidapp.Utils.Util.LISTADO_ALIMENTOS;
import static com.glucomeandroidapp.Utils.Util.LISTADO_EJERCICIOS;
import static com.glucomeandroidapp.Utils.Util.LISTADO_INYECCIONES;
import static com.glucomeandroidapp.Utils.Util.LISTADO_TOMA_MEDICAMENTO;
import static com.glucomeandroidapp.Utils.Util.MEDICIONES_LIST;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_DATA_BUNDLE;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_ID_BUNDLE;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_NOMBRE;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.apiService;

public class PacientesControlador {

    public static void getPacientesDelMedico(CallbackInterface<List<PacienteDataResponse>> callbackInterface) {

        Call<RespuestaApi<ListadoPacienteResponse>> obtenerPacientesMedicoRequest =
                apiService.getPacientesDelMedico(APP_JSON, AccountDataManager.obtenerIdMedico(), BEARER + AccountDataManager.obtenerToken());


        APIManager.obtenerRespuesta(obtenerPacientesMedicoRequest, new CallListener<ListadoPacienteResponse>() {
            @Override
            public void procesarRespuesta(ListadoPacienteResponse respuesta) {
                // Obtengo la data del paciente
                List<PacienteDataResponse> pacientes = respuesta.getPacientes();

                callbackInterface.onSuccess(pacientes);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_PACIENTES);
            }
        });
    }

    public static void getPacientesDelAllegado(CallbackInterface<List<PacienteDataResponse>> callbackInterface) {

        Call<RespuestaApi<ListadoPacienteResponse>> obtenerPacientesAllegadoRequest =
                apiService.getPacientesDelAllegado(APP_JSON, AccountDataManager.obtenerIdAllegado(), BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(obtenerPacientesAllegadoRequest, new CallListener<ListadoPacienteResponse>() {
            @Override
            public void procesarRespuesta(ListadoPacienteResponse respuesta) {
                // Obtengo la data del paciente
                List<PacienteDataResponse> pacientes = respuesta.getPacientes();

                callbackInterface.onSuccess(pacientes);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_PACIENTES);

            }
        });

    }

    public static void obtenerMedicionesPacientes(long idPaciente, String nombrePaciente) {

        Call<RespuestaApi<ListadoMedicionesResponse>> obtenerMedicionesPaciente =
                apiService.getMedicionesPaciente(APP_JSON, idPaciente, idPaciente, BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(obtenerMedicionesPaciente, new CallListener<ListadoMedicionesResponse>() {
            @Override
            public void procesarRespuesta(ListadoMedicionesResponse respuesta) {
                // Obtengo la data del paciente
                List<MedicionPacienteResponse> responses = respuesta.getMediciones();
                List<Medicion> mediciones = MedicionPacienteResponse.getToMedicion(responses);
                Bundle bundle = new Bundle();
                Gson gson = new Gson();
                bundle.putString(PACIENTE_NOMBRE, gson.toJson(nombrePaciente));
                bundle.putString(MEDICIONES_LIST, gson.toJson(mediciones));
                App.getInstance().iniciarActividad(HistorialDeMedicionesActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_MEDICIONES);

            }
        });

    }

    public static void guardarFactoresCorreccion(FactorCorreccionInsulinaRequest data, FactoresCorreccionInsulina factorCorreccionInsulina) {
        Call<RespuestaApi<IdResponse>> enviarFactoresCorreccion =
                apiService.enviarFactoresCorreccion(APP_JSON, AccountDataManager.obtenerIdPaciente(), data,
                        BEARER + AccountDataManager.obtenerToken());


        APIManager.obtenerRespuesta(enviarFactoresCorreccion, new CallListener<IdResponse>() {
            @Override
            public void procesarRespuesta(IdResponse respuesta) {
                // Actualizo la Inyeccion con el id que viene en el
                // InyeccionResponse que devuelve el backend.
                if (factorCorreccionInsulina != null) {
                    factorCorreccionInsulina.setIdBackend(respuesta.getId());
                    DataBaseManager.getDaoSession().getFactoresCorreccionInsulinaDao().update(factorCorreccionInsulina);
                }
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_ACTUALIZACION_FACTORES);
            }
        });
    }

    public static void obtenerInfoTarjetaAumentada(long idPaciente) {
/*
        Call<RespuestaApi<InfoTarjetaAumentadaResponse>> obtenerInfoTarjetaAumentada =
                apiService.getInfotarjetaAumentada(APP_JSON, idPaciente, BEARER + AccountDataManager.obtenerToken());
*/
        Call<RespuestaApi<ListadoMedicionesResponse>> getMedicionesRequest =
                apiService.getMedicionesPaciente(APP_JSON, idPaciente, idPaciente,BEARER + AccountDataManager.obtenerToken());
        APIManager.obtenerRespuesta(getMedicionesRequest, new CallListener<ListadoMedicionesResponse>() {
            @Override
            public void procesarRespuesta(ListadoMedicionesResponse respuesta) {
                // Obtengo la data del paciente
                List<MedicionPacienteResponse> responses = respuesta.getMediciones();
                List<Medicion> mediciones = MedicionPacienteResponse.getToMedicion(responses);

                Bundle bundle = new Bundle();
                bundle.putString(PACIENTE_ID_BUNDLE, String.valueOf(idPaciente));
                bundle.putSerializable(PACIENTE_DATA_BUNDLE, Util.getGsonHelper().toJson(mediciones));
                //bundle.putString("paciente_data", Util.getGsonHelper().toJson(respuesta));
                App.getInstance().iniciarActividad(TarjetaAumentadaActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_MEDICIONES);

            }
        });

    }

    public static void obtenerMedicamentosTomadosPacientes(long idPaciente, String nombrePaciente) {
        Call<RespuestaApi<ListadoMedicamentosIngeridosResponse>> getMedicamentosTomadosPaciente =
                apiService.getMedicamentosTomadosPaciente(APP_JSON, idPaciente, BEARER + AccountDataManager.obtenerToken(),new ListadoMedicamentoTomadosRequest());

        APIManager.obtenerRespuesta(getMedicamentosTomadosPaciente, new CallListener<ListadoMedicamentosIngeridosResponse>() {
            @Override
            public void procesarRespuesta(ListadoMedicamentosIngeridosResponse respuesta) {
                // Obtengo la data del paciente
                List<MedicamentoIngerido> response = respuesta.getMedicamentosIngeridos();
                List<TomaMedicamento> medicamentos = MedicamentoIngerido.toTomaMedicamento(response);
                Bundle bundle = new Bundle();
                Gson gson = new Gson();
                bundle.putString(PACIENTE_NOMBRE, gson.toJson(nombrePaciente));
                bundle.putString(LISTADO_TOMA_MEDICAMENTO, gson.toJson(medicamentos));
                App.getInstance().iniciarActividad(HistorialTomaMedicamentosActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_MEDICAMENTO);

            }
        });
    }

    public static void obtenerInyeccionesPacientes(long idPaciente, String nombrePaciente) {
        Call<RespuestaApi<ListadoInyeccionInsulinaResponse>> getInyeccionesPaciente =
                apiService.getInsulinaInyectadaPaciente(APP_JSON, idPaciente, BEARER + AccountDataManager.obtenerToken(),new ListadoInyeccionInsulinaRequest());

        APIManager.obtenerRespuesta(getInyeccionesPaciente, new CallListener<ListadoInyeccionInsulinaResponse>() {
            @Override
            public void procesarRespuesta(ListadoInyeccionInsulinaResponse respuesta) {
                // Obtengo la data del paciente
                List<InyeccionResponse> response = respuesta.getInsulinaInyectadas();
                List<Inyeccion> inyecciones = InyeccionResponse.toInyeccion(response);
                Bundle bundle = new Bundle();
                Gson gson = new Gson();
                bundle.putString(PACIENTE_NOMBRE, gson.toJson(nombrePaciente));
                bundle.putString(LISTADO_INYECCIONES, gson.toJson(inyecciones));
                App.getInstance().iniciarActividad(HistorialInyeccionesActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_INYECCIONES);

            }
        });
    }

    public static void obtenerAlimentosPacientes(long idPaciente, String nombrePaciente) {
        Call<RespuestaApi<ListadoAlimentoIngeridoResponse>> getAlimentosPaciente =
                apiService.getAlimentosPaciente(APP_JSON, idPaciente, BEARER + AccountDataManager.obtenerToken(),new ListadoAlimentoIngeridoRequest());

        APIManager.obtenerRespuesta(getAlimentosPaciente, new CallListener<ListadoAlimentoIngeridoResponse>() {
            @Override
            public void procesarRespuesta(ListadoAlimentoIngeridoResponse respuesta) {
                // Obtengo la data del paciente
                List<AlimentoResponse> response = respuesta.getAlimentoIngerido();
                List<IngresoAlimento> alimentos = AlimentoResponse.toIngresoAlimento(response);
                Bundle bundle = new Bundle();
                Gson gson = new Gson();
                bundle.putString(PACIENTE_NOMBRE, gson.toJson(nombrePaciente));
                bundle.putString(LISTADO_ALIMENTOS, gson.toJson(alimentos));
                App.getInstance().iniciarActividad(HistorialAlimentosIngresadosActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_ALIMENTOS);

            }
        });
    }

    public static void obtenerEjerciciosPacientes(long idPaciente, String nombrePaciente) {
        Call<RespuestaApi<ListadoEjercicioResponse>> getEjerciciosPaciente =
                apiService.getEjerciciosPaciente(APP_JSON, idPaciente, BEARER + AccountDataManager.obtenerToken(),new ListadoEjercicioRequest());

        APIManager.obtenerRespuesta(getEjerciciosPaciente, new CallListener<ListadoEjercicioResponse>() {
            @Override
            public void procesarRespuesta(ListadoEjercicioResponse respuesta) {
                // Obtengo la data del paciente
                List<EjercicioResponse> response = respuesta.getEjercicios();
                List<Ejercicio> ejercicios = EjercicioResponse.toEjercicio(response);
                Bundle bundle = new Bundle();
                Gson gson = new Gson();
                bundle.putString(PACIENTE_NOMBRE, gson.toJson(nombrePaciente));
                bundle.putString(LISTADO_EJERCICIOS, gson.toJson(ejercicios));
                App.getInstance().iniciarActividad(HistorialEjerciciosActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_ALIMENTOS);

            }
        });
    }
}
