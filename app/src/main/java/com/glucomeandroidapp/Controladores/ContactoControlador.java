package com.glucomeandroidapp.Controladores;

import android.os.Bundle;

import com.glucomeandroidapp.Activities.Contacto.ListadoContactosActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ContactoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.IdResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoContactoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.Contacto;
import com.glucomeandroidapp.POJO.ContactoDao;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.CONTACTOS_LIST;
import static com.glucomeandroidapp.Utils.Util.FALLO_ENVIO_CONTACTO;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.apiService;

public class ContactoControlador {

    public static void enviarContacto(Contacto contacto, ContactoRequest contactoRequest) {

        Call<RespuestaApi<IdResponse>> enviarContactoRequest =  apiService.enviarContacto(APP_JSON,BEARER + AccountDataManager.obtenerToken(), contactoRequest, AccountDataManager.obtenerIdPaciente());

        APIManager.obtenerRespuesta(enviarContactoRequest, new CallListener<IdResponse>() {
            @Override
            public void procesarRespuesta(IdResponse respuesta) {
                // Actualizo la medicion con el id que viene en el
                // MedicionResponse que devuelve el backend.
                if (respuesta != null) {
                    contacto.setIdBackend(respuesta.getId());
                    DataBaseManager.getDaoSession().getContactoDao().update(contacto);
                }
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_ENVIO_CONTACTO);
            }
        });


    }

    public static void getContactosBackground() {

        ContactoDao contactoDao = DataBaseManager.getDaoSession().getContactoDao();
        List<Contacto> contactosEnBd = contactoDao.loadAll();


        if(contactosEnBd !=  null && contactosEnBd.isEmpty()){
            Call<RespuestaApi<ListadoContactoResponse>> getContactosRequest =
                    apiService.getContactos(APP_JSON, AccountDataManager.obtenerIdPaciente(), BEARER + AccountDataManager.obtenerToken());

            APIManager.obtenerRespuesta(getContactosRequest, new CallListener<ListadoContactoResponse>() {
                @Override
                public void procesarRespuesta(ListadoContactoResponse respuesta) {
                    // Obtengo la data del paciente
                    List<Contacto> contactos = respuesta.getContactos();

                    for (Contacto contactoBackend : contactos) {
                        boolean estaEnAndroid = false;
                        for (Contacto contactoAndroid : contactosEnBd) {
                            if (contactoAndroid.getIdBackend() == contactoBackend.getIdBackend()) {
                                estaEnAndroid = true;
                                break;
                            }
                        }
                        if (!estaEnAndroid) {
                            contactoDao.save(contactoBackend);
                        }
                    }

                }

                @Override
                public void procesarError(int codigo) {
                    //App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA);
                }
            });
        }


    }

    public static void borrarContacto(Contacto contacto) {
        Call<RespuestaApi<IdResponse>> getContactosRequest = apiService.borrarContacto(APP_JSON, contacto.getIdBackend(), BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(getContactosRequest, new CallListener<IdResponse>() {
            @Override
            public void procesarRespuesta(IdResponse respuesta) {
            }

            @Override
            public void procesarError(int codigo) {
                //App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA);
            }
        });
    }

    public static void abrirListadoContactos() {
        List<Contacto> contactos = DataBaseManager.getDaoSession().getContactoDao().loadAll();
        Bundle bundle = new Bundle();
        bundle.putString(CONTACTOS_LIST, new Gson().toJson(contactos));
        App.getInstance().iniciarActividad(ListadoContactosActivity.class, bundle);
    }
}
