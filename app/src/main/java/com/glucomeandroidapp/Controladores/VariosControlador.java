package com.glucomeandroidapp.Controladores;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.DAO.InyeccionesDAO;
import com.glucomeandroidapp.DAO.MedicionesDAO;
import com.glucomeandroidapp.POJO.APIClasses.Requests.InyeccionRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.MedicionRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.Contacto;
import com.glucomeandroidapp.POJO.Inyeccion;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.glucomeandroidapp.Utils.Util;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.glucomeandroidapp.Controladores.FirebaseControlador.enviarTokenFirebase;
import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BAD_REQUEST;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.ENVIANDO_DATOS;
import static com.glucomeandroidapp.Utils.Util.OK_REQUEST;
import static com.glucomeandroidapp.Utils.Util.TOKEN_EXPIRADO;
import static com.glucomeandroidapp.Utils.Util.apiService;

public class VariosControlador {



    final public static  int REQUEST_CODE_ASK_PERMISSIONS = 123;
    /**
     * Método para enviar mediciones al backend
     */
    public static void sincronizar(Activity activity){

        LoadingDialogManager.mostrar(activity);


        // Para que el dialog llegue a cargar correctamente aunque no haya nada que enviar o recibir
        Handler handler = new Handler();
        handler.postDelayed(() -> {

            /**
             *  ENVIO COSAS
             */

            // Inicialmente envío todas las mediciones que tengan el atributo 'idBackend' vacío
            // (No se enviaron aún)
            List<Medicion> medicionesSinEnviar = MedicionesDAO.getMedicionesNoEnviadasAlBackend();
            if (!medicionesSinEnviar.isEmpty()) {
                for (int i = 0; i < medicionesSinEnviar.size(); i++) {
                    Medicion medicion = medicionesSinEnviar.get(i);
                    MedicionRequest medicionRequest = new MedicionRequest(
                            medicion.getFechaDeCreacion(), medicion.getValorMedicion().intValue(),
                            medicion.getMomento().ordinal(), medicion.getTipoMedicion().ordinal());

                    // Llamo al método que envia la medición al backend
                    MedicionesControlador.enviarMedicion(medicionRequest, medicion);
                }
            }

            // Inicialmente envío todas las inyecciones que tengan el atributo 'idBackend' vacío
            // (No se enviaron aún)
            List<Inyeccion> inyeccionsSinEnviar = InyeccionesDAO.getInyeccionesNoEnviadasAlBackend();
            if (!inyeccionsSinEnviar.isEmpty()) {
                for (int i = 0; i < inyeccionsSinEnviar.size(); i++) {
                    Inyeccion inyeccion = inyeccionsSinEnviar.get(i);
                    InyeccionRequest inyeccionRequest = new InyeccionRequest(
                            inyeccion.getFechaDeCreacion(), inyeccion.getValorInyeccion().intValue(),
                            inyeccion.getTipoInsulina().ordinal());

                    // Llamo al método que envia la medición al backend
                    InyeccionesControlador.enviarInyeccion(inyeccionRequest, inyeccion);
                }
            }

            // Envío el token de firebase actual
            enviarTokenFirebase();


            /**
             *  RECIBO COSAS
             */

            MedicosControlador.getMedicosBackground();
            AllegadosControlador.getAllegadosBackground();

            //Descargo los medicamentos que esten grabados en el backend del paciente
            //Puede haber diferencias entre lo cargado por el paciente en la app porque el medico las carga en el front
            MedicamentosControlador.getMedicamentosBackground();

            ContactoControlador.getContactosBackground();

            //Descargo los medicamentos que esten grabados en el backend del paciente
            //Puede haber diferencias entre lo cargado por el paciente en la app porque el medico las carga en el front

            MedicionesControlador.getMedicionesBackground();

            AlimentosControlador.getAlimentosBackground();

            InyeccionesControlador.getInyeccionesBackground();

            EjerciciosControlador.getEjerciciosBackground();

            LoadingDialogManager.ocultar();

            }, 5000);

    }


    public static void enviarNotificacionMedicionAUsuario(LoginResponse loginResponse, long idAvisado, String medicion, String ubicacion){
        apiService.enviarNotificacionMedicionFirebaseAUsuario(APP_JSON, loginResponse.getId(), idAvisado, medicion, ubicacion,
                BEARER + loginResponse.getToken())
                .enqueue(new Callback<JSONObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JSONObject> call,
                                           @NonNull Response<JSONObject> response) {

                        // Si el código de respuesta está entre 200 y 300
                        if (response.isSuccessful()) {
                            // Si el código es 200
                            if (response.code() == OK_REQUEST) {
                                Log.e(Util.TAG, "Envio de notificacion exitoso!");
                            }
                        } else if (response.code() == BAD_REQUEST) {
                            Log.e(Util.TAG, "BAD REQUEST");
                        } else if (response.code() == TOKEN_EXPIRADO) {
                            Log.e(Util.TAG, "Token expirado");
                        } else {
                            Log.e(Util.TAG, "Otro código de error");
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<JSONObject> call, @NonNull Throwable t) {
                        Log.e(Util.TAG, "FAILURE: " + t.getMessage());
                    }
                });
    }


    public static void enviarNotificacionMedicacionAUsuario(LoginResponse loginResponse, long idAvisado, long hora){
        apiService.enviarNotificacionMedicacionFirebaseAUsuario(APP_JSON, loginResponse.getId(), idAvisado, hora, BEARER + loginResponse.getToken())
                .enqueue(new Callback<JSONObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JSONObject> call,
                                           @NonNull Response<JSONObject> response) {

                        // Si el código de respuesta está entre 200 y 300
                        if (response.isSuccessful()) {
                            // Si el código es 200
                            if (response.code() == OK_REQUEST) {
                                Log.e(Util.TAG, "Envio de notificacion exitoso!");
                            }
                        } else if (response.code() == BAD_REQUEST) {
                            Log.e(Util.TAG, "BAD REQUEST");
                        } else if (response.code() == TOKEN_EXPIRADO) {
                            Log.e(Util.TAG, "Token expirado");
                        } else {
                            Log.e(Util.TAG, "Otro código de error");
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<JSONObject> call, @NonNull Throwable t) {
                        Log.e(Util.TAG, "FAILURE: " + t.getMessage());
                    }
                });
    }


    /**
     * Envia un sms, antes de enviar sms, se tuvo que pedir permiso al usuario
     * Ver método pidePermisoSms de esta clase.
     * Ejemplo en MedicionManualActivity.
     * @param contacto
     * @param medicion
     */
    public static void enviarSmsDeNotificacionAContactos(Contacto contacto, String medicion) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(contacto.getTelefono(), null, "Se detectó una medición de " + medicion + " de " + AccountDataManager.obtenerDatosPaciente().getNombre(), null, null);
    }


    /**
     * Método para pedir permiso para enviar sms
     * Tienen que sobreescribir el metodo onRequestPermissionsResult de la activity para utilizarlo.
     * Ver ejemplo en MedicionManualActivity.
     * @param activity
     * @return
     */
    public static void pidePermisoSms(Activity activity) {
        if(!tienePermisoSms(activity)){
                    activity.requestPermissions(new String[]{Manifest.permission.SEND_SMS},
                            REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    public static boolean tienePermisoSms(Activity activity) {
        int hasWriteContactsPermission = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hasWriteContactsPermission = activity.checkSelfPermission(Manifest.permission.SEND_SMS);
        }
        return  hasWriteContactsPermission == PackageManager.PERMISSION_GRANTED;

    }

}
