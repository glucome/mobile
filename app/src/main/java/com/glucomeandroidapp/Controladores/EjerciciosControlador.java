package com.glucomeandroidapp.Controladores;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.POJO.APIClasses.Requests.EjercicioRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ListadoEjercicioRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.EjercicioResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.IdResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoEjercicioResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.Ejercicio.Ejercicio;
import com.glucomeandroidapp.POJO.Ejercicio.EjercicioDao;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.util.List;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_ALIMENTOS;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.apiService;

public class EjerciciosControlador {

    public static void enviarEjercicio(EjercicioRequest EjercicioRequest,
                                       Ejercicio ejercicio) {


        Call<RespuestaApi<IdResponse>> enviarIngresoEjercicio =
                apiService.enviarIngresoEjercicio(APP_JSON, AccountDataManager.obtenerIdPaciente(), EjercicioRequest,
                        BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(enviarIngresoEjercicio, new CallListener<IdResponse>() {
            @Override
            public void procesarRespuesta(IdResponse respuesta) {
                // Actualizo la Inyeccion con el id que viene en el
                // InyeccionResponse que devuelve el backend.
                if (ejercicio != null) {
                    ejercicio.setIdBackend(respuesta.getId());
                    DataBaseManager.getDaoSession().getEjercicioDao().update(ejercicio);
                }
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_ALIMENTOS);
            }
        });

    }

    public static void getEjerciciosBackground() {
        EjercicioDao ejercicioDao= DataBaseManager.getDaoSession().getEjercicioDao();
        List<Ejercicio> ejerciciosEnBd = ejercicioDao.loadAll();


        if (ejerciciosEnBd != null && ejerciciosEnBd.isEmpty()) {
            long idPaciente = AccountDataManager.obtenerIdPaciente();
            Call<RespuestaApi<ListadoEjercicioResponse>> getEjerciciosBackend =
                    apiService.getEjerciciosPaciente(APP_JSON, idPaciente, BEARER + AccountDataManager.obtenerToken(), new ListadoEjercicioRequest());

            APIManager.obtenerRespuesta(getEjerciciosBackend, new CallListener<ListadoEjercicioResponse>() {
                @Override
                public void procesarRespuesta(ListadoEjercicioResponse respuesta) {
                    List<EjercicioResponse> responses = respuesta.getEjercicios();
                    List<Ejercicio> ejercicios = EjercicioResponse.toEjercicio(responses);

                    for (Ejercicio ejercicio : ejercicios) {
                        ejercicioDao.save(ejercicio);
                    }

                }

                @Override
                public void procesarError(int codigo) {
                    //App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA);
                }
            });
        }
    }
}
