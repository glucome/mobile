package com.glucomeandroidapp.Controladores;

import android.os.Bundle;

import com.glucomeandroidapp.Activities.Medicos.MedicoSeleccionadoActivity;
import com.glucomeandroidapp.Activities.Medicos.MisMedicosActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.Conexiones.CallbackInterface;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMedicoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.APIClasses.Response.SolicitudResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoSolicitud;
import com.glucomeandroidapp.POJO.SolicitudVinculacion;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_MEDICOS;
import static com.glucomeandroidapp.Utils.Util.FALLO_EN_OBTENER_SOLICITUD;
import static com.glucomeandroidapp.Utils.Util.MEDICO_BUSCADO_BUNDLE;
import static com.glucomeandroidapp.Utils.Util.MEDICO_SELECCIONADO;
import static com.glucomeandroidapp.Utils.Util.MIS_MEDICOS_LIST;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.SOLICITUD_MEDICO;
import static com.glucomeandroidapp.Utils.Util.apiService;

/**
 * Creado por MartinArtime el 21 de mayo del 2019
 */
public class MedicosControlador {

    public static void getMedicosBackground() {

        Call<RespuestaApi<ListadoMedicoResponse>> obtenerMedicosPacienteRequest =
                apiService.getMedicosDelPaciente(APP_JSON, AccountDataManager.obtenerIdPaciente(), BEARER + AccountDataManager.obtenerToken());


        APIManager.obtenerRespuesta(obtenerMedicosPacienteRequest, new CallListener<ListadoMedicoResponse>() {
            @Override
            public void procesarRespuesta(ListadoMedicoResponse respuesta) {
                // Obtengo la data del paciente
                List<MedicoDataResponse> medicos = respuesta.getMedicos();

                PacienteDataResponse datosDelPaciente = AccountDataManager.obtenerDatosPaciente();
                datosDelPaciente.setMedicos(medicos);
                AccountDataManager.almacenarDatosUsuario(datosDelPaciente);

            }

            @Override
            public void procesarError(int codigo) {}
        });

    }

    public static void getMedicos() {

        Call<RespuestaApi<ListadoMedicoResponse>> obtenerMedicosPacienteRequest =
                apiService.getMedicosDelPaciente(APP_JSON, AccountDataManager.obtenerIdPaciente(), BEARER + AccountDataManager.obtenerToken());


        APIManager.obtenerRespuesta(obtenerMedicosPacienteRequest, new CallListener<ListadoMedicoResponse>() {
            @Override
            public void procesarRespuesta(ListadoMedicoResponse respuesta) {
                // Obtengo la data del paciente
                List<MedicoDataResponse> medicos = respuesta.getMedicos();

                Bundle bundle = new Bundle();
                bundle.putString(MIS_MEDICOS_LIST, new Gson().toJson(medicos));
                App.getInstance().iniciarActividad(MisMedicosActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_MEDICOS);
            }
        });
    }

    public static void getMedicosPorNombre(String nombre, CallbackInterface<List<MedicoDataResponse>> callback) {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("nombre", nombre);

        Call<RespuestaApi<ListadoMedicoResponse>> obtenerMedicosPorNombreRequest =
                apiService.getListadoMedico(APP_JSON, parametros, BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(obtenerMedicosPorNombreRequest, new CallListener<ListadoMedicoResponse>() {
            @Override
            public void procesarRespuesta(ListadoMedicoResponse respuesta) {
                // Obtengo la data del paciente
                List<MedicoDataResponse> medicos = respuesta.getMedicos();
                callback.onSuccess(medicos);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_MEDICOS);
            }
        });

    }

    public static void getSolicitud(MedicoDataResponse medico, TipoSolicitud tipo) {

        try {

            Map<String, String> params = new HashMap<>();
            params.put("tipoSolicitud", String.valueOf(tipo.ordinal()));


            Call<RespuestaApi<SolicitudResponse>> obtenerSolicitudRequest =
                    apiService.getSolicitud(APP_JSON, AccountDataManager.obtenerIdPaciente(), medico.getId(), params, BEARER + AccountDataManager.obtenerToken());


            APIManager.obtenerRespuesta(obtenerSolicitudRequest, new CallListener<SolicitudResponse>() {
                @Override
                public void procesarRespuesta(SolicitudResponse respuesta) {

                    // Obtengo la data del paciente
                    SolicitudVinculacion solicitud = respuesta.getSolicitud();
                    Bundle bundle = new Bundle();
                    bundle.putString(MEDICO_SELECCIONADO, new Gson().toJson(medico));
                    bundle.putString(SOLICITUD_MEDICO, new Gson().toJson(solicitud));
                    bundle.putString(BUNDLE_NAME, MEDICO_BUSCADO_BUNDLE);
                    App.getInstance().iniciarActividad(MedicoSeleccionadoActivity.class, bundle);
                }

                @Override
                public void procesarError(int codigo) {
                    App.getInstance().mostrarMensajeFlotante(FALLO_EN_OBTENER_SOLICITUD);
                }
            });

        } catch (Exception e) {
            App.getInstance().mostrarMensajeFlotante(FALLO_EN_OBTENER_SOLICITUD);
        }
    }


}
