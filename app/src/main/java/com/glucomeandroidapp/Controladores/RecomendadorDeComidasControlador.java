package com.glucomeandroidapp.Controladores;

import android.os.Bundle;

import com.glucomeandroidapp.Activities.Comidas.RecomendadorComidasActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.POJO.APIClasses.Response.RecomendadorResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.Alimento;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.COMIDAS_RECOMENDADAS;
import static com.glucomeandroidapp.Utils.Util.FALLO_DE_COMIDAS;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.apiService;

/**
 * Creado por MartinArtime el 21 de mayo del 2019
 */
public class RecomendadorDeComidasControlador {

    public static void getRecomendacionDeComidas() {


        Call<RespuestaApi<RecomendadorResponse>> requestRecomendacion = apiService.getRecomendacion(APP_JSON, AccountDataManager.obtenerIdPaciente(), BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(requestRecomendacion, new CallListener<RecomendadorResponse>() {
            @Override
            public void procesarRespuesta(RecomendadorResponse respuesta) {
                List<Alimento> alimentos =
                        respuesta.getAlimentosRecomendados();
                Bundle bundle = new Bundle();
                bundle.putString(COMIDAS_RECOMENDADAS, new Gson().toJson(alimentos));
                App.getInstance().iniciarActividad(
                        RecomendadorComidasActivity.class, bundle);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_DE_COMIDAS);
            }
        });

    }

}
