package com.glucomeandroidapp.Controladores;

import android.app.Activity;

import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.Conexiones.CallbackInterface;
import com.glucomeandroidapp.POJO.APIClasses.Requests.LoginRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.RegistroAllegadoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.RegistroPacienteRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.BooleanResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.Enumeraciones.Rol;
import com.glucomeandroidapp.Receivers.RefreshTokenReceiver;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.Crypto;
import com.glucomeandroidapp.Utils.UtilesFecha;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import java.time.LocalDateTime;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.EMAIL_NO_EXISTE;
import static com.glucomeandroidapp.Utils.Util.FALLO_EL_LOGIN;
import static com.glucomeandroidapp.Utils.Util.FALLO_EL_REGISTRO;
import static com.glucomeandroidapp.Utils.Util.FALLO_LA_SOLICITUD;
import static com.glucomeandroidapp.Utils.Util.LOGIN_EXITOSO;
import static com.glucomeandroidapp.Utils.Util.NOT_ACCEPTABLE;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.apiService;

/**
 * Creado por MartinArtime el 21 de mayo del 2019
 */
public class LoginYRegistroControlador {

    /**
     * Realiza el envío al backend del usuario y contraseña para autenticación
     *
     * @param usuario, objeto LoginRequest del backend para autenticar
     */
    public static void loginPaciente(LoginRequest usuario, Activity activity) {


        Call<RespuestaApi<LoginResponse>> loginPacienteRequest =
                apiService.loginPaciente(APP_JSON, usuario);

        APIManager.obtenerRespuesta(loginPacienteRequest, new CallListener<LoginResponse>() {
            @Override
            public void procesarRespuesta(LoginResponse respuesta) {

                Gson gson = new Gson();
                String json = gson.toJson(respuesta.getAsociado(), LinkedTreeMap.class);

                // Obtengo la data del rol correspondiente
                if (respuesta.getRoles().get(0).equals(Rol.PACIENTE.getNombre()) || respuesta.getRoles().get(0).equals(Rol.PACIENTE_PREMIUM.getNombre())) {
                    PacienteDataResponse dataResponse = gson.fromJson(json, PacienteDataResponse.class);
                    dataResponse.setSexo(dataResponse.getSexo().substring(0, 1));
                    dataResponse.setTipoDiabetes(dataResponse.getTipoDiabetes().substring(0, 1));

                    // Guardo los datos del paciente en una SharedPreference
                    AccountDataManager.almacenarDatosUsuario(dataResponse);

                    VariosControlador.sincronizar(activity);
                } else if (respuesta.getRoles().get(0).equals(Rol.MEDICO.getNombre()) || respuesta.getRoles().get(0).equals(Rol.MEDICO_PREMIUM.getNombre())) {
                    MedicoDataResponse dataResponse = gson.fromJson(json, MedicoDataResponse.class);

                    // Guardo los datos del medico en una SharedPreference
                    AccountDataManager.almacenarDatosUsuario(dataResponse);
                } else {
                    AllegadoDataResponse dataResponse = gson.fromJson(json, AllegadoDataResponse.class);

                    // Guardo los datos del allegado en una SharedPreference
                    AccountDataManager.almacenarDatosUsuario(dataResponse);
                }


                // Vacio el paciente del cambiarPass response para almacenarlo
                respuesta.setAsociado(null);

                // Guardo la contraseña encriptada para futuros refresh del token
                respuesta.setPassword(Crypto.encrypt(usuario.getPassword()));

                //LocalDateTime horaDeExpiracion = LocalDateTime.now().plusHours(1);
                LocalDateTime horaDeExpiracion = LocalDateTime.now().plusMinutes(5);

                //Log.e(TAG, "EXPIRACION SETEADA PARA LAS: " + horaDeExpiracion.toString());

                // Seteo la fecha de expiración de token
                respuesta.setTokenFechaExpiracion(
                        UtilesFecha.getStringFromLocalDateTime(horaDeExpiracion));

                Long time = UtilesFecha.getTimeInMillisFromLocalDateTime(horaDeExpiracion);

                // Creo la alarma para antes de que llegue el vencimiento del token
                //RefreshTokenReceiver.setAlarm(activity.getApplicationContext(), time);
                RefreshTokenReceiver.setAlarm(App.getContext(), time);

                // Guardo la sesión en una SharedPreference
                AccountDataManager.loguearse(respuesta);

                // Sigo con el cambiarPass
                App.getInstance().mostrarMensajeFlotante(LOGIN_EXITOSO);
                App.getInstance().iniciarActividad(MenuPrincipalActivity.class);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_EL_LOGIN);
            }
        });

    }


    /**
     * Realiza el envío al backend del usuario nuevo registrado
     *
     * @param nuevoUsuario, objeto usuario a enviar
     */
    public static void enviarRegistro(RegistroPacienteRequest nuevoUsuario,
                                      CallbackInterface<LoginResponse> onSuccessCallback,
                                      CallbackInterface<String> onErrorCallback) {

        Call<RespuestaApi<LoginResponse>> registroPacienteRequest =
                apiService.registrarPaciente(APP_JSON, nuevoUsuario);

        APIManager.obtenerRespuesta(registroPacienteRequest, new CallListener<LoginResponse>() {
            @Override
            public void procesarRespuesta(LoginResponse respuesta) {
                // Sigo con el cambiarPass
                onSuccessCallback.onSuccess(respuesta);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);

                onErrorCallback.onSuccess(FALLO_EL_REGISTRO);
            }
        });

    }

    /**
     * Realiza el envío al backend del usuario y contraseñas nuevas
     *
     * @param nuevoUsuario, objeto usuario a enviar
     */
    public static void cambiarPass(LoginRequest nuevoUsuario,
                                      CallbackInterface<BooleanResponse> onSuccessCallback,
                                      CallbackInterface<String> onErrorCallback) {

        Call<RespuestaApi<BooleanResponse>> registroPacienteRequest =
                apiService.cambiarPass(APP_JSON, nuevoUsuario);

        APIManager.obtenerRespuesta(registroPacienteRequest, new CallListener<BooleanResponse>() {
            @Override
            public void procesarRespuesta(BooleanResponse respuesta) {
                // Sigo con el cambiarPass
                onSuccessCallback.onSuccess(respuesta);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION){
                    onErrorCallback.onSuccess(NO_HAY_INTERNET);
                } else if (codigo == NOT_ACCEPTABLE){
                    onErrorCallback.onSuccess(EMAIL_NO_EXISTE);
                } else {
                    onErrorCallback.onSuccess(FALLO_LA_SOLICITUD);
                }
            }
        });

    }

    /**
     * Realiza el envío al backend del usuario nuevo registrado (allegado)
     *
     * @param nuevoUsuario, objeto usuario a enviar
     */
    public static void enviarRegistroAllegado(RegistroAllegadoRequest nuevoUsuario,
                                              CallbackInterface<LoginResponse> onSuccessCallback,
                                              CallbackInterface<String> onErrorCallback) {


        Call<RespuestaApi<LoginResponse>> registroAllegadoRequest =
                apiService.registrarAllegado(APP_JSON, nuevoUsuario);

        APIManager.obtenerRespuesta(registroAllegadoRequest, new CallListener<LoginResponse>() {
            @Override
            public void procesarRespuesta(LoginResponse respuesta) {
                // Sigo con el cambiarPass
                onSuccessCallback.onSuccess(respuesta);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    onErrorCallback.onSuccess(FALLO_EL_REGISTRO);
            }
        });

    }


    /**
     * Verifica si existe el mail
     *
     * @param email, String con mail del usuario
     */
    public static void verificarSiExisteUsuario(String email,
                                      CallbackInterface<BooleanResponse> onSuccessCallback,
                                      CallbackInterface<String> onErrorCallback) {

        Call<RespuestaApi<BooleanResponse>> call  = apiService.existeMail(APP_JSON, email);

        APIManager.obtenerRespuesta(call, new CallListener<BooleanResponse>() {
            @Override
            public void procesarRespuesta(BooleanResponse respuesta) {
                onSuccessCallback.onSuccess(respuesta);
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);

                onErrorCallback.onSuccess(FALLO_LA_SOLICITUD);
            }
        });

    }
}
