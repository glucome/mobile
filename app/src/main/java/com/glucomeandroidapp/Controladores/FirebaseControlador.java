package com.glucomeandroidapp.Controladores;

import android.util.Log;

import androidx.annotation.NonNull;

import com.glucomeandroidapp.POJO.APIClasses.Requests.FirebaseTokenRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.LoginRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.FirebaseTokenResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoAllegadoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMedicoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.Crypto;
import com.glucomeandroidapp.Utils.Util;
import com.glucomeandroidapp.Utils.UtilesFunciones;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BAD_REQUEST;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.OK;
import static com.glucomeandroidapp.Utils.Util.OK_REQUEST;
import static com.glucomeandroidapp.Utils.Util.TAG;
import static com.glucomeandroidapp.Utils.Util.apiService;

/**
 * Creado por MartinArtime el 23 de junio del 2019
 */
public class FirebaseControlador {

    /**
     * Método para enviar el token de firebase actualizado al backend
     */
    public static void enviarTokenFirebase(){
        // Inicialmente hago algo similar a cuando renuevo el token de backend normal.

        LoginResponse loginResponse =  AccountDataManager.obtenerDatosUsuario();

        if(loginResponse!=null && loginResponse.getTokenFirebase()!=null){
            FirebaseTokenRequest ftr = new FirebaseTokenRequest(
                    loginResponse.getTokenFirebase(),
                    new LoginRequest(loginResponse.getUsuario(), Crypto.decrypt(loginResponse.getPassword())));

            if(UtilesFunciones.hayConexionAInternet()) {
                apiService.enviarTokenFirebaseDeUsuario(
                        APP_JSON, ftr, BEARER + loginResponse.getToken())
                        .enqueue(new Callback<RespuestaApi<LoginResponse>>() {
                            @Override
                            public void onResponse(@NonNull Call<RespuestaApi<LoginResponse>> call,
                                                   @NonNull Response<RespuestaApi<LoginResponse>> response) {

                                // Si el código de respuesta está entre 200 y 300
                                if(response.isSuccessful()) {
                                    // Si el código es 200
                                    if(response.code() == OK_REQUEST){
                                        RespuestaApi res = response.body();
                                        if(res!=null){
                                            if(res.getMensaje().equals(OK)){
                                                Log.e(TAG, "Token Firebase enviado a backend");
                                            }
                                        }
                                    }
                                } else{
                                    if(response.code() == BAD_REQUEST){
                                        Log.e(TAG, "BAD REQUEST em Envio de token firebase");
                                    } else {
                                        Log.e(TAG, "Otro código de error");
                                    }
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<RespuestaApi<LoginResponse>> call, @NonNull Throwable t) {
                                t.printStackTrace();
                            }
                        });
            } else {
                Log.e(TAG, "No hay conexion a internet para enviar token firebase a backend");
            }

        }

    }

    public static void obtenerTokenFirebaseDePacientesAsociados() {}

    /**
     * Método para obtener el token de firebase de los medicos asociados a un paciente
     * TODO ver si esta logica rancia es necesaria para lo que voy a haber hecho después o si se puede mejorar
     */
    public static void obtenerTokenFirebaseDeMedicosAsociados() {

        LoginResponse datosDelUsuario = AccountDataManager.obtenerDatosUsuario();

        if (datosDelUsuario != null) {
            PacienteDataResponse datosDelPaciente =
                    AccountDataManager.obtenerDatosPaciente();

            if (UtilesFunciones.hayConexionAInternet() && datosDelPaciente != null) {

                apiService.getMedicosDelPaciente(APP_JSON, datosDelPaciente.getIdUsuario(), BEARER + datosDelUsuario.getToken())
                        .enqueue(new Callback<RespuestaApi<ListadoMedicoResponse>>() {
                            @Override
                            public void onResponse(@NonNull Call<RespuestaApi<ListadoMedicoResponse>> call,
                                                   @NonNull Response<RespuestaApi<ListadoMedicoResponse>> response) {

                                // Si el código de respuesta está entre 200 y 300
                                if (response.isSuccessful()) {
                                    // Si el código es 200
                                    if (response.code() == OK_REQUEST) {
                                        RespuestaApi res = response.body();
                                        if (res != null) {
                                            if (res.getMensaje().equals(OK)) {
                                                // Obtengo la respuesta
                                                ListadoMedicoResponse recomendadorResponse =
                                                        (ListadoMedicoResponse) res.getRespuesta();
                                                // Obtengo la data del paciente
                                                List<MedicoDataResponse> medicos = recomendadorResponse.getMedicos();

                                                datosDelPaciente.setMedicos(medicos);

                                                AccountDataManager.almacenarDatosUsuario(datosDelPaciente);

                                                for (MedicoDataResponse medico : medicos) {
                                                    if (UtilesFunciones.hayConexionAInternet()) {
                                                        apiService.obtenerTokenFirebaseDeUsuario(
                                                                APP_JSON, medico.getIdUsuario(), BEARER + datosDelUsuario.getToken())
                                                                .enqueue(new Callback<RespuestaApi<FirebaseTokenResponse>>() {
                                                                    @Override
                                                                    public void onResponse(@NonNull Call<RespuestaApi<FirebaseTokenResponse>> call,
                                                                                           @NonNull Response<RespuestaApi<FirebaseTokenResponse>> response) {

                                                                        // Si el código de respuesta está entre 200 y 300
                                                                        if (response.isSuccessful()) {
                                                                            // Si el código es 200
                                                                            if (response.code() == OK_REQUEST) {
                                                                                RespuestaApi res = response.body();
                                                                                if (res != null) {
                                                                                    if (res.getMensaje().equals(OK)) {
                                                                                        Log.e(TAG, "Token Firebase de medico obtenido del backend!");
                                                                                        FirebaseTokenResponse ftr = (FirebaseTokenResponse) res.getRespuesta();
                                                                                        medico.setTokenFirebase(ftr.getToken());
                                                                                        Log.e(TAG, "Token: " + ftr.getToken());
                                                                                        medicos.set(Math.toIntExact(medico.getId()-1), medico);
                                                                                        datosDelPaciente.setMedicos(medicos);
                                                                                        AccountDataManager.almacenarDatosUsuario(datosDelPaciente);
                                                                                    }
                                                                                }
                                                                            }
                                                                        } else {
                                                                            if (response.code() == BAD_REQUEST) {
                                                                                Log.e(TAG, "BAD REQUEST em Envio de token firebase");
                                                                            } else {
                                                                                Log.e(TAG, "Otro código de error");
                                                                            }
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(@NonNull Call<RespuestaApi<FirebaseTokenResponse>> call, @NonNull Throwable t) {
                                                                        t.printStackTrace();
                                                                    }
                                                                });
                                                    } else {
                                                        Log.e(TAG, "No hay conexion a internet");
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<RespuestaApi<ListadoMedicoResponse>> call, Throwable t) {
                                t.printStackTrace();
                            }
                        });
            }
        }
    }

    /**
     * Método para obtener el token de firebase de los allegados a un paciente
     * TODO ver si esta logica rancia es necesaria para lo que voy a haber hecho después o si se puede mejorar
     */
    public static void obtenerTokenFirebaseDeAllegadosAsociados(){

        LoginResponse datosDelUsuario = AccountDataManager.obtenerDatosUsuario();

        if (datosDelUsuario != null) {
            PacienteDataResponse datosDelPaciente = AccountDataManager.obtenerDatosPaciente();

            if (UtilesFunciones.hayConexionAInternet() && datosDelPaciente != null) {

                for (AllegadoDataResponse allegado : datosDelPaciente.getAllegados()) {
                    Log.e(Util.TAG, "ID: "+allegado.getId());
                    Log.e(Util.TAG, "NOMBRE: "+allegado.getNombre());
                    Log.e(Util.TAG, "TOKEN: "+(allegado.getTokenFirebase()!=null ? allegado.getTokenFirebase() : "Null"));
                }

                apiService.getAllegadosDelPaciente(APP_JSON, datosDelPaciente.getIdUsuario(), BEARER + datosDelUsuario.getToken())
                        .enqueue(new Callback<RespuestaApi<ListadoAllegadoResponse>>() {
                            @Override
                            public void onResponse(@NonNull Call<RespuestaApi<ListadoAllegadoResponse>> call,
                                                   @NonNull Response<RespuestaApi<ListadoAllegadoResponse>> response) {

                                // Si el código de respuesta está entre 200 y 300
                                if (response.isSuccessful()) {
                                    // Si el código es 200
                                    if (response.code() == OK_REQUEST) {
                                        RespuestaApi res = response.body();
                                        if (res != null) {
                                            if (res.getMensaje().equals(OK)) {
                                                // Obtengo la respuesta
                                                ListadoMedicoResponse recomendadorResponse =
                                                        (ListadoMedicoResponse) res.getRespuesta();
                                                // Obtengo la data del paciente
                                                List<MedicoDataResponse> medicos = recomendadorResponse.getMedicos();

                                                datosDelPaciente.setMedicos(medicos);

                                                AccountDataManager.almacenarDatosUsuario(datosDelPaciente);

                                                for (MedicoDataResponse medico : medicos) {
                                                    if (UtilesFunciones.hayConexionAInternet()) {
                                                        apiService.obtenerTokenFirebaseDeUsuario(
                                                                APP_JSON, medico.getIdUsuario(), BEARER + datosDelUsuario.getToken())
                                                                .enqueue(new Callback<RespuestaApi<FirebaseTokenResponse>>() {
                                                                    @Override
                                                                    public void onResponse(@NonNull Call<RespuestaApi<FirebaseTokenResponse>> call,
                                                                                           @NonNull Response<RespuestaApi<FirebaseTokenResponse>> response) {

                                                                        // Si el código de respuesta está entre 200 y 300
                                                                        if (response.isSuccessful()) {
                                                                            // Si el código es 200
                                                                            if (response.code() == OK_REQUEST) {
                                                                                RespuestaApi res = response.body();
                                                                                if (res != null) {
                                                                                    if (res.getMensaje().equals(OK)) {
                                                                                        Log.e(TAG, "Token Firebase de medico obtenido del backend!");
                                                                                        FirebaseTokenResponse ftr = (FirebaseTokenResponse) res.getRespuesta();
                                                                                        medico.setTokenFirebase(ftr.getToken());
                                                                                        Log.e(TAG, "Token: " + ftr.getToken());
                                                                                        medicos.set(Math.toIntExact(medico.getId()-1), medico);
                                                                                    }
                                                                                }
                                                                            }
                                                                        } else {
                                                                            if (response.code() == BAD_REQUEST) {
                                                                                Log.e(TAG, "BAD REQUEST em Envio de token firebase");
                                                                            } else {
                                                                                Log.e(TAG, "Otro código de error");
                                                                            }
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(@NonNull Call<RespuestaApi<FirebaseTokenResponse>> call, @NonNull Throwable t) {
                                                                        t.printStackTrace();
                                                                    }
                                                                });
                                                    } else {
                                                        Log.e(TAG, "No hay conexion a internet");
                                                    }
                                                }
                                                datosDelPaciente.setMedicos(medicos);
                                                AccountDataManager.almacenarDatosUsuario(datosDelPaciente);
                                            }

                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<RespuestaApi<ListadoAllegadoResponse>> call, Throwable t) {
                                t.printStackTrace();
                            }
                        });
            } else {
                Log.e(TAG, "No hay conexion a internet");
            }
        }

    }


}
