package com.glucomeandroidapp.Controladores;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.Conexiones.CallbackInterface;
import com.glucomeandroidapp.POJO.APIClasses.Requests.IngresoAlimentoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ListadoAlimentoIngeridoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.AlimentoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.IdResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoAlimentoIngeridoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoAlimentoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.Alimento;
import com.glucomeandroidapp.POJO.Alimentos.IngresoAlimento;
import com.glucomeandroidapp.POJO.Alimentos.IngresoAlimentoDao;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.Util;
import com.google.gson.Gson;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.FALLO_BUSQUEDA_ALIMENTOS;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.apiService;

public class AlimentosControlador {

    public static void enviarIngresoAlimento(IngresoAlimentoRequest IngresoAlimentoRequest,
                                             IngresoAlimento IngresoAlimento, File fileImage) {

        RequestBody data = RequestBody.create(MediaType.parse("text/plain"), new Gson().toJson(IngresoAlimentoRequest));

        MultipartBody.Part body = null;
        if(fileImage != null){
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImage);
            body = MultipartBody.Part.createFormData("file", fileImage.getName(), reqFile);
        }

        Call<RespuestaApi<IdResponse>> enviarIngresoAlimentoRequest =
                apiService.enviarIngresoAlimento( AccountDataManager.obtenerIdPaciente(), body,data,
                        BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(enviarIngresoAlimentoRequest, new CallListener<IdResponse>() {
            @Override
            public void procesarRespuesta(IdResponse respuesta) {
                // Actualizo la Inyeccion con el id que viene en el
                // InyeccionResponse que devuelve el backend.
                if (IngresoAlimento != null) {
                    IngresoAlimento.setIdBackend(respuesta.getId());
                    DataBaseManager.getDaoSession().getIngresoAlimentoDao().update(IngresoAlimento);
                }
            }

            @Override
            public void procesarError(int codigo) {
                App.getInstance().mostrarMensajeFlotante(Util.FALLO_INGRESO_ALIMENTO);
            }
        });

    }

    public static void obtenerAlimentosPorNombre(String nombre, CallbackInterface<List<Alimento>> callback) {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("nombre", nombre);
        parametros.put("paciente", String.valueOf(AccountDataManager.obtenerIdPaciente()));


            Call<RespuestaApi<ListadoAlimentoResponse>> obtenerAlimentosPorNombreRequest =
                    apiService.getListadoAlimentos(APP_JSON, parametros, BEARER + AccountDataManager.obtenerToken());

            APIManager.obtenerRespuesta(obtenerAlimentosPorNombreRequest, new CallListener<ListadoAlimentoResponse>() {
                @Override
                public void procesarRespuesta(ListadoAlimentoResponse respuesta) {
                    List<Alimento> alimentos = respuesta.getAlimentos();
                    callback.onSuccess(alimentos);
                }

                @Override
                public void procesarError(int codigoError) {
                    if (codigoError == SIN_CONEXION)
                        App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                    else
                    App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA_ALIMENTOS);
                }
            });

    }

    public static void getAlimentosBackground() {
        IngresoAlimentoDao alimentoDao = DataBaseManager.getDaoSession().getIngresoAlimentoDao();
        List<IngresoAlimento> alimentosEnBd = alimentoDao.loadAll();


        if(alimentosEnBd !=  null && alimentosEnBd.isEmpty()){
            long idPaciente = AccountDataManager.obtenerIdPaciente();
            Call<RespuestaApi<ListadoAlimentoIngeridoResponse>> getAlimetosBackend =
                    apiService.getAlimentosPaciente(APP_JSON, idPaciente, BEARER + AccountDataManager.obtenerToken(), new ListadoAlimentoIngeridoRequest());

            APIManager.obtenerRespuesta(getAlimetosBackend, new CallListener<ListadoAlimentoIngeridoResponse>() {
                @Override
                public void procesarRespuesta(ListadoAlimentoIngeridoResponse respuesta) {
                    List<AlimentoResponse> responses = respuesta.getAlimentoIngerido();
                    List<IngresoAlimento> alimentos = AlimentoResponse.toIngresoAlimento(responses);

                    for (IngresoAlimento alimento : alimentos) {
                        alimentoDao.save(alimento);
                    }

                }

                @Override
                public void procesarError(int codigo) {
                    //App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA);
                }
            });
        }
    }
}
