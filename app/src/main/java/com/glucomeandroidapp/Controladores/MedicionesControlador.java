package com.glucomeandroidapp.Controladores;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIManager;
import com.glucomeandroidapp.Conexiones.CallListener;
import com.glucomeandroidapp.POJO.APIClasses.Requests.MedicionRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.IdResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMedicionesResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicionPacienteResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.POJO.Medicion.MedicionDao;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.util.List;

import retrofit2.Call;

import static com.glucomeandroidapp.Utils.Util.APP_JSON;
import static com.glucomeandroidapp.Utils.Util.BEARER;
import static com.glucomeandroidapp.Utils.Util.FALLO_INGRESO_MEDICION;
import static com.glucomeandroidapp.Utils.Util.NO_HAY_INTERNET;
import static com.glucomeandroidapp.Utils.Util.SIN_CONEXION;
import static com.glucomeandroidapp.Utils.Util.apiService;

/**
 * Creado por MartinArtime el 28 de mayo del 2019
 */
public class MedicionesControlador {

    public static void enviarMedicion(MedicionRequest medicionRequest, Medicion medicion) {

        Call<RespuestaApi<IdResponse>> enviarMedicionRequest =
                apiService.enviarMedicion(APP_JSON, AccountDataManager.obtenerIdPaciente(), medicionRequest,
                        BEARER + AccountDataManager.obtenerToken());


        APIManager.obtenerRespuesta(enviarMedicionRequest, new CallListener<IdResponse>() {
            @Override
            public void procesarRespuesta(IdResponse respuesta) {
                // Actualizo la medicion con el id que viene en el
                // MedicionResponse que devuelve el backend.
                if (medicion != null) {
                    medicion.setIdBackend(respuesta.getId());
                    DataBaseManager.getDaoSession().getMedicionDao().update(medicion);
                }
            }

            @Override
            public void procesarError(int codigo) {
                if (codigo == SIN_CONEXION)
                    App.getInstance().mostrarMensajeFlotante(NO_HAY_INTERNET);
                else
                    App.getInstance().mostrarMensajeFlotante(FALLO_INGRESO_MEDICION);
            }
        });


    }

    public static void enviarMedicionBackground(MedicionRequest medicionRequest, Medicion medicion) {

        Call<RespuestaApi<IdResponse>> enviarMedicionRequest =
                apiService.enviarMedicion(APP_JSON, AccountDataManager.obtenerIdPaciente(), medicionRequest,
                        BEARER + AccountDataManager.obtenerToken());

        APIManager.obtenerRespuesta(enviarMedicionRequest, new CallListener<IdResponse>() {
            @Override
            public void procesarRespuesta(IdResponse respuesta) {
                // Actualizo la medicion con el id que viene en el
                // MedicionResponse que devuelve el backend.
                if (medicion != null) {
                    medicion.setIdBackend(respuesta.getId());
                    DataBaseManager.getDaoSession().getMedicionDao().update(medicion);
                }
            }

            @Override
            public void procesarError(int codigo) {
                //App.getInstance().mostrarMensajeFlotante(FALLO_INGRESO_MEDICION);
            }
        });
    }

    public static void getMedicionesBackground() {
        MedicionDao medicionDao = DataBaseManager.getDaoSession().getMedicionDao();
        List<Medicion> medicionesEnBd = medicionDao.loadAll();


        if(medicionesEnBd !=  null && medicionesEnBd.isEmpty()){
            long idPaciente = AccountDataManager.obtenerIdPaciente();
            Call<RespuestaApi<ListadoMedicionesResponse>> getMedicionesRequest =
                    apiService.getMedicionesPaciente(APP_JSON, idPaciente, idPaciente,BEARER + AccountDataManager.obtenerToken());

            APIManager.obtenerRespuesta(getMedicionesRequest, new CallListener<ListadoMedicionesResponse>() {
                @Override
                public void procesarRespuesta(ListadoMedicionesResponse respuesta) {
                    List<MedicionPacienteResponse> responses = respuesta.getMediciones();
                    List<Medicion> mediciones = MedicionPacienteResponse.getToMedicion(responses);

                    for (Medicion medicion : mediciones) {
                        medicionDao.save(medicion);
                    }

                }

                @Override
                public void procesarError(int codigo) {
                    //App.getInstance().mostrarMensajeFlotante(FALLO_BUSQUEDA);
                }
            });
        }
    }

//    public static Double obtenerValorMedicionCalibrada(MedicionDispositivo medicionDispositivo, Calibracion calibracion, TipoCalculoCalibracion tipoCalculoCalibracion) {
//        Double valorMedicion = 0.0;
//        switch (tipoCalculoCalibracion) {
//            case PROMEDIO:
//                valorMedicion = obtenerValorPromedio(medicionDispositivo, calibracion);
//                break;
//            case PROMEDIO_PONDERADO:
//                valorMedicion = obtenerValorPromedioPonderado(medicionDispositivo, calibracion);
//                break;
//        }
//        return valorMedicion;
//    }
//
//    public static Double obtenerValorPromedio(MedicionDispositivo medicionDispositivo, Calibracion calibracion) {
//        Double valorMedicion, valor635, valor740, valor850, valor940;
//
//        Regresion regresion = calibracion.getRegresionPorTipo(TipoRegresion.REGRESION_635);
//        valor635 = (medicionDispositivo.getMedicion635() * regresion.getA()) + regresion.getB();
//        regresion = calibracion.getRegresionPorTipo(TipoRegresion.REGRESION_740);
//        valor740 = (medicionDispositivo.getMedicion740() * regresion.getA()) + regresion.getB();
//        regresion = calibracion.getRegresionPorTipo(TipoRegresion.REGRESION_850);
//        valor850 = (medicionDispositivo.getMedicion850() * regresion.getA()) + regresion.getB();
//        regresion = calibracion.getRegresionPorTipo(TipoRegresion.REGRESION_940);
//        valor940 = (medicionDispositivo.getMedicion940() * regresion.getA()) + regresion.getB();
//
//        return (valor635 + valor740 + valor850 + valor940) / 4;
//    }
//
//    public static Double obtenerValorPromedioPonderado(MedicionDispositivo medicionDispositivo, Calibracion calibracion) {
//        Double valorMedicion, valor635, valor740, valor850, valor940, ponderado635, ponderado740, ponderado850, ponderado940, sumaPonderado;
//        sumaPonderado = 0.0;
//
//        Regresion regresion = calibracion.getRegresionPorTipo(TipoRegresion.REGRESION_635);
//        valor635 = (medicionDispositivo.getMedicion635() * regresion.getA()) + regresion.getB();
//        sumaPonderado += regresion.getR();
//        ponderado635 = regresion.getR();
//        regresion = calibracion.getRegresionPorTipo(TipoRegresion.REGRESION_740);
//        valor740 = (medicionDispositivo.getMedicion740() * regresion.getA()) + regresion.getB();
//        sumaPonderado = regresion.getR();
//        ponderado740 = regresion.getR();
//        regresion = calibracion.getRegresionPorTipo(TipoRegresion.REGRESION_850);
//        valor850 = (medicionDispositivo.getMedicion850() * regresion.getA()) + regresion.getB();
//        sumaPonderado = regresion.getR();
//        ponderado850 = regresion.getR();
//        regresion = calibracion.getRegresionPorTipo(TipoRegresion.REGRESION_940);
//        valor940 = (medicionDispositivo.getMedicion940() * regresion.getA()) + regresion.getB();
//        sumaPonderado = regresion.getR();
//        ponderado940 = regresion.getR();
//
//        ponderado635 = ponderado635 / sumaPonderado;
//        ponderado740 = ponderado740 / sumaPonderado;
//        ponderado850 = ponderado850 / sumaPonderado;
//        ponderado940 = ponderado940 / sumaPonderado;
//
//        return (valor635 * ponderado635) + (valor740 * ponderado740) + (valor850 * ponderado850) + (valor940 * ponderado940);
//    }
}
