package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.POJO.Medicacion.HorarioMedicacion;
import com.glucomeandroidapp.R;

import java.util.List;

public class HorarioMedicacionAdapter extends RecyclerView.Adapter<HorarioMedicacionAdapter.MyViewHolder> {

    private List<HorarioMedicacion> horarios;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView horario;

        public MyViewHolder(View view) {
            super(view);
            horario = (TextView) view.findViewById(R.id.lbl_horario_medicacion);
        }
    }

    public HorarioMedicacionAdapter(List<HorarioMedicacion> horarios) {
        this.horarios = horarios;
    }

    @NonNull
    @Override
    public HorarioMedicacionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_horario_medicamento, viewGroup, false);

        return new HorarioMedicacionAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HorarioMedicacionAdapter.MyViewHolder myViewHolder, int i) {
        HorarioMedicacion horario= horarios.get(i);
        int hora = horario.getHora();
        int minuto = horario.getMinuto();
        myViewHolder.horario.setText((hora < 10 ? "0" + hora : hora) + ":" + (minuto < 10 ? "0" + minuto : minuto));

    }

    @Override
    public int getItemCount() {
        return horarios != null ? horarios.size() : 0;
    }
}
