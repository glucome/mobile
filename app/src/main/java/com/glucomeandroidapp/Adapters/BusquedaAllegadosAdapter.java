package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.ImagenLoader;

import java.util.List;

import static com.glucomeandroidapp.Utils.Util.URL_BASE;
import static com.glucomeandroidapp.Utils.Util.URL_IMAGEN_USUARIO;

public class BusquedaAllegadosAdapter extends RecyclerView.Adapter<BusquedaAllegadosAdapter.MyViewHolder> {

    private List<AllegadoDataResponse> allegados;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nombreAllegado;
        ImageView imagenAllegado;

        MyViewHolder(View view) {
            super(view);
            nombreAllegado = view.findViewById(R.id.lbl_nombre_allegado);
            imagenAllegado = view.findViewById(R.id.img_allegado);
        }
    }

    public BusquedaAllegadosAdapter(List<AllegadoDataResponse> allegados) {
        this.allegados = allegados;
    }

    @NonNull
    @Override
    public BusquedaAllegadosAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_busqueda_allegado, viewGroup, false);

        return new BusquedaAllegadosAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BusquedaAllegadosAdapter.MyViewHolder myViewHolder, int i) {
        AllegadoDataResponse allegado = allegados.get(i);
        myViewHolder.nombreAllegado.setText(allegado.getNombre());
        String url = URL_BASE+URL_IMAGEN_USUARIO+ allegado.getIdUsuario()+"/";
        new ImagenLoader(myViewHolder.imagenAllegado).execute(url);

    }

    @Override
    public int getItemCount() {
        return  allegados != null ? allegados.size() : 0;
    }
}