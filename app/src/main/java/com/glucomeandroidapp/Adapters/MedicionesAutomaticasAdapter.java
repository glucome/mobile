package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.POJO.Medicion.MedicionAutomatica;
import com.glucomeandroidapp.R;

import java.util.List;

public class MedicionesAutomaticasAdapter extends RecyclerView.Adapter<MedicionesAutomaticasAdapter.ViewHolder> {

    private List<MedicionAutomatica> medicionesAutomaticasList;



    public MedicionesAutomaticasAdapter(List<MedicionAutomatica> medicionesAutomaticasList) {
        this.medicionesAutomaticasList = medicionesAutomaticasList;
    }

    @NonNull
    @Override
    public MedicionesAutomaticasAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_medicion_automatica, viewGroup, false);

        return new MedicionesAutomaticasAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MedicionesAutomaticasAdapter.ViewHolder myViewHolder, int i) {
        MedicionAutomatica medicionAutomatica = medicionesAutomaticasList.get(i);
        myViewHolder.nombreMedicionAutomatica.setText(medicionAutomatica.getNombre());
        String horaAMostrar = medicionAutomatica.getHoraFormateada();
        myViewHolder.horaMedicionAutomatica.setText(horaAMostrar);
    }

    @Override
    public int getItemCount() {
        return medicionesAutomaticasList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView nombreMedicionAutomatica, horaMedicionAutomatica;

        ViewHolder(View view) {
            super(view);
            nombreMedicionAutomatica = view.findViewById(R.id.lbl_nombre_medicion_automatica);
            horaMedicionAutomatica = view.findViewById(R.id.lbl_hora_medicion_automatica);
        }
    }
}
