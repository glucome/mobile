package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Adapters.Models.HistorialMedicionAdapterModel;
import com.glucomeandroidapp.Adapters.Models.HistorialTomaMedicamentoAdapterModel;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

public class HistorialTomaMedicamentoAdapter extends RecyclerView.Adapter<HistorialTomaMedicamentoAdapter.ViewHolder> {

    private ArrayList<HistorialTomaMedicamentoAdapterModel> historial_toma_medicamento_modelArrayList;
    private ArrayList<HistorialTomaMedicamentoAdapterModel> historial_toma_medicamento_modelArrayList_filtrado;

    public HistorialTomaMedicamentoAdapter(ArrayList<HistorialTomaMedicamentoAdapterModel> historial_toma_medicamento_modelArrayList) {
        this.historial_toma_medicamento_modelArrayList = historial_toma_medicamento_modelArrayList;
        this.historial_toma_medicamento_modelArrayList_filtrado = historial_toma_medicamento_modelArrayList;
    }

    @Override
    public HistorialTomaMedicamentoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_historial_toma_medicamento, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistorialTomaMedicamentoAdapter.ViewHolder holder, final int position) {
        holder.cantidadMedicamento.setText(historial_toma_medicamento_modelArrayList.get(position).getCantidad());
        holder.marca.setText(historial_toma_medicamento_modelArrayList.get(position).getMedicamento().getMarca());
        holder.droga.setText(historial_toma_medicamento_modelArrayList.get(position).getMedicamento().getDroga());
        holder.laboratorio.setText(historial_toma_medicamento_modelArrayList.get(position).getMedicamento().getLaboratorio());
        holder.presentacion.setText(historial_toma_medicamento_modelArrayList.get(position).getMedicamento().getPresentacion());
        holder.fecha.setText(historial_toma_medicamento_modelArrayList.get(position).getFecha());
    }

    @Override
    public int getItemCount() {
        return historial_toma_medicamento_modelArrayList.size();
    }


    public void filter(String nombre, String fechaDesde, String fechaHasta){

        if (nombre==null && fechaDesde==null && fechaHasta==null) {
            historial_toma_medicamento_modelArrayList_filtrado = historial_toma_medicamento_modelArrayList;
        } else {
            boolean compararNombre = false;
            boolean compararFecha = false;

            LocalDateTime fechaDesdeComparable = null;
            LocalDateTime fechaHastaComparable = null;

            if(fechaDesde!=null && fechaHasta!=null){
                compararFecha = true;
                fechaDesdeComparable = UtilesFecha.getLocalDateTimeFromString(fechaDesde);
                fechaHastaComparable = UtilesFecha.getLocalDateTimeFromString(fechaHasta);
            }

            if(nombre!=null){
                compararNombre = true;
            }

            ArrayList<HistorialTomaMedicamentoAdapterModel> filteredList = new ArrayList<>();
            for (HistorialTomaMedicamentoAdapterModel medicacion : historial_toma_medicamento_modelArrayList) {

                // Evalúo la fecha
                if(compararFecha){
                    LocalDateTime fechaMedicacion = UtilesFecha.getLocalDateTimeFromString(medicacion.getFecha());
                    if(fechaMedicacion!=null && fechaDesdeComparable!=null && fechaHastaComparable!=null){
                        if (fechaDesdeComparable.isAfter(fechaMedicacion) || fechaHastaComparable.isBefore(fechaMedicacion)) {
                            continue;
                        }
                    }
                }

                // Evalúo el nombre del medicamento
                if(compararNombre){
                    if(!medicacion.getMedicamento().getDroga().contains(nombre) &&
                       !medicacion.getMedicamento().getLaboratorio().contains(nombre) &&
                       !medicacion.getMedicamento().getMarca().contains(nombre) &&
                       !medicacion.getMedicamento().getPresentacion().contains(nombre)){
                        continue;
                    }
                }

                filteredList.add(medicacion);
            }

            historial_toma_medicamento_modelArrayList_filtrado = filteredList;
        }

        Collections.sort(historial_toma_medicamento_modelArrayList_filtrado, (x, y)-> Math.negateExact(UtilesFecha.getLocalDateTimeFromString(x.getFecha()).compareTo(UtilesFecha.getLocalDateTimeFromString(y.getFecha()))));


        historial_toma_medicamento_modelArrayList = historial_toma_medicamento_modelArrayList_filtrado;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView cantidadMedicamento, marca, droga, laboratorio, presentacion, fecha;

        ViewHolder(View itemView) {
            super(itemView);

            cantidadMedicamento = itemView.findViewById(R.id.cantidadMedicamento);
            marca = itemView.findViewById(R.id.lbl_marca_medicamento);
            droga = itemView.findViewById(R.id.lbl_droga_medicamento);
            laboratorio = itemView.findViewById(R.id.lbl_laboratorio_medicamento);
            presentacion = itemView.findViewById(R.id.lbl_presentacion_medicamento);
            fecha = itemView.findViewById(R.id.fechaMedicamento);
        }
    }
}
