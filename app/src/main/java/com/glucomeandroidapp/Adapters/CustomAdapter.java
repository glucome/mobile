package com.glucomeandroidapp.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.glucomeandroidapp.R;

import static com.glucomeandroidapp.Utils.Util.TIPO_DE_ADAPTER_CON_IMAGEN;

/**
 * Creado por MartinArtime el 25 de abril del 2019
 *
 * Adapter para popular spinners y otras estructuras de forma genérica
 */
public class CustomAdapter extends BaseAdapter {

    private int[] icons;
    private String[] data;
    private LayoutInflater inflater;
    private String tipoDeAdapter;

    public CustomAdapter(Context applicationContext, int[] icons, String[] data, String tipoDeAdapter) {
        this.tipoDeAdapter = tipoDeAdapter;
        if(tipoDeAdapter.equals(TIPO_DE_ADAPTER_CON_IMAGEN)){
            this.icons = icons;
        }
        this.data = data;
        inflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        if(tipoDeAdapter.equals(TIPO_DE_ADAPTER_CON_IMAGEN)){
            return icons.length;
        }
        return data.length;
    }

    @Override
    public Object getItem(int i) {
        return data[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(tipoDeAdapter.equals(TIPO_DE_ADAPTER_CON_IMAGEN)){
            view = inflater.inflate(R.layout.items_custom_spinner_con_imagen, null);
            ImageView icon = view.findViewById(R.id.imagenDeSpinnerItem);
            icon.setImageResource(icons[i]);
        } else {
            view = inflater.inflate(R.layout.items_custom_spinner_solo_texto, null);
        }
        TextView names = view.findViewById(R.id.textoDeSpinnerItem);
        names.setText(data[i]);
        return view;
    }
}

