package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.POJO.APIClasses.Response.UsuarioChatResponse;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.ImagenLoader;

import java.util.List;

import static com.glucomeandroidapp.Utils.Util.URL_BASE;
import static com.glucomeandroidapp.Utils.Util.URL_IMAGEN_USUARIO;

public class ChatListadoAdapter  extends RecyclerView.Adapter<ChatListadoAdapter.MyViewHolder> {
    private List<UsuarioChatResponse> chats;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView estado;
        public TextView nombreUsuario;
        public ImageView imagenUsuario;
        public TextView cantidadMensajesNoLeidos;

        public MyViewHolder(View view) {
            super(view);
            cantidadMensajesNoLeidos = view.findViewById(R.id.lbl_cantidad_mensajes_no_leidos);
            estado = view.findViewById(R.id.lbl_estado);
            nombreUsuario =  view.findViewById(R.id.lbl_nombre_usuario);
            imagenUsuario = view.findViewById(R.id.img_usuario);
        }
    }

    public ChatListadoAdapter(List<UsuarioChatResponse> chats) {
        this.chats = chats;
    }

    @NonNull
    @Override
    public ChatListadoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_chat, viewGroup, false);

        return new ChatListadoAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatListadoAdapter.MyViewHolder myViewHolder, int i) {
        UsuarioChatResponse chat = chats.get(i);

        myViewHolder.estado.setText(chat.getEstado());
        myViewHolder.nombreUsuario.setText(chat.getNombre());
        myViewHolder.cantidadMensajesNoLeidos.setText("Mensajes no leídos: " + String.valueOf(chat.getCantidadMensajesNoLeidos()));
        String url = URL_BASE + URL_IMAGEN_USUARIO + chat.getIdUsuario()+"/";
        new ImagenLoader(myViewHolder.imagenUsuario).execute(url);

    }

    @Override
    public int getItemCount() {
        return  chats != null ? chats.size() : 0;
    }
}
