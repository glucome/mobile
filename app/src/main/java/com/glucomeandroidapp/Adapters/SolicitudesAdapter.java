package com.glucomeandroidapp.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Controladores.SolicitudesControlador;
import com.glucomeandroidapp.POJO.SolicitudVinculacionAllegadoPaciente;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.ImagenLoader;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.List;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;
import static com.glucomeandroidapp.Utils.Util.URL_BASE;
import static com.glucomeandroidapp.Utils.Util.URL_IMAGEN_USUARIO;

public class SolicitudesAdapter extends RecyclerView.Adapter<SolicitudesAdapter.MyViewHolder> {

    private List<SolicitudVinculacionAllegadoPaciente> solicitudes;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nombrePaciente;
        public ImageView imagenPaciente;
        public TextView btnAceptar;
        public TextView btnRechazar;

        public MyViewHolder(View view) {
            super(view);
            nombrePaciente = view.findViewById(R.id.lbl_nombre_paciente);
            imagenPaciente = view.findViewById(R.id.img_paciente);
            btnAceptar = view.findViewById(R.id.btn_aceptar);
            btnRechazar = view.findViewById(R.id.btn_rechazar);
        }
    }

    public SolicitudesAdapter(List<SolicitudVinculacionAllegadoPaciente> solicitudes, Activity activity) {
        this.solicitudes = solicitudes;
        this.activity = activity;
    }

    @NonNull
    @Override
    public SolicitudesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_solicitudes, viewGroup, false);

        return new SolicitudesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SolicitudesAdapter.MyViewHolder myViewHolder, int i) {
        SolicitudVinculacionAllegadoPaciente solicitud = solicitudes.get(i);
        myViewHolder.nombrePaciente.setText(solicitud.getPaciente().getNombre());
        String url = URL_BASE+URL_IMAGEN_USUARIO+ solicitud.getPaciente().getIdUsuario()+"/";
        new ImagenLoader(myViewHolder.imagenPaciente).execute(url);

        myViewHolder.btnRechazar.setOnClickListener(v -> {
            LoadingDialogManager.mostrar(activity);
            SolicitudesControlador.rechazarSolicitud(solicitud);
            activity.finish();
        });

        myViewHolder.btnAceptar.setOnClickListener(v -> {
            LoadingDialogManager.mostrar(activity);
            SolicitudesControlador.aceptarSolicitud(solicitud);
            activity.finish();
        });

    }

    @Override
    public int getItemCount() {
        return  solicitudes != null ? solicitudes.size() : 0;
    }
}