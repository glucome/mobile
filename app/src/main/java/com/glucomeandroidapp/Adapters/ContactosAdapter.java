package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.POJO.Contacto;
import com.glucomeandroidapp.R;

import java.util.List;

public class ContactosAdapter extends RecyclerView.Adapter<ContactosAdapter.MyViewHolder> {
    private List<Contacto> contactos;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nombre;
        public TextView telefono;

        public MyViewHolder(View view) {
            super(view);
            nombre =  view.findViewById(R.id.lbl_nombre);
            telefono =  view.findViewById(R.id.lbl_telefono);
        }
    }

    public ContactosAdapter(List<Contacto> contactos) {
        this.contactos = contactos;
    }

    @NonNull
    @Override
    public ContactosAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_contactos, viewGroup, false);

        return new ContactosAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactosAdapter.MyViewHolder myViewHolder, int i) {
        Contacto contacto = contactos.get(i);

        myViewHolder.nombre.setText(contacto.getNombre());
        myViewHolder.telefono.setText(contacto.getTelefono());

    }

    @Override
    public int getItemCount() {
        return  contactos != null ? contactos.size() : 0;
    }
}
