package com.glucomeandroidapp.Adapters.Models;

public class HistorialAlimentoIngresadoAdapterModel {


    private long idAlimento;
    private String fecha;
    private String nombre;
    private String cantidad;
    private String momentoMedicion;

    public HistorialAlimentoIngresadoAdapterModel() {}

    public HistorialAlimentoIngresadoAdapterModel(long idAlimento, String fecha, String nombre, String cantidad, String momentoMedicion) {
        this.idAlimento = idAlimento;
        this.fecha = fecha;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.momentoMedicion = momentoMedicion;
    }

    public long getIdAlimento() {
        return idAlimento;
    }

    public void setIdAlimento(long idAlimento) {
        this.idAlimento = idAlimento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getMomentoMedicion() {
        return momentoMedicion;
    }

    public void setMomentoMedicion(String momentoMedicion) {
        this.momentoMedicion = momentoMedicion;
    }


}
