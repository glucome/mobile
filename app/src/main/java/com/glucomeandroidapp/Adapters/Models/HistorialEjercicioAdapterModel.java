package com.glucomeandroidapp.Adapters.Models;

import com.glucomeandroidapp.POJO.Enumeraciones.TipoEjercicio;

public class HistorialEjercicioAdapterModel {


    private String fecha;
    private TipoEjercicio tipo;
    private String cantidad;

    public HistorialEjercicioAdapterModel() {
    }

    public HistorialEjercicioAdapterModel(String fecha, TipoEjercicio tipo, String cantidad) {
        this.fecha = fecha;
        this.tipo = tipo;
        this.cantidad = cantidad;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public TipoEjercicio getTipo() {
        return tipo;
    }

    public void setTipo(TipoEjercicio tipo) {
        this.tipo = tipo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }


}
