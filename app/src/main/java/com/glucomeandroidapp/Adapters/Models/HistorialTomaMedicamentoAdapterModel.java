package com.glucomeandroidapp.Adapters.Models;


import com.glucomeandroidapp.POJO.Medicacion.Medicamento;

public class HistorialTomaMedicamentoAdapterModel {

    private String cantidad;
    private String fecha;
    private Medicamento medicamento;

    public HistorialTomaMedicamentoAdapterModel() {
    }

    public HistorialTomaMedicamentoAdapterModel(String cantidad, String fecha, Medicamento medicamento) {
        this.cantidad = cantidad;
        this.fecha = fecha;
        this.medicamento = medicamento;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Medicamento getMedicamento() {
        return medicamento;
    }

    public void setMedicamento(Medicamento medicamento) {
        this.medicamento = medicamento;
    }
}
