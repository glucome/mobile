package com.glucomeandroidapp.Adapters.Models;

public class HistorialInyeccionAdapterModel {
    private String valor;
    private String tipo;
    private String fecha;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public HistorialInyeccionAdapterModel(Double valor, String tipo, String fecha) {
        this.valor = String.valueOf((int) Math.ceil(valor));
        this.tipo = tipo;
        this.fecha = fecha;
    }
}
