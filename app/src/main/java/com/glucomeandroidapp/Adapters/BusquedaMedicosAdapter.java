package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.ImagenLoader;

import java.util.List;

import static com.glucomeandroidapp.Utils.Util.URL_BASE;
import static com.glucomeandroidapp.Utils.Util.URL_IMAGEN_USUARIO;

public class BusquedaMedicosAdapter extends RecyclerView.Adapter<BusquedaMedicosAdapter.MyViewHolder> {

    private List<MedicoDataResponse> medicos;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nombreMedico;
        TextView matriculaMedico;
        ImageView imagenMedico;

        MyViewHolder(View view) {
            super(view);
            nombreMedico = view.findViewById(R.id.lbl_nombre_medico);
            matriculaMedico =  view.findViewById(R.id.lbl_matricula_medico);
            imagenMedico = view.findViewById(R.id.img_medico);
        }
    }

    public BusquedaMedicosAdapter(List<MedicoDataResponse> medicos) {
        this.medicos = medicos;
    }

    @NonNull
    @Override
    public BusquedaMedicosAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_busqueda_medico, viewGroup, false);

        return new BusquedaMedicosAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BusquedaMedicosAdapter.MyViewHolder myViewHolder, int i) {
        MedicoDataResponse medico = medicos.get(i);
        myViewHolder.nombreMedico.setText(medico.getNombre());
        myViewHolder.matriculaMedico.setText(String.valueOf(medico.getMatricula()));
        String url = URL_BASE + URL_IMAGEN_USUARIO + medico.getIdUsuario()+"/";
        new ImagenLoader(myViewHolder.imagenMedico).execute(url);

    }

    @Override
    public int getItemCount() {
        return  medicos != null ? medicos.size() : 0;
    }
}