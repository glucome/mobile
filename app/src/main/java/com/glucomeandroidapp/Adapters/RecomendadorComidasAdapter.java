package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.POJO.Alimento;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.ImagenLoader;

import java.util.List;

import static com.glucomeandroidapp.Utils.Util.URL_BASE;
import static com.glucomeandroidapp.Utils.Util.URL_IMAGEN_COMIDA;

public class RecomendadorComidasAdapter extends RecyclerView.Adapter<RecomendadorComidasAdapter.ViewHolder> {

    private List<Alimento> alimentos;

    public RecomendadorComidasAdapter(List<Alimento> alimentos) {
        this.alimentos = alimentos;
    }

    @NonNull
    @Override
    public RecomendadorComidasAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_recomendador_comida, viewGroup, false);

        return new RecomendadorComidasAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecomendadorComidasAdapter.ViewHolder myViewHolder, int i) {
        Alimento alimento = alimentos.get(i);
        String nombre = alimento.getNombre();
        myViewHolder.nombreAlimento.setText(nombre.length() > 25 ? nombre.substring(0,25)+ "..." : nombre);

        String url = URL_BASE + URL_IMAGEN_COMIDA + alimento.getId() + "/";

        new ImagenLoader(myViewHolder.imagenComida).execute(url);
    }



    @Override
    public int getItemCount() {
        return alimentos.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView nombreAlimento, indiceGlucemico;
        ImageView imagenComida;

        ViewHolder(View view) {
            super(view);
            nombreAlimento = view.findViewById(R.id.lbl_nombre_alimento);
            imagenComida = view.findViewById(R.id.imagen_comida);
        }
    }
}
