package com.glucomeandroidapp.Adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.Rol;
import com.glucomeandroidapp.POJO.Enumeraciones.Sexo;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoDiabetes;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;

import static com.glucomeandroidapp.Utils.Util.PERFIL_ALTURA;
import static com.glucomeandroidapp.Utils.Util.PERFIL_FECHA_DE_NACIMIENTO;
import static com.glucomeandroidapp.Utils.Util.PERFIL_MATRICULA;
import static com.glucomeandroidapp.Utils.Util.PERFIL_NOMBRE_DE_USUARIO;
import static com.glucomeandroidapp.Utils.Util.PERFIL_PESO;
import static com.glucomeandroidapp.Utils.Util.PERFIL_SEXO;
import static com.glucomeandroidapp.Utils.Util.PERFIL_TIPO_DE_DIABETES;
import static com.glucomeandroidapp.Utils.Util.TAG;

/**
 * Creado por MartinArtime el 28 de abril del 2019
 */
public class PerfilAdapter extends RecyclerView.Adapter<PerfilAdapter.ViewHolder> {

    private LoginResponse loginResponse;
    private String[] titulos;
    private PacienteDataResponse paciente;
    private MedicoDataResponse medico;
    private AllegadoDataResponse allegado;

    public PerfilAdapter(String[] titulos, LoginResponse loginResponse) {
        this.loginResponse = loginResponse;
        this.titulos = titulos;

        paciente = null;
        // Acá se asigna el paciente al tipo de usuario correspondiente para luego cargar los datos
        if(loginResponse.getRoles().get(0).equals(Rol.PACIENTE.getNombre()) || loginResponse.getRoles().get(0).equals(Rol.PACIENTE_PREMIUM.getNombre())) {
            paciente = AccountDataManager.obtenerDatosPaciente();
        } else if(loginResponse.getRoles().get(0).equals(Rol.MEDICO.getNombre()) || loginResponse.getRoles().get(0).equals(Rol.MEDICO_PREMIUM.getNombre())){
            medico = AccountDataManager.obtenerDatosMedico();
        } else {
            allegado = AccountDataManager.obtenerDatosAllegado();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_perfil, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String titulo = titulos[position];
        String valor = null;
        int drawableImagen = R.drawable.ic_perfil;

        // Acá se cargan los datos que van llegando acorde al usuario en cuestión
            if(paciente != null){
            switch(titulo){
                case PERFIL_NOMBRE_DE_USUARIO:
                    drawableImagen = R.drawable.ic_nombre;
                    valor = String.valueOf(paciente.getNombre());
                    break;
                case PERFIL_ALTURA:
                    drawableImagen = R.drawable.ic_medicion;
                    valor = String.valueOf(paciente.getAltura());
                    break;
                case PERFIL_PESO:
                    drawableImagen = R.drawable.ic_peso;
                    valor = String.valueOf(paciente.getPeso());
                    break;
                case PERFIL_SEXO:
                    drawableImagen = R.drawable.ic_sexo;
                    valor = Sexo.getById(Integer.valueOf(paciente.getSexo())).getNombre();
                    break;
                case PERFIL_FECHA_DE_NACIMIENTO:
                    drawableImagen = R.drawable.ic_calendario;
                    valor = paciente.getFechaNacimiento();
                    break;
                case PERFIL_TIPO_DE_DIABETES:
                    TipoDiabetes tipoDiabetes = TipoDiabetes.getById(Integer.valueOf(paciente.getTipoDiabetes()));
                    valor = tipoDiabetes.getNombre();
                    if(tipoDiabetes.equals(TipoDiabetes.TIPO_1))
                        drawableImagen = R.drawable.ic_tipo_1;
                    else if(tipoDiabetes.equals(TipoDiabetes.TIPO_2))
                        drawableImagen = R.drawable.ic_tipo_2;
                    break;
                default:
                    Log.e(TAG, "Titulo inexistente");
                    break;
            }
        } else if(medico != null) {
            switch (titulo) {
                case PERFIL_NOMBRE_DE_USUARIO:
                    drawableImagen = R.drawable.ic_nombre;
                    valor = String.valueOf(medico.getNombre());
                    break;
                case PERFIL_MATRICULA:
                    drawableImagen = R.drawable.ic_matricula;
                    valor = String.valueOf(medico.getMatricula());
                    break;
                default:
                    Log.e(TAG, "Titulo inexistente");
                    break;
            }
        } else {
            switch (titulo) {
                case PERFIL_NOMBRE_DE_USUARIO:
                    drawableImagen = R.drawable.ic_nombre;
                    valor = String.valueOf(allegado.getNombre());
                    break;
                default:
                    Log.e(TAG, "Titulo inexistente");
                    break;
            }
        }

        holder.imagen.setImageResource(drawableImagen);
        holder.tituloPerfil.setText(titulo);
        holder.valorTituloPerfil.setText(valor);
    }

    @Override
    public int getItemCount() {
        return titulos.length;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tituloPerfil, valorTituloPerfil;
        ImageView imagen;

        ViewHolder(View view) {
            super(view);
            tituloPerfil =  view.findViewById(R.id.txt_titulo_perfil);
            valorTituloPerfil = view.findViewById(R.id.txt_valor_titulo_perfil);
            imagen = view.findViewById(R.id.item_imagen);
        }
    }
}


