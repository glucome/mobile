package com.glucomeandroidapp.Adapters;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Calibracion.EditarItemCalibracionActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.Calibracion.ItemCalibracion;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.R;

import java.util.List;

import static com.glucomeandroidapp.Utils.Util.ITEM_CALIBRACION_ID;
import static com.glucomeandroidapp.Utils.Util.ITEM_CALIBRACION_NO_EDITABLE;

public class CalibracionDispositivoAdapter extends RecyclerView.Adapter<CalibracionDispositivoAdapter.ViewHolder>  {

    private List<ItemCalibracion> itemsCalibracionList;

    public CalibracionDispositivoAdapter(List<ItemCalibracion> itemsCalibracionList){
        this.itemsCalibracionList = itemsCalibracionList;
    }

    @NonNull
    @Override
    public CalibracionDispositivoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_calibracion_dispositivo, viewGroup, false);

        return new CalibracionDispositivoAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CalibracionDispositivoAdapter.ViewHolder myViewHolder, int i) {
        ItemCalibracion itemCalibracion = itemsCalibracionList.get(i);
        myViewHolder.momentoMedicion.setText(itemCalibracion.getMomentoMedicion() != MomentoMedicion.NA ? itemCalibracion.getMomentoMedicion().getNombre() : "Madrugada");
        if(itemCalibracion.getFechaDeMedicionCalibracion() != "" && itemCalibracion.getMedicionInvasiva() != -1){
//            myViewHolder.realizadaMedicion.setVisibility(View.VISIBLE);
            myViewHolder.imgFlechaDerecha.setVisibility(View.INVISIBLE);
            myViewHolder.valorMedicion.setVisibility(View.VISIBLE);
            myViewHolder.horarioMedicion.setText(itemCalibracion.getFechaDeMedicionCalibracion());
            myViewHolder.valorMedicion.setText(itemCalibracion.getMedicionInvasiva().toString());
            myViewHolder.horarioMedicion.setVisibility(View.VISIBLE);
            myViewHolder.imagenMedicion.setImageResource(R.drawable.ic_check);
            myViewHolder.layoutMedicion.setOnClickListener(v -> App.getInstance().mostrarMensajeFlotante(ITEM_CALIBRACION_NO_EDITABLE));
        } else {
//            myViewHolder.realizadaMedicion.setVisibility(View.VISIBLE);
            myViewHolder.imgFlechaDerecha.setVisibility(View.VISIBLE);
            myViewHolder.valorMedicion.setVisibility(View.INVISIBLE);
            myViewHolder.imagenMedicion.setImageResource(R.drawable.ic_horario);
            myViewHolder.layoutMedicion.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putLong(ITEM_CALIBRACION_ID, itemCalibracion.getId());
                App.getInstance().iniciarActividad(
                        EditarItemCalibracionActivity.class, bundle);
            });
        }
    }

    @Override
    public int getItemCount() {
        return itemsCalibracionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout layoutMedicion;
        TextView momentoMedicion, horarioMedicion, valorMedicion;
        ImageView imagenMedicion, imgFlechaDerecha;

        ViewHolder(View view) {
            super(view);
            layoutMedicion = view.findViewById(R.id.componente);
            horarioMedicion = view.findViewById(R.id.horario_medicion);
            momentoMedicion = view.findViewById(R.id.momento_medicion);
            valorMedicion = view.findViewById(R.id.valor_medicion);
            imagenMedicion = view.findViewById(R.id.realizada_medicion);
            imgFlechaDerecha = view.findViewById(R.id.imgrightarrow);
        }
    }
}
