package com.glucomeandroidapp.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.R;

import java.util.ArrayList;

/**
 * Creado por MartinArtime el 14 de mayo del 2019
 */
public abstract class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    protected Activity activity;
    protected ArrayList<MenuAdapterModel> menuAdapterModelArrayList;

    public MenuAdapter(Activity activity, ArrayList<MenuAdapterModel> menuAdapterModelArrayList) {
        this.activity = activity;
        this.menuAdapterModelArrayList = menuAdapterModelArrayList;
    }

    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MenuAdapter.ViewHolder holder, final int position) {
        holder.titulo.setText(menuAdapterModelArrayList.get(position).getTitulo());
        holder.icono.setImageResource(menuAdapterModelArrayList.get(position).getIcono());

        holder.linearLayout.setOnClickListener(view -> {
            ejecutarAccion(position);
        });
    }

    public abstract void ejecutarAccion(int position);

    @Override
    public int getItemCount() {
        return menuAdapterModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView titulo;
        ImageView icono;
        LinearLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            titulo = itemView.findViewById(R.id.titulo_item_menu);
            icono = itemView.findViewById(R.id.icono_item_menu);
            linearLayout = itemView.findViewById(R.id.componente);
        }
    }

}
