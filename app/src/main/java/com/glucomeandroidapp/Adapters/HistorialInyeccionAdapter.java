package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Adapters.Models.HistorialInyeccionAdapterModel;
import com.glucomeandroidapp.Adapters.Models.HistorialTomaMedicamentoAdapterModel;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoInsulina;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

public class HistorialInyeccionAdapter extends RecyclerView.Adapter<HistorialInyeccionAdapter.ViewHolder> {

    private ArrayList<HistorialInyeccionAdapterModel> historial_inyeccion_modelArrayList;
    private ArrayList<HistorialInyeccionAdapterModel> historial_inyeccion_modelArrayList_filtrado;

    public HistorialInyeccionAdapter(ArrayList<HistorialInyeccionAdapterModel> historial_inyeccion_modelArrayList) {
        this.historial_inyeccion_modelArrayList = historial_inyeccion_modelArrayList;
        this.historial_inyeccion_modelArrayList_filtrado = historial_inyeccion_modelArrayList;
    }

    @Override
    public HistorialInyeccionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_historial_inyeccion,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistorialInyeccionAdapter.ViewHolder holder, final int position) {
        holder.valor.setText(historial_inyeccion_modelArrayList.get(position).getValor());
        holder.tipo.setText(historial_inyeccion_modelArrayList.get(position).getTipo());
        holder.fecha.setText(historial_inyeccion_modelArrayList.get(position).getFecha());
    }


    public void filter(int cantidadInferior, int cantidadSuperior, int tipoInsulina, String fechaDesde, String fechaHasta){

        if (cantidadInferior==-1 && cantidadSuperior==-1 && tipoInsulina==-1 && fechaDesde==null && fechaHasta==null) {
            historial_inyeccion_modelArrayList_filtrado = historial_inyeccion_modelArrayList;
        } else {
            boolean compararCantidad = false;
            boolean compararTipo = false;
            boolean compararFecha = false;

            LocalDateTime fechaDesdeComparable = null;
            LocalDateTime fechaHastaComparable = null;

            if(fechaDesde!=null && fechaHasta!=null){
                compararFecha = true;
                fechaDesdeComparable = UtilesFecha.getLocalDateTimeFromString(fechaDesde);
                fechaHastaComparable = UtilesFecha.getLocalDateTimeFromString(fechaHasta);
            }

            if(cantidadInferior!=-1 && cantidadSuperior!=-1){
                compararCantidad = true;
            }

            if(tipoInsulina!=-1){
                compararTipo = true;
            }

            ArrayList<HistorialInyeccionAdapterModel> filteredList = new ArrayList<>();
            for (HistorialInyeccionAdapterModel inyeccion : historial_inyeccion_modelArrayList) {

                // Evalúo la fecha
                if(compararFecha){
                    LocalDateTime fechaInyeccion = UtilesFecha.getLocalDateTimeFromString(inyeccion.getFecha());
                    if(fechaInyeccion!=null && fechaDesdeComparable!=null && fechaHastaComparable!=null){
                        if (fechaDesdeComparable.isAfter(fechaInyeccion) || fechaHastaComparable.isBefore(fechaInyeccion)) {
                            continue;
                        }
                    }
                }

                // Evalúo la cantidad
                if(compararCantidad){
                    Integer valor = Integer.valueOf(inyeccion.getValor());
                    if(cantidadInferior > valor || cantidadSuperior < valor){
                        continue;
                    }
                }

                // Evalúo el tipo de inyección
                if(compararTipo){
                    if(!inyeccion.getTipo().equals(TipoInsulina.getById(tipoInsulina).getNombre())){
                        continue;
                    }
                }

                filteredList.add(inyeccion);
            }

            historial_inyeccion_modelArrayList_filtrado = filteredList;
        }

        Collections.sort(historial_inyeccion_modelArrayList_filtrado, (x, y)-> Math.negateExact(UtilesFecha.getLocalDateTimeFromString(x.getFecha()).compareTo(UtilesFecha.getLocalDateTimeFromString(y.getFecha()))));


        historial_inyeccion_modelArrayList = historial_inyeccion_modelArrayList_filtrado;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return historial_inyeccion_modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView valor, tipo, fecha;
        LinearLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);

            valor=itemView.findViewById(R.id.valorMedicion);
            tipo=itemView.findViewById(R.id.tipoMedicion);
            fecha=itemView.findViewById(R.id.fechaMedicion);
            linearLayout=itemView.findViewById(R.id.componente);
        }
    }
}
