package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.POJO.Chat.MensajeDeChat;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private static final int LEFT = 1;
    private static final int RIGTH = 2;
    private List<MensajeDeChat> chats;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView msg;

        public MyViewHolder(View view) {
            super(view);
            msg = view.findViewById(R.id.txt_msg);
        }
    }

    // determine which layout to use for the row
    @Override
    public int getItemViewType(int position) {
        MensajeDeChat item = chats.get(position);
        long idUsuario = AccountDataManager.obtenerIdUsuario();
        if (item.getFromId() == idUsuario) {
            return RIGTH;
        } else  {
            return LEFT;
        }
    }

    public ChatAdapter(List<MensajeDeChat> chats) {
        this.chats = chats;
    }

    @NonNull
    @Override
    public ChatAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int tipo) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate( tipo == LEFT ? R.layout.item_chat_izquierda :  R.layout.item_chat_derecha , viewGroup, false);

        return new ChatAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatAdapter.MyViewHolder myViewHolder, int i) {
        MensajeDeChat chat = chats.get(i);
        myViewHolder.msg.setText(chat.getMessage());
    }

    @Override
    public int getItemCount() {
        return  chats != null ? chats.size() : 0;
    }
}
