package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoDiabetes;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.ImagenLoader;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.util.ArrayList;
import java.util.List;

import static com.glucomeandroidapp.Utils.Util.URL_BASE;
import static com.glucomeandroidapp.Utils.Util.URL_IMAGEN_USUARIO;

public class BusquedaPacientesAdapter extends RecyclerView.Adapter<BusquedaPacientesAdapter.MyViewHolder> {

    private List<PacienteDataResponse> pacientes;
    private List<PacienteDataResponse> pacientesFiltrado;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nombrePaciente, tipoDiabetesPaciente, edadPaciente;
        ImageView imagenPaciente;

        MyViewHolder(View view) {
            super(view);
            nombrePaciente = view.findViewById(R.id.lbl_nombre_paciente);
            tipoDiabetesPaciente = view.findViewById(R.id.lbl_tipo_diabetes_paciente);
            imagenPaciente = view.findViewById(R.id.img_paciente);
            edadPaciente = view.findViewById(R.id.lbl_edad_paciente);
        }
    }

    public BusquedaPacientesAdapter(List<PacienteDataResponse> pacientes) {
        this.pacientes = pacientes;
        this.pacientesFiltrado = pacientes;
    }

    public void filter(String nombre, int tipoDeDiabetes, int edadDesde, int edadHasta){

        if (nombre==null && tipoDeDiabetes==-1 && edadDesde==-1 && edadHasta==-1) {
            pacientesFiltrado = pacientes;
        } else {
            boolean compararNombre = false;
            boolean compararTipoDiabetes = false;
            boolean compararEdad = false;

            if(nombre!=null){
                compararNombre = true;
            }

            if(tipoDeDiabetes!=-1){
                compararTipoDiabetes = true;
            }

            if(edadDesde!=-1 && edadHasta!=-1){
                compararEdad = true;
            }

            ArrayList<PacienteDataResponse> filteredList = new ArrayList<>();
            for (PacienteDataResponse paciente : pacientes) {

                // Evalúo el nombre del paciente
                if(compararNombre){
                    if(!paciente.getNombre().contains(nombre)){
                        continue;
                    }
                }

                // Evalúo el tipo de diabetes
                if(compararTipoDiabetes){
                    int tipoDiabetesPaciente = Integer.valueOf(paciente.getTipoDiabetes());
                    if(tipoDiabetesPaciente != tipoDeDiabetes){
                        continue;
                    }
                }

                // Evalúo la edad
                if(compararEdad){
                    Integer edadPaciente = Integer.valueOf(UtilesFecha.obtenerEdad(paciente.getFechaNacimiento()));
                    if(edadPaciente < edadDesde || edadPaciente > edadHasta){
                        continue;
                    }
                }

                filteredList.add(paciente);
            }

            pacientesFiltrado = filteredList;
        }

        pacientes = pacientesFiltrado;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BusquedaPacientesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_busqueda_paciente, viewGroup, false);

        return new BusquedaPacientesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BusquedaPacientesAdapter.MyViewHolder myViewHolder, int i) {
        PacienteDataResponse paciente = pacientes.get(i);
        myViewHolder.nombrePaciente.setText(paciente.getNombre());
        myViewHolder.tipoDiabetesPaciente.setText(TipoDiabetes.getById(Integer.valueOf(paciente.getTipoDiabetes())).getNombre());
        myViewHolder.edadPaciente.setText(UtilesFecha.obtenerEdad(paciente.getFechaNacimiento()));
        String url = URL_BASE + URL_IMAGEN_USUARIO + paciente.getIdUsuario() + "/";
        new ImagenLoader(myViewHolder.imagenPaciente).execute(url);

    }

    @Override
    public int getItemCount() {
        return pacientes != null ? pacientes.size() : 0;
    }
}