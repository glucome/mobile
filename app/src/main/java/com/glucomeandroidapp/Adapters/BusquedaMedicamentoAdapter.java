package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.POJO.Medicacion.Medicamento;
import com.glucomeandroidapp.R;

import java.util.List;

public class BusquedaMedicamentoAdapter extends RecyclerView.Adapter<BusquedaMedicamentoAdapter.MyViewHolder> {

    private List<Medicamento> medicamentos;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView drogaMedicamento;
        TextView laboratorioMedicamento;
        TextView marcaMedicamento;
        TextView presentacionMedicamento;

        MyViewHolder(View view) {
            super(view);
            drogaMedicamento = view.findViewById(R.id.lbl_droga_medicamento);
            laboratorioMedicamento = view.findViewById(R.id.lbl_laboratorio_medicamento);
            marcaMedicamento = view.findViewById(R.id.lbl_marca_medicamento);
            presentacionMedicamento = view.findViewById(R.id.lbl_presentacion_medicamento);
        }
    }

    public BusquedaMedicamentoAdapter(List<Medicamento> medicamentos) {
        this.medicamentos = medicamentos;
    }

    @NonNull
    @Override
    public BusquedaMedicamentoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_busqueda_medicamento, viewGroup, false);

        return new BusquedaMedicamentoAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BusquedaMedicamentoAdapter.MyViewHolder myViewHolder, int i) {
        Medicamento medicamento= medicamentos.get(i);
        myViewHolder.drogaMedicamento.setText(medicamento.getDroga());
        myViewHolder.laboratorioMedicamento.setText(String.valueOf(medicamento.getLaboratorio()));
        myViewHolder.marcaMedicamento.setText(medicamento.getMarca());
        myViewHolder.presentacionMedicamento.setText(String.valueOf(medicamento.getPresentacion()));
    }

    @Override
    public int getItemCount() {
        return medicamentos != null ? medicamentos.size() : 0;
    }
}
