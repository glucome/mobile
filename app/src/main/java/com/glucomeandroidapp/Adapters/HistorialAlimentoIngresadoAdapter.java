package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Adapters.Models.HistorialAlimentoIngresadoAdapterModel;
import com.glucomeandroidapp.Adapters.Models.HistorialInyeccionAdapterModel;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoAlimento;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoInsulina;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.ImagenLoader;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

import static com.glucomeandroidapp.Utils.Util.URL_BASE;
import static com.glucomeandroidapp.Utils.Util.URL_IMAGEN_COMIDA;

public class HistorialAlimentoIngresadoAdapter extends RecyclerView.Adapter<HistorialAlimentoIngresadoAdapter.ViewHolder> {

    private ArrayList<HistorialAlimentoIngresadoAdapterModel> historialAlimentoIngresadoAdapterModelArrayList;
    private ArrayList<HistorialAlimentoIngresadoAdapterModel> historialAlimentoIngresadoAdapterModelArrayList_filtrado;

    public HistorialAlimentoIngresadoAdapter(ArrayList<HistorialAlimentoIngresadoAdapterModel> historialAlimentoIngresadoAdapterModelArrayList) {
        this.historialAlimentoIngresadoAdapterModelArrayList = historialAlimentoIngresadoAdapterModelArrayList;
        this.historialAlimentoIngresadoAdapterModelArrayList_filtrado = historialAlimentoIngresadoAdapterModelArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewHolder, int i) {
        View view = LayoutInflater.from(viewHolder.getContext()).inflate(R.layout.item_historial_alimento_ingerido, viewHolder, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.cantidad.setText(historialAlimentoIngresadoAdapterModelArrayList.get(i).getCantidad());
        viewHolder.nombre.setText(historialAlimentoIngresadoAdapterModelArrayList.get(i).getNombre());
        viewHolder.fecha.setText(historialAlimentoIngresadoAdapterModelArrayList.get(i).getFecha());
        String url = URL_BASE + URL_IMAGEN_COMIDA + historialAlimentoIngresadoAdapterModelArrayList.get(i).getIdAlimento() + "/";
        new ImagenLoader(viewHolder.imagen).execute(url);
    }

    public void filter(int momentoAlimento, String nombre, String fechaDesde, String fechaHasta){

        if (momentoAlimento==-1 && nombre==null && fechaDesde==null && fechaHasta==null) {
            historialAlimentoIngresadoAdapterModelArrayList_filtrado = historialAlimentoIngresadoAdapterModelArrayList;
        } else {
            boolean compararNombre = false;
            boolean compararMomento = false;
            boolean compararFecha = false;

            LocalDateTime fechaDesdeComparable = null;
            LocalDateTime fechaHastaComparable = null;

            if(fechaDesde!=null && fechaHasta!=null){
                compararFecha = true;
                fechaDesdeComparable = UtilesFecha.getLocalDateTimeFromString(fechaDesde);
                fechaHastaComparable = UtilesFecha.getLocalDateTimeFromString(fechaHasta);
            }

            if(nombre!=null){
                nombre = nombre.toLowerCase();
                compararNombre = true;
            }

            if(momentoAlimento!=-1){
                compararMomento = true;
            }

            ArrayList<HistorialAlimentoIngresadoAdapterModel> filteredList = new ArrayList<>();
            for (HistorialAlimentoIngresadoAdapterModel alimentoIngresado : historialAlimentoIngresadoAdapterModelArrayList) {

                // Evalúo la fecha
                if(compararFecha){
                    LocalDateTime fechaInyeccion = UtilesFecha.getLocalDateTimeFromString(alimentoIngresado.getFecha());
                    if(fechaInyeccion!=null && fechaDesdeComparable!=null && fechaHastaComparable!=null){
                        if (fechaDesdeComparable.isAfter(fechaInyeccion) || fechaHastaComparable.isBefore(fechaInyeccion)) {
                            continue;
                        }
                    }
                }

                // Evalúo el nombre del alimento
                if(compararNombre){
                    String nombreAlimento = alimentoIngresado.getNombre();
                    if(!nombreAlimento.toLowerCase().contains(nombre)){
                        continue;
                    }
                }

                // Evalúo el momento de ingesta
                if(compararMomento){
                    if(!alimentoIngresado.getMomentoMedicion().equals(MomentoAlimento.getById(momentoAlimento).getNombre())){
                        continue;
                    }
                }

                filteredList.add(alimentoIngresado);
            }

            historialAlimentoIngresadoAdapterModelArrayList_filtrado = filteredList;
        }

        Collections.sort(historialAlimentoIngresadoAdapterModelArrayList_filtrado, (x, y)-> Math.negateExact(UtilesFecha.getLocalDateTimeFromString(x.getFecha()).compareTo(UtilesFecha.getLocalDateTimeFromString(y.getFecha()))));


        historialAlimentoIngresadoAdapterModelArrayList = historialAlimentoIngresadoAdapterModelArrayList_filtrado;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return historialAlimentoIngresadoAdapterModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView cantidad, nombre, fecha;
        ImageView imagen;
        RelativeLayout layout;

        ViewHolder(View itemView) {
            super(itemView);

            nombre = itemView.findViewById(R.id.lbl_nombre_alimento);
            cantidad = itemView.findViewById(R.id.lbl_cantidad_alimento);
            fecha = itemView.findViewById(R.id.fecha);
            imagen = itemView.findViewById(R.id.imagen_comida);
            layout = itemView.findViewById(R.id.componente);
        }
    }
}
