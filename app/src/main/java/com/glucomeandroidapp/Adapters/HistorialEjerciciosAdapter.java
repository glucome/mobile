package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Adapters.Models.HistorialEjercicioAdapterModel;
import com.glucomeandroidapp.Adapters.Models.HistorialMedicionAdapterModel;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoEjercicio;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

public class HistorialEjerciciosAdapter extends RecyclerView.Adapter<HistorialEjerciciosAdapter.ViewHolder>  {

    private ArrayList<HistorialEjercicioAdapterModel> historialEjercicioAdapterModelArrayList;
    private ArrayList<HistorialEjercicioAdapterModel> historialEjercicioAdapterModelArrayList_filtrado;

    public HistorialEjerciciosAdapter(ArrayList<HistorialEjercicioAdapterModel> historialEjercicioAdapterModelArrayList) {
        this.historialEjercicioAdapterModelArrayList = historialEjercicioAdapterModelArrayList;
        this.historialEjercicioAdapterModelArrayList_filtrado = historialEjercicioAdapterModelArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewHolder, int i) {
        View view = LayoutInflater.from(viewHolder.getContext()).inflate(R.layout.item_historial_ejercicio, viewHolder, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        HistorialEjercicioAdapterModel ejercicio = historialEjercicioAdapterModelArrayList.get(i);
        viewHolder.cantidad.setText(ejercicio.getCantidad());
        viewHolder.tipo.setText(ejercicio.getTipo().getNombre());
        viewHolder.fecha.setText(ejercicio.getFecha());
        if(ejercicio.getTipo() == TipoEjercicio.DEPORTE){
            viewHolder.imagen.setImageResource(R.drawable.ic_ejercicio_deporte);
        }
    }

    @Override
    public int getItemCount() {
        return historialEjercicioAdapterModelArrayList.size();
    }

    public void filter(String fechaDesde, String fechaHasta, int cantidadSuperior,
                       int cantidadInferior, int tipoDeEjercicio){

        if (fechaDesde==null && fechaHasta==null && cantidadInferior==-1 &&
                cantidadSuperior==-1 && tipoDeEjercicio==-1) {
            historialEjercicioAdapterModelArrayList_filtrado = historialEjercicioAdapterModelArrayList;
        } else {
            boolean compararTipo = false;
            boolean compararCantidad = false;
            boolean compararFecha = false;

            LocalDateTime fechaDesdeComparable = null;
            LocalDateTime fechaHastaComparable = null;

            if(fechaDesde!=null && fechaHasta!=null){
                compararFecha = true;
                fechaDesdeComparable = UtilesFecha.getLocalDateTimeFromString(fechaDesde);
                fechaHastaComparable = UtilesFecha.getLocalDateTimeFromString(fechaHasta);
            }

            if(tipoDeEjercicio!=-1){
                compararTipo = true;
            }

            if(cantidadInferior!=-1 && cantidadSuperior!=-1){
                compararCantidad = true;
            }

            ArrayList<HistorialEjercicioAdapterModel> filteredList = new ArrayList<>();
            for (HistorialEjercicioAdapterModel ejercicio : historialEjercicioAdapterModelArrayList) {

                // Evalúo la fecha
                if(compararFecha){
                    LocalDateTime fechaMedicion = UtilesFecha.getLocalDateTimeFromString(ejercicio.getFecha());
                    if(fechaMedicion!=null && fechaDesdeComparable!=null && fechaHastaComparable!=null){
                        if (fechaDesdeComparable.isAfter(fechaMedicion) || fechaHastaComparable.isBefore(fechaMedicion)) {
                            continue;
                        }
                    }
                }

                // Evalúo el rango de cantidades
                if(compararCantidad){
                    Integer valor = Integer.valueOf(ejercicio.getCantidad());
                    if(valor < cantidadInferior || valor > cantidadSuperior){
                        continue;
                    }
                }

                // Evalúo el tipo de ejercicio
                if(compararTipo){
                    if(!ejercicio.getTipo().getNombre().equals(TipoEjercicio.getById(tipoDeEjercicio).getNombre())){
                        continue;
                    }
                }

                filteredList.add(ejercicio);
            }

            historialEjercicioAdapterModelArrayList_filtrado = filteredList;
        }

        Collections.sort(historialEjercicioAdapterModelArrayList_filtrado, (x, y)-> Math.negateExact(UtilesFecha.getLocalDateTimeFromString(x.getFecha()).compareTo(UtilesFecha.getLocalDateTimeFromString(y.getFecha()))));


        historialEjercicioAdapterModelArrayList = historialEjercicioAdapterModelArrayList_filtrado;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView cantidad, tipo, fecha;
        ImageView imagen;
        RelativeLayout layout;

        ViewHolder(View itemView) {
            super(itemView);

            tipo = itemView.findViewById(R.id.lbl_tipo_ejercicio);
            cantidad = itemView.findViewById(R.id.lbl_cantidad_minutos);
            fecha = itemView.findViewById(R.id.fecha);
            imagen = itemView.findViewById(R.id.imagen_ejercicio);
            layout = itemView.findViewById(R.id.componente);
        }
    }
}
