package com.glucomeandroidapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Adapters.Models.HistorialMedicionAdapterModel;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

public class HistorialMedicionAdapter extends RecyclerView.Adapter<HistorialMedicionAdapter.ViewHolder> {

    private ArrayList<HistorialMedicionAdapterModel> historial_medicion_modelArrayList;
    private ArrayList<HistorialMedicionAdapterModel> historial_medicion_modelArrayList_filtrado;

    public HistorialMedicionAdapter(ArrayList<HistorialMedicionAdapterModel> historial_medicion_modelArrayList) {
        this.historial_medicion_modelArrayList = historial_medicion_modelArrayList;
        this.historial_medicion_modelArrayList_filtrado = historial_medicion_modelArrayList;
    }

    public HistorialMedicionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_historial_medicion, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistorialMedicionAdapter.ViewHolder holder, final int position) {
        holder.valor.setText(historial_medicion_modelArrayList.get(position).getValor());
        holder.tipo.setText(historial_medicion_modelArrayList.get(position).getTipo());
        holder.fecha.setText(historial_medicion_modelArrayList.get(position).getFecha());
    }

    @Override
    public int getItemCount() {
        return historial_medicion_modelArrayList.size();
    }

    public void filter(String fechaDesde, String fechaHasta, int valorDeMedicionSuperior,
                       int valorDeMedicionInferior, int momentoMedicion){

        if (fechaDesde==null && fechaHasta==null && valorDeMedicionInferior==-1 &&
                valorDeMedicionSuperior==-1 && momentoMedicion==-1) {
            historial_medicion_modelArrayList_filtrado = historial_medicion_modelArrayList;
        } else {
            boolean compararMomento = false;
            boolean compararValor = false;
            boolean compararFecha = false;

            LocalDateTime fechaDesdeComparable = null;
            LocalDateTime fechaHastaComparable = null;

            if(fechaDesde!=null && fechaHasta!=null){
                compararFecha = true;
                fechaDesdeComparable = UtilesFecha.getLocalDateTimeFromString(fechaDesde);
                fechaHastaComparable = UtilesFecha.getLocalDateTimeFromString(fechaHasta);
            }

            if(momentoMedicion!=-1){
                compararMomento = true;
            }

            if(valorDeMedicionInferior!=-1 && valorDeMedicionSuperior!=-1){
                compararValor = true;
            }

            ArrayList<HistorialMedicionAdapterModel> filteredList = new ArrayList<>();
            for (HistorialMedicionAdapterModel medicion : historial_medicion_modelArrayList) {

                // Evalúo la fecha
                if(compararFecha){
                    LocalDateTime fechaMedicion = UtilesFecha.getLocalDateTimeFromString(medicion.getFecha());
                    if(fechaMedicion!=null && fechaDesdeComparable!=null && fechaHastaComparable!=null){
                        if (fechaDesdeComparable.isAfter(fechaMedicion) || fechaHastaComparable.isBefore(fechaMedicion)) {
                            continue;
                        }
                    }
                }

                // Evalúo el rango de valores
                if(compararValor){
                    Integer valor = Integer.valueOf(medicion.getValor());
                    if(valor < valorDeMedicionInferior || valor > valorDeMedicionSuperior){
                        continue;
                    }
                }

                // Evalúo el momento de la medición
                if(compararMomento){
                    if(!medicion.getTipo().equals(MomentoMedicion.getById(momentoMedicion).getNombre())){
                        continue;
                    }
                }

                filteredList.add(medicion);
            }

            historial_medicion_modelArrayList_filtrado = filteredList;
        }

        Collections.sort(historial_medicion_modelArrayList_filtrado, (x,y)-> Math.negateExact(UtilesFecha.getLocalDateTimeFromString(x.getFecha()).compareTo(UtilesFecha.getLocalDateTimeFromString(y.getFecha()))));

        historial_medicion_modelArrayList = historial_medicion_modelArrayList_filtrado;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView valor, tipo, fecha;
        LinearLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);

            valor=itemView.findViewById(R.id.valorMedicion);
            tipo=itemView.findViewById(R.id.tipoMedicion);
            fecha=itemView.findViewById(R.id.fechaMedicion);
            linearLayout=itemView.findViewById(R.id.componente);
        }
    }
}
