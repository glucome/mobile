package com.glucomeandroidapp.DAO;

import com.glucomeandroidapp.Utils.DataBaseManager;

public class CalibracionDAO {

    public static boolean borrarCalibracion(){
        try{
            DataBaseManager.getDaoSession().getRegresionDao().deleteAll();
            DataBaseManager.getDaoSession().getMedicionDispositivoDao().deleteAll();
            DataBaseManager.getDaoSession().getItemCalibracionDao().deleteAll();
            DataBaseManager.getDaoSession().getCalibracionDao().deleteAll();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
