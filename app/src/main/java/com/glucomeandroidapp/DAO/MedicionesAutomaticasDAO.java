package com.glucomeandroidapp.DAO;

import android.widget.TimePicker;

import com.glucomeandroidapp.POJO.Medicion.MedicionAutomatica;
import com.glucomeandroidapp.POJO.Medicion.MedicionAutomaticaDao;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.sql.Time;
import java.util.List;

public class MedicionesAutomaticasDAO {

    public static Long getSiguienteId() {
        MedicionAutomaticaDao medicionAutomaticaDao = DataBaseManager.getDaoSession().getMedicionAutomaticaDao();
        List<MedicionAutomatica> ids = medicionAutomaticaDao.queryBuilder()
                .where(MedicionAutomaticaDao.Properties.Id.isNotNull())
                .orderDesc(MedicionAutomaticaDao.Properties.Id)
                .limit(1)
                .list();
        Long siguienteId = !ids.isEmpty() ? ids.get(0).getId() + 1 : 0;
        return siguienteId;
    }

    public static List<MedicionAutomatica> getMedicionAutomaticas() {
        MedicionAutomaticaDao medicionAutomaticaDao = DataBaseManager.getDaoSession().getMedicionAutomaticaDao();
        return medicionAutomaticaDao.queryBuilder()
                .where(MedicionAutomaticaDao.Properties.Id.isNotNull())
                .orderAsc(MedicionAutomaticaDao.Properties.Hora)
                .orderAsc(MedicionAutomaticaDao.Properties.Minuto)
                .list();
    }

    public static List<MedicionAutomatica> getMedicionAutomaticaById(Integer id) {
        MedicionAutomaticaDao medicionAutomaticaDao = DataBaseManager.getDaoSession().getMedicionAutomaticaDao();
        return medicionAutomaticaDao.queryBuilder()
                .where(MedicionAutomaticaDao.Properties.Id.isNotNull(),
                        MedicionAutomaticaDao.Properties.Id.eq(id)).list();
    }

    public static boolean yaExisteUnaMedicionAutomaticaALaMismaHora(TimePicker tiempo) {
        MedicionAutomaticaDao medicionAutomaticaDao = DataBaseManager.getDaoSession().getMedicionAutomaticaDao();
        return medicionAutomaticaDao.queryBuilder()
                .where(MedicionAutomaticaDao.Properties.Id.isNotNull(),
                        MedicionAutomaticaDao.Properties.Hora.eq(tiempo.getHour()),
                        MedicionAutomaticaDao.Properties.Minuto.eq(tiempo.getMinute()))
                .count()>0;
    }

    public static void borrarMedicion(Long id) {
        DataBaseManager.getDaoSession().getMedicionAutomaticaDao().deleteByKey(id);
    }
}
