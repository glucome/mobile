package com.glucomeandroidapp.DAO;

import com.glucomeandroidapp.POJO.Contacto;
import com.glucomeandroidapp.POJO.ContactoDao;
import com.glucomeandroidapp.POJO.InyeccionDao;
import com.glucomeandroidapp.Utils.DataBaseManager;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

public class ContactoDAO {

    public static List<Contacto> getContactosNoEnviadossAlBackend() {
        QueryBuilder<Contacto> qb = DataBaseManager.getDaoSession().getContactoDao().queryBuilder()
                .where(ContactoDao.Properties.IdBackend.isNull());
        return qb.list();
    }
}
