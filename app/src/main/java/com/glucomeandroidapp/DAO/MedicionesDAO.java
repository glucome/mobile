package com.glucomeandroidapp.DAO;

import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.POJO.Medicion.MedicionDao;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.UtilesFecha;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.Collections;
import java.util.List;

public class MedicionesDAO {

    public static Long getSiguienteId() {
        MedicionDao medicionDao = DataBaseManager.getDaoSession().getMedicionDao();
        List<Medicion> ids = medicionDao.queryBuilder()
                .where(MedicionDao.Properties.Id.isNotNull())
                .orderDesc(MedicionDao.Properties.Id)
                .limit(1)
                .list();
        return !ids.isEmpty() ? ids.get(0).getId() + 1 : 0;
    }

    public static List<Medicion> getAllMediciones() {
        MedicionDao medicionDao = DataBaseManager.getDaoSession().getMedicionDao();
        return medicionDao.queryBuilder()
                .where(MedicionDao.Properties.Id.isNotNull())
                .orderDesc(MedicionDao.Properties.FechaDeCreacion)
                .list();
    }

    public static Medicion getUltimaMedicion() {
        List<Medicion> mediciones = getAllMediciones();
        if(mediciones != null && !mediciones.isEmpty()){
            Collections.sort(mediciones, (x, y)-> Math.negateExact(UtilesFecha.getLocalDateTimeFromString(x.getFechaDeCreacion()).compareTo(UtilesFecha.getLocalDateTimeFromString(y.getFechaDeCreacion()))));
            return mediciones.get(0);
        }
        return null;
    }

    public static List<Medicion> getUltimas10Mediciones() {
        MedicionDao medicionDao = DataBaseManager.getDaoSession().getMedicionDao();
        return medicionDao.queryBuilder()
                .where(MedicionDao.Properties.Id.isNotNull())
                .orderDesc(MedicionDao.Properties.FechaDeCreacion)
                .limit(10)
                .list();
    }

    public static List<Medicion> getMedicionesNoEnviadasAlBackend() {
        QueryBuilder<Medicion> qb = DataBaseManager.getDaoSession().getMedicionDao().queryBuilder()
                .where(MedicionDao.Properties.IdBackend.isNull());
        return qb.list();
    }

    public static void borrarMedicion(Long id) {
        DataBaseManager.getDaoSession().getMedicionDao().deleteByKey(id);
    }
}
