package com.glucomeandroidapp.DAO;

import com.glucomeandroidapp.POJO.Notificacion;
import com.glucomeandroidapp.POJO.NotificacionDao;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.util.List;

public class NotificacionesDAO {

    public static Long getSiguienteId() {
        NotificacionDao notificacionDao = DataBaseManager.getDaoSession().getNotificacionDao();
        List<Notificacion> ids = notificacionDao.queryBuilder()
                .where(NotificacionDao.Properties.Id.isNotNull())
                .orderDesc(NotificacionDao.Properties.Id)
                .limit(1)
                .list();
        Long siguienteId = !ids.isEmpty() ? ids.get(0).getId() + 1 : 0;
        return siguienteId;
    }
}
