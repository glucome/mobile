package com.glucomeandroidapp.DAO;

import com.glucomeandroidapp.POJO.Alimentos.IngresoAlimento;
import com.glucomeandroidapp.POJO.Alimentos.IngresoAlimentoDao;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.util.List;

public class IngresoAlimentoDAO {

    public static List<IngresoAlimento> getAllAlimentosIngresados() {
         IngresoAlimentoDao ingresoAlimentoDao = DataBaseManager.getDaoSession().getIngresoAlimentoDao();
        return ingresoAlimentoDao.queryBuilder()
                .where(IngresoAlimentoDao.Properties.Id.isNotNull())
                .orderAsc(IngresoAlimentoDao.Properties.FechaDeCreacion)
                .list();
    }

}
