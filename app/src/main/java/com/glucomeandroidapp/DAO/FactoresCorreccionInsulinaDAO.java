package com.glucomeandroidapp.DAO;

import com.glucomeandroidapp.POJO.FactoresCorreccionInsulina;
import com.glucomeandroidapp.POJO.FactoresCorreccionInsulinaDao;
import com.glucomeandroidapp.Utils.DataBaseManager;

public class FactoresCorreccionInsulinaDAO {

    public static FactoresCorreccionInsulina obtener(){
        FactoresCorreccionInsulinaDao dao = DataBaseManager.getDaoSession().getFactoresCorreccionInsulinaDao();
        return dao.queryBuilder().unique();
    }

    public static void insertar(FactoresCorreccionInsulina factoresCorreccionInsulina){
        FactoresCorreccionInsulinaDao dao = DataBaseManager.getDaoSession().getFactoresCorreccionInsulinaDao();
        dao.deleteAll();
        dao.insert(factoresCorreccionInsulina);
    }

}
