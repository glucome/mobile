package com.glucomeandroidapp.DAO;

import com.glucomeandroidapp.POJO.Medicacion.HorarioMedicacion;
import com.glucomeandroidapp.POJO.Medicacion.HorarioMedicacionDao;
import com.glucomeandroidapp.POJO.Medicion.MedicionAutomatica;
import com.glucomeandroidapp.POJO.Medicion.MedicionAutomaticaDao;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.util.List;

public class HorarioMedicacionDAO {

    public static List<HorarioMedicacion> getHorarios(Long idMedicamento){
        HorarioMedicacionDao horarioMedicacionDao = DataBaseManager.getDaoSession().getHorarioMedicacionDao();
        return horarioMedicacionDao.queryBuilder()
                .where(HorarioMedicacionDao.Properties.MedicamentoId.eq(idMedicamento))
                .orderAsc(HorarioMedicacionDao.Properties.Hora)
                .orderAsc(HorarioMedicacionDao.Properties.Minuto)
                .list();
    }

    public static List<HorarioMedicacion> getHorarioMedicacionById(Integer id) {
        HorarioMedicacionDao horarioMedicacionDao = DataBaseManager.getDaoSession().getHorarioMedicacionDao();
        return horarioMedicacionDao.queryBuilder()
                .where(HorarioMedicacionDao.Properties.Id.isNotNull(),
                        HorarioMedicacionDao.Properties.Id.eq(id)).list();
    }
}
