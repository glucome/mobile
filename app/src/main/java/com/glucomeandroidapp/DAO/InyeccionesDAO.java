package com.glucomeandroidapp.DAO;

import com.glucomeandroidapp.POJO.Inyeccion;
import com.glucomeandroidapp.POJO.InyeccionDao;
import com.glucomeandroidapp.POJO.InyeccionDao.Properties;
import com.glucomeandroidapp.Utils.DataBaseManager;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

public class InyeccionesDAO {

    public static Long getSiguienteId() {
        InyeccionDao InyeccionDao = DataBaseManager.getDaoSession().getInyeccionDao();
        List<Inyeccion> ids = InyeccionDao.queryBuilder()
                .where(Properties.Id.isNotNull())
                .orderDesc(Properties.Id)
                .limit(1)
                .list();
        return !ids.isEmpty() ? ids.get(0).getId() + 1 : 0;
    }

    public static List<Inyeccion> getAllInyecciones() {
        InyeccionDao InyeccionDao = DataBaseManager.getDaoSession().getInyeccionDao();
        return InyeccionDao.queryBuilder()
                .where(Properties.Id.isNotNull())
                .orderAsc(Properties.FechaDeCreacion)
                .list();
    }

    public static List<Inyeccion> getUltimas10Inyecciones() {
        InyeccionDao InyeccionDao = DataBaseManager.getDaoSession().getInyeccionDao();
        return InyeccionDao.queryBuilder()
                .where(Properties.Id.isNotNull())
                .orderDesc(Properties.FechaDeCreacion)
                .limit(10)
                .list();
    }
    
    public static List<Inyeccion> getInyeccionesNoEnviadasAlBackend() {
        QueryBuilder<Inyeccion> qb = DataBaseManager.getDaoSession().getInyeccionDao().queryBuilder()
                .where(Properties.IdBackend.isNull());
        return qb.list();
    }

    public static void borrarInyeccion(Long id) {
        DataBaseManager.getDaoSession().getInyeccionDao().deleteByKey(id);
    }
}
