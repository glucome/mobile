package com.glucomeandroidapp.DAO;


import com.glucomeandroidapp.POJO.Medicacion.TomaMedicamento;
import com.glucomeandroidapp.POJO.Medicacion.TomaMedicamentoDao;
import com.glucomeandroidapp.POJO.Medicacion.TomaMedicamentoDao.Properties;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.UtilesFecha;

import org.greenrobot.greendao.query.QueryBuilder;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;

public class TomasMedicamentosDAO {

    public static Long getSiguienteId() {
        TomaMedicamentoDao tomaMedicamentoDao = DataBaseManager.getDaoSession().getTomaMedicamentoDao();
        List<TomaMedicamento> ids = tomaMedicamentoDao.queryBuilder()
                .where(Properties.Id.isNotNull())
                .orderDesc(Properties.Id)
                .limit(1)
                .list();
        return !ids.isEmpty() ? ids.get(0).getId() + 1 : 0;
    }

    public static List<TomaMedicamento> getAllTomasMedicamentos() {
        TomaMedicamentoDao tomaMedicamentoDao = DataBaseManager.getDaoSession().getTomaMedicamentoDao();
        return tomaMedicamentoDao.queryBuilder()
                .where(Properties.Id.isNotNull())
                .orderAsc(Properties.FechaDeCreacion)
                .list();
    }

    public static boolean verificarSiTomoElMedicamentoEnLaUltimaHora(long medicamentoBackendId, long hora) {

        LocalDateTime horaAlarma = UtilesFecha.millsToLocalDateTime(hora);

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MINUTE, horaAlarma.getHour() - 1);

        LocalDateTime horaDeSeteoDeAlarma = UtilesFecha.millsToLocalDateTime(cal.getTimeInMillis());

        String strHoraDeSeteoDeAlarma = UtilesFecha.getStringFromLocalDateTime(horaAlarma);
        String strHoraDeAlarma = UtilesFecha.getStringFromLocalDateTime(horaDeSeteoDeAlarma);

        TomaMedicamentoDao tomaMedicamentoDao = DataBaseManager.getDaoSession().getTomaMedicamentoDao();

        long countMedicamentoEnUltimaHora = tomaMedicamentoDao.queryBuilder()
                .where( Properties.IdMedicamentoBackend.eq(medicamentoBackendId),
                        Properties.FechaDeCreacion.gt(strHoraDeAlarma))
                .count();

        return countMedicamentoEnUltimaHora>0;
    }

    public static List<TomaMedicamento> getUltimas10TomasMedicamentos() {
        TomaMedicamentoDao tomaMedicamentoDao = DataBaseManager.getDaoSession().getTomaMedicamentoDao();
        return tomaMedicamentoDao.queryBuilder()
                .where(Properties.Id.isNotNull())
                .orderDesc(Properties.FechaDeCreacion)
                .limit(10)
                .list();
    }
    
    public static List<TomaMedicamento> getTomasMedicamentosNoEnviadasAlBackend() {
        QueryBuilder<TomaMedicamento> qb = DataBaseManager.getDaoSession().getTomaMedicamentoDao().queryBuilder()
                .where(Properties.IdBackend.isNull());
        return qb.list();
    }

}
