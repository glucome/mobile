package com.glucomeandroidapp.DAO;

import com.glucomeandroidapp.POJO.Ejercicio.Ejercicio;
import com.glucomeandroidapp.POJO.Ejercicio.EjercicioDao;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.util.List;

public class EjercicioDAO {

    public static List<Ejercicio> getAllEjercicios() {
        EjercicioDao ejercicioDao = DataBaseManager.getDaoSession().getEjercicioDao();
        return ejercicioDao.queryBuilder()
                .where(EjercicioDao.Properties.Id.isNotNull())
                .orderAsc(EjercicioDao.Properties.FechaDeCreacion)
                .list();
    }
}
