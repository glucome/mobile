package com.glucomeandroidapp.Firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static com.glucomeandroidapp.Controladores.FirebaseControlador.enviarTokenFirebase;
import static com.glucomeandroidapp.Utils.Util.CANAL_ALERTAS;
import static com.glucomeandroidapp.Utils.Util.CANAL_ALERTAS_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.CANAL_ALERTAS_ID;
import static com.glucomeandroidapp.Utils.Util.TAG;

/**
 * Creado por MartinArtime el 13 de junio del 2019
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String title = null;
        String body = null;
        String click_action = null;

        if (remoteMessage.getNotification() != null) {
            title = remoteMessage.getNotification().getTitle();
            body = remoteMessage.getNotification().getBody();
            click_action = remoteMessage.getNotification().getClickAction();
            Log.e(TAG, "MENSAJE RECIBIDO DE FIREBASE, TITULO: " + title);
            Log.e(TAG, "MENSAJE RECIBIDO DE FIREBASE, BODY: " + body);
            Log.e(TAG, "MENSAJE RECIBIDO DE FIREBASE, CLICK: " + click_action);
        } else {
            Log.e(TAG, "NOTIFICACION DE FIREBASE = NULL");
        }

        String ubicacion = null;
        String mensaje = null;

        if (remoteMessage.getData().size() > 0) {
            ubicacion = remoteMessage.getData().get("ubicacion");
            mensaje = remoteMessage.getData().get("mensaje");
            Log.e(TAG, "MENSAJE RECIBIDO DE FIREBASE, UBICACION: " + ubicacion);
            Log.e(TAG, "MENSAJE RECIBIDO DE FIREBASE, MENSAJE: " + mensaje);
        } else {
            Log.e(TAG, "DATOS DE FIREBASE SIZE == 0");
        }

        if(title!=null && body!=null && click_action!=null){
            Intent intent = new Intent(click_action);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent p = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            NotificationChannel channel = new NotificationChannel(CANAL_ALERTAS_ID, CANAL_ALERTAS,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(CANAL_ALERTAS_DESCRIPCION);
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CANAL_ALERTAS_ID);
            builder.setContentTitle(title);
            builder.setContentText(body);
            builder.setSmallIcon(R.mipmap.ic_launcher);
            builder.setContentIntent(p);

            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(0, builder.build());

        }


    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.e(TAG, "Refreshed firebase token: " + token);

        LoginResponse loginResponse = AccountDataManager.obtenerDatosUsuario();

        if (loginResponse != null) {
            loginResponse.setTokenFirebase(token);
            AccountDataManager.loguearse(loginResponse);

            enviarTokenFirebase();
        }

    }

}
