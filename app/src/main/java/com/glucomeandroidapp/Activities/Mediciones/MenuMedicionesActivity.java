package com.glucomeandroidapp.Activities.Mediciones;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Calibracion.MenuCalibracionActivity;
import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuMedicionesActivity extends BaseActivity {

    @BindView(R.id.rc_menu) RecyclerView rcMenu;
    @BindView(R.id.back_button) ImageView backButton;

    //private BluetoothDeviceManager bluetoothDeviceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_generic);
        ButterKnife.bind(this);

        super.inicializar(R.string.menu_medicion);

        backButton.setOnClickListener(v -> App.getInstance().iniciarActividad(MenuPrincipalActivity.class));

        MenuAdapter menuAdapter;
        ArrayList<MenuAdapterModel> menuAdapterModelArrayList;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(App.getContext());
        rcMenu.setLayoutManager(layoutManager);
        rcMenu.setItemAnimator(new DefaultItemAnimator());

        menuAdapterModelArrayList = new ArrayList<>();

        int[] iconos = {R.drawable.ic_manual, R.drawable.ic_manual, R.drawable.ic_notificaciones, R.drawable.ic_horario};
        String[] titulos = getResources().getStringArray(R.array.menu_mediciones);

        for (int i = 0; i < iconos.length; i++) {
            MenuAdapterModel view = new MenuAdapterModel(
                    titulos[i],
                    iconos[i]);
            menuAdapterModelArrayList.add(view);
        }

        menuAdapter = new MenuAdapter(this, menuAdapterModelArrayList) {
            @Override
            public void ejecutarAccion(int position) {
                switch (position) {
                    case 0:
                        App.getInstance().iniciarActividad(MedicionAutomaticaPaso1Activity.class);
                        break;
                    case 1:
                        App.getInstance().iniciarActividad(MedicionManualActivity.class);
                        break;
                    case 2:
                        App.getInstance().iniciarActividad(MedicionesAutomaticasActivity.class);
                        break;
                    case 3:
                        App.getInstance().iniciarActividad(HistorialDeMedicionesActivity.class);
                        break;
                    case 4:
                        App.getInstance().iniciarActividad(MenuCalibracionActivity.class);
                        break;

                    default:
                        break;
                }
            }
        };

        rcMenu.setAdapter(menuAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        App.getInstance().iniciarActividad(MenuPrincipalActivity.class);
    }
}
