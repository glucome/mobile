package com.glucomeandroidapp.Activities.Mediciones;

import android.os.Bundle;
import android.widget.Spinner;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Controladores.MedicionesControlador;
import com.glucomeandroidapp.POJO.APIClasses.Requests.MedicionRequest;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AlertasAOtrosUsuariosManager;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.MEDICION;

public class MedicionAutomaticaPaso3Activity extends BaseActivity {

    Medicion medicion;

    @BindView(R.id.valor_de_la_medicion_automatica)
    TextView txtMedicionAutomatica;

    @BindView(R.id.spinner_momento_medicion)
    Spinner momentoMedicion;

    @BindView(R.id.btn_aceptar)
    TextView btnAceptar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicion_automatica_paso_3);
        ButterKnife.bind(this);
        super.inicializar(R.string.medicion_automatica_titulo);

        medicion = obtenerBundle(MEDICION, Medicion.class);

        String valorMedicion = String.valueOf(medicion.getValorMedicion().intValue());

        txtMedicionAutomatica.setText(valorMedicion);

        btnAceptar.setOnClickListener(v ->
                {
                    int momento = momentoMedicion.getSelectedItemPosition();

                    medicion.setValorMedicion(medicion.getValorMedicion());
                    medicion.setMomento(MomentoMedicion.getById(momento));

                    // Guardo la nueva medicion en la BD
                    DataBaseManager.getDaoSession().getMedicionDao().insert(medicion);

                    AlertasAOtrosUsuariosManager.enviarAlertasAAsociadosSiCorresponde(this,
                            String.valueOf(medicion.getValorMedicion().intValue()));

                    MedicionRequest medicionRequest = new MedicionRequest(
                            medicion.getFechaDeCreacion(), medicion.getValorMedicion().intValue(),
                            medicion.getMomento().ordinal(), medicion.getTipoMedicion().ordinal());

                    //Envío la medición al backend
                    LoadingDialogManager.mostrar(this);
                    MedicionesControlador.enviarMedicion(medicionRequest, medicion);

                    finish();
                }
        );


    }

}
