package com.glucomeandroidapp.Activities.Mediciones;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.Medicion.MedicionAutomatica;
import com.glucomeandroidapp.POJO.Notificacion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.LocalStorageManager;
import com.glucomeandroidapp.Utils.Util;

import java.time.LocalDateTime;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.DAO.MedicionesAutomaticasDAO.yaExisteUnaMedicionAutomaticaALaMismaHora;
import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.GUARDADO_EXITOSO_MEDICION_AUTOMATICA;
import static com.glucomeandroidapp.Utils.Util.GUARDADO_FALLIDO;
import static com.glucomeandroidapp.Utils.Util.MEDICIONES_AUTOMATICAS;
import static com.glucomeandroidapp.Utils.Util.MEDICION_AUTOMATICA_ID;
import static com.glucomeandroidapp.Utils.Util.NUMERO_NOTIFICACION;
import static com.glucomeandroidapp.Utils.Util.TEXTO_MEDICION_AUTOMATICA;
import static com.glucomeandroidapp.Utils.Util.TITULO_MEDICION_AUTOMATICA;
import static com.glucomeandroidapp.Utils.UtilesFunciones.cancelarNotificacion;
import static com.glucomeandroidapp.Utils.UtilesFunciones.programarNotificacion;

public class EditarMedicionAutomaticaActivity extends BaseActivity {

    @BindView(R.id.tiempo) TimePicker tiempo;
    @BindView(R.id.input_nombre_medicion_automatica) EditText nombre;
    @BindView(R.id.btn_grabar_medicion_automatica) TextView btnGrabar;

    Integer numero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mediciones_automaticas_edicion);

        ButterKnife.bind(this);

        super.inicializar("Horario de medición");

        long id=0;
        Bundle bundle = getIntent().getBundleExtra(BUNDLE_NAME);
        if(bundle!=null){
            id = bundle.getLong(MEDICION_AUTOMATICA_ID);
        }

        MedicionAutomatica medicionAutomaticaAMostrar = DataBaseManager.getDaoSession().getMedicionAutomaticaDao().load(id);
        mostrarMedicion(medicionAutomaticaAMostrar);

        btnGrabar.setOnClickListener(v -> {
            if (!nombre.getText().toString().isEmpty()) {
                MedicionAutomatica medicionAutomatica;
                if(medicionAutomaticaAMostrar != null){
                    medicionAutomatica = medicionAutomaticaAMostrar;
                } else {
                    medicionAutomatica = new MedicionAutomatica();
                    medicionAutomatica.setNotificacionId(-1L);
                }

                if(yaExisteUnaMedicionAutomaticaALaMismaHora(tiempo)){

                    App.getInstance().mostrarMensajeFlotante("Ya existe una medición automática a esa hora!");

                } else {

                    medicionAutomatica.setHora(tiempo.getHour());
                    medicionAutomatica.setMinuto(tiempo.getMinute());
                    medicionAutomatica.setNombre(nombre.getText().toString());
                    if(medicionAutomatica.getNotificacionId() != -1L){
                        cancelarNotificacion(getApplicationContext(), (int) medicionAutomatica.getNotificacionId());
                    }

                    // Obtengo el numero de notificación nueva a usar
                    numero = LocalStorageManager.obtener(NUMERO_NOTIFICACION, Integer.class);
                    if(numero==null){
                        numero = 1;
                    }

                    // Creo y guardo la notificación
                    Notificacion notificacion = new Notificacion();
                    notificacion.setTipoNotificacion(MEDICIONES_AUTOMATICAS);
                    notificacion.setId(Long.valueOf(numero));
                    DataBaseManager.getDaoSession().getNotificacionDao().insertOrReplace(notificacion);

                    // Guardo la notificación en la medición automática y la guardo en la BD
                    medicionAutomatica.setNotificacion(notificacion);
                    medicionAutomatica.setNotificacionId(notificacion.getId());
                    DataBaseManager.getDaoSession().getMedicionAutomaticaDao().insertOrReplace(medicionAutomatica);

                    Log.e(Util.TAG, "Numero de notificación: "+numero);

                    // Programo la notificación para que suene a la hora y minutos seteados
                    programarNotificacion(getApplicationContext(), numero, TITULO_MEDICION_AUTOMATICA,
                            TEXTO_MEDICION_AUTOMATICA + medicionAutomatica.getNombre(), tiempo.getHour(),
                            tiempo.getMinute(), medicionAutomatica.getId().intValue(), false, MEDICIONES_AUTOMATICAS);

                    ((App)getApplication()).mostrarMensajeFlotante(GUARDADO_EXITOSO_MEDICION_AUTOMATICA);
                    ((App)getApplication()).iniciarActividad(MedicionesAutomaticasActivity.class, null);
                }

            } else {
                ((App)getApplication()).mostrarMensajeFlotante(GUARDADO_FALLIDO);
            }
        });
    }

    public void mostrarMedicion(MedicionAutomatica medicionAutomatica){
        nombre.setText(medicionAutomatica != null ? medicionAutomatica.getNombre() : "");
        tiempo.setIs24HourView(true);
        LocalDateTime ahora = LocalDateTime.now();
        tiempo.setHour(medicionAutomatica != null ? medicionAutomatica.getHora() : ahora.getHour());
        tiempo.setMinute(medicionAutomatica != null ? medicionAutomatica.getMinuto() : ahora.getMinute());
    }
}
