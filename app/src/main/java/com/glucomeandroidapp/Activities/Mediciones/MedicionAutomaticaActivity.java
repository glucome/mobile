package com.glucomeandroidapp.Activities.Mediciones;

import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Spinner;
import android.widget.TextView;

import com.glucomeandroidapp.Utils.MedicionRequestTask;
import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.MedicionesControlador;
import com.glucomeandroidapp.POJO.APIClasses.Requests.MedicionRequest;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoMedicion;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AlertasAOtrosUsuariosManager;
import com.glucomeandroidapp.Utils.BluetoothDeviceManager;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.time.LocalDateTime;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.BT_DEVICE_GLUKO_INVASIVE;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;

public class MedicionAutomaticaActivity extends BaseActivity {

    @BindView(R.id.valor_de_la_medicion_automatica)
    TextView txtMedicionAutomatica;

    @BindView(R.id.spinner_momento_medicion)
    Spinner momentoMedicion;

    @BindView(R.id.btn_aceptar)
    TextView btnAceptar;

    // Bluetooth
    private BluetoothDeviceManager bluetoothDeviceManager;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothSocket bluetoothSocket;

    // Async Task
    private MedicionRequestTask medicionRequestTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicion_automatica_paso_3);
        ButterKnife.bind(this);

        super.inicializar(R.string.medicion_automatica_titulo);


        bluetoothDeviceManager = new BluetoothDeviceManager(btDevice -> {

            if(btDevice == null){
                finish();
                return;
            }

            TipoMedicion tipoMedicion;
            if (btDevice.getName().toLowerCase().equals(BT_DEVICE_GLUKO_INVASIVE))
                tipoMedicion = TipoMedicion.INVASIVA;
            else
                tipoMedicion = TipoMedicion.NO_INVASIVA;

            // Logica
            btnAceptar.setOnClickListener(v -> {

                String medicion = txtMedicionAutomatica.getText().toString();

                String fechaDeCreacion = UtilesFecha.formatearFecha(LocalDateTime.now());

                int momento = momentoMedicion.getSelectedItemPosition();
                int tipo = tipoMedicion.ordinal();

                Medicion nuevaMedicion = new Medicion(null, null,
                        UUID.randomUUID().toString(), Double.valueOf(medicion),
                        MomentoMedicion.getById(momento), TipoMedicion.getById(tipo),
                        fechaDeCreacion);

                // Guardo la nueva medicion en la BD
                DataBaseManager.getDaoSession().getMedicionDao().insert(nuevaMedicion);

                AlertasAOtrosUsuariosManager.enviarAlertasAAsociadosSiCorresponde(this, medicion);

                MedicionRequest medicionRequest = new MedicionRequest(
                        nuevaMedicion.getFechaDeCreacion(), nuevaMedicion.getValorMedicion().intValue(),
                        nuevaMedicion.getMomento().ordinal(), nuevaMedicion.getTipoMedicion().ordinal());

                //Envío la medición al backend
                LoadingDialogManager.mostrar(this);
                MedicionesControlador.enviarMedicion(medicionRequest, nuevaMedicion);

                finish();
            });

            // Conectarme al bluetooth
            try {
                bluetoothSocket = btDevice.createRfcommSocketToServiceRecord(MY_UUID);
                bluetoothSocket.connect();
            } catch (Exception e) {
                e.printStackTrace();
                App.getInstance().mostrarMensajeFlotante("Ocurrió un error al iniciar la conexión con el dispositivo.");
                finish();
                return;
            }

            // poner a correr la async task
            medicionRequestTask = new MedicionRequestTask(bluetoothSocket, valor -> {
                if(valor <= 0.0 || valor >=800){
                    App.getInstance().mostrarMensajeFlotante("Hubo un error al intentar obtener la medición del dispositivo.");
                    finish();
                } else {
                    txtMedicionAutomatica.setText(String.valueOf(valor));
                }
                LoadingDialogManager.ocultar();
            });
            medicionRequestTask.execute();
        });

        bluetoothDeviceManager.searchDevice(this, true);

        //bluetoothDevice = (BluetoothDevice) getIntent().getExtras().get(BLUETOOTH_DEVICE);
        //bluetoothDevice = obtenerBundle(BLUETOOTH_DEVICE, BluetoothDevice.class);





    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        bluetoothDeviceManager.onDestroy();

        if (medicionRequestTask != null && !medicionRequestTask.isCancelled())
            medicionRequestTask.cancel(true);

//        if (mBluetoothAdapter != null) {
//            mBluetoothAdapter.disable();
//        }

        // Desconectar Bluetooth
        try {
            if(bluetoothSocket != null)
                bluetoothSocket.close();

        } catch (Exception e) {
            e.printStackTrace();
            App.getInstance().mostrarMensajeFlotante("Ocurrió un error al cerrar la conexión con el dispositivo.");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == BluetoothDeviceManager.REQUEST_ENABLE_BT ){
            if (resultCode == RESULT_OK) {
                bluetoothDeviceManager.searchDevice(this, false);
            }else if(resultCode == RESULT_CANCELED){
                App.getInstance().mostrarMensajeFlotante("Se necesita acceso para poder realizar la medición.");
                finish();
            }
        }

    }

}
