package com.glucomeandroidapp.Activities.Mediciones;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.Adapters.MedicionesAutomaticasAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.DAO.MedicionesAutomaticasDAO;
import com.glucomeandroidapp.POJO.Medicion.MedicionAutomatica;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.UtilesFunciones;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.LoadingDialogManager.prepararDialogSiNo;
import static com.glucomeandroidapp.Utils.Util.BORRADO_EXITOSO_MEDICION_AUTOMATICA;
import static com.glucomeandroidapp.Utils.Util.BORRAR_MEDICION_AUTOMATICA_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.BORRAR_MEDICION_AUTOMATICA_TITULO;
import static com.glucomeandroidapp.Utils.Util.MEDICION_AUTOMATICA_ID;
import static java.lang.Math.toIntExact;

public class MedicionesAutomaticasActivity extends BaseActivity {

    @BindView(R.id.mediciones_automaticas_list) RecyclerView list;
    @BindView(R.id.back_button) ImageView backButton;

    private List<MedicionAutomatica> medicionesAutomaticas;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mediciones_automaticas);

        activity = this;

        ButterKnife.bind(this);

        super.inicializar(R.string.menu_mediciones_automaticas);

        backButton.setOnClickListener(v -> App.getInstance().iniciarActividad(MenuMedicionesActivity.class));

        medicionesAutomaticas = MedicionesAutomaticasDAO.getMedicionAutomaticas();

        MedicionesAutomaticasAdapter medicionesAutomaticasAdapter =
                new MedicionesAutomaticasAdapter(medicionesAutomaticas);
        RecyclerView recyclerView = list;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(medicionesAutomaticasAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        MedicionAutomatica medicionAutomatica = medicionesAutomaticas.get(position);
                        Bundle bundle = new Bundle();
                        bundle.putLong(MEDICION_AUTOMATICA_ID, medicionAutomatica.getId());
                        App.getInstance().iniciarActividad(EditarMedicionAutomaticaActivity.class, bundle);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        MedicionAutomatica medicionAutomatica = medicionesAutomaticas.get(position);

                        AlertDialog dialog = prepararDialogSiNo(activity, BORRAR_MEDICION_AUTOMATICA_TITULO, BORRAR_MEDICION_AUTOMATICA_DESCRIPCION);

                        dialog.findViewById(R.id.button_no).setOnClickListener(v -> dialog.dismiss());
                        dialog.findViewById(R.id.button_si).setOnClickListener(v -> {
                                UtilesFunciones.cancelarNotificacion(App.getContext(), (int) medicionAutomatica.getNotificacionId());
                                DataBaseManager.getDaoSession().getNotificacionDao().delete(medicionAutomatica.getNotificacion());
                                DataBaseManager.getDaoSession().getMedicionAutomaticaDao().delete(medicionAutomatica);
                                ((App) getApplication()).iniciarActividad(MedicionesAutomaticasActivity.class, null);
                                App.getInstance().mostrarMensajeFlotante(BORRADO_EXITOSO_MEDICION_AUTOMATICA);
                        });
                    }
                })
        );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        App.getInstance().iniciarActividad(MenuMedicionesActivity.class);
    }

    public void onAgregarMedicion(View v) {
        Bundle bundle = new Bundle();
        bundle.putLong(MEDICION_AUTOMATICA_ID, 0);
        ((App)getApplication()).iniciarActividad(
                EditarMedicionAutomaticaActivity.class, bundle);
    }

}
