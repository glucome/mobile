package com.glucomeandroidapp.Activities.Mediciones;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.MedicionesControlador;
import com.glucomeandroidapp.POJO.APIClasses.Requests.MedicionRequest;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoMedicion;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AlertasAOtrosUsuariosManager;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicionManualActivity extends BaseActivity {

    @BindView(R.id.valor_de_la_medicion_manual)
    EditText valorMedicion;
    @BindView(R.id.spinner_momento_medicion)
    Spinner momentoMedicion;
    @BindView(R.id.spinner_tipo_medicion)
    Spinner tipoMedicion;
    @BindView(R.id.fecha_de_medicion_manual)
    DatePicker fechaMedicion;
    @BindView(R.id.hora_de_medicion_manual)
    TimePicker horaMedicion;
    @BindView(R.id.btn_registrar_medicion)
    TextView btnGuardar;

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicion_manual);

        ButterKnife.bind(this);
        super.inicializar(R.string.medicion_manual_titulo);

        // Logica
        btnGuardar.setOnClickListener(v -> {

            String medicion = valorMedicion.getText().toString();

            if(medicion.equals("")){
                App.getInstance().mostrarMensajeFlotante("Debe ingresar un valor en la medición!");
            } else {
                String fechaDeCreacion = UtilesFecha.formatearFecha(fechaMedicion, horaMedicion);

                int momento = momentoMedicion.getSelectedItemPosition();
                int tipo = tipoMedicion.getSelectedItemPosition();

                Medicion nuevaMedicion = new Medicion(null, null,
                        UUID.randomUUID().toString(), Double.valueOf(medicion),
                        MomentoMedicion.getById(momento), TipoMedicion.getById(tipo),
                        fechaDeCreacion);

                // Guardo la nueva medicion en la BD
                DataBaseManager.getDaoSession().getMedicionDao().insert(nuevaMedicion);

                AlertasAOtrosUsuariosManager.enviarAlertasAAsociadosSiCorresponde(this, medicion);

                MedicionRequest medicionRequest = new MedicionRequest(
                        nuevaMedicion.getFechaDeCreacion(), nuevaMedicion.getValorMedicion().intValue(),
                        nuevaMedicion.getMomento().ordinal(), nuevaMedicion.getTipoMedicion().ordinal());

                //Envío la medición al backend
                //LoadingDialogManager.Mostrar(this);
                MedicionesControlador.enviarMedicion(medicionRequest, nuevaMedicion);

                finish();
            }

        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                } else {
                    // Permission Denied
                    Toast.makeText(MedicionManualActivity.this, "Mensaje SMS denegado", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


}
