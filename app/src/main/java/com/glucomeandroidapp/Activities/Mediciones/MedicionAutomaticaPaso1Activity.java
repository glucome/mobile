package com.glucomeandroidapp.Activities.Mediciones;

import android.os.Bundle;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicionAutomaticaPaso1Activity extends BaseActivity {

    @BindView(R.id.btn_siguiente)
    TextView btnSiguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicion_automatica_paso_1);
        ButterKnife.bind(this);
        super.inicializar(R.string.medicion_automatica_titulo);

        btnSiguiente.setOnClickListener(v ->
                {
                    App.getInstance().iniciarActividad(MedicionAutomaticaPaso2Activity.class);
                    finish();
                }
        );

    }

}
