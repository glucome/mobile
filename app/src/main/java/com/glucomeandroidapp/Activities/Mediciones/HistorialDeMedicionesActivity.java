package com.glucomeandroidapp.Activities.Mediciones;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.HistorialMedicionAdapter;
import com.glucomeandroidapp.Adapters.Models.HistorialMedicionAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.DAO.MedicionesDAO;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.FECHA_DESDE;
import static com.glucomeandroidapp.Utils.Util.FECHA_HASTA;
import static com.glucomeandroidapp.Utils.Util.MEDICIONES_LIST;
import static com.glucomeandroidapp.Utils.Util.MOMENTO_MEDICION;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_NOMBRE;
import static com.glucomeandroidapp.Utils.Util.TAG_ACTIVITY_FILTRO;
import static com.glucomeandroidapp.Utils.Util.VALOR_MEDICION_INFERIOR;
import static com.glucomeandroidapp.Utils.Util.VALOR_MEDICION_SUPERIOR;

/**
 * Created by MartinArtime on 15 de mayo del 2019
 **/
public class HistorialDeMedicionesActivity extends BaseActivity {


    @BindView(R.id.recyclerview) RecyclerView recyclerView;
    @BindView(R.id.btn_filtrar) ImageButton btnFiltrar;
    @BindView(R.id.sv_historial) ScrollView svHistorial;
    @BindView(R.id.sv_sin_datos) ScrollView svSinDatos;
    @BindView(R.id.img_no_data) ImageView imgNoData;

    public List<Medicion> mediciones;
    public HistorialMedicionAdapter historial_medicion_adapter;

    /**
     * Cargo la vista y bindeo
     * Obtengo las mediciones
     * Agrego las mediciones a la tabla
     *
     * @param savedInstanceState, bundle autogenerado
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_generic);

        ButterKnife.bind(this);

        btnFiltrar.setOnClickListener(v -> {
            Intent intent = new Intent(App.getContext(), FiltrarHistorialMedicionesActivity.class);
            startActivityForResult(intent, TAG_ACTIVITY_FILTRO);
        });

        String nombrePantalla = getResources().getString(R.string.menu_historial_medicion);

        if (!AccountDataManager.obtenerRolEnum().esPaciente()) {
            nombrePantalla += " " + obtenerBundle(PACIENTE_NOMBRE, String.class);
            mediciones = obtenerListaBundle(MEDICIONES_LIST, Medicion.class);

            if(!mediciones.isEmpty()){
                agregarDatos(null, null, -1, -1, -1);
            } else {
                svSinDatos.setVisibility(View.VISIBLE);
                svHistorial.setVisibility(View.GONE);
                imgNoData.setImageResource(R.drawable.ic_medicion100);
            }
        } else{
            mediciones = MedicionesDAO.getAllMediciones();
        }

        super.inicializar(nombrePantalla);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HistorialDeMedicionesActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        agregarDatos(null, null, -1, -1, -1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == TAG_ACTIVITY_FILTRO) {
            if(resultCode == Activity.RESULT_OK){
                String fechaDesde = data.getStringExtra(FECHA_DESDE);
                String fechaHasta = data.getStringExtra(FECHA_HASTA);
                int valorDeMedicionSuperior = data.getIntExtra(VALOR_MEDICION_SUPERIOR, -1);
                int valorDeMedicionInferior = data.getIntExtra(VALOR_MEDICION_INFERIOR, -1);
                int momentoMedicion = data.getIntExtra(MOMENTO_MEDICION, -1);

                agregarDatos(fechaDesde, fechaHasta, valorDeMedicionSuperior, valorDeMedicionInferior, momentoMedicion);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.e(Util.TAG, "No se pudo filtrar");
            }
        }
    }

    /**
     * Popular la tabla con los valores obtenidos de la base de datos
     */
    @SuppressWarnings("all")
    private void agregarDatos(String fechaDesde, String fechaHasta, int valorDeMedicionSuperior,
                              int valorDeMedicionInferior, int momentoMedicion) {
        ArrayList<HistorialMedicionAdapterModel> historial_medicion_modelArrayList = new ArrayList<>();

        for (Medicion medicion : mediciones) {
            HistorialMedicionAdapterModel view = new HistorialMedicionAdapterModel(
                    medicion.getValorMedicion(),
                    medicion.getMomento().getNombre(),
                    medicion.getFechaDeCreacion());
            historial_medicion_modelArrayList.add(view);
        }

        historial_medicion_adapter = new HistorialMedicionAdapter(historial_medicion_modelArrayList);
        historial_medicion_adapter.filter(fechaDesde, fechaHasta, valorDeMedicionSuperior, valorDeMedicionInferior, momentoMedicion);
        recyclerView.setAdapter(historial_medicion_adapter);
    }

}
