package com.glucomeandroidapp.Activities.Insulina;

import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.DAO.FactoresCorreccionInsulinaDAO;
import com.glucomeandroidapp.DAO.InyeccionesDAO;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.FALTAN_VALORES_INSULINA_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.FALTAN_VALORES_INSULINA_TITULO;
import static com.glucomeandroidapp.Utils.Util.LISTADO_INYECCIONES;

public class MenuInsulinaActivity extends BaseActivity {

    @BindView(R.id.rc_menu) RecyclerView rcMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_generic);
        ButterKnife.bind(this);

        super.inicializar(R.string.menu_insulina);

        MenuAdapter menuAdapter;
        ArrayList<MenuAdapterModel> menuAdapterModelArrayList;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(App.getContext());
        rcMenu.setLayoutManager(layoutManager);
        rcMenu.setItemAnimator(new DefaultItemAnimator());

        menuAdapterModelArrayList = new ArrayList<>();

        int[] iconos = {R.drawable.ic_nuevo,    R.drawable.ic_info,    R.drawable.ic_horario};
        String[] titulos = getResources().getStringArray(R.array.menu_insulina);

        for(int i=0; i<iconos.length; i++){
            MenuAdapterModel view = new MenuAdapterModel(
                    titulos[i],
                    iconos[i]);
            menuAdapterModelArrayList.add(view);
        }

        menuAdapter = new MenuAdapter(this, menuAdapterModelArrayList) {
            @Override
            public void ejecutarAccion(int position) {
                switch (position) {
                    case 0:
                        App.getInstance().iniciarActividad(InyeccionInsulinaActivity.class);
                        break;
                    case 1:
                        if(FactoresCorreccionInsulinaDAO.obtener() != null){
                            App.getInstance().iniciarActividad(MenuRecomendadorActivity.class);
                        } else{
                            LoadingDialogManager.crearDialogConfirmacion(activity, FALTAN_VALORES_INSULINA_TITULO, FALTAN_VALORES_INSULINA_DESCRIPCION);
                        }
                        break;
                    case 2:
                        Bundle bundle = new Bundle();
                        bundle.putString(LISTADO_INYECCIONES, new Gson().toJson(InyeccionesDAO.getAllInyecciones()));
                        App.getInstance().iniciarActividad(HistorialInyeccionesActivity.class, bundle);
                        break;
                    default:
                        break;
                }
            }
        };

        rcMenu.setAdapter(menuAdapter);

    }
}
