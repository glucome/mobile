package com.glucomeandroidapp.Activities.Insulina;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.HistorialInyeccionAdapter;
import com.glucomeandroidapp.Adapters.Models.HistorialInyeccionAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.Inyeccion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.CANTIDAD_DESDE;
import static com.glucomeandroidapp.Utils.Util.CANTIDAD_HASTA;
import static com.glucomeandroidapp.Utils.Util.FECHA_DESDE;
import static com.glucomeandroidapp.Utils.Util.FECHA_HASTA;
import static com.glucomeandroidapp.Utils.Util.LISTADO_INYECCIONES;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_NOMBRE;
import static com.glucomeandroidapp.Utils.Util.TAG_ACTIVITY_FILTRO;
import static com.glucomeandroidapp.Utils.Util.TIPO_INSULINA;

public class HistorialInyeccionesActivity extends BaseActivity {

    @BindView(R.id.recyclerview) RecyclerView recyclerView;
    @BindView(R.id.btn_cargar_mas) TextView botonCargarMas;
    @BindView(R.id.btn_filtrar) ImageButton btnFiltrar;
    @BindView(R.id.sv_historial) ScrollView svHistorial;
    @BindView(R.id.sv_sin_datos) ScrollView svSinDatos;
    @BindView(R.id.img_no_data) ImageView imgNoData;

    private List<Inyeccion> inyecciones;
    public HistorialInyeccionAdapter historial_inyeccion_adapter;

    private String titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_generic);

        ButterKnife.bind(this);

        titulo = obtenerBundle(PACIENTE_NOMBRE,String.class);

        btnFiltrar.setOnClickListener(v -> {
            Intent intent = new Intent(App.getContext(), FiltrarHistorialInyeccionesActivity.class);
            startActivityForResult(intent, TAG_ACTIVITY_FILTRO);
        });

        if(titulo != null && titulo.equals("")){
            super.inicializar("Insulina de " + titulo);
            ocultarBotonDeCarga();
        }else{
            super.inicializar(R.string.menu_historial_insulina);
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HistorialInyeccionesActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        inyecciones =  obtenerListaBundle(LISTADO_INYECCIONES, Inyeccion.class);

        if(!inyecciones.isEmpty()){
            agregarDatos(-1, -1, -1, null, null);
        } else {
            svSinDatos.setVisibility(View.VISIBLE);
            svHistorial.setVisibility(View.GONE);
            imgNoData.setImageResource(R.drawable.ic_inyeccion);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == TAG_ACTIVITY_FILTRO) {
            if(resultCode == Activity.RESULT_OK){
                String fechaDesde = data.getStringExtra(FECHA_DESDE);
                String fechaHasta = data.getStringExtra(FECHA_HASTA);
                int cantidadDesde = data.getIntExtra(CANTIDAD_DESDE, -1);
                int cantidadHasta = data.getIntExtra(CANTIDAD_HASTA, -1);
                int tipoInsulina = data.getIntExtra(TIPO_INSULINA, -1);

                agregarDatos(cantidadDesde, cantidadHasta, tipoInsulina, fechaDesde, fechaHasta);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.e(Util.TAG, "No se pudo filtrar");
            }
        }
    }

    private void ocultarBotonDeCarga() {
       botonCargarMas.setVisibility(View.GONE);
    }

    private void agregarDatos(int cantidadDesde, int cantidadHasta, int tipoInsulina, String fechaDesde, String fechaHasta) {

        ArrayList<HistorialInyeccionAdapterModel> historial_inyeccion_modelArrayList = new ArrayList<>();

        if(inyecciones != null){
            for(Inyeccion inyeccion : inyecciones){
                HistorialInyeccionAdapterModel view = new HistorialInyeccionAdapterModel(
                        inyeccion.getValorInyeccion(),
                        inyeccion.getTipoInsulina().getNombre(),
                        inyeccion.getFechaDeCreacion());
                historial_inyeccion_modelArrayList.add(view);
            }
        }

        historial_inyeccion_adapter = new HistorialInyeccionAdapter(historial_inyeccion_modelArrayList);
        historial_inyeccion_adapter.filter(cantidadDesde, cantidadHasta, tipoInsulina, fechaDesde, fechaHasta);
        recyclerView.setAdapter(historial_inyeccion_adapter);
    }
}
