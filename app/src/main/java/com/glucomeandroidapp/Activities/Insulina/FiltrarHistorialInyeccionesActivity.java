package com.glucomeandroidapp.Activities.Insulina;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.CustomComponents.EditText_Numerico;
import com.glucomeandroidapp.CustomComponents.MyTextView_Roboto_Regular;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.UtilesFecha;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.CANTIDAD_DESDE;
import static com.glucomeandroidapp.Utils.Util.CANTIDAD_HASTA;
import static com.glucomeandroidapp.Utils.Util.FECHA_DESDE;
import static com.glucomeandroidapp.Utils.Util.FECHA_HASTA;
import static com.glucomeandroidapp.Utils.Util.MOMENTO_MEDICION;
import static com.glucomeandroidapp.Utils.Util.TIPO_INSULINA;
import static com.glucomeandroidapp.Utils.Util.VALOR_MEDICION_INFERIOR;
import static com.glucomeandroidapp.Utils.Util.VALOR_MEDICION_SUPERIOR;

/**
 * Creado por MartinArtime el 12 de septiembre del 2019
 */
public class FiltrarHistorialInyeccionesActivity extends BaseActivity {

    @BindView(R.id.nombre_pantalla) protected TextView txtNombrePantalla;
    @BindView(R.id.back_button) protected ImageView btnAtras;
    @BindView(R.id.sp_filtros) Spinner spFiltros;
    @BindView(R.id.img_btn_agregar_filtro) protected ImageButton btnAgregarFiltro;
    @BindView(R.id.img_btn_eliminar_filtro) protected ImageButton btnEliminarFiltro;
    @BindView(R.id.lay_valor_inferior) protected LinearLayout layValorInferior;
    @BindView(R.id.valor_inferior) protected EditText_Numerico valorInferiorFiltro;
    @BindView(R.id.lay_valor_superior) protected LinearLayout layValorSuperior;
    @BindView(R.id.valor_superior) protected EditText_Numerico valorSuperiorFiltro;
    @BindView(R.id.lay_fecha_desde_filtro) protected LinearLayout layFechaDesdeFiltro;
    @BindView(R.id.fecha_desde_filtro) protected DatePicker fechaDesdeFiltro;
    @BindView(R.id.lay_fecha_hasta_filtro) protected LinearLayout layFechaHastaFiltro;
    @BindView(R.id.fecha_hasta_filtro) protected DatePicker fechaHastaFiltro;
    @BindView(R.id.lay_tipo_insulina_filtro) protected LinearLayout layTipoInsulinaFiltro;
    @BindView(R.id.sp_tipo_insulina_filtro) protected Spinner tipoInsulinaFiltro;
    @BindView(R.id.btn_filtrar) protected MyTextView_Roboto_Regular btnFiltrar;

    String fechaDesde = null;
    String fechaHasta = null;
    int cantidadDeInsulinaSuperior = -1;
    int cantidadDeInsulinaInferior = -1;
    int tipoDeInsulina = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtrar_historial_inyecciones);

        ButterKnife.bind(this);

        txtNombrePantalla.setText(getResources().getString(R.string.menu_filtrar));

        btnAtras.setOnClickListener(v -> volverAtrasSinFiltros());

        btnFiltrar.setOnClickListener(v -> {
            if(layValorInferior.getVisibility() == View.VISIBLE){
                cantidadDeInsulinaInferior = valorInferiorFiltro.getText();
                cantidadDeInsulinaSuperior = valorSuperiorFiltro.getText();
            }

            if(layFechaDesdeFiltro.getVisibility() == View.VISIBLE){
                fechaDesde = UtilesFecha.formatearSoloDate(fechaDesdeFiltro);
                fechaHasta = UtilesFecha.formatearSoloDate(fechaHastaFiltro);
            }

            if(layTipoInsulinaFiltro.getVisibility() == View.VISIBLE){
                tipoDeInsulina = tipoInsulinaFiltro.getSelectedItemPosition();
            }

            volverAtrasConFiltros();
        });

        btnAgregarFiltro.setOnClickListener(v -> {
            int filtroSeleccionado = spFiltros.getSelectedItemPosition();

            switch(filtroSeleccionado){
                case 0:
                    if(layValorInferior.getVisibility() == View.VISIBLE){
                        App.getInstance().mostrarMensajeFlotante("Ya ingreso ese filtro");
                    } else {
                        layValorSuperior.setVisibility(View.VISIBLE);
                        layValorInferior.setVisibility(View.VISIBLE);
                    }
                    break;
                case 1:
                    if(layFechaDesdeFiltro.getVisibility() == View.VISIBLE){
                        App.getInstance().mostrarMensajeFlotante("Ya ingreso ese filtro");
                    } else {
                        layFechaDesdeFiltro.setVisibility(View.VISIBLE);
                        layFechaHastaFiltro.setVisibility(View.VISIBLE);
                    }
                    break;
                case 2:
                    if(layTipoInsulinaFiltro.getVisibility() == View.VISIBLE){
                        App.getInstance().mostrarMensajeFlotante("Ya ingreso ese filtro");
                    } else {
                        layTipoInsulinaFiltro.setVisibility(View.VISIBLE);
                    }
                    break;
            }

        });

        btnEliminarFiltro.setOnClickListener(v -> {
            int filtroSeleccionado = spFiltros.getSelectedItemPosition();

            switch(filtroSeleccionado){
                case 0:
                    if(!(layValorInferior.getVisibility() == View.VISIBLE)){
                        App.getInstance().mostrarMensajeFlotante("No se está usando ese filtro");
                    } else {
                        layValorSuperior.setVisibility(View.GONE);
                        layValorInferior.setVisibility(View.GONE);
                    }
                    break;
                case 1:
                    if(!(layFechaDesdeFiltro.getVisibility() == View.VISIBLE)){
                        App.getInstance().mostrarMensajeFlotante("No se está usando ese filtro");
                    } else {
                        layFechaDesdeFiltro.setVisibility(View.GONE);
                        layFechaHastaFiltro.setVisibility(View.GONE);
                    }
                    break;
                case 2:
                    if(!(layTipoInsulinaFiltro.getVisibility() == View.VISIBLE)){
                        App.getInstance().mostrarMensajeFlotante("No se está usando ese filtro");
                    } else {
                        layTipoInsulinaFiltro.setVisibility(View.GONE);
                    }
                    break;
            }

        });

    }

    private void volverAtrasSinFiltros(){
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }


    private void volverAtrasConFiltros(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra(FECHA_DESDE, fechaDesde);
        returnIntent.putExtra(FECHA_HASTA, fechaHasta);
        returnIntent.putExtra(CANTIDAD_DESDE, cantidadDeInsulinaInferior);
        returnIntent.putExtra(CANTIDAD_HASTA, cantidadDeInsulinaSuperior);
        returnIntent.putExtra(TIPO_INSULINA, tipoDeInsulina);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

}
