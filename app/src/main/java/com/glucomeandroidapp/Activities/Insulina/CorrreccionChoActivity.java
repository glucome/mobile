package com.glucomeandroidapp.Activities.Insulina;

import android.os.Bundle;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.CustomComponents.EditText_Numerico;
import com.glucomeandroidapp.DAO.FactoresCorreccionInsulinaDAO;
import com.glucomeandroidapp.Logic.CalculadoraInsulinaManagement;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.CONFIRMACION_INSULINA_TITULO;

public class CorrreccionChoActivity extends BaseActivity {

    @BindView(R.id.lbl_peso)
    TextView lblPeso;
    @BindView(R.id.btn_calcular)
    TextView btnCalcular;
    @BindView(R.id.input_cho)
    EditText_Numerico txtCarbohidratos;
    @BindView(R.id.lbl_factor_correccion)
    TextView lblFactorCorreccion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_correccion_cho);

        ButterKnife.bind(this);

        super.inicializar(R.string.calculadora_de_insulina);

        double peso = AccountDataManager.obtenerDatosPaciente().getPeso();
        lblPeso.setText(String.format(Locale.getDefault(), "Peso: %.2f kg.", peso));

        double factorCHO = FactoresCorreccionInsulinaDAO.obtener().getFactorCorreccionCHO();
        lblFactorCorreccion.setText(String.format(Locale.getDefault(), "Su factor de corrección es de %.2f gr. por unidad de insulina.", factorCHO));

        // Accion botón Calcular Insulina
        btnCalcular.setOnClickListener(v -> {
            if (!txtCarbohidratos.getText().toString().isEmpty()) {


                double resultado = CalculadoraInsulinaManagement.CalcularInsulinaPreComida(
                        Double.valueOf(txtCarbohidratos.getText().toString()),
                        factorCHO);

                String resultadoAMostrar = "Usted debería inyectarse " +
                        String.format(Locale.getDefault(), "%.2f (%d unidades)", resultado, (int) Math.ceil(resultado));

                LoadingDialogManager.crearDialogConfirmacion(this, CONFIRMACION_INSULINA_TITULO, resultadoAMostrar);

            }
        });
    }
}
