package com.glucomeandroidapp.Activities.Insulina;

import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Controladores.InyeccionesControlador;
import com.glucomeandroidapp.CustomComponents.EditText_Numerico;
import com.glucomeandroidapp.POJO.APIClasses.Requests.InyeccionRequest;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoInsulina;
import com.glucomeandroidapp.POJO.Inyeccion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;

public class InyeccionInsulinaActivity extends BaseActivity {

    @BindView(R.id.valor_de_inyeccion)
    EditText_Numerico valorInyeccion;
    @BindView(R.id.spinner_tipo_insulina)
    Spinner tipoInsulina;
    @BindView(R.id.fecha_de_inyeccion)
    DatePicker fechaInyeccion;
    @BindView(R.id.hora_de_inyeccion)
    TimePicker horaInyeccion;
    @BindView(R.id.btn_registrar_inyeccion)
    TextView btnGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inyeccion_insulina);

        ButterKnife.bind(this);

        super.inicializar(R.string.inyeccion);

        // Logica

        btnGuardar.setOnClickListener(v -> {

            String inyeccion = valorInyeccion.getText().toString();

            String fechaDeCreacion = UtilesFecha.formatearFecha(fechaInyeccion, horaInyeccion);

            int tipo = tipoInsulina.getSelectedItemPosition();

            Inyeccion nuevaInyeccion = new Inyeccion(null, null,
                    UUID.randomUUID().toString(), Double.valueOf(inyeccion),
                    TipoInsulina.getById(tipo), fechaDeCreacion);

            DataBaseManager.getDaoSession().getInyeccionDao().insert(nuevaInyeccion);


            InyeccionRequest inyeccionRequest = new InyeccionRequest(
                    nuevaInyeccion.getFechaDeCreacion(),
                    nuevaInyeccion.getValorInyeccion().intValue(),
                    nuevaInyeccion.getTipoInsulina().ordinal());

            //Envío la medición al backend
            LoadingDialogManager.mostrar(InyeccionInsulinaActivity.this);
            InyeccionesControlador.enviarInyeccion(inyeccionRequest, nuevaInyeccion);

            finish();

        });


    }

}
