package com.glucomeandroidapp.Activities.Insulina;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Logic.CalculadoraInsulinaManagement;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CalculcadoraInsulinaActivity extends BaseActivity {

    @BindView(R.id.lbl_peso) TextView lblPeso;
    @BindView(R.id.btn_calcular_insulina_cho) TextView btnCalcularInsulinaCHO;
    @BindView(R.id.btn_calcular_insulina_azucar) TextView btnCalcularInsulinaAzucar;
    @BindView(R.id.input_cho) EditText txtCHO;
    @BindView(R.id.input_azucar_actual) EditText txtAzucarActual;
    @BindView(R.id.input_azucar_objetivo) EditText txtAzucarObjetivo;
    @BindView(R.id.lbl_insulina_cho_resultado) TextView lblInsulinaCHO;
    @BindView(R.id.lbl_insulina_azucar_resultado) TextView lblInsulinaAzucar;
    @BindView(R.id.np_factor_cho) NumberPicker npFactorCHO;
    @BindView(R.id.np_factor_azucar) NumberPicker npFactorAzucar;
    @BindView(R.id.lbl_factor_CHO) TextView lblFactorCHO;
    @BindView(R.id.lbl_factor_azucar) TextView lblFactorAzucar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculcadora_insulina);

        ButterKnife.bind(this);

        super.inicializar(R.string.calculadora_de_insulina);

        double peso = AccountDataManager.obtenerDatosPaciente().getPeso();
        lblPeso.setText(String.format(Locale.getDefault(), "Peso: %.2f kg.", peso));

        // Factores
        double factorCHO = CalculadoraInsulinaManagement.CalcularFactorCHOInsulina(peso);
        double factorAzucar = CalculadoraInsulinaManagement.CalcularFactorAzucarInsulina(peso);

        // Configurar NumberPickers
        npFactorCHO.setMinValue(6);
        npFactorCHO.setMaxValue(24);
        npFactorCHO.setValue(((int) factorCHO));
        npFactorCHO.setWrapSelectorWheel(true);
        lblFactorCHO.setText(String.format(Locale.getDefault(),
                "Carbohidratos x u. de Insulina (Sugerido: %.0f gr)",
                factorCHO));

        npFactorAzucar.setMinValue(20);
        npFactorAzucar.setMaxValue(80);
        npFactorAzucar.setValue((int) factorAzucar);
        npFactorAzucar.setWrapSelectorWheel(true);
        lblFactorAzucar.setText(String.format(Locale.getDefault(),
                "Glucemia x u. de Insulina (Sugerido: %.0f mg/dl)",
                factorAzucar));




    }
}
