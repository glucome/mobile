package com.glucomeandroidapp.Activities.Insulina;

import android.os.Bundle;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Controladores.PacientesControlador;
import com.glucomeandroidapp.CustomComponents.EditText_Numerico;
import com.glucomeandroidapp.DAO.FactoresCorreccionInsulinaDAO;
import com.glucomeandroidapp.Logic.CalculadoraInsulinaManagement;
import com.glucomeandroidapp.POJO.APIClasses.Requests.FactorCorreccionInsulinaRequest;
import com.glucomeandroidapp.POJO.FactoresCorreccionInsulina;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;


public class FactoresCorreccionInsulinaActivity extends BaseActivity {


    @BindView(R.id.lbl_factor_azucar)
    EditText_Numerico valorFactorAzucar;
    @BindView(R.id.lbl_factor_cho)
    EditText_Numerico valorFactorCho;
    @BindView(R.id.lbl_factor_correccion_azucar)
    TextView lblFactorCorreccionAzucar;
    @BindView(R.id.lbl_factor_correccion_cho)
    TextView lblFactorCorreccionCho;
    @BindView(R.id.btn_almacenar_factores)
    TextView btnGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factores_correccion_insulina);

        ButterKnife.bind(this);

        super.inicializar(R.string.configuracion);

        // Logica
        // Se realiza el calculo de los factores sugeridos segun su peso
        double peso = AccountDataManager.obtenerDatosPaciente().getPeso();
        double factorAzucar = CalculadoraInsulinaManagement.CalcularFactorAzucarInsulina(peso);
        double factorCHO = CalculadoraInsulinaManagement.CalcularFactorCHOInsulina(peso);
        lblFactorCorreccionAzucar.setText(String.format(Locale.getDefault(), "Su factor de corrección de azúcar sugerido es de %.2f mg/dl. por unidad de insulina.", factorAzucar));
        lblFactorCorreccionCho.setText(String.format(Locale.getDefault(), "Su factor de corrección de carbohidratos sugerido es de %.2f gr. por unidad de insulina.", factorCHO));

        // En caso de existir una configuración, mostrarla
        FactoresCorreccionInsulina factoresCorreccionInsulinaActuales = FactoresCorreccionInsulinaDAO.obtener();
        if(factoresCorreccionInsulinaActuales != null){
            valorFactorAzucar.setText(factoresCorreccionInsulinaActuales.getFactorCorreccionAzucar().intValue());
            valorFactorCho.setText(factoresCorreccionInsulinaActuales.getFactorCorreccionCHO().intValue());
        }

        btnGuardar.setOnClickListener(v -> {

            String factorCorreccionAzucar = valorFactorAzucar.getText().toString();
            String factorCorreccionCHO = valorFactorCho.getText().toString();

            FactoresCorreccionInsulina factoresCorreccionInsulina = new FactoresCorreccionInsulina(
                    null, null, Double.valueOf(factorCorreccionAzucar), Double.valueOf(factorCorreccionCHO));

            FactoresCorreccionInsulinaDAO.insertar(factoresCorreccionInsulina);

            FactorCorreccionInsulinaRequest factorCorreccionInsulinaRequest = new FactorCorreccionInsulinaRequest(
                    factoresCorreccionInsulina.getFactorCorreccionAzucar(),
                    factoresCorreccionInsulina.getFactorCorreccionCHO());

            LoadingDialogManager.mostrar(FactoresCorreccionInsulinaActivity.this);

            PacientesControlador.guardarFactoresCorreccion(factorCorreccionInsulinaRequest, factoresCorreccionInsulina);

            finish();

        });




    }

}
