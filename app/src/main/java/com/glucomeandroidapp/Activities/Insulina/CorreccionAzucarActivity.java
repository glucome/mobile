package com.glucomeandroidapp.Activities.Insulina;

import android.os.Bundle;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.CustomComponents.EditText_Numerico;
import com.glucomeandroidapp.DAO.FactoresCorreccionInsulinaDAO;
import com.glucomeandroidapp.DAO.MedicionesDAO;
import com.glucomeandroidapp.Logic.CalculadoraInsulinaManagement;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.CONFIRMACION_INSULINA_TITULO;

public class CorreccionAzucarActivity extends BaseActivity {

    @BindView(R.id.lbl_peso)
    TextView lblPeso;
    @BindView(R.id.btn_calcular)
    TextView btnCalcular;
    @BindView(R.id.input_azucar_actual)
    EditText_Numerico txtAzucarActual;
    @BindView(R.id.input_azucar_objetivo)
    EditText_Numerico txtAzucarObjetivo;
    @BindView(R.id.lbl_factor_correccion)
    TextView lblFactorCorreccion;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_correccion_azucar);

        ButterKnife.bind(this);

        super.inicializar(R.string.calculadora_de_insulina);

        double peso = AccountDataManager.obtenerDatosPaciente().getPeso();
        lblPeso.setText(String.format(Locale.getDefault(), "Peso: %.2f kg.", peso));

        double factorAzucar = FactoresCorreccionInsulinaDAO.obtener().getFactorCorreccionAzucar();
        lblFactorCorreccion.setText(String.format(Locale.getDefault(), "Su factor de corrección es de %.2f mg/dl. por unidad de insulina.", factorAzucar));

        Medicion medicion = MedicionesDAO.getUltimaMedicion();
        txtAzucarActual.setText(medicion != null ? medicion.getValorMedicion().intValue() : 150);

        // Accion botón Calcular Insulina Azucar
        btnCalcular.setOnClickListener(v -> {
            if (!txtAzucarActual.getText().toString().isEmpty() &&
                    !txtAzucarObjetivo.getText().toString().isEmpty()) {



                double resultado = CalculadoraInsulinaManagement.CalcularInsulinaCorreccionAzucar(
                        Double.valueOf(txtAzucarActual.getText().toString()),
                        Double.valueOf(txtAzucarObjetivo.getText().toString()),
                        factorAzucar);


                String resultadoAMostrar = "Usted debería inyectarse " +
                        String.format(Locale.getDefault(), "%.2f (%d unidades)", resultado, (int) Math.ceil(resultado));

                LoadingDialogManager.crearDialogConfirmacion(this, CONFIRMACION_INSULINA_TITULO, resultadoAMostrar);
            }
        });
    }
}
