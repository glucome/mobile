package com.glucomeandroidapp.Activities.Allegados;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Controladores.SolicitudesControlador;
import com.glucomeandroidapp.POJO.APIClasses.Requests.SolicitudRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.EstadoSolicitud;
import com.glucomeandroidapp.POJO.SolicitudVinculacion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.ImagenLoader;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.ALLEGADO_SELECCIONADO;
import static com.glucomeandroidapp.Utils.Util.SOLICITUD_ALLEGADO;
import static com.glucomeandroidapp.Utils.Util.URL_BASE;
import static com.glucomeandroidapp.Utils.Util.URL_IMAGEN_ALLEGADO;

public class AllegadoSeleccionadoActivity extends BaseActivity {

    @BindView(R.id.img_allegado)
    ImageView imagen;
    @BindView(R.id.lbl_nombre_allegado)
    TextView nombreAllegado;
    @BindView(R.id.btn_agregar_allegado)
    TextView agregarAllegado;
    @BindView(R.id.btn_cancelar_solicitud)
    TextView cancelarSolicitud;
    @BindView(R.id.btn_borrar_allegado)
    TextView borrarAllegado;

    AllegadoDataResponse allegado;
    SolicitudVinculacion solicitudVinculacion;
    Activity activity = this;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allegado_seleccionado);
        ButterKnife.bind(this);
        super.inicializar(R.string.allegado);

        allegado = obtenerBundle(ALLEGADO_SELECCIONADO, AllegadoDataResponse.class);
        solicitudVinculacion = obtenerBundle(SOLICITUD_ALLEGADO, SolicitudVinculacion.class);

        agregarAllegado.setVisibility(View.GONE);
        cancelarSolicitud.setVisibility(View.GONE);
        borrarAllegado.setVisibility(View.GONE);


        if (solicitudVinculacion == null) {
            agregarAllegado.setVisibility(View.VISIBLE);
        } else {
            EstadoSolicitud estado = EstadoSolicitud.get(solicitudVinculacion.getEstado());
            if (estado == EstadoSolicitud.PENDIENTE) {
                cancelarSolicitud.setVisibility(View.VISIBLE);
            } else {
                borrarAllegado.setVisibility(View.VISIBLE);
            }
        }
        context = activity.getApplicationContext();

        String url = URL_BASE + URL_IMAGEN_ALLEGADO + allegado.getIdUsuario() + "/";

        new ImagenLoader(imagen).execute(url);

        nombreAllegado.setText(allegado.getNombre());


        // info.setText(alimento.getDescripcion());

        agregarAllegado.setOnClickListener(v -> {
            PacienteDataResponse datosDelPaciente = AccountDataManager.obtenerDatosPaciente();
            SolicitudRequest solicitud = new SolicitudRequest(datosDelPaciente.getId(), allegado.getId());
            LoadingDialogManager.mostrar(activity);
            SolicitudesControlador.agregarAllegado(solicitud);
            finish();
        });

        cancelarSolicitud.setOnClickListener(v -> {
            LoadingDialogManager.mostrar(activity);
            SolicitudesControlador.cancelarSolicitud(solicitudVinculacion);
            finish();
        });

        borrarAllegado.setOnClickListener(v -> {
//            LoginResponse<PacienteDataResponse> datosDelUsuario =
//                    AccountDataManager.obtenerDatosUsuario();
            PacienteDataResponse datosDelPaciente = AccountDataManager.obtenerDatosPaciente();
            SolicitudRequest solicitud = new SolicitudRequest(datosDelPaciente.getId(), allegado.getId());
            LoadingDialogManager.mostrar(this);
            SolicitudesControlador.borrarAllegado(solicitud);
            finish();
        });
    }

    public void cambiarVisibilidad(String solicitud) {
        if (solicitud.equals("Cancelar") || solicitud.equals("Borrar")) {
            agregarAllegado.setVisibility(View.VISIBLE);
            borrarAllegado.setVisibility(View.GONE);
            cancelarSolicitud.setVisibility(View.GONE);
        } else if (solicitud.equals("Agregar")) {
            agregarAllegado.setVisibility(View.GONE);
            borrarAllegado.setVisibility(View.GONE);
            cancelarSolicitud.setVisibility(View.VISIBLE);
        }
    }

}
