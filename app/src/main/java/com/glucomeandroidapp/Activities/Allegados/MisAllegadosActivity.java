package com.glucomeandroidapp.Activities.Allegados;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.Adapters.BusquedaAllegadosAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.AllegadosControlador;
import com.glucomeandroidapp.Controladores.SolicitudesControlador;
import com.glucomeandroidapp.POJO.APIClasses.Requests.SolicitudRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoSolicitud;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.LoadingDialogManager.prepararDialogSiNo;
import static com.glucomeandroidapp.Utils.Util.BORRAR_ALLEGADO_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.BORRAR_ALLEGADO_TITULO;
import static com.glucomeandroidapp.Utils.Util.MIS_ALLEGADOS_LIST;

public class MisAllegadosActivity extends BaseActivity {

    @BindView(R.id.mis_allegados_list) RecyclerView list;

    List<AllegadoDataResponse> allegados = new ArrayList<>();

    Activity activity;

    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_allegados);
        ButterKnife.bind(this);
        super.inicializar(R.string.menu_allegados_items);
        activity = this;

        allegados = obtenerListaBundle(MIS_ALLEGADOS_LIST, AllegadoDataResponse.class);

        BusquedaAllegadosAdapter busquedaAllegadosAdapter = new BusquedaAllegadosAdapter(allegados);
        RecyclerView recyclerView = list;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(busquedaAllegadosAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        AllegadoDataResponse allegado = allegados.get(position);
                        LoadingDialogManager.mostrar(activity);
                        AllegadosControlador.getSolicitud(allegado, TipoSolicitud.ALLEGADO_PACIENTE);

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        AllegadoDataResponse allegado = allegados.get(position);

                        AlertDialog dialog = prepararDialogSiNo(activity, BORRAR_ALLEGADO_TITULO, BORRAR_ALLEGADO_DESCRIPCION);

                        dialog.findViewById(R.id.button_no).setOnClickListener(v -> dialog.dismiss());
                        dialog.findViewById(R.id.button_si).setOnClickListener(v -> {
                            PacienteDataResponse datosDelPaciente = AccountDataManager.obtenerDatosPaciente();
                            SolicitudRequest solicitud = new SolicitudRequest(datosDelPaciente.getId(), allegado.getId());
                            LoadingDialogManager.mostrar(activity);
                            SolicitudesControlador.borrarAllegado(solicitud);
                            App.getInstance().iniciarActividad(MenuPrincipalActivity.class);
                            finish();
                        });
                    }
                })
        );

    }

}
