package com.glucomeandroidapp.Activities.Allegados;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.BusquedaAllegadosAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.AllegadosControlador;
import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoSolicitud;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.BUSQUEDA_VACIA;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;

public class BuscarAllegadoActivity extends BaseActivity {

    @BindView(R.id.input_nombre) EditText nombre;
    @BindView(R.id.btn_buscar) TextView btnBuscar;
    @BindView(R.id.lista_resultados) RecyclerView list;

    Activity activity = this;
    List<AllegadoDataResponse> allegados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_nombre_generic);

        ButterKnife.bind(this);

        super.inicializar(R.string.menu_allegados_items);

        btnBuscar.setOnClickListener(v -> {

            if (!nombre.getText().toString().isEmpty()) {

                LoadingDialogManager.mostrar(activity);

                AllegadosControlador.obtenerAllegadosPorNombre(nombre.getText().toString(), respuesta -> {
                    BusquedaAllegadosAdapter busquedaAllegadosAdapter = new BusquedaAllegadosAdapter(respuesta);
                    RecyclerView recyclerView = list;
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setHasFixedSize(true);
                    allegados = respuesta;
                    recyclerView.setAdapter(busquedaAllegadosAdapter);
                    recyclerView.addOnItemTouchListener(
                            new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    AllegadoDataResponse allegado = allegados.get(position);
                                    LoadingDialogManager.mostrar(activity);
                                    AllegadosControlador.getSolicitud(allegado, TipoSolicitud.ALLEGADO_PACIENTE);

                                }

                                @Override
                                public void onLongItemClick(View view, int position) {

                                }
                            })
                    );
                    recyclerView.setHasFixedSize(true);
                });


            } else {
                ((App) getApplication()).mostrarMensajeFlotante(BUSQUEDA_VACIA);
            }
        });
    }


}
