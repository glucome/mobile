package com.glucomeandroidapp.Activities.Allegados;

import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.AllegadosControlador;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;

public class MenuAllegadosActivity extends BaseActivity {

    @BindView(R.id.rc_menu) RecyclerView rcMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_generic);
        ButterKnife.bind(this);

        super.inicializar(R.string.menu_allegados);

        MenuAdapter menuAdapter;
        ArrayList<MenuAdapterModel> menuAdapterModelArrayList;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(App.getContext());
        rcMenu.setLayoutManager(layoutManager);
        rcMenu.setItemAnimator(new DefaultItemAnimator());

        menuAdapterModelArrayList = new ArrayList<>();

        int[] iconos = {R.drawable.ic_nuevo, R.drawable.ic_busqueda};
        String[] titulos = getResources().getStringArray(R.array.menu_allegados);

        for(int i=0; i<iconos.length; i++){
            MenuAdapterModel view = new MenuAdapterModel(
                    titulos[i],
                    iconos[i]);
            menuAdapterModelArrayList.add(view);
        }

        menuAdapter = new MenuAdapter(this, menuAdapterModelArrayList) {
            @Override
            public void ejecutarAccion(int position) {
                switch (position) {
                    case 0:
                        App.getInstance().iniciarActividad(BuscarAllegadoActivity.class, null);
                        break;
                    case 1:
                        AllegadosControlador.getAllegados();
                        break;
                    default:
                        break;
                }
            }
        };

        rcMenu.setAdapter(menuAdapter);

    }
}
