package com.glucomeandroidapp.Activities.Informacion;

import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuInformacionActivity extends BaseActivity {

    @BindView(R.id.rc_menu)
    RecyclerView rcMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_generic);
        ButterKnife.bind(this);

        super.inicializar(R.string.titulo_informacion_menues);

        MenuAdapter menuAdapter;
        ArrayList<MenuAdapterModel> menuAdapterModelArrayList;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(App.getContext());
        rcMenu.setLayoutManager(layoutManager);
        rcMenu.setItemAnimator(new DefaultItemAnimator());

        menuAdapterModelArrayList = new ArrayList<>();

        int[] iconos = {R.drawable.ic_info, R.drawable.ic_tipo_1, R.drawable.ic_tipo_2, R.drawable.ic_gestacional, R.drawable.ic_tratamiento};
        String[] titulos = getResources().getStringArray(R.array.menu_informacion);

        for (int i = 0; i < iconos.length; i++) {
            MenuAdapterModel view = new MenuAdapterModel(
                    titulos[i],
                    iconos[i]);
            menuAdapterModelArrayList.add(view);
        }

        menuAdapter = new MenuAdapter(this, menuAdapterModelArrayList) {
            @Override
            public void ejecutarAccion(int position) {
                switch (position) {
                    case 0:
                        App.getInstance().iniciarActividad(InformacionGeneralActivity.class);
                        break;
                    case 1:
                        App.getInstance().iniciarActividad(InformacionTipo1Activity.class);
                        break;
                    case 2:
                        App.getInstance().iniciarActividad(InformacionTipo2Activity.class);
                        break;
                    case 3:
                        App.getInstance().iniciarActividad(InformacionGestacionalActivity.class);
                        break;
                    case 4:
                        App.getInstance().iniciarActividad(MenuTratamientoActivity.class);
                        break;
                    default:
                        break;
                }
            }
        };

        rcMenu.setAdapter(menuAdapter);

    }
}
