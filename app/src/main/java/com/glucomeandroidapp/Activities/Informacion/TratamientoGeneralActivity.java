package com.glucomeandroidapp.Activities.Informacion;

import android.os.Bundle;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TratamientoGeneralActivity extends BaseActivity {
    @BindView(R.id.btn_mas_info)
    TextView btnMasInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tratamiento_general);
        ButterKnife.bind(this);

        super.inicializar(R.string.titulo_informacion_menues);

        btnMasInfo.setOnClickListener(v -> {
            App.getInstance().iniciarBusquedaWeb("Tratamiento diabetes");
        });
    }
}
