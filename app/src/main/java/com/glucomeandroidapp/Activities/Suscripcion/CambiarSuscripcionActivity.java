package com.glucomeandroidapp.Activities.Suscripcion;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Controladores.SuscripcionControlador;
import com.glucomeandroidapp.CustomComponents.MyTextView_Roboto_Regular;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.Rol;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.LoadingDialogManager.prepararDialogSiNo;
import static com.glucomeandroidapp.Utils.Util.CAMBIAR_SUSCRIPCION_BOTON;
import static com.glucomeandroidapp.Utils.Util.CAMBIAR_SUSCRIPCION_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.CAMBIAR_SUSCRIPCION_TITULO;
import static com.glucomeandroidapp.Utils.Util.CANCELAR_SUSCRIPCION_BOTON;
import static com.glucomeandroidapp.Utils.Util.CANCELAR_SUSCRIPCION_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.CANCELAR_SUSCRIPCION_TITULO;

public class CambiarSuscripcionActivity extends BaseActivity {

    @BindView(R.id.btn_cambiar_suscripcion) MyTextView_Roboto_Regular btnCambiarSuscripcion;

    Activity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambio_suscripcion);

        ButterKnife.bind(this);

        super.inicializar("Cambio de Suscripción");

        LoginResponse datosUsuario = AccountDataManager.obtenerDatosUsuario();
        boolean esBasico = datosUsuario.getRoles().contains(Rol.PACIENTE.getNombre());

        if(esBasico){
            btnCambiarSuscripcion.setText(CAMBIAR_SUSCRIPCION_BOTON);
            btnCambiarSuscripcion.setBackground(getDrawable(R.drawable.rect1));
            btnCambiarSuscripcion.setOnClickListener(v -> {

                AlertDialog dialog = prepararDialogSiNo(activity, CAMBIAR_SUSCRIPCION_TITULO, CAMBIAR_SUSCRIPCION_DESCRIPCION);

                dialog.findViewById(R.id.button_no).setOnClickListener(x -> dialog.dismiss());
                dialog.findViewById(R.id.button_si).setOnClickListener(x ->
                        SuscripcionControlador.cambiarRolUsuario(datosUsuario, activity));

            });
        } else {
            btnCambiarSuscripcion.setText(CANCELAR_SUSCRIPCION_BOTON);
            btnCambiarSuscripcion.setBackground(getDrawable(R.drawable.rect1_danger));
            btnCambiarSuscripcion.setOnClickListener(v -> {
                AlertDialog dialog = prepararDialogSiNo(activity, CANCELAR_SUSCRIPCION_TITULO, CANCELAR_SUSCRIPCION_DESCRIPCION);

                dialog.findViewById(R.id.button_no).setOnClickListener(x -> dialog.dismiss());
                dialog.findViewById(R.id.button_si).setOnClickListener(x ->
                        SuscripcionControlador.cambiarRolUsuario(datosUsuario, activity));

            });
        }
    }
}
