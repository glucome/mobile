package com.glucomeandroidapp.Activities.TarjetaAumentada;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.DownloadTask;
import com.glucomeandroidapp.Utils.QrManager;
import com.glucomeandroidapp.Utils.Util;
import com.google.zxing.WriterException;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MiTarjetaAumentadaActivity extends BaseActivity {

    @BindView(R.id.img_mi_tarjeta_aumentada)
    ImageView imgTarjetaAumentada;
    @BindView(R.id.btn_guardar)
    TextView btnGuardar;
    @BindView(R.id.btn_compartir)
    TextView btnCompartir;

    private static final String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/tarjeta_aumentada.pdf";

    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_tarjeta_aumentada);
        ButterKnife.bind(this);

        super.inicializar(R.string.mi_tarjeta_aumentada);

        activity = this;

        try {
            Bitmap qrCode = QrManager.encodeAsBitmap(String.valueOf(AccountDataManager.obtenerIdPaciente()));
            imgTarjetaAumentada.setImageBitmap(qrCode);

            btnGuardar.setOnClickListener(this::onClickGuardar);

            btnCompartir.setOnClickListener(this::onClickCompartir);
        } catch (WriterException e) {
            App.getInstance().mostrarMensajeFlotante("Error al generar mi tarjeta aumentada.");
        }
    }

    private void onClickGuardar(View v) {
        // Check permission
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                       // generarPdf();
                        new DownloadTask(activity.getApplicationContext(), Util.URL_DESCARGA_TARJETA+AccountDataManager.obtenerIdPaciente()+"/", false,activity);

                        App.getInstance().mostrarMensajeFlotante("El documento se almacenó en la carpeta de Descargas.");
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        finish();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .check();
    }

    private void onClickCompartir(View v) {
        // Check permission
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {

                        new DownloadTask(activity.getApplicationContext(), Util.URL_DESCARGA_TARJETA+AccountDataManager.obtenerIdPaciente()+"/", true,activity);

                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        finish();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .check();
    }

}
