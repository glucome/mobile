package com.glucomeandroidapp.Activities.TarjetaAumentada;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.PacientesControlador;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.glucomeandroidapp.Utils.Util;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;

public class ScanQrActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {


    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        final Activity activity = this;
        // Check permission
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // Programmatically initialize the scanner view
                        mScannerView = new ZXingScannerView(activity);
                        // Set the scanner view as the content view
                        setContentView(mScannerView);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        finish();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .check();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mScannerView != null) {
            // Register ourselves as a handler for scan results.
            mScannerView.setResultHandler(this);
            // Start camera on resume
            mScannerView.startCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        // Stop camera on pause
        if (mScannerView != null)
            mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        LoadingDialogManager.mostrar(this);
        try {
            PacientesControlador.obtenerInfoTarjetaAumentada(Integer.valueOf(rawResult.getText()));
        } catch (Exception ex){
            Log.e(Util.TAG, "Excepción al parsear el qr");
            App.getInstance().mostrarMensajeFlotante("No se pudo leer el QR correctamente");
            LoadingDialogManager.ocultar();
            finish();
        }
    }
}
