package com.glucomeandroidapp.Activities.TarjetaAumentada;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.glucomeandroidapp.Activities.Custom.BundleHandlerActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.APIClasses.Response.InfoTarjetaAumentadaResponse;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.R;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.Frame;
import com.google.ar.sceneform.FrameTime;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.glucomeandroidapp.Utils.Util.PACIENTE_DATA_BUNDLE;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_ID_BUNDLE;

public class TarjetaAumentadaActivity extends BundleHandlerActivity {

    private TarjetaAumentadaFragment arFragment;
    private ImageView fitToScanView;

    //InfoTarjetaAumentadaResponse pacienteData = null;
    List<Medicion> mediciones = null;

    // Augmented image and its associated center pose anchor, keyed by the augmented image in
    // the database.
    private final Map<AugmentedImage, TarjetaAumentadaNode> augmentedImageMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tarjeta_aumentada);

        //pacienteData = obtenerBundle(PACIENTE_DATA_BUNDLE, InfoTarjetaAumentadaResponse.class);
        String pacienteId = obtenerBundle(PACIENTE_ID_BUNDLE, String.class);
        mediciones = obtenerListaBundle(PACIENTE_DATA_BUNDLE, Medicion.class);

        arFragment = (TarjetaAumentadaFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);
        arFragment.setIdPaciente(pacienteId);

        fitToScanView = findViewById(R.id.image_view_fit_to_scan);

        arFragment.getArSceneView().getScene().addOnUpdateListener(this::onUpdateFrame);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (augmentedImageMap.isEmpty()) {
            fitToScanView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Registered with the Sceneform Scene object, this method is called at the start of each frame.
     *
     * @param frameTime - time since last frame.
     */
    private void onUpdateFrame(FrameTime frameTime) {
        Frame frame = arFragment.getArSceneView().getArFrame();

        // If there is no frame, just return.
        if (frame == null) {
            return;
        }

        Collection<AugmentedImage> updatedAugmentedImages =
                frame.getUpdatedTrackables(AugmentedImage.class);
        for (AugmentedImage augmentedImage : updatedAugmentedImages) {
            switch (augmentedImage.getTrackingState()) {
                case PAUSED:
                    // When an image is in PAUSED state, but the camera is not PAUSED, it has been detected,
                    // but not yet tracked.
                    String text = "Detectando tarjeta..."; //+ augmentedImage.getIndex();
                    App.getInstance().mostrarMensajeFlotante(text);
                    break;

                case TRACKING:
                    // Have to switch to UI Thread to update View.
                    fitToScanView.setVisibility(View.GONE);

                    // Create a new anchor for newly found images.
                    if (!augmentedImageMap.containsKey(augmentedImage)) {
                        //TarjetaAumentadaNode node = new TarjetaAumentadaNode(this, arFragment, pacienteData.getPaciente());
                        TarjetaAumentadaNode node = new TarjetaAumentadaNode(this, arFragment, mediciones);
                        node.setImage(augmentedImage);
                        augmentedImageMap.put(augmentedImage, node);
                        arFragment.getArSceneView().getScene().addChild(node);
                    }
                    break;

                case STOPPED:
                    augmentedImageMap.remove(augmentedImage);
                    break;
            }
        }
    }
}
