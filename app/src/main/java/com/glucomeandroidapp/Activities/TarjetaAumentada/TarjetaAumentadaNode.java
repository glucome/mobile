package com.glucomeandroidapp.Activities.TarjetaAumentada;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Mediciones.HistorialDeMedicionesActivity;
import com.glucomeandroidapp.Adapters.HistorialMedicionAdapter;
import com.glucomeandroidapp.Adapters.Models.HistorialMedicionAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.Sexo;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoDiabetes;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.R;
import com.google.ar.core.AugmentedImage;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.google.ar.sceneform.ux.ArFragment;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"AndroidApiChecker"})
public class TarjetaAumentadaNode extends AnchorNode {

    private static final String TAG = "AugmentedImageNode";

    // The augmented image represented by this node.
    private AugmentedImage image;
    private Context context;
    //private PacienteDataResponse paciente;
    private ArFragment arFragment;
    private List<Medicion> mediciones;

    //public TarjetaAumentadaNode(Context context, ArFragment arFragment, PacienteDataResponse paciente) {
    public TarjetaAumentadaNode(Context context, ArFragment arFragment, List<Medicion> mediciones) {
        this.context = context;
        this.mediciones= mediciones;
        this.arFragment = arFragment;
    }

    /**
     * Called when the AugmentedImage is detected and should be rendered. A Sceneform node tree is
     * created based on an Anchor created from the image. The corners are then positioned based on the
     * extents of the image. There is no need to worry about world coordinates since everything is
     * relative to the center of the image, which is the parent node of the corners.
     */
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    public void setImage(AugmentedImage image) {
        this.image = image;

        // Set the anchor based on the center of the image.
        setAnchor(image.createAnchor(image.getCenterPose()));

        AnchorNode anchorNode = this;


        ViewRenderable.builder().setView(context, R.layout.component_tarjeta_aumentada)
                .setVerticalAlignment(ViewRenderable.VerticalAlignment.CENTER)
                .build()
                .thenAccept(viewRenderable -> {


                    Node nameView = new Node();

                    //nameView.setLocalPosition(new Vector3(0, 0, (float) viewRenderable.getView().getHeight() / 2));

                    Quaternion q1 = nameView.getLocalRotation();
                    Quaternion q2 = Quaternion.axisAngle(Vector3.left(), 90);
                    nameView.setLocalRotation(Quaternion.multiply(q1, q2));

                    nameView.setParent(anchorNode);
                    nameView.setRenderable(viewRenderable);
                    nameView.setLocalScale(new Vector3(0.15f, 0.15f, 0.15f));

                    View view = viewRenderable.getView();

                    ArrayList<HistorialMedicionAdapterModel> historial_medicion_modelArrayList = new ArrayList<>();

                    for (Medicion medicion : mediciones) {
                        HistorialMedicionAdapterModel viewNew = new HistorialMedicionAdapterModel(
                                medicion.getValorMedicion(),
                                medicion.getMomento().getNombre(),
                                medicion.getFechaDeCreacion());
                        historial_medicion_modelArrayList.add(viewNew);
                    }

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(App.getContext());
                    RecyclerView recyclerView = view.findViewById(R.id.rc_menu);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());

                    HistorialMedicionAdapter historial_medicion_adapter = new HistorialMedicionAdapter(historial_medicion_modelArrayList);
                    recyclerView.setAdapter(historial_medicion_adapter);

/*
                    TextView txtName = view.findViewById(R.id.txt_valor_nombre);
                    txtName.setText(paciente.getNombre());

                    TextView txtAltura = view.findViewById(R.id.txt_valor_altura);
                    txtAltura.setText(String.valueOf(paciente.getAltura()));

                    TextView txtPeso = view.findViewById(R.id.txt_peso);
                    txtPeso.setText(String.valueOf(paciente.getPeso()));

                    TextView txtSexo = view.findViewById(R.id.txt_sexo);
                    txtSexo.setText(Sexo.getById(Integer.valueOf(paciente.getSexo())).getNombre());

                    TextView txtFechaNacimiento = view.findViewById(R.id.txt_fecha_de_nacimiento);
                    txtFechaNacimiento.setText(paciente.getFechaNacimiento());

                    TextView txtTipoDiabetes = view.findViewById(R.id.txt_tipo_diabetes);
                    txtTipoDiabetes.setText(TipoDiabetes.getById(Integer.valueOf(paciente.getTipoDiabetes())).getNombre());

 */

                });
    }

    public AugmentedImage getImage() {
        return image;
    }
}

