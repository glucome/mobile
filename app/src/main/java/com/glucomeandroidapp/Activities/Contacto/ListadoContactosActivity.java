package com.glucomeandroidapp.Activities.Contacto;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.Adapters.ContactosAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.ContactoControlador;
import com.glucomeandroidapp.Controladores.SolicitudesControlador;
import com.glucomeandroidapp.POJO.APIClasses.Requests.SolicitudRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Contacto;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.LoadingDialogManager.prepararDialogSiNo;
import static com.glucomeandroidapp.Utils.Util.BORRAR_ALLEGADO_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.BORRAR_ALLEGADO_TITULO;
import static com.glucomeandroidapp.Utils.Util.BORRAR_CONTACTO_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.BORRAR_CONTACTO_TITULO;
import static com.glucomeandroidapp.Utils.Util.CONTACTOS_LIST;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;

public class ListadoContactosActivity extends BaseActivity {

    @BindView(R.id.contactos_list)
    RecyclerView list;
    @BindView(R.id.btn_agregar_contactos) TextView btnAgregar;

    List<Contacto> contactos = new ArrayList<>();

    private Activity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_contactos);

        ButterKnife.bind(this);

        super.inicializar(R.string.listado_contactos);

        this.activity = this;

        contactos = obtenerListaBundle( CONTACTOS_LIST, Contacto.class);

        ContactosAdapter contactosAdapter = new ContactosAdapter(contactos);
        RecyclerView recyclerView = list;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(contactosAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {}

                    @Override public void onLongItemClick(View view, int position) {
                        Contacto contacto = contactos.get(position);

                        AlertDialog dialog = prepararDialogSiNo(activity, BORRAR_CONTACTO_TITULO, BORRAR_CONTACTO_DESCRIPCION);

                        dialog.findViewById(R.id.button_no).setOnClickListener(v -> dialog.dismiss());
                        dialog.findViewById(R.id.button_si).setOnClickListener(v -> {
                            DataBaseManager.getDaoSession().getContactoDao().delete(contacto);
                            ContactoControlador.borrarContacto(contacto);
                            ((App)activity.getApplication()).iniciarActividad(MenuPrincipalActivity.class, null);
                            ((App)activity.getApplication()).mostrarMensajeFlotante("Contacto borrado correctamente");
                        });
                    }
                })
        );

        btnAgregar.setOnClickListener(v -> {
            ((App)activity.getApplication()).iniciarActividad(AgregarContactoActivity.class, null);
        });
    }
}
