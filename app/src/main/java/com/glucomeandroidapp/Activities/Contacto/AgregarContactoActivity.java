package com.glucomeandroidapp.Activities.Contacto;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.ContactoControlador;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ContactoRequest;
import com.glucomeandroidapp.POJO.Contacto;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;
import static com.glucomeandroidapp.Utils.Util.FALLO_DATOS_CONTACTO;

public class AgregarContactoActivity extends BaseActivity {

    @BindView(R.id.input_nombre) EditText nombre;
    @BindView(R.id.input_telefono) EditText telefono;
    @BindView(R.id.btn_grabar) TextView btnGrabar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_contacto);


        ButterKnife.bind(this);

        super.inicializar(R.string.agregar_contacto);


        btnGrabar.setOnClickListener(v -> {

            String nombre = this.nombre.getText().toString();
            String telefono = this.telefono.getText().toString();

            if(nombre == "" || telefono == ""){
                App.getInstance().mostrarMensajeFlotante(FALLO_DATOS_CONTACTO);
                return;
            }

            Contacto contacto = new Contacto(nombre,telefono);

            // Guardo la nueva medicion en la BD
            DataBaseManager.getDaoSession().getContactoDao().insert(contacto);

            ContactoRequest contactoRequest = new ContactoRequest(nombre,telefono);

            //Envío la medición al backend
            LoadingDialogManager.mostrar(this);
            ContactoControlador.enviarContacto(contacto,contactoRequest);

            ((App) this.getApplication()).iniciarActividad(MenuPrincipalActivity.class, null);

        });
    }
}
