package com.glucomeandroidapp.Activities.Calibracion;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.Adapters.CalibracionDispositivoAdapter;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Logic.CalibracionManagement;
import com.glucomeandroidapp.POJO.Calibracion.Calibracion;
import com.glucomeandroidapp.POJO.Calibracion.ItemCalibracion;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.FINALIZACION_CALIBRACION_COMPLETA;
import static com.glucomeandroidapp.Utils.Util.FINALIZACION_CALIBRACION_INCOMPLETA;

public class CalibracionDispositivoActivity extends BaseActivity {

    @BindView(R.id.calibraciones_dispositivo_list) RecyclerView recyclerView;

    Calibracion calibracion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibracion_dispositivo);

        ButterKnife.bind(this);

        super.inicializar(R.string.menu_realizar_calibraciones);
    }

    @Override
    protected void onStart() {
        super.onStart();

        //SIEMPRE debería haber solo UNA instancia del objeto Calibracion
        calibracion = DataBaseManager.getDaoSession().getCalibracionDao().load(1L);
        if(calibracion == null){
            calibracion = new Calibracion();
            calibracion.setId(1L);
            List<ItemCalibracion> itemsCalibracion = new ArrayList<ItemCalibracion>();
            for(MomentoMedicion momento : MomentoMedicion.values()){
                ItemCalibracion itemCalibracion = new ItemCalibracion();
                itemCalibracion.setIdCalibracion(1L);
                itemCalibracion.setMomentoMedicion(momento);
                itemCalibracion.setFechaDeMedicionCalibracion("");
                itemCalibracion.setMedicionInvasiva(-1);
                itemsCalibracion.add(itemCalibracion);
                DataBaseManager.getDaoSession().getItemCalibracionDao().insertOrReplace(itemCalibracion);
            }
            DataBaseManager.getDaoSession().getCalibracionDao().insertOrReplace(calibracion);
        }

        CalibracionDispositivoAdapter calibracionesDispositivosAdapter =
                new CalibracionDispositivoAdapter(calibracion.getItemCalibracionList());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(calibracionesDispositivosAdapter);
    }

    public void onFinalizarCalibracion(View v) {
        if(calibracion.cargaFinalizada()){
            CalibracionManagement.calcularCalibracion(calibracion);
            ((App)getApplication()).mostrarMensajeFlotante(FINALIZACION_CALIBRACION_COMPLETA);
            App.getInstance().iniciarActividad(MenuPrincipalActivity.class);
        } else {
            ((App)getApplication()).mostrarMensajeFlotante(FINALIZACION_CALIBRACION_INCOMPLETA);
        }
    }
}
