package com.glucomeandroidapp.Activities.Calibracion;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.DAO.CalibracionDAO;
import com.glucomeandroidapp.POJO.Calibracion.Calibracion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.MENU_CALIBRACION_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.MENU_CALIBRACION_TITULO;

public class MenuCalibracionActivity extends BaseActivity {

    @BindView(R.id.rc_menu)
    RecyclerView rcMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_generic);
        ButterKnife.bind(this);

        super.inicializar(R.string.menu_calibracion);

        MenuAdapter menuAdapter;
        ArrayList<MenuAdapterModel> menuAdapterModelArrayList;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(App.getContext());
        rcMenu.setLayoutManager(layoutManager);
        rcMenu.setItemAnimator(new DefaultItemAnimator());

        menuAdapterModelArrayList = new ArrayList<>();

        int[] iconos = {R.drawable.ic_calibracion, R.drawable.ic_horario};
        String[] titulos = getResources().getStringArray(R.array.menu_calibracion);

        for (int i = 0; i < iconos.length; i++) {
            MenuAdapterModel view = new MenuAdapterModel(
                    titulos[i],
                    iconos[i]);
            menuAdapterModelArrayList.add(view);
        }

        menuAdapter = new MenuAdapter(this, menuAdapterModelArrayList) {
            @Override
            public void ejecutarAccion(int position) {
                switch (position) {
                    case 0:
                        Calibracion calibracion = DataBaseManager.getDaoSession().getCalibracionDao().load(1L);
                        if (calibracion == null || !calibracion.cargaFinalizada()) {
                            App.getInstance().iniciarActividad(CalibracionDispositivoActivity.class);
                        } else {
                            AlertDialog dialog = LoadingDialogManager.prepararDialogSiNo(activity, MENU_CALIBRACION_TITULO, MENU_CALIBRACION_DESCRIPCION);
                            dialog.findViewById(R.id.button_no).setOnClickListener(v -> dialog.dismiss());
                            dialog.findViewById(R.id.button_si).setOnClickListener(v -> {
                                CalibracionDAO.borrarCalibracion();
                                App.getInstance().iniciarActividad(CalibracionDispositivoActivity.class);
                            });

                        }
                        break;
                    case 1:
                        App.getInstance().iniciarActividad(HorariosCalibracionDispositivoActivity.class);
                        break;
                    default:
                        break;
                }
            }
        };

        rcMenu.setAdapter(menuAdapter);

    }
}
