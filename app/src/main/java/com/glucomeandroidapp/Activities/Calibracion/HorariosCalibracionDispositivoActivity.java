package com.glucomeandroidapp.Activities.Calibracion;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.TimePicker;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.Calibracion.HorarioCalibracion;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.POJO.Notificacion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.GUARDADO_EXITOSO_HORARIOS_CALIBRACION;
import static com.glucomeandroidapp.Utils.Util.HORARIOS_CALIBRACION;
import static com.glucomeandroidapp.Utils.Util.TEXTO_CALIBRACION;
import static com.glucomeandroidapp.Utils.Util.TITULO_CALIBRACION;
import static com.glucomeandroidapp.Utils.UtilesFunciones.cancelarNotificacion;
import static com.glucomeandroidapp.Utils.UtilesFunciones.programarNotificacion;
import static java.lang.Math.toIntExact;

public class HorariosCalibracionDispositivoActivity extends BaseActivity {

    @BindView(R.id.horario_calibracion_1) TimePicker horarioCalibracionAntesDesayuno;
    @BindView(R.id.horario_calibracion_2) TimePicker horarioCalibracionDespuesDesayuno;
    @BindView(R.id.horario_calibracion_3) TimePicker horarioCalibracionAntesAlmuerzo;
    @BindView(R.id.horario_calibracion_4) TimePicker horarioCalibracionDespuesAlmuerzo;
    @BindView(R.id.horario_calibracion_5) TimePicker horarioCalibracionAntesMerienda;
    @BindView(R.id.horario_calibracion_6) TimePicker horarioCalibracionDespuesMerienda;
    @BindView(R.id.horario_calibracion_7) TimePicker horarioCalibracionAntesCena;
    @BindView(R.id.horario_calibracion_8) TimePicker horarioCalibracionDespuesCena;
    @BindView(R.id.horario_calibracion_9) TimePicker horarioCalibracionMadrugada;
    @BindView(R.id.lbl_nombre_horario_calibracion_1) TextView nombreHorarioCalibracionAntesDesayuno;
    @BindView(R.id.lbl_nombre_horario_calibracion_2) TextView nombreHorarioCalibracionDespuesDesayuno;
    @BindView(R.id.lbl_nombre_horario_calibracion_3) TextView nombreHorarioCalibracionAntesAlmuerzo;
    @BindView(R.id.lbl_nombre_horario_calibracion_4) TextView nombreHorarioCalibracionDespuesAlmuerzo;
    @BindView(R.id.lbl_nombre_horario_calibracion_5) TextView nombreHorarioCalibracionAntesMerienda;
    @BindView(R.id.lbl_nombre_horario_calibracion_6) TextView nombreHorarioCalibracionDespuesMerienda;
    @BindView(R.id.lbl_nombre_horario_calibracion_7) TextView nombreHorarioCalibracionAntesCena;
    @BindView(R.id.lbl_nombre_horario_calibracion_8) TextView nombreHorarioCalibracionDespuesCena;
    @BindView(R.id.lbl_nombre_horario_calibracion_9) TextView nombreHorarioCalibracionMadrugada;
    @BindView(R.id.btn_grabar_horarios_calibracion) TextView btnGrabar;

    private List<HorarioCalibracion> horariosCalibracion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horarios_calibracion_dispositivo);

        ButterKnife.bind(this);

        super.inicializar(R.string.calibracion);

        horariosCalibracion = DataBaseManager.getDaoSession().getHorarioCalibracionDao().loadAll();
        if (horariosCalibracion.isEmpty()) {
            horariosCalibracion = HorarioCalibracion.obtenerHorarios();
        }
        inicializarTimePickersYTextos();

        btnGrabar.setOnClickListener(v -> {
            for(HorarioCalibracion horario : horariosCalibracion){
                switch (horario.getMomentoMedicion()){
                    case ANTES_DESAYUNO:
                        grabarHorarioYNotificacion(horario, horarioCalibracionAntesDesayuno);
                        break;
                    case DESPUES_DESAYUNO:
                        grabarHorarioYNotificacion(horario, horarioCalibracionDespuesDesayuno);
                        break;
                    case ANTES_ALMUERZO:
                        grabarHorarioYNotificacion(horario, horarioCalibracionAntesAlmuerzo);
                        break;
                    case DESPUES_ALMUERZO:
                        grabarHorarioYNotificacion(horario, horarioCalibracionDespuesAlmuerzo);
                        break;
                    case ANTES_MERIENDA:
                        grabarHorarioYNotificacion(horario, horarioCalibracionAntesMerienda);
                        break;
                    case DESPUES_MERIENDA:
                        grabarHorarioYNotificacion(horario, horarioCalibracionDespuesMerienda);
                        break;
                    case ANTES_CENA:
                        grabarHorarioYNotificacion(horario, horarioCalibracionAntesCena);
                        break;
                    case DESPUES_CENA:
                        grabarHorarioYNotificacion(horario, horarioCalibracionDespuesCena);
                        break;
                    case NA:
                        grabarHorarioYNotificacion(horario, horarioCalibracionMadrugada);
                        break;
                }
            }
            ((App)getApplication()).mostrarMensajeFlotante(GUARDADO_EXITOSO_HORARIOS_CALIBRACION);
        });
    }

    public void armarNotificacion(Long id, TimePicker tiempo, MomentoMedicion momento){
        String texto = (momento != MomentoMedicion.NA ? momento.getNombre() : "Madrugada") + TEXTO_CALIBRACION;
        programarNotificacion(getApplicationContext(), id.intValue(), TITULO_CALIBRACION,
                texto, tiempo.getHour(), tiempo.getMinute(), id.intValue(), false, "");
    }

    public void grabarHorarioYNotificacion(HorarioCalibracion horario, TimePicker timePicker){
        if(horario.getNotificacionId() != 0)
            cancelarNotificacion(getApplicationContext(), toIntExact(horario.getNotificacion().getId()));
        horario.setHora(timePicker.getHour());
        horario.setMinuto(timePicker.getMinute());
        Notificacion notificacion = new Notificacion();
        notificacion.setTipoNotificacion(HORARIOS_CALIBRACION);
        DataBaseManager.getDaoSession().getNotificacionDao().insertOrReplace(notificacion);
        horario.setNotificacion(notificacion);
        armarNotificacion(notificacion.getId(), timePicker, horario.getMomentoMedicion());
        DataBaseManager.getDaoSession().getHorarioCalibracionDao().insertOrReplace(horario);
    }

    public void inicializarTimePickersYTextos(){
        for(HorarioCalibracion horario : horariosCalibracion){
            switch (horario.getMomentoMedicion()){
                case ANTES_DESAYUNO:
                    horarioCalibracionAntesDesayuno.setIs24HourView(true);
                    nombreHorarioCalibracionAntesDesayuno.setText(MomentoMedicion.ANTES_DESAYUNO.getNombre());
                    if(horario.getId() != null){
                        horarioCalibracionAntesDesayuno.setHour(horario.getHora());
                        horarioCalibracionAntesDesayuno.setMinute(horario.getMinuto());
                    }
                    break;
                case DESPUES_DESAYUNO:
                    horarioCalibracionDespuesDesayuno.setIs24HourView(true);
                    nombreHorarioCalibracionDespuesDesayuno.setText(MomentoMedicion.DESPUES_DESAYUNO.getNombre());
                    if(horario.getId() != null){
                        horarioCalibracionDespuesDesayuno.setHour(horario.getHora());
                        horarioCalibracionDespuesDesayuno.setMinute(horario.getMinuto());
                    }
                    break;
                case ANTES_ALMUERZO:
                    horarioCalibracionAntesAlmuerzo.setIs24HourView(true);
                    nombreHorarioCalibracionAntesAlmuerzo.setText(MomentoMedicion.ANTES_ALMUERZO.getNombre());
                    if(horario.getId() != null){
                        horarioCalibracionAntesAlmuerzo.setHour(horario.getHora());
                        horarioCalibracionAntesAlmuerzo.setMinute(horario.getMinuto());
                    }
                    break;
                case DESPUES_ALMUERZO:
                    horarioCalibracionDespuesAlmuerzo.setIs24HourView(true);
                    nombreHorarioCalibracionDespuesAlmuerzo.setText(MomentoMedicion.DESPUES_ALMUERZO.getNombre());
                    if(horario.getId() != null){
                        horarioCalibracionDespuesAlmuerzo.setHour(horario.getHora());
                        horarioCalibracionDespuesAlmuerzo.setMinute(horario.getMinuto());
                    }
                    break;
                case ANTES_MERIENDA:
                    horarioCalibracionAntesMerienda.setIs24HourView(true);
                    nombreHorarioCalibracionAntesMerienda.setText(MomentoMedicion.ANTES_MERIENDA.getNombre());
                    if(horario.getId() != null){
                        horarioCalibracionAntesMerienda.setHour(horario.getHora());
                        horarioCalibracionAntesMerienda.setMinute(horario.getMinuto());
                    }
                    break;
                case DESPUES_MERIENDA:
                    horarioCalibracionDespuesMerienda.setIs24HourView(true);
                    nombreHorarioCalibracionDespuesMerienda.setText(MomentoMedicion.DESPUES_MERIENDA.getNombre());
                    if(horario.getId() != null){
                        horarioCalibracionDespuesMerienda.setHour(horario.getHora());
                        horarioCalibracionDespuesMerienda.setMinute(horario.getMinuto());
                    }
                    break;
                case ANTES_CENA:
                    horarioCalibracionAntesCena.setIs24HourView(true);
                    nombreHorarioCalibracionAntesCena.setText(MomentoMedicion.ANTES_CENA.getNombre());
                    if(horario.getId() != null){
                        horarioCalibracionAntesCena.setHour(horario.getHora());
                        horarioCalibracionAntesCena.setMinute(horario.getMinuto());
                    }
                    break;
                case DESPUES_CENA:
                    horarioCalibracionDespuesCena.setIs24HourView(true);
                    nombreHorarioCalibracionDespuesCena.setText(MomentoMedicion.DESPUES_CENA.getNombre());
                    if(horario.getId() != null){
                        horarioCalibracionDespuesCena.setHour(horario.getHora());
                        horarioCalibracionDespuesCena.setMinute(horario.getMinuto());
                    }
                    break;
                case NA:
                    horarioCalibracionMadrugada.setIs24HourView(true);
                    nombreHorarioCalibracionMadrugada.setText("Madrugada");
                    if(horario.getId() != null){
                        horarioCalibracionMadrugada.setHour(horario.getHora());
                        horarioCalibracionMadrugada.setMinute(horario.getMinuto());
                    }
                    break;
            }
        }
    }


}
