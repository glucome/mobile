package com.glucomeandroidapp.Activities.Calibracion;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.Calibracion.ItemCalibracion;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.POJO.Medicion.MedicionDispositivo;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.time.LocalDateTime;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.FALLO_OBTENCION_MEDICION_CALIBRACION;
import static com.glucomeandroidapp.Utils.Util.GUARDADO_SIN_DATOS_CALIBRACION;
import static com.glucomeandroidapp.Utils.Util.ITEM_CALIBRACION_ID;
import static com.glucomeandroidapp.Utils.Util.MEDICION_OBTENIDA_CALIBRACION;

public class EditarItemCalibracionActivity extends BaseActivity {

    @BindView(R.id.input_medicion_invasiva) EditText inputMedicionInvasiva;
    @BindView(R.id.btn_finalizar_edicion_item_calibracion) TextView btnGrabar;
    @BindView(R.id.btn_obtener_medicion) TextView btnObtenerMedicion;

    private ItemCalibracion itemCalibracion;
    private MedicionDispositivo medicionDispositivo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibracion_dispositivo_edicion);

        ButterKnife.bind(this);

        super.inicializar(R.string.menu_realizar_calibraciones);

        long id = 0;
        Bundle bundle = getIntent().getBundleExtra(BUNDLE_NAME);
        if (bundle != null) {
            id = bundle.getLong(ITEM_CALIBRACION_ID);
        }

        itemCalibracion = DataBaseManager.getDaoSession().getItemCalibracionDao().load(id);
        txtNombrePantalla.setText(itemCalibracion.getMomentoMedicion() != MomentoMedicion.NA ? itemCalibracion.getMomentoMedicion().getNombre() : "Madrugada");

        btnObtenerMedicion.setOnClickListener(v -> {
            medicionDispositivo = new MedicionDispositivo();
            try{
                medicionDispositivo.obtenerMediciones();
                ((App)getApplication()).mostrarMensajeFlotante(MEDICION_OBTENIDA_CALIBRACION);
            } catch (Exception e) {
                ((App)getApplication()).mostrarMensajeFlotante(FALLO_OBTENCION_MEDICION_CALIBRACION);
            }
        });

        btnGrabar.setOnClickListener(v -> {
            if(medicionDispositivo != null && !inputMedicionInvasiva.getText().toString().equals("") && Integer.parseInt(inputMedicionInvasiva.getText().toString()) != 0){
                DataBaseManager.getDaoSession().getMedicionDispositivoDao().insertOrReplace(medicionDispositivo);
                itemCalibracion.setMedicionDispositivo(medicionDispositivo);
                itemCalibracion.setMedicionInvasiva(Integer.parseInt(inputMedicionInvasiva.getText().toString()));
                itemCalibracion.setFechaDeMedicionCalibracion(UtilesFecha.getStringFromLocalDateTime(LocalDateTime.now()));
                DataBaseManager.getDaoSession().getItemCalibracionDao().insertOrReplace(itemCalibracion);
//                ((App)getApplication()).iniciarActividad(
//                        CalibracionDispositivoActivity.class, null);
                finish();
            } else {
                ((App)getApplication()).mostrarMensajeFlotante(GUARDADO_SIN_DATOS_CALIBRACION);
            }
        });
    }


}
