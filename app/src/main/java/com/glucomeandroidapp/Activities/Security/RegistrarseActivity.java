package com.glucomeandroidapp.Activities.Security;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.glucomeandroidapp.Adapters.CustomAdapter;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.LoginYRegistroControlador;
import com.glucomeandroidapp.POJO.APIClasses.Requests.RegistroAllegadoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.RegistroPacienteRequest;
import com.glucomeandroidapp.POJO.Enumeraciones.Rol;
import com.glucomeandroidapp.POJO.Enumeraciones.Sexo;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoDiabetes;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoUsuario;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;
import static com.glucomeandroidapp.Utils.Util.FALLO_EL_REGISTRO;
import static com.glucomeandroidapp.Utils.Util.INPUT_EDAD;
import static com.glucomeandroidapp.Utils.Util.INPUT_EMAIL;
import static com.glucomeandroidapp.Utils.Util.INPUT_NOMBRE;
import static com.glucomeandroidapp.Utils.Util.INPUT_PASSWORD;
import static com.glucomeandroidapp.Utils.Util.REGISTRO_EXITOSO;
import static com.glucomeandroidapp.Utils.Util.TIPO_DE_ADAPTER_CON_IMAGEN;

/**
 * Activity para registrar un nuevo usuario.
 * <p>
 * Creado por MartinArtime el 24 de abril del 2019
 **/
public class RegistrarseActivity extends AppCompatActivity {

    @BindView(R.id.input_nya)
    EditText txtNombre;
    @BindView(R.id.input_email)
    EditText txtEmail;
    @BindView(R.id.input_usuario)
    EditText txtUsuario;
    @BindView(R.id.input_password)
    EditText txtPass;
    @BindView(R.id.spinner_rol)
    Spinner spinnerRol;
    @BindView(R.id.spinner_sexo)
    Spinner spinnerSexo;
    @BindView(R.id.spinner_tipo_de_diabetes)
    Spinner spinnerTipoDeDiabetes;
    @BindView(R.id.txt_fecha_de_nacimiento)
    TextView txtFechaNacimiento;
    @BindView(R.id.fecha_de_nacimiento)
    DatePicker datePkrFechaDeNacimiento;
    @BindView(R.id.peso)
    EditText txtPeso;
    @BindView(R.id.altura)
    EditText txtAltura;
    @BindView(R.id.btn_registrarse)
    TextView btnRegistrarse;
    @BindView(R.id.link_login)
    TextView linkLogin;
    @BindView(R.id.nombre_pantalla)
    TextView nombrePantalla;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.view_sexo)
    View viewSexo;
    @BindView(R.id.view_tipo_de_diabetes)
    View viewTipoDiabetes;
    @BindView(R.id.view_fecha_de_nacimiento)
    View viewFechaNacimiento;
    @BindView(R.id.view_peso)
    View viewPeso;
    @BindView(R.id.view_altura)
    View viewAltura;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);

        ButterKnife.bind(this);


        nombrePantalla.setText(R.string.nueva_cuenta);
        backButton.setOnClickListener(v -> {
            onBackPressed();
        });

        int[] fotosSpinnerPerfil = {R.drawable.ic_paciente, R.drawable.ic_allegado};
        int[] fotosSpinnerSexo = {R.drawable.ic_sexo_m, R.drawable.ic_sexo_f, R.drawable.ic_sexo_nn};
        int[] fotosSpinnerTipoDeDiabetes = {R.drawable.ic_tipo_1, R.drawable.ic_tipo_2, R.drawable.ic_na};

        // Inicializo los spinners con imágenes y textos correspondientes
        CustomAdapter customAdapterPerfil = new CustomAdapter(getApplicationContext(),
                fotosSpinnerPerfil, TipoUsuario.getTiposUsuarioRegistro(), TIPO_DE_ADAPTER_CON_IMAGEN);
        CustomAdapter customAdapterSexo = new CustomAdapter(getApplicationContext(),
                fotosSpinnerSexo, Sexo.getSexos(), TIPO_DE_ADAPTER_CON_IMAGEN);
        CustomAdapter customAdapterTipoDeDiabetes = new CustomAdapter(getApplicationContext(),
                fotosSpinnerTipoDeDiabetes, TipoDiabetes.getTiposDeDiabetes(), TIPO_DE_ADAPTER_CON_IMAGEN);

        spinnerRol.setAdapter(customAdapterPerfil);
        spinnerSexo.setAdapter(customAdapterSexo);
        spinnerTipoDeDiabetes.setAdapter(customAdapterTipoDeDiabetes);

        btnRegistrarse.setOnClickListener(v -> registrarse());

        linkLogin.setOnClickListener(v -> {
            // Cerrar el registro y volver al cambiarPass
            finish();
        });

        spinnerRol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                int index = arg0.getSelectedItemPosition();
                if (index == Rol.PACIENTE.ordinal()) {
                    spinnerSexo.setVisibility(View.VISIBLE);
                    spinnerTipoDeDiabetes.setVisibility(View.VISIBLE);
                    txtFechaNacimiento.setVisibility(View.VISIBLE);
                    datePkrFechaDeNacimiento.setVisibility(View.VISIBLE);
                    txtPeso.setVisibility(View.VISIBLE);
                    txtAltura.setVisibility(View.VISIBLE);
                    viewSexo.setVisibility(View.VISIBLE);
                    viewTipoDiabetes.setVisibility(View.VISIBLE);
                    viewFechaNacimiento.setVisibility(View.VISIBLE);
                    viewPeso.setVisibility(View.VISIBLE);
                    viewAltura.setVisibility(View.VISIBLE);
                } else {
                    spinnerSexo.setVisibility(View.GONE);
                    spinnerTipoDeDiabetes.setVisibility(View.GONE);
                    txtFechaNacimiento.setVisibility(View.GONE);
                    datePkrFechaDeNacimiento.setVisibility(View.GONE);
                    txtPeso.setVisibility(View.GONE);
                    txtAltura.setVisibility(View.GONE);
                    viewSexo.setVisibility(View.GONE);
                    viewTipoDiabetes.setVisibility(View.GONE);
                    viewFechaNacimiento.setVisibility(View.GONE);
                    viewPeso.setVisibility(View.GONE);
                    viewAltura.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    /**
     * Método donde se valida y genera el registro de un usuario nuevo
     */
    public void registrarse() {

        // Bloqueo el boton
        btnRegistrarse.setEnabled(false);

        // Obtengo todos los datos
        String nombre = txtNombre.getText().toString();
        String usuario = txtUsuario.getText().toString();
        String email = txtEmail.getText().toString();
        String password = txtPass.getText().toString();
        TipoUsuario tipoUsuario = TipoUsuario.getByOrdenRegistro(spinnerRol.getSelectedItemPosition());
        Rol rol = Rol.getBasicoPorTipoUsuario(tipoUsuario);
        String mes, dia;
        String nacimiento = null;
        int diaDeNacimiento, mesDeNacimiento, anioDeNacimiento;
        Sexo sexo = null;
        TipoDiabetes tipoDeDiabetes = null;

        double peso = 0.0;
        double altura = 0.0;

        if (rol.name().equals(Rol.PACIENTE.name())) {
            diaDeNacimiento = datePkrFechaDeNacimiento.getDayOfMonth();
            mesDeNacimiento = datePkrFechaDeNacimiento.getMonth() + 1;
            anioDeNacimiento = datePkrFechaDeNacimiento.getYear();

            // Le agrego el 0 antes para la comunicación con el backend
            mes = mesDeNacimiento < 10 ? "0" + mesDeNacimiento : String.valueOf(mesDeNacimiento);
            dia = diaDeNacimiento < 10 ? "0" + diaDeNacimiento : String.valueOf(diaDeNacimiento);
            nacimiento = dia + "/" + mes + "/" + anioDeNacimiento;

            // Genero fecha de nacimiento en formato fecha y calculo edad
        /*
        LocalDate fechaDeNacimiento = UtilesFecha.getLocalDateFromString(nacimiento);
        String edad = getEdadFromFechaDeNacimiento(fechaDeNacimiento);
         */

            if (!txtPeso.getText().toString().isEmpty()) {
                peso = Double.valueOf(txtPeso.getText().toString());
            }
            if (!txtAltura.getText().toString().isEmpty()) {
                altura = Double.valueOf(txtAltura.getText().toString());
            }


            sexo = Sexo.getById(spinnerSexo.getSelectedItemPosition());
            tipoDeDiabetes = TipoDiabetes.getById(
                    spinnerTipoDeDiabetes.getSelectedItemPosition());
        }

        // Valido input
        HashMap<String, String> input = new HashMap<>();
        input.put(INPUT_EMAIL, email);
        input.put(INPUT_PASSWORD, password);
        input.put(INPUT_NOMBRE, nombre);
        //input.put(INPUT_EDAD, edad);

        HashMap<String, String> respuesta = new HashMap<>();

        boolean valido = ((App) getApplication()).validarInput(input, respuesta);
        actualizarInput(respuesta);
        if (!valido) {
            App.getInstance().mostrarMensajeFlotante(FALLO_EL_REGISTRO);
            btnRegistrarse.setEnabled(true);
            return;
        }

        // Muestro el popup de carga
        LoadingDialogManager.mostrar(this);


        // Mando la info al backend
        if (tipoUsuario.getDescripcion().equals(TipoUsuario.PACIENTE.getDescripcion())) {
            RegistroPacienteRequest reg = new RegistroPacienteRequest(usuario, email, password,
                    nombre, altura, nacimiento, peso, sexo.ordinal(), tipoDeDiabetes.ordinal());
            LoginYRegistroControlador.enviarRegistro(reg, usuarioResponse -> {
                App.getInstance().mostrarMensajeFlotante(REGISTRO_EXITOSO);
                finish();
            }, error -> {
                App.getInstance().mostrarMensajeFlotante(error);
                btnRegistrarse.setEnabled(true);
            });
        }
        if (tipoUsuario.getDescripcion().equals(TipoUsuario.ALLEGADO.getDescripcion())) {
            RegistroAllegadoRequest reg = new RegistroAllegadoRequest(usuario, email, password, nombre);
            LoginYRegistroControlador.enviarRegistroAllegado(reg, usuarioResponse -> {
                App.getInstance().mostrarMensajeFlotante(REGISTRO_EXITOSO);
                finish();
            }, error -> {
                App.getInstance().mostrarMensajeFlotante(error);
                btnRegistrarse.setEnabled(true);
            });
        }
    }


    /**
     * Actualiza la vista correspondiente a la respuesta del validador
     *
     * @param respuesta, hashmap que tiene el nombre del input y la respuesta del validador
     */
    protected void actualizarInput(HashMap<String, String> respuesta) {
        for (Map.Entry<String, String> entry : respuesta.entrySet()) {
            String vista = entry.getKey();
            String dato = entry.getValue();

            switch (vista) {
                case INPUT_EMAIL:
                    txtEmail.setError(dato);
                    break;
                case INPUT_PASSWORD:
                    txtPass.setError(dato);
                    break;
                case INPUT_NOMBRE:
                    txtNombre.setError(dato);
                    break;
                case INPUT_EDAD:
                    //clndrFechaDeNacimiento.setError(dato);
                    break;
                default:
                    break;
            }
        }
    }

//    public void onSignupSuccess(List<String> roles) {
//        ((App) getApplication()).mostrarMensajeFlotante(REGISTRO_EXITOSO);
//        btnRegistrarse.setEnabled(true);
//        Intent i = new Intent();
//        i.putExtra(Util.ROLE, roles.get(0));
//        setResult(RESULT_OK, null);
//        finish();
//    }

//    public void onSignupFailed(String mensaje) {
//        ((App) getApplication()).mostrarMensajeFlotante(mensaje);
//        btnRegistrarse.setEnabled(true);
//    }
}
