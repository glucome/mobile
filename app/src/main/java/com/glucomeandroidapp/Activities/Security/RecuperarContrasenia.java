package com.glucomeandroidapp.Activities.Security;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.LoginYRegistroControlador;
import com.glucomeandroidapp.CustomComponents.EditText_Roboto_Regular;
import com.glucomeandroidapp.POJO.APIClasses.Requests.LoginRequest;
import com.glucomeandroidapp.R;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.CAMBIO_PASS_EXITOSO;
import static com.glucomeandroidapp.Utils.Util.FALLO_LA_SOLICITUD;
import static com.glucomeandroidapp.Utils.Util.INPUT_EMAIL;
import static com.glucomeandroidapp.Utils.Util.INPUT_PASSWORD;

/**
 * Activity inicial, tiene el cambiarPass y el registro.
 * <p>
 * Creado por MartinArtime el 24 de abril del 2019
 */
public class RecuperarContrasenia extends AppCompatActivity {

    @BindView(R.id.input_email_recuperacion) EditText_Roboto_Regular txtEmail;
    @BindView(R.id.input_pass) EditText_Roboto_Regular txtPass;
    @BindView(R.id.btn_recuperar_contrasenia) TextView btnRecuperarContrasenia;
    @BindView(R.id.nombre_pantalla) TextView nombrePantalla;
    @BindView(R.id.back_button) ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_contrasenia);

        ButterKnife.bind(this);

        backButton.setOnClickListener(v -> onBackPressed());

        nombrePantalla.setText(R.string.recuperar_contrasenia_titulo);

        btnRecuperarContrasenia.setOnClickListener(v -> cambiarPass());
    }

    /**
     * Usuario se intenta loguear
     */
    public void cambiarPass() {

        // Obtengo los datos ingresados
        String email = txtEmail.getText().toString();
        String nuevaPass = txtPass.getText().toString();

        // Valido los campos ingresados
        HashMap<String, String> input = new HashMap<>();
        input.put(INPUT_EMAIL, email);
        input.put(INPUT_PASSWORD, nuevaPass);

        HashMap<String, String> respuesta = new HashMap<>();

        boolean valido = App.getInstance().validarInput(input, respuesta);
        actualizarInput(respuesta);
        if (!valido) {
            App.getInstance().mostrarMensajeFlotante(FALLO_LA_SOLICITUD);
            return;
        }

        LoginRequest loginUsuario = new LoginRequest(email, nuevaPass);

        LoginYRegistroControlador.cambiarPass(loginUsuario, booleanResponse -> {
            if(booleanResponse.isValor()){

                App.getInstance().mostrarMensajeFlotante(CAMBIO_PASS_EXITOSO);
                App.getInstance().iniciarActividad(LoginActivity.class);

            } else {
                App.getInstance().mostrarMensajeFlotante(FALLO_LA_SOLICITUD);
            }
        }, error -> App.getInstance().mostrarMensajeFlotante(error));


    }


    /**
     * Actualiza la vista correspondiente a la respuesta del validador
     *
     * @param respuesta, hashmap que tiene el nombre del input y la respuesta del validador
     */
    protected void actualizarInput(HashMap<String, String> respuesta) {
        for (Map.Entry<String, String> entry : respuesta.entrySet()) {
            String vista = entry.getKey();
            String dato = entry.getValue();

            if (INPUT_EMAIL.equals(vista)) {
                txtEmail.setError(dato);
            }

            if (INPUT_PASSWORD.equals(vista)) {
                txtPass.setError(dato);
            }
        }
    }

}

