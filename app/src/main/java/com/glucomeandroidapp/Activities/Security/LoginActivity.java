package com.glucomeandroidapp.Activities.Security;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.LoginYRegistroControlador;
import com.glucomeandroidapp.POJO.APIClasses.Requests.LoginRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.glucomeandroidapp.Utils.Util;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.FALLO_EL_LOGIN;
import static com.glucomeandroidapp.Utils.Util.INPUT_EMAIL;
import static com.glucomeandroidapp.Utils.Util.INPUT_PASSWORD;

/**
 * Activity inicial, tiene el cambiarPass y el registro.
 * <p>
 * Creado por MartinArtime el 24 de abril del 2019
 */
public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.input_email) EditText txtEmail;
    @BindView(R.id.input_password) EditText txtPass;
    @BindView(R.id.btn_login) TextView btnLogin;
    @BindView(R.id.link_registrarse) TextView linkRegistrarse;
    @BindView(R.id.nombre_pantalla) TextView nombrePantalla;
    @BindView(R.id.btn_olvidaste_contraseña) TextView btnOlvidasteContraseña;

    private static final int REQUEST_SIGNUP = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        nombrePantalla.setText(R.string.iniciar_sesion);

        // Veo si el usuario ya se logueo antes, en ese caso lo dejo pasar directo.
        LoginResponse usuarioYaLogueado = AccountDataManager.obtenerDatosUsuario();

        if (usuarioYaLogueado != null) {
            App.getInstance().iniciarActividad(MenuPrincipalActivity.class, null);
        } else {
            new Thread() {
                @Override
                public void run() {
                    try {
                        Log.e(Util.TAG, "Borrando FirebaseInstanceID en Login");
                        FirebaseInstanceId.getInstance().deleteInstanceId();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            };
        }


        btnLogin.setOnClickListener(v -> login());

        btnOlvidasteContraseña.setOnClickListener(v -> App.getInstance().iniciarActividad(RecuperarContrasenia.class));

        linkRegistrarse.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), RegistrarseActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivityForResult(intent, REQUEST_SIGNUP);
        });
    }

    /**
     * Usuario se intenta loguear
     */
    public void login() {

        // Obtengo los datos ingresados
        String email = txtEmail.getText().toString();
        String password = txtPass.getText().toString();

        // Valido los campos ingresados
        HashMap<String, String> input = new HashMap<>();
        //input.put(INPUT_EMAIL, email);
        input.put(INPUT_PASSWORD, password);

        HashMap<String, String> respuesta = new HashMap<>();

        boolean valido = App.getInstance().validarInput(input, respuesta);
        actualizarInput(respuesta);
        if (!valido) {
            App.getInstance().mostrarMensajeFlotante(FALLO_EL_LOGIN);
            return;
        }

        // Bloqueo el botón
//        btnLogin.setEnabled(false);

        // Creo el popup de carga
        LoadingDialogManager.mostrar(this);

        // Envío la petición de cambiarPass
        LoginRequest loginUsuario = new LoginRequest(email, password);

        LoginYRegistroControlador.loginPaciente(loginUsuario, this);

    }


    /**
     * Actualiza la vista correspondiente a la respuesta del validador
     *
     * @param respuesta, hashmap que tiene el nombre del input y la respuesta del validador
     */
    protected void actualizarInput(HashMap<String, String> respuesta) {
        for (Map.Entry<String, String> entry : respuesta.entrySet()) {
            String vista = entry.getKey();
            String dato = entry.getValue();

            switch (vista) {
                case INPUT_EMAIL:
                    //txtEmail.setError(dato);
                    break;
                case INPUT_PASSWORD:
                    txtPass.setError(dato);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                App.getInstance().iniciarActividad(MenuPrincipalActivity.class);
            }
        }
    }

}
