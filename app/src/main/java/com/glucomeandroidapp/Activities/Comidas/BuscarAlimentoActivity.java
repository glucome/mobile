package com.glucomeandroidapp.Activities.Comidas;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.RecomendadorComidasAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.AlimentosControlador;
import com.glucomeandroidapp.POJO.Alimento;
import com.glucomeandroidapp.POJO.Enumeraciones.Rol;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.ALIMENTO;
import static com.glucomeandroidapp.Utils.Util.BUSQUEDA_VACIA_ALIMENTOS;
import static com.glucomeandroidapp.Utils.Util.COMIDAS_RECOMENDADAS;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;

public class BuscarAlimentoActivity extends BaseActivity {

    @BindView(R.id.input_nombre) EditText nombreAlimento;
    @BindView(R.id.btn_buscar) TextView btnBuscar;
    @BindView(R.id.lista_resultados) RecyclerView list;

    Context context;
    private BuscarAlimentoActivity activity;
    List<Alimento> listadoAlimentoResponse;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_buscar_nombre_generic);
        ButterKnife.bind(this);

        super.inicializar(R.string.menu_comida_items);

        activity = this;
        context = this.getApplicationContext();

        btnBuscar.setOnClickListener(v -> {

            if (!nombreAlimento.getText().toString().isEmpty()) {

                // Creo el popup de carga
                LoadingDialogManager.mostrar(activity);




                AlimentosControlador.obtenerAlimentosPorNombre(nombreAlimento.getText().toString(), respuesta -> {
                    RecomendadorComidasAdapter busquedaAlimentoAdapter = new RecomendadorComidasAdapter(respuesta);
                    RecyclerView recyclerView = list;
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setAdapter(busquedaAlimentoAdapter);
                    listadoAlimentoResponse = respuesta;
                    recyclerView.addOnItemTouchListener(
                            new RecyclerItemClickListener(context, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    String rol = AccountDataManager.obtenerRol();
                                    if (rol.equals(Rol.ALLEGADO.getNombre())){
                                        Alimento alimento = listadoAlimentoResponse.get(position);
                                        Bundle bundle = new Bundle();
                                        bundle.putString(COMIDAS_RECOMENDADAS, new Gson().toJson(alimento));
                                        //bundle.putString(BUNDLE_NAME, COMIDAS_RECOMENDADAS_BUNDLE);
                                        App.getInstance().iniciarActividad(EspecificacionComidaActivity.class, bundle);
                                    }else{
                                        Alimento alimento = listadoAlimentoResponse.get(position);
                                        Bundle bundle = new Bundle();
                                        bundle.putString(ALIMENTO, new Gson().toJson(alimento));
                                        App.getInstance().iniciarActividad(IngresoAlimentoActivity.class, bundle);
                                    }




                                }

                                @Override
                                public void onLongItemClick(View view, int position) {

                                }

                            })
                    );
                    recyclerView.setHasFixedSize(true);
                });

            } else {
                App.getInstance().mostrarMensajeFlotante(BUSQUEDA_VACIA_ALIMENTOS);
            }


        });

    }
}
