package com.glucomeandroidapp.Activities.Comidas;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.ImagePickerActivity;
import com.glucomeandroidapp.Controladores.AlimentosControlador;
import com.glucomeandroidapp.CustomComponents.EditText_Numerico;
import com.glucomeandroidapp.POJO.APIClasses.Requests.IngresoAlimentoRequest;
import com.glucomeandroidapp.POJO.Alimento;
import com.glucomeandroidapp.POJO.Alimentos.IngresoAlimento;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoAlimento;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.GlideApp;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.glucomeandroidapp.Utils.UtilesFecha;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.glucomeandroidapp.Utils.Util.ALIMENTO;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;
import static com.glucomeandroidapp.Utils.Util.INDICE_GLUCEMICO;
import static com.glucomeandroidapp.Utils.Util.PORCIONES_REFERENCIA;
import static com.glucomeandroidapp.Utils.Util.TAG;

public class IngresoAlimentoActivity extends BaseActivity {

    @BindView(R.id.lbl_titulo_ingreso_alimento)
    TextView tituloIngresoAlimento;
    @BindView(R.id.lbl_subtitulo_ingreso_alimento)
    TextView subtituloIngresoAlimento;
    @BindView(R.id.cantidad_comida)
    EditText_Numerico cantidadComida;
    @BindView(R.id.spinner_momento_alimento)
    Spinner momentoAlimento;
    @BindView(R.id.fecha_de_alimeto)
    DatePicker fechaAlimento;
    @BindView(R.id.hora_de_alimento)
    TimePicker horaAlimento;
    @BindView(R.id.observacion_comida)
    TextView txtObservacion;
    @BindView(R.id.btn_ingresar_alimeto)
    TextView btnGuardar;
    @BindView(R.id.porciones_referencia)
    TextView porcionesReferencia;
    @BindView(R.id.img_alimentoIngerido)
    ImageView img_alimentoIngerido;

    Alimento alimento;

    Activity activity;

    public static final int REQUEST_IMAGE = 100;

    private Bitmap imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso_alimento);

        ButterKnife.bind(this);

        super.inicializar(R.string.menu_comida_items);

        this.activity = this;


        alimento = obtenerBundle(ALIMENTO, Alimento.class);

        tituloIngresoAlimento.setText(alimento.getNombre() != null ? alimento.getNombre() : "");
        String txtIndiceGlucemico = INDICE_GLUCEMICO + " " + alimento.getIndiceGlucemico();
        subtituloIngresoAlimento.setText(txtIndiceGlucemico);
        porcionesReferencia.setText(PORCIONES_REFERENCIA + alimento.getUnidadReferencia());

        // Logica
        btnGuardar.setOnClickListener(v -> {

            String cantidad = cantidadComida.getText().toString();
            String observacion = txtObservacion.getText().toString();
            String fechaDeCreacion = UtilesFecha.formatearFecha(fechaAlimento, horaAlimento);
            int tipo = momentoAlimento.getSelectedItemPosition();

            IngresoAlimento ingresoAlimento = new IngresoAlimento(null, null, alimento.getId(),
                    Double.valueOf(cantidad), MomentoAlimento.getById(tipo), fechaDeCreacion, observacion, alimento.getNombre()
            );

            DataBaseManager.getDaoSession().getIngresoAlimentoDao().insert(ingresoAlimento);

            IngresoAlimentoRequest ingresoAlimentoRequest = new IngresoAlimentoRequest(
                    ingresoAlimento.getFechaDeCreacion(),
                    ingresoAlimento.getCantidad().intValue(),
                    ingresoAlimento.getMomentoAlimento().ordinal(),
                    ingresoAlimento.getIdAlimentoBackend(),
                    ingresoAlimento.getObservacion());

            //Envío la medición al backend
            LoadingDialogManager.mostrar(IngresoAlimentoActivity.this);
            try {
                AlimentosControlador.enviarIngresoAlimento(ingresoAlimentoRequest, ingresoAlimento, getFileImage());
            } catch (IOException e) {
                e.printStackTrace();
            }

            finish();
        });

    }

    private File getFileImage() throws IOException {
        if(imagen == null){
            return null;
        }

        File file = new File(activity.getApplication().getCodeCacheDir(), (alimento.getNombre() + " - " + LocalDateTime.now()).replaceAll(":","-").replace(".","-").concat(".png"));
        file.createNewFile();

//Convert bitmap to byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        imagen.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }


    @OnClick({R.id.fab_perfil, R.id.img_alimentoIngerido})
    void onProfileImageClick() {
        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(activity, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(activity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
                ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(activity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
                ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {

                    // Obtengo foto de perfil y la envío al servidor
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);

                    this.imagen = bitmap;

                    // Actualizo la vista con la foto
                    loadProfile(uri.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(getString(R.string.dialog_titulo_permisos));
        builder.setMessage(getString(R.string.dialog_mensaje_perisos));
        builder.setPositiveButton(getString(R.string.ir_a_configuracion), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(R.string.cancelar), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    private void loadProfile(String url) {
        Log.d(TAG, "Image cache path: " + url);

        GlideApp.with(this).load(url)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(img_alimentoIngerido);
        img_alimentoIngerido.setColorFilter(ContextCompat.getColor(activity, android.R.color.transparent));
    }

    // Llevo al usuario a la configuración
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

}
