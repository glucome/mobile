package com.glucomeandroidapp.Activities.Comidas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.CustomComponents.EditText_Roboto_Regular;
import com.glucomeandroidapp.CustomComponents.MyTextView_Roboto_Regular;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.FECHA_DESDE;
import static com.glucomeandroidapp.Utils.Util.FECHA_HASTA;
import static com.glucomeandroidapp.Utils.Util.MOMENTO_ALIMENTO;
import static com.glucomeandroidapp.Utils.Util.NOMBRE;

/**
 * Creado por MartinArtime el 12 de septiembre del 2019
 */
public class FiltrarHistorialAlimentosIngresadosActivity extends BaseActivity {

    @BindView(R.id.nombre_pantalla) protected TextView txtNombrePantalla;
    @BindView(R.id.back_button) protected ImageView btnAtras;
    @BindView(R.id.sp_filtros) Spinner spFiltros;
    @BindView(R.id.img_btn_agregar_filtro) protected ImageButton btnAgregarFiltro;
    @BindView(R.id.img_btn_eliminar_filtro) protected ImageButton btnEliminarFiltro;
    @BindView(R.id.lay_nombre_alimento_ingerido_filtro) protected LinearLayout layNombre;
    @BindView(R.id.input_filtro_nombre_alimento_ingerido) protected EditText_Roboto_Regular nombreFiltro;
    @BindView(R.id.lay_fecha_desde_filtro) protected LinearLayout layFechaDesdeFiltro;
    @BindView(R.id.fecha_desde_filtro) protected DatePicker fechaDesdeFiltro;
    @BindView(R.id.lay_fecha_hasta_filtro) protected LinearLayout layFechaHastaFiltro;
    @BindView(R.id.fecha_hasta_filtro) protected DatePicker fechaHastaFiltro;
    @BindView(R.id.lay_momento_filtro) protected LinearLayout layMomentoFiltro;
    @BindView(R.id.sp_momento_filtro) protected Spinner momentoFiltro;
    @BindView(R.id.btn_filtrar) protected MyTextView_Roboto_Regular btnFiltrar;

    String fechaDesde = null;
    String fechaHasta = null;
    String nombre = null;
    int momento = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtrar_historial_alimentos_ingresados);

        ButterKnife.bind(this);

        txtNombrePantalla.setText(getResources().getString(R.string.menu_filtrar));

        btnAtras.setOnClickListener(v -> volverAtrasSinFiltros());

        btnFiltrar.setOnClickListener(v -> {
            if(layNombre.getVisibility() == View.VISIBLE){
                nombre = Objects.requireNonNull(nombreFiltro.getText()).toString();
            }

            if(layFechaDesdeFiltro.getVisibility() == View.VISIBLE){
                fechaDesde = UtilesFecha.formatearSoloDate(fechaDesdeFiltro);
                fechaHasta = UtilesFecha.formatearSoloDate(fechaHastaFiltro);
            }

            if(layMomentoFiltro.getVisibility() == View.VISIBLE){
                momento = momentoFiltro.getSelectedItemPosition();
            }

            volverAtrasConFiltros();
        });

        btnAgregarFiltro.setOnClickListener(v -> {
            int filtroSeleccionado = spFiltros.getSelectedItemPosition();

            switch(filtroSeleccionado){
                case 0:
                    if(layNombre.getVisibility() == View.VISIBLE){
                        App.getInstance().mostrarMensajeFlotante("Ya ingreso ese filtro");
                    } else {
                        layNombre.setVisibility(View.VISIBLE);
                    }
                    break;
                case 1:
                    if(layFechaDesdeFiltro.getVisibility() == View.VISIBLE){
                        App.getInstance().mostrarMensajeFlotante("Ya ingreso ese filtro");
                    } else {
                        layFechaDesdeFiltro.setVisibility(View.VISIBLE);
                        layFechaHastaFiltro.setVisibility(View.VISIBLE);
                    }
                    break;
                case 2:
                    if(layMomentoFiltro.getVisibility() == View.VISIBLE){
                        App.getInstance().mostrarMensajeFlotante("Ya ingreso ese filtro");
                    } else {
                        layMomentoFiltro.setVisibility(View.VISIBLE);
                    }
                    break;
            }

        });

        btnEliminarFiltro.setOnClickListener(v -> {
            int filtroSeleccionado = spFiltros.getSelectedItemPosition();

            switch(filtroSeleccionado){
                case 0:
                    if(!(layNombre.getVisibility() == View.VISIBLE)){
                        App.getInstance().mostrarMensajeFlotante("No se está usando ese filtro");
                    } else {
                        layNombre.setVisibility(View.GONE);
                    }
                    break;
                case 1:
                    if(!(layFechaDesdeFiltro.getVisibility() == View.VISIBLE)){
                        App.getInstance().mostrarMensajeFlotante("No se está usando ese filtro");
                    } else {
                        layFechaDesdeFiltro.setVisibility(View.GONE);
                        layFechaHastaFiltro.setVisibility(View.GONE);
                    }
                    break;
                case 2:
                    if(!(layMomentoFiltro.getVisibility() == View.VISIBLE)){
                        App.getInstance().mostrarMensajeFlotante("No se está usando ese filtro");
                    } else {
                        layMomentoFiltro.setVisibility(View.GONE);
                    }
                    break;
            }

        });

    }

    private void volverAtrasSinFiltros(){
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }


    private void volverAtrasConFiltros(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra(FECHA_DESDE, fechaDesde);
        returnIntent.putExtra(FECHA_HASTA, fechaHasta);
        returnIntent.putExtra(NOMBRE, nombre);
        returnIntent.putExtra(MOMENTO_ALIMENTO, momento);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

}
