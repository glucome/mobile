package com.glucomeandroidapp.Activities.Comidas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.HistorialAlimentoIngresadoAdapter;
import com.glucomeandroidapp.Adapters.Models.HistorialAlimentoIngresadoAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.Alimentos.IngresoAlimento;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.FECHA_DESDE;
import static com.glucomeandroidapp.Utils.Util.FECHA_HASTA;
import static com.glucomeandroidapp.Utils.Util.LISTADO_ALIMENTOS;
import static com.glucomeandroidapp.Utils.Util.MOMENTO_ALIMENTO;
import static com.glucomeandroidapp.Utils.Util.NOMBRE;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_NOMBRE;
import static com.glucomeandroidapp.Utils.Util.TAG_ACTIVITY_FILTRO;

public class HistorialAlimentosIngresadosActivity extends BaseActivity {

    @BindView(R.id.recyclerview) RecyclerView recyclerView;
    @BindView(R.id.btn_cargar_mas) TextView botonCargarMas;
    @BindView(R.id.btn_filtrar) ImageButton btnFiltrar;
    @BindView(R.id.sv_historial) ScrollView svHistorial;
    @BindView(R.id.sv_sin_datos) ScrollView svSinDatos;
    @BindView(R.id.img_no_data) ImageView imgNoData;

    private List<IngresoAlimento> ingresoAlimentoList;
    public HistorialAlimentoIngresadoAdapter historialAlimentoIngresadoAdapter;

    private String titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_generic);

        ButterKnife.bind(this);

        titulo = obtenerBundle(PACIENTE_NOMBRE,String.class);

        btnFiltrar.setOnClickListener(v -> {
            Intent intent = new Intent(App.getContext(), FiltrarHistorialAlimentosIngresadosActivity.class);
            startActivityForResult(intent, TAG_ACTIVITY_FILTRO);
        });

        if(titulo != null && !titulo.equals("")){
            super.inicializar("Alimentos de " + titulo);
            ocultarBotonDeCarga();
        }else{
            super.inicializar(R.string.menu_historial_alimentos_ingresados);
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HistorialAlimentosIngresadosActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        ingresoAlimentoList = obtenerListaBundle(LISTADO_ALIMENTOS,IngresoAlimento.class);

        if(!ingresoAlimentoList.isEmpty()){
            agregarDatos(-1, null, null, null);
        } else {
            svSinDatos.setVisibility(View.VISIBLE);
            svHistorial.setVisibility(View.GONE);
            imgNoData.setImageResource(R.drawable.ic_comida100);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == TAG_ACTIVITY_FILTRO) {
            if(resultCode == Activity.RESULT_OK){
                String fechaDesde = data.getStringExtra(FECHA_DESDE);
                String fechaHasta = data.getStringExtra(FECHA_HASTA);
                String nombre = data.getStringExtra(NOMBRE);
                int momentoAlimento = data.getIntExtra(MOMENTO_ALIMENTO, -1);

                agregarDatos(momentoAlimento, nombre, fechaDesde, fechaHasta);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.e(Util.TAG, "No se pudo filtrar");
            }
        }
    }



    private void agregarDatos(int momentoAlimento, String nombre, String fechaDesde, String fechaHasta) {

        ArrayList<HistorialAlimentoIngresadoAdapterModel> historialAlimentoIngresadoAdapterModelArrayList = new ArrayList<>();

        if(ingresoAlimentoList != null){
            for(IngresoAlimento ingresoAlimento : ingresoAlimentoList){
                HistorialAlimentoIngresadoAdapterModel view = new HistorialAlimentoIngresadoAdapterModel(
                        ingresoAlimento.getIdAlimentoBackend(),
                        ingresoAlimento.getFechaDeCreacion(),
                        ingresoAlimento.getNombreAlimento(),
                        Integer.valueOf(ingresoAlimento.getCantidad().intValue()).toString(),
                        ingresoAlimento.getMomentoAlimento().getNombre());
                historialAlimentoIngresadoAdapterModelArrayList.add(view);
            }
        }

        historialAlimentoIngresadoAdapter = new HistorialAlimentoIngresadoAdapter(historialAlimentoIngresadoAdapterModelArrayList);
        historialAlimentoIngresadoAdapter.filter(momentoAlimento, nombre, fechaDesde, fechaHasta);
        recyclerView.setAdapter(historialAlimentoIngresadoAdapter);

    }

    private void ocultarBotonDeCarga() {
        botonCargarMas.setVisibility(View.GONE);
    }
}
