package com.glucomeandroidapp.Activities.Comidas;

import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.RecomendadorDeComidasControlador;
import com.glucomeandroidapp.DAO.IngresoAlimentoDAO;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;
import static com.glucomeandroidapp.Utils.Util.LISTADO_ALIMENTOS;

public class MenuComidasActivity extends BaseActivity {

    @BindView(R.id.rc_menu) RecyclerView rcMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_generic);
        ButterKnife.bind(this);

        super.inicializar(R.string.menu_comida);

        MenuAdapter menuAdapter;
        ArrayList<MenuAdapterModel> menuAdapterModelArrayList;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(App.getContext());
        rcMenu.setLayoutManager(layoutManager);
        rcMenu.setItemAnimator(new DefaultItemAnimator());

        menuAdapterModelArrayList = new ArrayList<>();

        int[] iconos = {R.drawable.ic_nuevo,    R.drawable.ic_estrella,    R.drawable.ic_horario};
        String[] titulos = getResources().getStringArray(R.array.menu_comidas);

        for(int i=0; i<iconos.length; i++){
            MenuAdapterModel view = new MenuAdapterModel(
                    titulos[i],
                    iconos[i]);
            menuAdapterModelArrayList.add(view);
        }

        menuAdapter = new MenuAdapter(this, menuAdapterModelArrayList) {
            @Override
            public void ejecutarAccion(int position) {
                switch (position) {
                    case 0:
                        App.getInstance().iniciarActividad(BuscarAlimentoActivity.class);
                        break;
                    case 1:
                        LoadingDialogManager.mostrar(activity);
                        RecomendadorDeComidasControlador.getRecomendacionDeComidas();
                        break;
                    case 2:
                        Bundle bundle = new Bundle();
                        bundle.putString(LISTADO_ALIMENTOS, new Gson().toJson(IngresoAlimentoDAO.getAllAlimentosIngresados()));
                        App.getInstance().iniciarActividad(HistorialAlimentosIngresadosActivity.class, bundle);
                        break;
                    default:
                        break;
                }
            }
        };

        rcMenu.setAdapter(menuAdapter);

    }
}
