package com.glucomeandroidapp.Activities.Comidas;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.RecomendadorComidasAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.DAO.MedicionesDAO;
import com.glucomeandroidapp.POJO.Alimento;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.COMIDAS_RECOMENDADAS;

public class RecomendadorComidasActivity extends BaseActivity {

    @BindView(R.id.recomendador_comidas_list) RecyclerView list;

    @BindView(R.id.lbl_ultima_medicion)
    TextView txtUltimaMedicion;

    private List<Alimento> alimentos = new ArrayList<>();
    private Medicion ultimaMedicion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recomendador_comidas);


        //alimentos = obtenerListaBundle( COMIDAS_RECOMENDADAS, Alimento.class);

        alimentos = obtenerListaBundle(COMIDAS_RECOMENDADAS, Alimento.class);

        ultimaMedicion = MedicionesDAO.getUltimaMedicion();

        ButterKnife.bind(this);


        super.inicializar(R.string.menu_comida_items);

        if(ultimaMedicion != null){
            String mensaje = "Se ha tenido en cuenta tu última medición que fue de " + ultimaMedicion.getValorMedicion().intValue() + (ultimaMedicion.getMomento() != MomentoMedicion.NA ?  " realizada " + ultimaMedicion.getMomento().getNombre() : "");
            this.txtUltimaMedicion.setText(mensaje);
        }

        Activity activity = this;
        RecomendadorComidasAdapter recomendadorComidasAdapterAdapter = new RecomendadorComidasAdapter(alimentos);
        RecyclerView recyclerView = list;
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Alimento alimento = alimentos.get(position);
                        Bundle bundle = new Bundle();
                        bundle.putString(COMIDAS_RECOMENDADAS, new Gson().toJson(alimento));
                        //bundle.putString(BUNDLE_NAME, COMIDAS_RECOMENDADAS_BUNDLE);
                        App.getInstance().iniciarActividad(EspecificacionComidaActivity.class, bundle);
                    }

                    @Override public void onLongItemClick(View view, int position) {

                    }
                })
        );
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(recomendadorComidasAdapterAdapter);
    }


}
