package com.glucomeandroidapp.Activities.Comidas;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.Alimento;
import com.glucomeandroidapp.POJO.Enumeraciones.Rol;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.ImagenLoader;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.ALIMENTO;
import static com.glucomeandroidapp.Utils.Util.COMIDAS_RECOMENDADAS;
import static com.glucomeandroidapp.Utils.Util.INDICE_GLUCEMICO;
import static com.glucomeandroidapp.Utils.Util.RECETAS;
import static com.glucomeandroidapp.Utils.Util.URL_BASE;
import static com.glucomeandroidapp.Utils.Util.URL_IMAGEN_COMIDA;

public class EspecificacionComidaActivity extends BaseActivity {

    @BindView(R.id.img_comida)
    ImageView imagen;
    @BindView(R.id.lbl_nombre_comida)
    TextView nombreComida;
    @BindView(R.id.lbl_indice_glucemico)
    TextView indiceGlucemico;
    @BindView(R.id.lbl_info)
    TextView info;
    @BindView(R.id.btn_buscar_recetas)
    TextView buscarRecetas;
    @BindView(R.id.btn_ingresar_alimento)
    TextView btnIngresarAlimento;

    Alimento alimento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_especificacion_comida);

        alimento = obtenerBundle(COMIDAS_RECOMENDADAS, Alimento.class);


        ButterKnife.bind(this);
        super.inicializar(R.string.menu_comida_items);
        String url = URL_BASE + URL_IMAGEN_COMIDA + alimento.getId() + "/";

        new ImagenLoader(imagen).execute(url);

        nombreComida.setText(alimento.getNombre());

        String rol = AccountDataManager.obtenerRol();
        if (rol.equals(Rol.ALLEGADO.getNombre())){
            btnIngresarAlimento.setVisibility(View.GONE);
        }else{
            btnIngresarAlimento.setVisibility(View.VISIBLE);
        }


        String txtIndiceGlucemico = INDICE_GLUCEMICO + " " + alimento.getIndiceGlucemico();
        indiceGlucemico.setText(txtIndiceGlucemico);

        info.setText(alimento.getDescripcion());

        buscarRecetas.setOnClickListener(v ->
                buscarInformacion(RECETAS + alimento.getNombre()));

        btnIngresarAlimento.setOnClickListener(v -> {
                    Bundle bundle = new Bundle();
                    bundle.putString(ALIMENTO, new Gson().toJson(alimento));
                    App.getInstance().iniciarActividad(IngresoAlimentoActivity.class, bundle);
                }
        );
    }

    private void buscarInformacion(String nombreAlimento) {
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, nombreAlimento);
        startActivity(intent);
    }
}
