package com.glucomeandroidapp.Activities.Perfil;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.ImagePickerActivity;
import com.glucomeandroidapp.Activities.Security.LoginActivity;
import com.glucomeandroidapp.Adapters.PerfilAdapter;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.FotosControlador;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.GlideApp;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.glucomeandroidapp.Utils.Util.TAG;
import static com.glucomeandroidapp.Utils.Util.URL_BASE;
import static com.glucomeandroidapp.Utils.Util.URL_IMAGEN_USUARIO;

public class MiPerfilActivity extends BaseActivity {

    @BindView(R.id.rc_perfil)
    RecyclerView rcPerfil;
    @BindView(R.id.fab_perfil)
    TextView fab_foto;
    @BindView(R.id.lbl_username)
    TextView txtUsername;
    @BindView(R.id.lbl_rol)
    TextView txtRol;
    @BindView(R.id.img_de_perfil)
    ImageView img_perfil;
    @BindView(R.id.btn_cerrar_sesion)
    TextView btnCerrarSesion;

    public static final int REQUEST_IMAGE = 100;

    Activity activity;
    Context context;
    LoginResponse loginResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_perfil);

        ButterKnife.bind(this);

        super.inicializar(R.string.perfil);

        activity = this;
        context = App.getContext();

        String fotoPerfil = null;

      //  fotoPerfil = AccountDataManager.obtenerDatosUsuario().getImagen();

        fotoPerfil = URL_BASE + URL_IMAGEN_USUARIO +  AccountDataManager.obtenerIdUsuario() + "/";

        if (fotoPerfil != null) {
            loadProfile(fotoPerfil);
        } else {
            loadProfileDefault();
        }

        ImagePickerActivity.clearCache(activity);

        loginResponse = AccountDataManager.obtenerDatosUsuario();

        txtUsername.setText(loginResponse.getUsuario());
        txtRol.setText(AccountDataManager.obtenerRol());

        String[] titulos = new String[0];

        switch (AccountDataManager.obtenerRolEnum()) {
            case MEDICO_PREMIUM:
            case MEDICO:
                titulos = getResources().getStringArray(R.array.perfil_medico);
                break;
            case PACIENTE_PREMIUM:
            case PACIENTE:
                titulos = getResources().getStringArray(R.array.perfil_paciente);
                break;
            case ALLEGADO:
                titulos = getResources().getStringArray(R.array.perfil_allegado);
                break;
        }

        PerfilAdapter adapter = new PerfilAdapter(titulos, loginResponse);

        rcPerfil.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        rcPerfil.setLayoutManager(llm);
        rcPerfil.setItemAnimator(new DefaultItemAnimator());

        btnCerrarSesion.setOnClickListener(view -> {
            AccountDataManager.desoleguarse();
            App.getInstance().iniciarActividad(LoginActivity.class);
        });
    }

    /**
     * Trata la seleccion o toma de imagenes de perfil de usuario
     */
    @OnClick({R.id.fab_perfil, R.id.img_de_perfil})
    void onProfileImageClick() {
        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(activity, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(activity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
                ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(activity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
                ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {

                    // Obtengo foto de perfil y la envío al servidor
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);

                    FotosControlador.enviarFotoDePerfil(bitmap);

                    // Actualizo el perfil del usuario con la foto
                    LoginResponse usuario = AccountDataManager.obtenerDatosUsuario();
                    //usuario.setImagen(uri.toString());
                    AccountDataManager.actualizarCuentaUsuario(usuario);


                    // Actualizo la vista con la foto
                    loadProfile(uri.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(getString(R.string.dialog_titulo_permisos));
        builder.setMessage(getString(R.string.dialog_mensaje_perisos));
        builder.setPositiveButton(getString(R.string.ir_a_configuracion), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(R.string.cancelar), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // Llevo al usuario a la configuración
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    private void loadProfile(String url) {
        Log.d(TAG, "Image cache path: " + url);

        GlideApp.with(this).load(url)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(img_perfil);
        img_perfil.setColorFilter(ContextCompat.getColor(activity, android.R.color.transparent));
    }

    private void loadProfileDefault() {
        GlideApp.with(this)
                .load(R.drawable.ic_usuario)
                .into(img_perfil);
    }
}
