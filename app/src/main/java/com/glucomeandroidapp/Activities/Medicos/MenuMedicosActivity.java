package com.glucomeandroidapp.Activities.Medicos;

import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.ChatControlador;
import com.glucomeandroidapp.Controladores.MedicosControlador;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;

public class MenuMedicosActivity extends BaseActivity {

    @BindView(R.id.rc_menu) RecyclerView rcMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_generic);
        ButterKnife.bind(this);

        super.inicializar(R.string.menu_medicos);

        MenuAdapter menuAdapter;
        ArrayList<MenuAdapterModel> menuAdapterModelArrayList;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(App.getContext());
        rcMenu.setLayoutManager(layoutManager);
        rcMenu.setItemAnimator(new DefaultItemAnimator());

        menuAdapterModelArrayList = new ArrayList<>();

        int[] iconos = {R.drawable.ic_nuevo, R.drawable.ic_busqueda, R.drawable.ic_chat};
        String[] titulos = getResources().getStringArray(R.array.menu_medicos);

        for(int i=0; i<iconos.length; i++){
            MenuAdapterModel view = new MenuAdapterModel(
                    titulos[i],
                    iconos[i]);
            menuAdapterModelArrayList.add(view);
        }

        menuAdapter = new MenuAdapter(this, menuAdapterModelArrayList) {
            @Override
            public void ejecutarAccion(int position) {
                switch (position) {
                    case 0:
                        App.getInstance().iniciarActividad(BuscarMedicoActivity.class, null);
                        break;
                    case 1:
                        MedicosControlador.getMedicos();
                        break;
                    case 2:
                        LoadingDialogManager.mostrar(activity);
                        ChatControlador.getListadoChats();
                        break;
                    default:
                        break;
                }
            }
        };

        rcMenu.setAdapter(menuAdapter);

    }
}
