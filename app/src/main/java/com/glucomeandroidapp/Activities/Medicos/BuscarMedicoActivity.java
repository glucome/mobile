package com.glucomeandroidapp.Activities.Medicos;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.BusquedaMedicosAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.MedicosControlador;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoSolicitud;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.BUSQUEDA_VACIA;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;

public class BuscarMedicoActivity extends BaseActivity {

    @BindView(R.id.input_nombre) EditText nombre;
    @BindView(R.id.btn_buscar) TextView btnBuscar;
    @BindView(R.id.lista_resultados) RecyclerView list;

    Activity activity = this;
    
    private List<MedicoDataResponse> medicos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_nombre_generic);

        ButterKnife.bind(this);

        super.inicializar(R.string.menu_medicos_items);

        btnBuscar.setOnClickListener(v -> {

            if (!nombre.getText().toString().isEmpty()) {
                // Creo el popup de carga
                LoadingDialogManager.mostrar(this);

                MedicosControlador.getMedicosPorNombre(nombre.getText().toString(), respuesta -> {
                    BusquedaMedicosAdapter busquedaMedicosAdapter = new BusquedaMedicosAdapter(respuesta);
                    RecyclerView recyclerView = list;
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setHasFixedSize(true);
                    medicos = respuesta;
                    recyclerView.setAdapter(busquedaMedicosAdapter);
                    recyclerView.addOnItemTouchListener(
                            new RecyclerItemClickListener(App.getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    MedicoDataResponse medico = medicos.get(position);
                                    LoadingDialogManager.mostrar(activity);
                                    MedicosControlador.getSolicitud(medico, TipoSolicitud.MEDICO_PACIENTE);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {

                                }
                            })
                    );
                    recyclerView.setHasFixedSize(true);
                });

            } else {
                ((App)getApplication()).mostrarMensajeFlotante(BUSQUEDA_VACIA);
            }
        });
    }


}
