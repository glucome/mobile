package com.glucomeandroidapp.Activities.Medicos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.CustomComponents.MyTextView_Roboto_Black;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.Util;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.MEDICO_SELECCIONADO;

public class UbicacionMedicoActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double latitud = 0;
    private double longitud = 0;


    @BindView(R.id.nombre_pantalla)
    TextView nombrePantalla;
    private MedicoDataResponse medico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubicacion_medico);

        ButterKnife.bind(this);



        Bundle bundle = getIntent().getExtras();
        if(bundle!=null) {
            medico = obtenerBundle(MEDICO_SELECCIONADO, MedicoDataResponse.class);
            nombrePantalla.setText("Ubicación de " + medico.getNombre());
            getCoordenadas(medico);
            if (latitud == 0 && longitud == 0) {

                App.getInstance().mostrarMensajeFlotante("El médico no ingreso su ubicación");

            } else {

                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);

                mapFragment.getMapAsync(this);

            }

        }
    }

    private void getCoordenadas(MedicoDataResponse medico) {
        String ubicacion = medico.getUbicacion();
        if(ubicacion !=  null){
            String[] latlong = ubicacion.split(";");
            latitud = Double.valueOf(latlong[0]);
            longitud = Double.valueOf(latlong[1]);
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng location = new LatLng(latitud, longitud);

        CameraPosition googlePlex = CameraPosition.builder()
                .target(location)
                .zoom(16)
                .bearing(0)
                .build();

        mMap.addMarker(new MarkerOptions().position(location).title("Paciente"));
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(googlePlex));
    }

    public  MedicoDataResponse obtenerBundle(String key, Class<MedicoDataResponse> clase) {
        try {
            Type listType = TypeToken.getParameterized(clase).getType();
            return Util.getGsonHelper().fromJson(getIntent().getExtras().getBundle(BUNDLE_NAME).getString(key), listType);
        } catch (NullPointerException e) {
            return null;
        }
    }
}

