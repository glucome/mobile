package com.glucomeandroidapp.Activities.Medicos;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.Adapters.BusquedaMedicosAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.MedicosControlador;
import com.glucomeandroidapp.Controladores.SolicitudesControlador;
import com.glucomeandroidapp.POJO.APIClasses.Requests.SolicitudRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoSolicitud;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.LoadingDialogManager.prepararDialogSiNo;
import static com.glucomeandroidapp.Utils.Util.BORRAR_ALLEGADO_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.BORRAR_ALLEGADO_TITULO;
import static com.glucomeandroidapp.Utils.Util.BORRAR_MEDICO_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.BORRAR_MEDICO_TITULO;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;
import static com.glucomeandroidapp.Utils.Util.MIS_MEDICOS_LIST;

public class MisMedicosActivity extends BaseActivity {

    @BindView(R.id.mis_medicos_list) RecyclerView list;

    List<MedicoDataResponse> medicos = new ArrayList<>();

    Activity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_medicos);
        ButterKnife.bind(this);

        super.inicializar(R.string.menu_medicos_items);
        activity = this;

        medicos = obtenerListaBundle(MIS_MEDICOS_LIST, MedicoDataResponse.class);

        BusquedaMedicosAdapter busquedaMedicosAdapter = new BusquedaMedicosAdapter(medicos);
        RecyclerView recyclerView = list;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(busquedaMedicosAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        MedicoDataResponse medico = medicos.get(position);
                        LoadingDialogManager.mostrar(activity);
                        MedicosControlador.getSolicitud(medico, TipoSolicitud.MEDICO_PACIENTE);

                    }

                    @Override public void onLongItemClick(View view, int position) {
                        MedicoDataResponse medico = medicos.get(position);

                        AlertDialog dialog = prepararDialogSiNo(activity, BORRAR_MEDICO_TITULO, BORRAR_MEDICO_DESCRIPCION);

                        dialog.findViewById(R.id.button_no).setOnClickListener(v -> dialog.dismiss());
                        dialog.findViewById(R.id.button_si).setOnClickListener(v -> {
                            PacienteDataResponse datosDelPaciente = AccountDataManager.obtenerDatosPaciente();
                            if(datosDelPaciente!=null){
                                SolicitudRequest solicitud = new SolicitudRequest(datosDelPaciente.getId(), medico.getId());
                                LoadingDialogManager.mostrar(activity);
                                SolicitudesControlador.borrarMedico(solicitud);
                                finish();
                            } else {
                                App.getInstance().mostrarMensajeFlotante("No se pudo borrar el medico.");
                            }
                            ((App)activity.getApplication()).iniciarActividad(MenuPrincipalActivity.class);
                        });
                    }
                })
        );

    }

}
