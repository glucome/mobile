package com.glucomeandroidapp.Activities.Medicos;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.SolicitudesControlador;
import com.glucomeandroidapp.POJO.APIClasses.Requests.SolicitudRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.EstadoSolicitud;
import com.glucomeandroidapp.POJO.SolicitudVinculacion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.ImagenLoader;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;
import static com.glucomeandroidapp.Utils.Util.MATRICULA;
import static com.glucomeandroidapp.Utils.Util.MEDICO_BUSCADO_BUNDLE;
import static com.glucomeandroidapp.Utils.Util.MEDICO_SELECCIONADO;
import static com.glucomeandroidapp.Utils.Util.SOLICITUD_MEDICO;
import static com.glucomeandroidapp.Utils.Util.UBICACION_NOTIFICACION;
import static com.glucomeandroidapp.Utils.Util.URL_BASE;
import static com.glucomeandroidapp.Utils.Util.URL_IMAGEN_USUARIO;

public class MedicoSeleccionadoActivity extends BaseActivity {

    @BindView(R.id.img_medico)
    ImageView imagen;
    @BindView(R.id.lbl_nombre_medico)
    TextView nombreMedico;
    @BindView(R.id.lbl_matricula_medico)
    TextView matriculaMedico;
    @BindView(R.id.lbl_info)
    TextView info;
    @BindView(R.id.btn_ver_ubicacion)
    TextView verUbicacion;
    @BindView(R.id.btn_agregar_medico)
    TextView agregarMedico;
    @BindView(R.id.btn_cancelar_solicitud)
    TextView cancelarSolicitud;
    @BindView(R.id.btn_borrar_medico)
    TextView borrarMedico;
    @BindView(R.id.linea_abajo)
    View linea_abajo;

    MedicoDataResponse medico;
    SolicitudVinculacion solicitudVinculacion;
    Activity activity = this;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medico_seleccionado);
        ButterKnife.bind(this);

        super.inicializar(R.string.medico);

        medico = obtenerBundle(MEDICO_SELECCIONADO, MedicoDataResponse.class);
        solicitudVinculacion = obtenerBundle(SOLICITUD_MEDICO, SolicitudVinculacion.class);

        agregarMedico.setVisibility(View.GONE);
        cancelarSolicitud.setVisibility(View.GONE);
        borrarMedico.setVisibility(View.GONE);


        if (solicitudVinculacion == null) {
            agregarMedico.setVisibility(View.VISIBLE);
        } else {
            EstadoSolicitud estado = EstadoSolicitud.get(solicitudVinculacion.getEstado());
            if (estado == EstadoSolicitud.PENDIENTE) {
                cancelarSolicitud.setVisibility(View.VISIBLE);
            } else {
                borrarMedico.setVisibility(View.VISIBLE);
            }
        }
        context = activity.getApplicationContext();

        String url = URL_BASE + URL_IMAGEN_USUARIO + medico.getIdUsuario() + "/";

        new ImagenLoader(imagen).execute(url);

        nombreMedico.setText(medico.getNombre());


        String mensaje = MATRICULA + " " + medico.getMatricula();
        matriculaMedico.setText(mensaje);

        String infoTexto = medico.getDescripcion();
        if(infoTexto == null){
            info.setVisibility(View.GONE);
            linea_abajo.setVisibility(View.GONE);
        } else {
            info.setText(infoTexto);
        }

        verUbicacion.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString(MEDICO_SELECCIONADO, new Gson().toJson(medico));
            App.getInstance().iniciarActividad(UbicacionMedicoActivity.class, bundle);
        });

        agregarMedico.setOnClickListener(v -> {
            LoginResponse datosDelUsuario = AccountDataManager.obtenerDatosUsuario();
            PacienteDataResponse datosDelPaciente = AccountDataManager.obtenerDatosPaciente();
            if (datosDelUsuario != null && datosDelPaciente != null) {
                SolicitudRequest solicitud = new SolicitudRequest(datosDelPaciente.getId(), medico.getId());
                LoadingDialogManager.mostrar(activity);
                SolicitudesControlador.agregarMedico(solicitud);
                finish();
            }
        });

        cancelarSolicitud.setOnClickListener(v -> {
            LoadingDialogManager.mostrar(activity);
            SolicitudesControlador.cancelarSolicitud(solicitudVinculacion);
            finish();
        });

        borrarMedico.setOnClickListener(v -> {
            LoginResponse datosDelUsuario =
                    AccountDataManager.obtenerDatosUsuario();
            PacienteDataResponse datosDelPaciente =AccountDataManager.obtenerDatosPaciente();
            if (datosDelUsuario != null && datosDelPaciente != null) {
                SolicitudRequest solicitud = new SolicitudRequest(datosDelPaciente.getId(), medico.getId());
                LoadingDialogManager.mostrar(activity);
                SolicitudesControlador.borrarMedico(solicitud);
                finish();
            }
        });
    }

    public void cambiarVisibilidad(String solicitud) {
        if (solicitud.equals("Cancelar") || solicitud.equals("Borrar")) {
            agregarMedico.setVisibility(View.VISIBLE);
            borrarMedico.setVisibility(View.GONE);
            cancelarSolicitud.setVisibility(View.GONE);
        } else if (solicitud.equals("Agregar")) {
            agregarMedico.setVisibility(View.GONE);
            borrarMedico.setVisibility(View.GONE);
            cancelarSolicitud.setVisibility(View.VISIBLE);
        }
    }

}
