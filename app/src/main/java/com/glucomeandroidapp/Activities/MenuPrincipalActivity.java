package com.glucomeandroidapp.Activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.glucomeandroidapp.Activities.Pacientes.MisPacientesFragment;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.CallbackInterface;
import com.glucomeandroidapp.Controladores.AllegadosControlador;
import com.glucomeandroidapp.Controladores.MedicosControlador;
import com.glucomeandroidapp.Controladores.VariosControlador;
import com.glucomeandroidapp.Fragments.ConfiguracionPacienteFragment;
import com.glucomeandroidapp.Fragments.MenuPrincipalAllegadoFragment;
import com.glucomeandroidapp.Fragments.MenuPrincipalMedicoFragment;
import com.glucomeandroidapp.Fragments.MenuPrincipalPacienteFragment;
import com.glucomeandroidapp.Fragments.ResumenPacienteFragment;
import com.glucomeandroidapp.Fragments.SolicitudesAllegadoFragment;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.Rol;
import com.glucomeandroidapp.POJO.SolicitudVinculacionAllegadoPaciente;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Receivers.RefreshTokenReceiver;
import com.glucomeandroidapp.Receivers.SincronizacionAutomaticaReceiver;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.glucomeandroidapp.Utils.LocalStorageManager;
import com.glucomeandroidapp.Utils.Util;
import com.glucomeandroidapp.Utils.UtilesFecha;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Controladores.FirebaseControlador.enviarTokenFirebase;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;
import static com.glucomeandroidapp.Utils.Util.SOLICITUDES_BUNDLE;
import static com.glucomeandroidapp.Utils.Util.ULTIMO_ENVIO_A_BACKEND;

/**
 * Activity que representa el menu principal de un usuario logueado en la app
 * Creado por MartinArtime el 24 de abril del 2019
 */
public class MenuPrincipalActivity extends AppCompatActivity {

    @BindView(R.id.nombre_pantalla) TextView nombrePantalla;
    @BindView(R.id.navigation) BottomNavigationView navigation;
    @BindView(R.id.btn_filtrar) ImageButton btnFiltrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu_principal);

        ButterKnife.bind(this);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.e(Util.TAG, "getInstanceId de Firebase failed", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    InstanceIdResult instance = task.getResult();
                    if(instance!=null){
                        String token = instance.getToken();

                        LoginResponse loginResponse =  AccountDataManager.obtenerDatosUsuario();

                        if(loginResponse!=null){
                            loginResponse.setTokenFirebase(token);
                            AccountDataManager.loguearse(loginResponse);

                            enviarTokenFirebase();
                        }

                        Log.e(Util.TAG, "Token de App en Firebase: "+token);
                        //Toast.makeText(this, "Token de App en Firebase: "+token, Toast.LENGTH_SHORT).show();

                    }
                });

        LoginResponse usuarioLogueado = AccountDataManager.obtenerDatosUsuario();

        if(usuarioLogueado != null){

            // Seteo la alarma para renovar el token
            LocalDateTime ultimoToken = UtilesFecha.getLocalDateTimeFromString(usuarioLogueado.getTokenFechaExpiracion());

            //Log.e(Util.TAG, "Menu Principal VENCIMIENTO DE TOKEN:"+ultimoToken.toString());
            //Log.e(Util.TAG, "Menu Principal usuario TOKEN:"+usuarioLogueado.getToken());

            if(ultimoToken == null || ultimoToken.isBefore(LocalDateTime.now())){
                RefreshTokenReceiver.setAlarm(getApplicationContext(), LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
            }

            // Actualizo el token firebase en el backend (Se supone que Firebase lo cambia cada una hora así que enviar
            // A nuestro backend cada media como el Token nuestro, estaría bien
            enviarTokenFirebase();

            // Envío ahora novedades y ya seteo para que envíe novedades cada 6 horas al backend
            LocalDateTime ultimoEnvioABackend = LocalStorageManager.obtener(ULTIMO_ENVIO_A_BACKEND, LocalDateTime.class);

            LocalDateTime now = LocalDateTime.now();
            if(ultimoEnvioABackend != null){
                if(now != null && ultimoEnvioABackend.isBefore(now)){
                    SincronizacionAutomaticaReceiver.setAlarm(App.getContext(), LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
                }
            }
        }

        String rol = AccountDataManager.obtenerRol();
        if (rol.equals(Rol.PACIENTE.getNombre()) || rol.equals(Rol.PACIENTE_PREMIUM.getNombre())) {

            VariosControlador.pidePermisoSms(this);

            //VariosControlador.sincronizar(this);

            //MedicosControlador.getMedicosBackground();
            //AllegadosControlador.getAllegadosBackground();

            navigation.inflateMenu(R.menu.bottom_navigator_paciente);
            navigation.getMenu().getItem(1).setChecked(true);

            navigation.setOnNavigationItemSelectedListener
                    (item -> {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_resumen:
                                nombrePantalla.setText(R.string.resumen);
                                selectedFragment = ResumenPacienteFragment.newInstance();
                                break;
                            case R.id.action_menu_principal:
                                nombrePantalla.setText(R.string.menu_principal);
                                selectedFragment = MenuPrincipalPacienteFragment.newInstance();
                                break;
                            case R.id.action_configuracion:
                                nombrePantalla.setText(R.string.configuracion);
                                selectedFragment = ConfiguracionPacienteFragment.newInstance();
                                break;
                            default:
                                nombrePantalla.setText(R.string.resumen);
                                selectedFragment = ResumenPacienteFragment.newInstance();
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_main_menu, selectedFragment);
                        transaction.commit();
                        return true;
                    });

            nombrePantalla.setText(R.string.menu_principal);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_main_menu, MenuPrincipalPacienteFragment.newInstance());
            transaction.commit();

        } else if (rol.equals(Rol.MEDICO.getNombre()) || rol.equals(Rol.MEDICO_PREMIUM.getNombre())) {

            //PacientesControlador.getPacientesBackground();

            navigation.inflateMenu(R.menu.bottom_navigator_medico);
            navigation.getMenu().getItem(0).setChecked(true);

            navigation.setOnNavigationItemSelectedListener
                    (item -> {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_pacientes:
                                btnFiltrar.setVisibility(View.VISIBLE);
                                nombrePantalla.setText(R.string.pacientes);
                                selectedFragment = new MisPacientesFragment();
                                break;
                            case R.id.action_menu_principal:
                                btnFiltrar.setVisibility(View.GONE);
                                nombrePantalla.setText(R.string.menu_principal);
                                selectedFragment = MenuPrincipalMedicoFragment.newInstance();
                                break;
                            case R.id.action_notificaciones:
                                btnFiltrar.setVisibility(View.GONE);
                                nombrePantalla.setText(R.string.notificaciones);
                                selectedFragment = new Fragment();
                                break;
                            default:
                                nombrePantalla.setText(R.string.menu_principal);
                                selectedFragment = MenuPrincipalMedicoFragment.newInstance();
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_main_menu, selectedFragment);
                        transaction.commit();
                        return true;
                    });

            nombrePantalla.setText(R.string.pacientes);
            btnFiltrar.setVisibility(View.VISIBLE);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_main_menu, new MisPacientesFragment());
            transaction.commit();

        } else {

            //PacientesControlador.getPacientesBackground();

            navigation.inflateMenu(R.menu.bottom_navigator_allegado);
            navigation.getMenu().getItem(0).setChecked(true);

            navigation.setOnNavigationItemSelectedListener
                    (item -> {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_pacientes:
                                nombrePantalla.setText(R.string.pacientes);
                                selectedFragment = new MisPacientesFragment();
                                break;
                            case R.id.action_menu_principal:
                                nombrePantalla.setText(R.string.menu_principal);
                                selectedFragment = MenuPrincipalAllegadoFragment.newInstance();
                                break;
                            case R.id.action_notificaciones:
                                nombrePantalla.setText(R.string.notificaciones);
                                LoadingDialogManager.mostrar(MenuPrincipalActivity.this);
                                AllegadosControlador.getSolicitudesDelAllegado(respuesta -> {
                                    Bundle bundle = new Bundle();
                                    bundle.putString(SOLICITUDES_BUNDLE, new Gson().toJson(respuesta));
                                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                                    SolicitudesAllegadoFragment solicitudesAllegadoFragment = SolicitudesAllegadoFragment.newInstance();
                                    solicitudesAllegadoFragment.setArguments(bundle);
                                    transaction.replace(R.id.frame_main_menu, solicitudesAllegadoFragment);
                                    transaction.commit();
                                });
                                return true;
                            default:
                                nombrePantalla.setText(R.string.pacientes);
                                selectedFragment = new MisPacientesFragment();
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_main_menu, selectedFragment);
                        transaction.commit();
                        return true;
                    });

            nombrePantalla.setText(R.string.pacientes);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_main_menu, new MisPacientesFragment());
            transaction.commit();

        }



    }

    @Override
    public void onBackPressed() {}
}
