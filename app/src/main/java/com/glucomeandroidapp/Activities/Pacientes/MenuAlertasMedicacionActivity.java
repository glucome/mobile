package com.glucomeandroidapp.Activities.Pacientes;

import android.os.Bundle;
import android.widget.Switch;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LocalStorageManager;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.ALERTAS_PACIENTE_A_ALLEGADO_MEDICACIONES;
import static com.glucomeandroidapp.Utils.Util.ALERTAS_PACIENTE_A_MEDICO_MEDICACIONES;

/**
 * Creado por MartinArtime el 17 de junio del 2019
 */
public class MenuAlertasMedicacionActivity extends BaseActivity {

    @BindView(R.id.btn_guardar_alertas_medicacion_paciente_a_asociado)
    TextView btnGuardar;
    @BindView(R.id.sw_setear_alertas_medicacion_a_allegados)
    Switch swEnviarAlertasMedicacionAllegados;
    @BindView(R.id.sw_setear_alertas_medicacion_a_medicos)
    Switch swEnviarAlertasMedicacionMedicos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertas_paciente_a_asociados_medicaciones);

        ButterKnife.bind(this);

        super.inicializar(R.string.alertas);

        String alertasAAllegados =
                LocalStorageManager.obtener(ALERTAS_PACIENTE_A_ALLEGADO_MEDICACIONES, String.class);

        String alertasAMedicos =
                LocalStorageManager.obtener(ALERTAS_PACIENTE_A_MEDICO_MEDICACIONES, String.class);

        if (alertasAAllegados != null && alertasAAllegados.equals("true")) {
            swEnviarAlertasMedicacionAllegados.setChecked(true);
        } else {
            swEnviarAlertasMedicacionAllegados.setChecked(false);
        }

        if (alertasAMedicos != null && alertasAMedicos.equals("true")) {
            swEnviarAlertasMedicacionMedicos.setChecked(true);
        } else {
            swEnviarAlertasMedicacionMedicos.setChecked(false);
        }


        btnGuardar.setOnClickListener(v -> {

            // Lamentablemente guardarEnSharedPreferences no permite guardar en tipos de datos basicos
            String alertar;
            if (swEnviarAlertasMedicacionAllegados.isChecked()) {
                alertar = "true";
            } else {
                alertar = "false";
            }
            // Guardo true si está chequeado el Switch y false en caso contrario
            LocalStorageManager.guardar(ALERTAS_PACIENTE_A_ALLEGADO_MEDICACIONES, alertar);

            String alertar2;
            if (swEnviarAlertasMedicacionMedicos.isChecked()) {
                alertar2 = "true";
            } else {
                alertar2 = "false";
            }
            // Guardo true si está chequeado el Switch y false en caso contrario
            LocalStorageManager.guardar(ALERTAS_PACIENTE_A_MEDICO_MEDICACIONES, alertar);

            finish();

        });

    }

}
