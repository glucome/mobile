package com.glucomeandroidapp.Activities.Pacientes;

import android.os.Bundle;
import android.widget.Switch;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LocalStorageManager;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.ALERTAS_PACIENTE_A_ALLEGADO_MEDICIONES;
import static com.glucomeandroidapp.Utils.Util.ALERTAS_PACIENTE_A_MEDICO_MEDICIONES;

/**
 * Creado por MartinArtime el 17 de junio del 2019
 */
public class MenuAlertasMedicionActivity extends BaseActivity {

    @BindView(R.id.btn_guardar_alertas_medicion_paciente_a_allegado)
    TextView btnGuardar;
    @BindView(R.id.sw_enviar_alertas_mediciones_fuera_de_rango_a_allegados)
    Switch swEnviarAlertasFueraDeRangoAllegados;
    @BindView(R.id.sw_enviar_alertas_mediciones_fuera_de_rango_a_medicos)
    Switch swEnviarAlertasFueraDeRangoMedicos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertas_paciente_a_asociados_mediciones);

        ButterKnife.bind(this);

        super.inicializar(R.string.alertas);

        String alertasAAllegados =
                LocalStorageManager.obtener(ALERTAS_PACIENTE_A_ALLEGADO_MEDICIONES, String.class);

        String alertasAMedicos =
                LocalStorageManager.obtener(ALERTAS_PACIENTE_A_MEDICO_MEDICIONES, String.class);

        if (alertasAAllegados != null && alertasAAllegados.equals("true")) {
            swEnviarAlertasFueraDeRangoAllegados.setChecked(true);
        } else {
            swEnviarAlertasFueraDeRangoAllegados.setChecked(false);
        }

        if (alertasAMedicos != null && alertasAMedicos.equals("true")) {
            swEnviarAlertasFueraDeRangoMedicos.setChecked(true);
        } else {
            swEnviarAlertasFueraDeRangoMedicos.setChecked(false);
        }


        btnGuardar.setOnClickListener(v -> {

            // Lamentablemente guardarEnSharedPreferences no permite guardar en tipos de datos basicos
            String alertar;
            if (swEnviarAlertasFueraDeRangoAllegados.isChecked()) {
                alertar = "true";
            } else {
                alertar = "false";
            }
            // Guardo true si está chequeado el Switch y false en caso contrario
            LocalStorageManager.guardar(ALERTAS_PACIENTE_A_ALLEGADO_MEDICIONES, alertar);

            String alertar2;
            if (swEnviarAlertasFueraDeRangoMedicos.isChecked()) {
                alertar2 = "true";
            } else {
                alertar2 = "false";
            }
            // Guardo true si está chequeado el Switch y false en caso contrario
            LocalStorageManager.guardar(ALERTAS_PACIENTE_A_MEDICO_MEDICIONES, alertar);

            finish();

        });

    }

}
