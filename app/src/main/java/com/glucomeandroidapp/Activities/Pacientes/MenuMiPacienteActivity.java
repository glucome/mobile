package com.glucomeandroidapp.Activities.Pacientes;

import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.PacientesControlador;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_ID;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_NOMBRE;

public class MenuMiPacienteActivity extends BaseActivity {

    @BindView(R.id.rc_menu) RecyclerView rcMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_generic);
        ButterKnife.bind(this);

        String nombre = "";
        long idPaciente = 0;
        Bundle bundle = getIntent().getBundleExtra(BUNDLE_NAME);
        if (bundle != null) {
            nombre = bundle.getString(PACIENTE_NOMBRE);
            idPaciente = bundle.getLong(PACIENTE_ID);
        }

        super.inicializar(nombre);

        MenuAdapter opcionesPacientesAdapter;
        ArrayList<MenuAdapterModel> menuAdapterModelArrayList;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(App.getContext());
        rcMenu.setLayoutManager(layoutManager);
        rcMenu.setItemAnimator(new DefaultItemAnimator());

        menuAdapterModelArrayList = new ArrayList<>();

        int[] iconos = {R.drawable.ic_medicion, R.drawable.ic_medicamento, R.drawable.ic_inyeccion, R.drawable.ic_comida, R.drawable.ic_ejercicio};
        String[] titulos = getResources().getStringArray(R.array.menu_mi_paciente);

        for (int i = 0; i < iconos.length; i++) {
            MenuAdapterModel view = new MenuAdapterModel(
                    titulos[i],
                    iconos[i]);
            menuAdapterModelArrayList.add(view);
        }

        //opcionesPacientesAdapter = new OpcionesPacientesAdapter(this, menuAdapterModelArrayList, idPaciente, nombre);

        long finalIdPaciente = idPaciente;
        String finalNombre = nombre;
        opcionesPacientesAdapter = new MenuAdapter(this, menuAdapterModelArrayList) {
            @Override
            public void ejecutarAccion(int position) {
                switch (position) {
                    case 0:
                        LoadingDialogManager.mostrar(activity);
                        PacientesControlador.obtenerMedicionesPacientes(finalIdPaciente, finalNombre);
                        break;
                   case 1:
                        LoadingDialogManager.mostrar(activity);
                        PacientesControlador.obtenerMedicamentosTomadosPacientes(finalIdPaciente, finalNombre);
                        break;
                   case 2:
                        LoadingDialogManager.mostrar(activity);
                        PacientesControlador.obtenerInyeccionesPacientes(finalIdPaciente, finalNombre);
                      break;
                    case 3:
                        LoadingDialogManager.mostrar(activity);
                        PacientesControlador.obtenerAlimentosPacientes(finalIdPaciente, finalNombre);
                        break;
                   case 4:
                       LoadingDialogManager.mostrar(activity);
                      PacientesControlador.obtenerEjerciciosPacientes(finalIdPaciente, finalNombre);
                       break;
                    default:
                        break;
                }
            }
        };

        rcMenu.setAdapter(opcionesPacientesAdapter);
    }
}
