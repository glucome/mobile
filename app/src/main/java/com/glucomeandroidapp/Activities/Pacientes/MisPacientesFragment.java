package com.glucomeandroidapp.Activities.Pacientes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Mediciones.FiltrarHistorialMedicionesActivity;
import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.Adapters.BusquedaPacientesAdapter;
import com.glucomeandroidapp.Adapters.HistorialMedicionAdapter;
import com.glucomeandroidapp.Adapters.Models.HistorialMedicionAdapterModel;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.PacientesControlador;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Medicion.Medicion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.EDAD;
import static com.glucomeandroidapp.Utils.Util.EDAD_DESDE;
import static com.glucomeandroidapp.Utils.Util.EDAD_HASTA;
import static com.glucomeandroidapp.Utils.Util.FECHA_DESDE;
import static com.glucomeandroidapp.Utils.Util.FECHA_HASTA;
import static com.glucomeandroidapp.Utils.Util.MOMENTO_MEDICION;
import static com.glucomeandroidapp.Utils.Util.NOMBRE;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_ID;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_NOMBRE;
import static com.glucomeandroidapp.Utils.Util.TAG_ACTIVITY_FILTRO;
import static com.glucomeandroidapp.Utils.Util.TIPO_DIABETES;
import static com.glucomeandroidapp.Utils.Util.VALOR_MEDICION_INFERIOR;
import static com.glucomeandroidapp.Utils.Util.VALOR_MEDICION_SUPERIOR;

public class MisPacientesFragment extends Fragment {

    @BindView(R.id.mis_pacientes_list) RecyclerView list;

    List<PacienteDataResponse> pacientes = new ArrayList<>();
    BusquedaPacientesAdapter busquedaPacientesAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mis_pacientes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(AccountDataManager.obtenerRolEnum().esMedico())
            PacientesControlador.getPacientesDelMedico(this::onSuccess);
        else if(AccountDataManager.obtenerRolEnum().esAllegado())
            PacientesControlador.getPacientesDelAllegado(this::onSuccess);

        Objects.requireNonNull(getActivity()).findViewById(R.id.btn_filtrar).setOnClickListener(v -> {
            Intent intent = new Intent(App.getContext(), FiltrarMisPacientesActivity.class);
            startActivityForResult(intent, TAG_ACTIVITY_FILTRO);
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == TAG_ACTIVITY_FILTRO) {
            if(resultCode == Activity.RESULT_OK){
                String nombre = data.getStringExtra(NOMBRE);
                int tipoDeDiabetes = data.getIntExtra(TIPO_DIABETES, -1);
                int edadDesde = data.getIntExtra(EDAD_DESDE, -1);
                int edadHasta= data.getIntExtra(EDAD_HASTA, -1);

                actualizarConFiltro(nombre, tipoDeDiabetes, edadDesde, edadHasta);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.e(Util.TAG, "No se pudo filtrar");
            }
        }
    }

    private void actualizarConFiltro(String nombre, int tipoDeDiabetes, int edadDesde, int edadHasta) {
        busquedaPacientesAdapter = new BusquedaPacientesAdapter(pacientes);
        busquedaPacientesAdapter.filter(nombre, tipoDeDiabetes, edadDesde, edadHasta);
        list.setAdapter(busquedaPacientesAdapter);
    }

    private void onSuccess(List<PacienteDataResponse> respuesta) {
        pacientes = respuesta;
        busquedaPacientesAdapter = new BusquedaPacientesAdapter(pacientes);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setHasFixedSize(true);
        list.setAdapter(busquedaPacientesAdapter);
        list.addOnItemTouchListener(
                new RecyclerItemClickListener(App.getContext(), list, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        PacienteDataResponse paciente = pacientes.get(position);
                        Bundle bundle = new Bundle();
                        bundle.putLong(PACIENTE_ID, paciente.getId());
                        bundle.putString(PACIENTE_NOMBRE, paciente.getNombre());
                        App.getInstance().iniciarActividad(MenuMiPacienteActivity.class, bundle);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {}
                })
        );
    }
}
