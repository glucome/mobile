package com.glucomeandroidapp.Activities.Pacientes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.CustomComponents.EditText_Numerico;
import com.glucomeandroidapp.CustomComponents.EditText_Roboto_Regular;
import com.glucomeandroidapp.CustomComponents.MyTextView_Roboto_Regular;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.EDAD_DESDE;
import static com.glucomeandroidapp.Utils.Util.EDAD_HASTA;
import static com.glucomeandroidapp.Utils.Util.FECHA_DESDE;
import static com.glucomeandroidapp.Utils.Util.FECHA_HASTA;
import static com.glucomeandroidapp.Utils.Util.NOMBRE;
import static com.glucomeandroidapp.Utils.Util.TIPO_DIABETES;

/**
 * Creado por MartinArtime el 12 de septiembre del 2019
 */
public class FiltrarMisPacientesActivity extends BaseActivity {

    @BindView(R.id.nombre_pantalla) protected TextView txtNombrePantalla;
    @BindView(R.id.back_button) protected ImageView btnAtras;
    @BindView(R.id.sp_filtros) Spinner spFiltros;
    @BindView(R.id.img_btn_agregar_filtro) protected ImageButton btnAgregarFiltro;
    @BindView(R.id.img_btn_eliminar_filtro) protected ImageButton btnEliminarFiltro;
    @BindView(R.id.lay_nombre_filtro) protected LinearLayout layNombre;
    @BindView(R.id.input_filtro_nombre) protected EditText_Roboto_Regular nombreFiltro;
    @BindView(R.id.lay_tipo_diabetes_filtro) protected LinearLayout layTipoDeDiabetesFiltro;
    @BindView(R.id.sp_tipo_diabetes_filtro) protected Spinner tipoDeDiabetesFiltro;
    @BindView(R.id.lay_valor_inferior) protected LinearLayout layValorInferior;
    @BindView(R.id.valor_inferior) protected EditText_Numerico valorInferiorEdadFiltro;
    @BindView(R.id.lay_valor_superior) protected LinearLayout layValorSuperior;
    @BindView(R.id.valor_superior) protected EditText_Numerico valorSuperiorEdadFiltro;
    @BindView(R.id.btn_filtrar) protected MyTextView_Roboto_Regular btnFiltrar;

    String nombre = null;
    int tipoDeDiabetes = -1;
    int edadInferior = -1;
    int edadSuperior = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtrar_mis_pacientes);

        ButterKnife.bind(this);

        txtNombrePantalla.setText(getResources().getString(R.string.menu_filtrar));

        btnAtras.setOnClickListener(v -> volverAtrasSinFiltros());

        btnFiltrar.setOnClickListener(v -> {

            if(layNombre.getVisibility() == View.VISIBLE){
                nombre = Objects.requireNonNull(nombreFiltro.getText()).toString();
            }

            if(layTipoDeDiabetesFiltro.getVisibility() == View.VISIBLE){
                tipoDeDiabetes = tipoDeDiabetesFiltro.getSelectedItemPosition();
            }

            if(layValorInferior.getVisibility() == View.VISIBLE){
                edadInferior = valorInferiorEdadFiltro.getText();
                edadSuperior = valorSuperiorEdadFiltro.getText();
            }

            volverAtrasConFiltros();
        });

        btnAgregarFiltro.setOnClickListener(v -> {
            int filtroSeleccionado = spFiltros.getSelectedItemPosition();

            switch(filtroSeleccionado){
                case 0:
                    if(layNombre.getVisibility() == View.VISIBLE){
                        App.getInstance().mostrarMensajeFlotante("Ya ingreso ese filtro");
                    } else {
                        layNombre.setVisibility(View.VISIBLE);
                    }
                    break;
                case 1:
                    if(layTipoDeDiabetesFiltro.getVisibility() == View.VISIBLE){
                        App.getInstance().mostrarMensajeFlotante("Ya ingreso ese filtro");
                    } else {
                        layTipoDeDiabetesFiltro.setVisibility(View.VISIBLE);
                    }
                    break;
                case 2:
                    if(layValorInferior.getVisibility() == View.VISIBLE){
                        App.getInstance().mostrarMensajeFlotante("Ya ingreso ese filtro");
                    } else {
                        layValorInferior.setVisibility(View.VISIBLE);
                        layValorSuperior.setVisibility(View.VISIBLE);
                    }
                    break;
            }

        });

        btnEliminarFiltro.setOnClickListener(v -> {
            int filtroSeleccionado = spFiltros.getSelectedItemPosition();

            switch(filtroSeleccionado){
                case 0:
                    if(!(layNombre.getVisibility() == View.VISIBLE)){
                        App.getInstance().mostrarMensajeFlotante("No se está usando ese filtro");
                    } else {
                        layNombre.setVisibility(View.GONE);
                    }
                    break;
                case 1:
                    if(!(layTipoDeDiabetesFiltro.getVisibility() == View.VISIBLE)){
                        App.getInstance().mostrarMensajeFlotante("No se está usando ese filtro");
                    } else {
                        layTipoDeDiabetesFiltro.setVisibility(View.GONE);
                    }
                    break;
                case 2:
                    if(!(layValorInferior.getVisibility() == View.VISIBLE)){
                        App.getInstance().mostrarMensajeFlotante("No se está usando ese filtro");
                    } else {
                        layValorInferior.setVisibility(View.GONE);
                        layValorSuperior.setVisibility(View.GONE);
                    }
                    break;

            }

        });

    }

    private void volverAtrasSinFiltros(){
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }


    private void volverAtrasConFiltros(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra(NOMBRE, nombre);
        returnIntent.putExtra(TIPO_DIABETES, tipoDeDiabetes);
        returnIntent.putExtra(EDAD_DESDE, edadInferior);
        returnIntent.putExtra(EDAD_HASTA, edadSuperior);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

}
