package com.glucomeandroidapp.Activities;


import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.CustomComponents.MyTextView_Roboto_Black;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.Util;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Creado por MartinArtime el 16 de agosto del 2019
 */

public class VerNotificacionActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String ubicacion;
    private String mensaje;
    private double latitud = 0;
    private double longitud = 0;

    @BindView(R.id.lbl_mensaje_notificacion) MyTextView_Roboto_Black mensajeBody;
    @BindView(R.id.nombre_pantalla) TextView nombrePantalla;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_notificacion);

        ButterKnife.bind(this);

        nombrePantalla.setText(R.string.notificacion);

        Bundle b = getIntent().getExtras();
        if(b!=null){
            ubicacion = b.getString("ubicacion");
            mensaje = b.getString("mensaje");

            Log.e(Util.TAG, "UBICACION: "+ubicacion);
            Log.e(Util.TAG, "MENSAJE: "+mensaje);

            mensajeBody.setText(mensaje);
            parsearUbicacion(ubicacion);
        }

        if(latitud==0 && longitud==0){

            App.getInstance().mostrarMensajeFlotante("No se puede cargar el mapa");

        } else {

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);

            mapFragment.getMapAsync(this);

        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng location = new LatLng(latitud, longitud);

        CameraPosition googlePlex = CameraPosition.builder()
                .target(location)
                .zoom(16)
                .bearing(0)
                .build();

        mMap.addMarker(new MarkerOptions().position(location).title("Paciente"));
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(googlePlex));
    }


    /**
     * Esto transforma el string que llega con la ubicacion del paciente y lo transforma en latitud y longitud usables
     * @param ubicacion, latitud y longitud divididas por una coma (,)
     */
    public void parsearUbicacion(String ubicacion){
        String[] latlong = ubicacion.split(",");
        latitud = Double.valueOf(latlong[0]);
        longitud = Double.valueOf(latlong[1]);
    }
}
