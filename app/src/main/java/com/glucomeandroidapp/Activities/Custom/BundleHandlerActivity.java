package com.glucomeandroidapp.Activities.Custom;

import androidx.appcompat.app.AppCompatActivity;

import com.glucomeandroidapp.Utils.Util;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;

public class BundleHandlerActivity extends AppCompatActivity {
    protected <T> T obtenerBundle(String key, Class clase) {
        try {
            Type listType = TypeToken.getParameterized(clase).getType();
            return Util.getGsonHelper().fromJson(getIntent().getExtras().getBundle(BUNDLE_NAME).getString(key), listType);
        } catch (NullPointerException e) {
            return null;
        }

    }

    protected <T> T obtenerListaBundle(String key, Class claseItem) {
        try {
            Type listType = TypeToken.getParameterized(ArrayList.class, claseItem).getType();
            return Util.getGsonHelper().fromJson(getIntent().getExtras().getBundle(BUNDLE_NAME).getString(key), listType);
        } catch (NullPointerException e) {
            return null;
        }

    }
}
