package com.glucomeandroidapp.Activities.Custom;

import android.widget.ImageView;
import android.widget.TextView;

import com.glucomeandroidapp.R;

import butterknife.BindView;

public class BaseActivity extends BundleHandlerActivity {

    @BindView(R.id.nombre_pantalla)
    protected TextView txtNombrePantalla;
    @BindView(R.id.back_button)
    protected ImageView btnAtras;

    protected void inicializar(int recursoNombrePantalla) {
        txtNombrePantalla.setText(recursoNombrePantalla);
        btnAtras.setOnClickListener(v -> onBackPressed());
    }

    protected void inicializar(String nombrePantalla) {
        txtNombrePantalla.setText(nombrePantalla);
        btnAtras.setOnClickListener(v -> onBackPressed());
    }


}
