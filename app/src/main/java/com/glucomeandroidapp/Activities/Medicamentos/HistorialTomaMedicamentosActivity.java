package com.glucomeandroidapp.Activities.Medicamentos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.Mediciones.FiltrarHistorialMedicionesActivity;
import com.glucomeandroidapp.Adapters.HistorialTomaMedicamentoAdapter;
import com.glucomeandroidapp.Adapters.Models.HistorialTomaMedicamentoAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.Medicacion.Medicamento;
import com.glucomeandroidapp.POJO.Medicacion.MedicamentoDao;
import com.glucomeandroidapp.POJO.Medicacion.TomaMedicamento;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.Util;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.FECHA_DESDE;
import static com.glucomeandroidapp.Utils.Util.FECHA_HASTA;
import static com.glucomeandroidapp.Utils.Util.LISTADO_TOMA_MEDICAMENTO;
import static com.glucomeandroidapp.Utils.Util.MOMENTO_MEDICION;
import static com.glucomeandroidapp.Utils.Util.NOMBRE;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_NOMBRE;
import static com.glucomeandroidapp.Utils.Util.TAG_ACTIVITY_FILTRO;
import static com.glucomeandroidapp.Utils.Util.VALOR_MEDICION_INFERIOR;
import static com.glucomeandroidapp.Utils.Util.VALOR_MEDICION_SUPERIOR;

public class HistorialTomaMedicamentosActivity extends BaseActivity {

    @BindView(R.id.recyclerview) RecyclerView recyclerView;
    @BindView(R.id.btn_cargar_mas) TextView botonCargarMas;
    @BindView(R.id.btn_filtrar) ImageButton btnFiltrar;
    @BindView(R.id.sv_historial) ScrollView svHistorial;
    @BindView(R.id.sv_sin_datos) ScrollView svSinDatos;
    @BindView(R.id.img_no_data) ImageView imgNoData;

    private List<TomaMedicamento> tomaMedicamentos;
    public HistorialTomaMedicamentoAdapter historial_toma_medicamento_adapter;

    private String titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_generic);

        ButterKnife.bind(this);

        btnFiltrar.setOnClickListener(v -> {
            Intent intent = new Intent(App.getContext(), FiltrarHistorialMedicacionesActivity.class);
            startActivityForResult(intent, TAG_ACTIVITY_FILTRO);
        });

        titulo = obtenerBundle(PACIENTE_NOMBRE,String.class);
        if(titulo != null && !titulo.equals("")){
            super.inicializar("Medicamentos de " + titulo);
            ocultarBotonDeCarga();
        }else{
            super.inicializar(R.string.menu_historial_toma_medicamentos);
        }


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HistorialTomaMedicamentosActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        tomaMedicamentos = obtenerListaBundle(LISTADO_TOMA_MEDICAMENTO, TomaMedicamento.class);

        if(!tomaMedicamentos.isEmpty()){
            agregarDatos(null, null, null);
        } else {
            svSinDatos.setVisibility(View.VISIBLE);
            svHistorial.setVisibility(View.GONE);
            imgNoData.setImageResource(R.drawable.ic_medicamento100);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == TAG_ACTIVITY_FILTRO) {
            if(resultCode == Activity.RESULT_OK){
                String fechaDesde = data.getStringExtra(FECHA_DESDE);
                String fechaHasta = data.getStringExtra(FECHA_HASTA);
                String nombre = data.getStringExtra(NOMBRE);

                agregarDatos(nombre, fechaDesde, fechaHasta);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.e(Util.TAG, "No se pudo filtrar");
            }
        }
    }

    private void agregarDatos(String nombre, String fechaDesde, String fechaHasta) {

        ArrayList<HistorialTomaMedicamentoAdapterModel> historial_toma_medicamentos_modelArrayList = new ArrayList<>();
        if(tomaMedicamentos != null){
            for(TomaMedicamento tomaMedicamento : tomaMedicamentos){
                Medicamento medicamento = tomaMedicamento.getMedicamento();
                if (medicamento == null) {
                     medicamento = DataBaseManager.getDaoSession().getMedicamentoDao().queryBuilder()
                            .where(MedicamentoDao.Properties.Id.eq(tomaMedicamento.getIdMedicamentoBackend())).unique();

                }

                String fechaNoParseada = tomaMedicamento.getFechaDeCreacion();
                LocalDateTime fecha;
                if(fechaNoParseada.length() != 16) {
                    fecha = LocalDateTime.parse(fechaNoParseada, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
                } else {
                    fecha = UtilesFecha.getLocalDateTimeFromString(fechaNoParseada);
                }

                HistorialTomaMedicamentoAdapterModel view = new HistorialTomaMedicamentoAdapterModel(
                        Integer.valueOf(tomaMedicamento.getCantidad().intValue()).toString(),
                        UtilesFecha.getStringFromLocalDateTime(fecha),
                        medicamento);
                historial_toma_medicamentos_modelArrayList.add(view);
            }
        }
        historial_toma_medicamento_adapter =
                new HistorialTomaMedicamentoAdapter(historial_toma_medicamentos_modelArrayList);
        historial_toma_medicamento_adapter.filter(nombre, fechaDesde, fechaHasta);
        recyclerView.setAdapter(historial_toma_medicamento_adapter);
    }

    private void ocultarBotonDeCarga() {
        botonCargarMas.setVisibility(View.GONE);
    }
}
