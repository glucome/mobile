package com.glucomeandroidapp.Activities.Medicamentos;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.HorarioMedicacionAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.DAO.HorarioMedicacionDAO;
import com.glucomeandroidapp.POJO.Medicacion.HorarioMedicacion;
import com.glucomeandroidapp.POJO.Medicacion.Medicamento;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.LoadingDialogManager.prepararDialogSiNo;
import static com.glucomeandroidapp.Utils.Util.BORRAR_HORARIO_MEDICACION_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.BORRAR_HORARIO_MEDICACION_TITULO;
import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.HORARIOS_MEDICAMENTOS_LIST;
import static com.glucomeandroidapp.Utils.Util.HORARIO_MEDICACION;
import static com.glucomeandroidapp.Utils.Util.HORARIO_MEDICACION_BUNDLE;
import static com.glucomeandroidapp.Utils.Util.MEDICAMENTO;
import static com.glucomeandroidapp.Utils.UtilesFunciones.cancelarNotificacion;

public class ListaDeHorariosMedicamentoActivity extends BaseActivity {

    @BindView(R.id.horarios_medicamentos_list) RecyclerView list;
    @BindView(R.id.btn_agregar_horario_medicamento) TextView btnAgregar;
    @BindView(R.id.back_button) ImageView backButton;

    List<HorarioMedicacion> horarios = new ArrayList<>();
    Medicamento medicamento;
    private ListaDeHorariosMedicamentoActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_horarios_medicamento);

        ButterKnife.bind(this);

        backButton.setOnClickListener(v -> {
            //List<HorarioMedicacion> horarios = HorarioMedicacionDAO.getHorarios(medicamento.getIdAndroid());
            //Bundle bundle = new Bundle();
            //bundle.putString(HORARIOS_MEDICAMENTOS_LIST, new Gson().toJson(horarios));
            App.getInstance().iniciarActividad(MenuMedicamentosActivity.class);
        });

        medicamento = obtenerBundle(MEDICAMENTO, Medicamento.class);
        horarios = obtenerListaBundle( HORARIOS_MEDICAMENTOS_LIST, HorarioMedicacion.class);

        super.inicializar(R.string.horario_medicamentos_items);

        this.activity = this;

        HorarioMedicacionAdapter horarioMedicacionAdapter= new HorarioMedicacionAdapter( horarios);
        RecyclerView recyclerView = list;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(horarioMedicacionAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        HorarioMedicacion horario = horarios.get(position);
                        Bundle bundle = new Bundle();
                        bundle.putString(MEDICAMENTO, new Gson().toJson(medicamento));
                        bundle.putString(HORARIO_MEDICACION, new Gson().toJson(horario));
                        bundle.putString(BUNDLE_NAME, HORARIO_MEDICACION_BUNDLE);
                        ((App)activity.getApplication()).iniciarActividad(ConfiguracionMedicamentoActivity.class, bundle);

                    }

                    @Override public void onLongItemClick(View view, int position) {
                        HorarioMedicacion horario = horarios.get(position);
                        horario = DataBaseManager.getDaoSession().getHorarioMedicacionDao().load(horario.getId());

                        AlertDialog dialog = prepararDialogSiNo(activity, BORRAR_HORARIO_MEDICACION_TITULO, BORRAR_HORARIO_MEDICACION_DESCRIPCION);

                        dialog.findViewById(R.id.button_no).setOnClickListener(v -> dialog.dismiss());
                        HorarioMedicacion finalHorario = horario;
                        dialog.findViewById(R.id.button_si).setOnClickListener(v -> {
                            cancelarNotificacion(getApplicationContext(), (int) finalHorario.getNotificacionId());
                            DataBaseManager.getDaoSession().getNotificacionDao().delete(finalHorario.getNotificacion());
                            DataBaseManager.getDaoSession().getHorarioMedicacionDao().delete(finalHorario);
                            List<HorarioMedicacion> horarios = HorarioMedicacionDAO.getHorarios(medicamento.getIdAndroid());
                            Bundle bundle = new Bundle();
                            bundle.putString(HORARIOS_MEDICAMENTOS_LIST, new Gson().toJson(horarios));
                            ((App)activity.getApplication()).iniciarActividad(ListaDeHorariosMedicamentoActivity.class, bundle);
                            ((App)activity.getApplication()).mostrarMensajeFlotante("Horario borrado correctamente");
                        });
                    }
                })
        );

        btnAgregar.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString(MEDICAMENTO, new Gson().toJson(medicamento));
            bundle.putString(HORARIO_MEDICACION, new Gson().toJson(new HorarioMedicacion()));
            bundle.putString(BUNDLE_NAME, HORARIO_MEDICACION_BUNDLE);
            ((App)activity.getApplication()).iniciarActividad(ConfiguracionMedicamentoActivity.class, bundle);
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //List<HorarioMedicacion> horarios = HorarioMedicacionDAO.getHorarios(medicamento.getIdAndroid());
        //Bundle bundle = new Bundle();
        //bundle.putString(HORARIOS_MEDICAMENTOS_LIST, new Gson().toJson(horarios));
        App.getInstance().iniciarActividad(MenuMedicamentosActivity.class);
    }
}
