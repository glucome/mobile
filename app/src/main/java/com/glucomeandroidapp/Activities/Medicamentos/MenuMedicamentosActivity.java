package com.glucomeandroidapp.Activities.Medicamentos;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.Mediciones.MenuMedicionesActivity;
import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.Adapters.MenuAdapter;
import com.glucomeandroidapp.Adapters.Models.MenuAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.Enumeraciones.Acciones.AccionMisMedicamentos;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.LISTADO_TOMA_MEDICAMENTO;
import static com.glucomeandroidapp.Utils.Util.MIS_MEDICAMENTOS_LIST;
import static com.glucomeandroidapp.Utils.Util.TIPO_ACCION_MIS_MEDICAMENTOS;

public class MenuMedicamentosActivity extends BaseActivity {

    @BindView(R.id.rc_menu) RecyclerView rcMenu;
    @BindView(R.id.back_button) ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_generic);
        ButterKnife.bind(this);

        super.inicializar(R.string.menu_medicamentos);

        backButton.setOnClickListener(v -> App.getInstance().iniciarActividad(MenuPrincipalActivity.class));

        MenuAdapter menuAdapter;
        ArrayList<MenuAdapterModel> menuAdapterModelArrayList;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(App.getContext());
        rcMenu.setLayoutManager(layoutManager);
        rcMenu.setItemAnimator(new DefaultItemAnimator());

        menuAdapterModelArrayList = new ArrayList<>();

        int[] iconos = {R.drawable.ic_tomar_medicamento, R.drawable.ic_notificaciones, R.drawable.ic_busqueda};
        String[] titulos = getResources().getStringArray(R.array.menu_medicamentos);

        for(int i=0; i<iconos.length; i++){
            MenuAdapterModel view = new MenuAdapterModel(
                    titulos[i],
                    iconos[i]);
            menuAdapterModelArrayList.add(view);
        }

        menuAdapter = new MenuAdapter(this, menuAdapterModelArrayList) {
            @Override
            public void ejecutarAccion(int position) {
                Bundle bundle = new Bundle();

                switch (position) {
                    case 0:
                        bundle.putString(MIS_MEDICAMENTOS_LIST, new Gson().toJson(DataBaseManager.getDaoSession().getMedicamentoDao().loadAll()));
                        bundle.putString(TIPO_ACCION_MIS_MEDICAMENTOS, new Gson().toJson(AccionMisMedicamentos.TOMAR_MEDICAMENTO));
                        App.getInstance().iniciarActividad(MisMedicamentosActivity.class, bundle);
                        break;
                    case 1:
                        bundle.putString(MIS_MEDICAMENTOS_LIST, new Gson().toJson(DataBaseManager.getDaoSession().getMedicamentoDao().loadAll()));
                        bundle.putString(TIPO_ACCION_MIS_MEDICAMENTOS, new Gson().toJson(AccionMisMedicamentos.CONFIGURAR_HORARIO));
                        App.getInstance().iniciarActividad(MisMedicamentosActivity.class, bundle);
                        break;
                    case 2:
                        bundle.putString(LISTADO_TOMA_MEDICAMENTO, new Gson().toJson(DataBaseManager.getDaoSession().getTomaMedicamentoDao().loadAll()));
                        bundle.putString(TIPO_ACCION_MIS_MEDICAMENTOS, new Gson().toJson(AccionMisMedicamentos.CONFIGURAR_HORARIO));
                        App.getInstance().iniciarActividad(HistorialTomaMedicamentosActivity.class, bundle);
                        break;
                    default:
                        break;
                }
            }
        };

        rcMenu.setAdapter(menuAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        App.getInstance().iniciarActividad(MenuPrincipalActivity.class);
    }
}
