package com.glucomeandroidapp.Activities.Medicamentos;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.DAO.HorarioMedicacionDAO;
import com.glucomeandroidapp.POJO.Medicacion.HorarioMedicacion;
import com.glucomeandroidapp.POJO.Medicacion.Medicamento;
import com.glucomeandroidapp.POJO.Notificacion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Receivers.VerificarSiTomoMedicacionReceiver;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.LocalStorageManager;
import com.google.gson.Gson;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.GUARDADO_EXITOSO_HORARIO_MEDICACION;
import static com.glucomeandroidapp.Utils.Util.HORARIOS_MEDICAMENTOS_LIST;
import static com.glucomeandroidapp.Utils.Util.HORARIO_MEDICACION;
import static com.glucomeandroidapp.Utils.Util.MEDICAMENTO;
import static com.glucomeandroidapp.Utils.Util.MEDICIONES_AUTOMATICAS;
import static com.glucomeandroidapp.Utils.Util.NUMERO_NOTIFICACION;
import static com.glucomeandroidapp.Utils.Util.TITULO_MEDICACION;
import static com.glucomeandroidapp.Utils.Util.TOMAR_MEDICAMENTOS;
import static com.glucomeandroidapp.Utils.UtilesFunciones.cancelarNotificacion;
import static com.glucomeandroidapp.Utils.UtilesFunciones.programarNotificacion;

public class ConfiguracionMedicamentoActivity extends BaseActivity {
    @BindView(R.id.lbl_droga_medicamento)
    TextView drogaMedicamento;
    @BindView(R.id.lbl_laboratorio_medicamento)
    TextView laboratorioMedicamento;
    @BindView(R.id.lbl_marca_medicamento)
    TextView marcaMedicamento;
    @BindView(R.id.lbl_presentacion_medicamento)
    TextView presentacionMedicamento;
    @BindView(R.id.horario_medicacion)
    TimePicker horarioMedicacion;

    Medicamento medicamento;
    HorarioMedicacion horario;
    Integer numero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion_medicamento);

        ButterKnife.bind(this);

        super.inicializar(R.string.horario_medicamento_items);

        numero = LocalStorageManager.obtener(NUMERO_NOTIFICACION, Integer.class);
        if(numero==null){
            numero = 1;
        }

        medicamento = obtenerBundle(MEDICAMENTO, Medicamento.class);
        horario = obtenerBundle(HORARIO_MEDICACION, HorarioMedicacion.class);

        LocalDateTime ahora = LocalDateTime.now();
        horarioMedicacion.setHour(horario != null ? horario.getHora() : ahora.getHour());
        horarioMedicacion.setMinute(horario != null ? horario.getMinuto() : ahora.getMinute());

        drogaMedicamento.setText(medicamento.getDroga() != null ? medicamento.getDroga() : "");
        laboratorioMedicamento.setText(medicamento.getLaboratorio() != null ? medicamento.getLaboratorio() : "");
        marcaMedicamento.setText(medicamento.getMarca() != null ? medicamento.getMarca() : "");
        presentacionMedicamento.setText(medicamento.getPresentacion() != null ? medicamento.getPresentacion() : "");

        horarioMedicacion.setIs24HourView(true);
    }

    public void grabarNotificacion(View v) {
        if (horario.getNotificacionId() != 0)
            cancelarNotificacion(getApplicationContext(), (int) horario.getNotificacionId());
        horario.setHora(horarioMedicacion.getHour());
        horario.setMinuto(horarioMedicacion.getMinute());
        horario.setMedicamento(this.medicamento);

        Notificacion notificacion = new Notificacion();
        notificacion.setTipoNotificacion(TOMAR_MEDICAMENTOS);
        notificacion.setId(Long.valueOf(numero));
        DataBaseManager.getDaoSession().getNotificacionDao().insertOrReplace(notificacion);

        horario.setNotificacion(notificacion);
        horario.setNotificacionId(numero);
        DataBaseManager.getDaoSession().getHorarioMedicacionDao().insertOrReplace(horario);

        armarNotificacion(horario.getId(), horarioMedicacion);


        ((App) this.getApplication()).mostrarMensajeFlotante(GUARDADO_EXITOSO_HORARIO_MEDICACION);

        List<HorarioMedicacion> horarios = HorarioMedicacionDAO.getHorarios(medicamento.getIdAndroid());
        Bundle bundle = new Bundle();
        bundle.putString(MEDICAMENTO, new Gson().toJson(medicamento));
        bundle.putString(HORARIOS_MEDICAMENTOS_LIST, new Gson().toJson(horarios));

        ((App) this.getApplication()).iniciarActividad(ListaDeHorariosMedicamentoActivity.class, bundle);
    }

    public void armarNotificacion(Long id, TimePicker tiempo) {
        String texto = this.medicamento.getDroga() + " - " + this.medicamento.getLaboratorio() + " - " + this.medicamento.getMarca() + " - " + this.medicamento.getPresentacion();

        programarNotificacion(getApplicationContext(), numero, TITULO_MEDICACION,
                texto, tiempo.getHour(), tiempo.getMinute(), id.intValue(), false, TOMAR_MEDICAMENTOS);


        // Seteo la alarma para ver si tomó la medicación en una hora
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MINUTE, tiempo.getMinute() + 1);
        //cal.set(Calendar.HOUR, tiempo.getHour() + 1);

        VerificarSiTomoMedicacionReceiver.setAlarm(App.getContext(), cal.getTimeInMillis(), this.medicamento);
    }

}
