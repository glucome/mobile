package com.glucomeandroidapp.Activities.Medicamentos;

import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Controladores.MedicamentosControlador;
import com.glucomeandroidapp.CustomComponents.EditText_Numerico;
import com.glucomeandroidapp.POJO.APIClasses.Requests.TomaMedicamentoRequest;
import com.glucomeandroidapp.POJO.Medicacion.Medicamento;
import com.glucomeandroidapp.POJO.Medicacion.TomaMedicamento;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.glucomeandroidapp.Utils.UtilesFecha;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;
import static com.glucomeandroidapp.Utils.Util.MEDICAMENTO;

public class TomaMedicamentoActivity extends BaseActivity {


    @BindView(R.id.lbl_titulo_toma_medicamento)
    TextView tituloTomaMedicamento;
    @BindView(R.id.lbl_subtitulo_toma_medicamento)
    TextView subtituloTomaMedicamento;
    @BindView(R.id.valor_toma_medicamento)
    EditText_Numerico valorTomaMedicamento;
    @BindView(R.id.fecha_de_toma_medicamento)
    DatePicker fechaTomaMedicamento;
    @BindView(R.id.hora_de_toma_medicamento)
    TimePicker horaTomaMedicamento;
    @BindView(R.id.btn_registrar_toma_medicamento)
    TextView btnGuardar;

    Medicamento medicamento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tomar_medicamento);

        ButterKnife.bind(this);

        super.inicializar(R.string.toma_medicamento);

        medicamento = obtenerBundle(MEDICAMENTO, Medicamento.class);

        tituloTomaMedicamento.setText(medicamento.getDroga() != null ? medicamento.getDroga() : "");
        subtituloTomaMedicamento.setText(medicamento.getLaboratorio() != null ? medicamento.getLaboratorio() : "");

        btnGuardar.setOnClickListener(v -> {

            String cantidad = valorTomaMedicamento.getText().toString();

            String fechaDeCreacion = UtilesFecha.formatearFecha(fechaTomaMedicamento, horaTomaMedicamento);


            TomaMedicamento nuevaTomaMedicamento = new TomaMedicamento(medicamento.getId(),
                    Double.valueOf(cantidad),
                    fechaDeCreacion);

            DataBaseManager.getDaoSession().getTomaMedicamentoDao().insert(nuevaTomaMedicamento);


            TomaMedicamentoRequest tomaMedicamentoRequest = new TomaMedicamentoRequest(
                    nuevaTomaMedicamento.getIdMedicamentoBackend(),
                    nuevaTomaMedicamento.getFechaDeCreacion(),
                    nuevaTomaMedicamento.getCantidad());

            //Envío la medición al backend
            LoadingDialogManager.mostrar(this);

            MedicamentosControlador.enviarTomaMedicamento(tomaMedicamentoRequest, nuevaTomaMedicamento);

            finish();

        });

    }


}
