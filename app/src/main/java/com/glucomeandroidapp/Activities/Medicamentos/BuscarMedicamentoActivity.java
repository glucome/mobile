package com.glucomeandroidapp.Activities.Medicamentos;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.BusquedaMedicamentoAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.Controladores.MedicamentosControlador;
import com.glucomeandroidapp.POJO.Medicacion.Medicamento;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LoadingDialogManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;

public class BuscarMedicamentoActivity extends BaseActivity {

    @BindView(R.id.input_nombre)
    EditText medicamento;
    @BindView(R.id.btn_buscar)
    TextView btnBuscar;
    @BindView(R.id.lista_resultados)
    RecyclerView list;

    Context context;
    private BuscarMedicamentoActivity activity;
    List<Medicamento> medicamentos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_nombre_generic);
        ButterKnife.bind(this);

        super.inicializar(R.string.menu_medicamentos_items);

        activity = this;
        context = this.getApplicationContext();

        btnBuscar.setOnClickListener(v -> {
            if (!medicamento.getText().toString().isEmpty()) {
                // Creo el popup de carga
                LoadingDialogManager.mostrar(activity);

                MedicamentosControlador.getMedicamentosPorNombre(medicamento.getText().toString(), respuesta -> {
                    BusquedaMedicamentoAdapter busquedaMedicamentoAdapter = new BusquedaMedicamentoAdapter(respuesta);
                    list.setLayoutManager(new LinearLayoutManager(activity));
                    list.setAdapter(busquedaMedicamentoAdapter);
                    medicamentos = respuesta;
                    list.setHasFixedSize(true);
                    list.addOnItemTouchListener(
                            new RecyclerItemClickListener(context, list, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Medicamento medicamento = medicamentos.get(position);

                                    MedicamentosControlador.mostrarPopupMedicamento(medicamento, activity);

                                }

                                @Override
                                public void onLongItemClick(View view, int position) {

                                }
                            })
                    );
                });
            }
        });
    }
}

