package com.glucomeandroidapp.Activities.Medicamentos;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.Mediciones.MenuMedicionesActivity;
import com.glucomeandroidapp.Adapters.BusquedaMedicamentoAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.DAO.HorarioMedicacionDAO;
import com.glucomeandroidapp.POJO.Enumeraciones.Acciones.AccionMisMedicamentos;
import com.glucomeandroidapp.POJO.Medicacion.HorarioMedicacion;
import com.glucomeandroidapp.POJO.Medicacion.Medicamento;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.HORARIOS_MEDICAMENTOS_LIST;
import static com.glucomeandroidapp.Utils.Util.MEDICAMENTO;
import static com.glucomeandroidapp.Utils.Util.MEDICAMENTO_BUNDLE;
import static com.glucomeandroidapp.Utils.Util.MIS_MEDICAMENTOS_LIST;
import static com.glucomeandroidapp.Utils.Util.TIPO_ACCION_MIS_MEDICAMENTOS;

public class MisMedicamentosActivity extends BaseActivity {

    @BindView(R.id.btn_agregar_medicamento) TextView btnAgregar;
    @BindView(R.id.mis_medicamentos_list) RecyclerView list;
    @BindView(R.id.back_button) ImageView backButton;

    List<Medicamento> medicamentos = new ArrayList<>();
    AccionMisMedicamentos tipoAccion;
    private MisMedicamentosActivity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_medicamentos);

        ButterKnife.bind(this);
        super.inicializar(R.string.menu_medicamentos_items);

        backButton.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString(MIS_MEDICAMENTOS_LIST, new Gson().toJson(DataBaseManager.getDaoSession().getMedicamentoDao().loadAll()));
            bundle.putString(TIPO_ACCION_MIS_MEDICAMENTOS, new Gson().toJson(AccionMisMedicamentos.CONFIGURAR_HORARIO));
            App.getInstance().iniciarActividad(MenuMedicamentosActivity.class);
        });

        medicamentos = obtenerListaBundle(MIS_MEDICAMENTOS_LIST, Medicamento.class);
        tipoAccion = obtenerBundle(TIPO_ACCION_MIS_MEDICAMENTOS, AccionMisMedicamentos.class);

        BusquedaMedicamentoAdapter busquedaMedicosAdapter = new BusquedaMedicamentoAdapter(medicamentos);
        RecyclerView recyclerView = list;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(busquedaMedicosAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        Medicamento medicamento = medicamentos.get(position);
                        List<HorarioMedicacion> horarios = HorarioMedicacionDAO.getHorarios(medicamento.getIdAndroid());
                        Bundle bundle = new Bundle();
                        bundle.putString(MEDICAMENTO, new Gson().toJson(medicamento));
                        bundle.putString(BUNDLE_NAME, MEDICAMENTO_BUNDLE);

                        switch (tipoAccion) {
                            case CONFIGURAR_HORARIO:
                                bundle.putString(HORARIOS_MEDICAMENTOS_LIST, new Gson().toJson(horarios));
                                App.getInstance().iniciarActividad(ListaDeHorariosMedicamentoActivity.class, bundle);
                                break;
                            case TOMAR_MEDICAMENTO:
                                App.getInstance().iniciarActividad(TomaMedicamentoActivity.class, bundle);
                                break;
                            case NA:

                                break;
                        }

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {}
                })
        );

        btnAgregar.setOnClickListener(v -> {
            App.getInstance().iniciarActividad(BuscarMedicamentoActivity.class, null);
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bundle bundle = new Bundle();
        bundle.putString(MIS_MEDICAMENTOS_LIST, new Gson().toJson(DataBaseManager.getDaoSession().getMedicamentoDao().loadAll()));
        bundle.putString(TIPO_ACCION_MIS_MEDICAMENTOS, new Gson().toJson(AccionMisMedicamentos.CONFIGURAR_HORARIO));
        App.getInstance().iniciarActividad(MenuMedicamentosActivity.class);
    }
}
