package com.glucomeandroidapp.Activities.Chat;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.ChatListadoAdapter;
import com.glucomeandroidapp.Adapters.RecyclerItemClickListener;
import com.glucomeandroidapp.Controladores.ChatControlador;
import com.glucomeandroidapp.POJO.APIClasses.Response.UsuarioChatResponse;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;
import static com.glucomeandroidapp.Utils.Util.MIS_CHAT_LIST;

public class ListadoChatActivity extends BaseActivity {

    @BindView(R.id.mis_chat_list) RecyclerView list;

    List<UsuarioChatResponse> chats = new ArrayList<>();

    Activity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_chat);
        ButterKnife.bind(this);
        activity = this;

        super.inicializar(R.string.mis_chats);
        Type listType = new TypeToken<ArrayList<UsuarioChatResponse>>(){}.getType();
        chats = new Gson().fromJson(getIntent().getExtras().getBundle(BUNDLE_NAME).getString(MIS_CHAT_LIST),listType);

        ChatListadoAdapter chatMedicosAdapter = new ChatListadoAdapter(chats);
        RecyclerView recyclerView = list;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(chatMedicosAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        UsuarioChatResponse chat = chats.get(position);
                        LoadingDialogManager.mostrar(activity);
                        ChatControlador.irAlChat(chat);
                    }

                    @Override public void onLongItemClick(View view, int position) {}
                })
        );
    }
}
