package com.glucomeandroidapp.Activities.Chat;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Adapters.ChatAdapter;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.ChatControlador;
import com.glucomeandroidapp.POJO.APIClasses.Response.UsuarioChatResponse;
import com.glucomeandroidapp.POJO.Chat.MensajeDeChat;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.AccountDataManager;
import com.glucomeandroidapp.Utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;

import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.MENSAJES_LIST;
import static com.glucomeandroidapp.Utils.Util.MIS_CHAT_LIST;
import static com.glucomeandroidapp.Utils.Util.URL_WEB_SOCKET;


public class ChatActivity extends BaseActivity {

    private StompClient mStompClient;

    @BindView(R.id.list_msg)
    RecyclerView chatList;
    @BindView(R.id.btn_chat_send)  View btnSend;
    @BindView(R.id.msg_type)  EditText editText;

    List<MensajeDeChat> mensajes = new ArrayList<>();

    private UsuarioChatResponse chat;
    private ChatAdapter adapter;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        activity =this;
       // conectarWebSocket();
        ButterKnife.bind(this);

        Type listType = new TypeToken<UsuarioChatResponse>(){}.getType();
        chat = new Gson().fromJson(getIntent().getExtras().getBundle(BUNDLE_NAME).getString(MIS_CHAT_LIST),listType);

        listType = new TypeToken<ArrayList<MensajeDeChat>>(){}.getType();
        mensajes = new Gson().fromJson(getIntent().getExtras().getBundle(BUNDLE_NAME).getString(MENSAJES_LIST),listType);

        super.inicializar(R.string.chat);
        txtNombrePantalla.setText(txtNombrePantalla.getText().toString().concat(" con " + chat.getNombre()));


        if(mensajes == null){
            mensajes = new ArrayList<>();
        }
        connect();

        adapter = new ChatAdapter(mensajes);
        chatList.setLayoutManager(new LinearLayoutManager(this));
        chatList.setHasFixedSize(true);
        chatList.setAdapter(adapter);
        chatList.smoothScrollToPosition(adapter.getItemCount());

        editText.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                ChatControlador.marcarMensajesComoLeidos(chat.getIdChat(),
                        AccountDataManager.obtenerIdUsuario(), LocalDateTime.now().toString());
            }
        });

        editText.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                enviarMensaje();
                handled = true;
            }
            return handled;
        });

        btnSend.setOnClickListener(v -> enviarMensaje());

    }

    private void enviarMensaje() {
        if (editText.getText().toString().trim().equals("")) {
            App.getInstance().mostrarMensajeFlotante("El mensaje no debe ser vacío");
        } else {
            //add message to list
            //ChatMessage chatMessage = new ChatMessage(editText.getText().toString(), isMine);
            //chatMessages.add(chatMessage);
            MensajeDeChat mensajeDeChat = new MensajeDeChat();
            mensajeDeChat.setChatId(chat.getIdChat());
            mensajeDeChat.setFromId(AccountDataManager.obtenerIdUsuario());
            mensajeDeChat.setToId(chat.getIdUsuario());
            mensajeDeChat.setDateSent(LocalDateTime.now().toString());
            mensajeDeChat.setMessage(editText.getText().toString().trim());
            mensajeDeChat.setType(1);
            mensajes.add(mensajeDeChat);
            adapter.notifyDataSetChanged();
            chatList.smoothScrollToPosition(adapter.getItemCount());
            mStompClient.send("/socket-subscriber/send/message",new Gson().toJson(mensajeDeChat) ).subscribe();
            editText.setText("");

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mStompClient.disconnect();
        ChatControlador.actualizarEstado(false);
    }

    private void connect() {

        mStompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, URL_WEB_SOCKET);

        mStompClient.connect();

        mStompClient.topic("/socket-publisher/"+ AccountDataManager.obtenerIdUsuario()).subscribe(message -> {
            Log.println(Log.ERROR,"TAG",message.getPayload());
            MensajeDeChat mensajeDeChat = new Gson().fromJson(message.getPayload(), MensajeDeChat.class);
            if(mensajeDeChat.getChatId() == chat.getIdChat()){
                mensajes.add(mensajeDeChat);
                actualizarRecycler();
            }
        });

        ChatControlador.actualizarEstado(true);
    }

    private void actualizarRecycler() {
        ChatActivity.this.runOnUiThread(() -> {

            adapter.notifyDataSetChanged();
            chatList.smoothScrollToPosition(adapter.getItemCount());
        });
    }
}
