package com.glucomeandroidapp.Activities.Ejercicios;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Activities.Mediciones.FiltrarHistorialMedicionesActivity;
import com.glucomeandroidapp.Adapters.HistorialEjerciciosAdapter;
import com.glucomeandroidapp.Adapters.Models.HistorialEjercicioAdapterModel;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.Ejercicio.Ejercicio;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.CANTIDAD_DESDE;
import static com.glucomeandroidapp.Utils.Util.CANTIDAD_HASTA;
import static com.glucomeandroidapp.Utils.Util.FECHA_DESDE;
import static com.glucomeandroidapp.Utils.Util.FECHA_HASTA;
import static com.glucomeandroidapp.Utils.Util.LISTADO_EJERCICIOS;
import static com.glucomeandroidapp.Utils.Util.MOMENTO_MEDICION;
import static com.glucomeandroidapp.Utils.Util.PACIENTE_NOMBRE;
import static com.glucomeandroidapp.Utils.Util.TAG_ACTIVITY_FILTRO;
import static com.glucomeandroidapp.Utils.Util.TIPO_EJERCICIO;
import static com.glucomeandroidapp.Utils.Util.VALOR_MEDICION_INFERIOR;
import static com.glucomeandroidapp.Utils.Util.VALOR_MEDICION_SUPERIOR;

public class HistorialEjerciciosActivity extends BaseActivity {

    @BindView(R.id.recyclerview) RecyclerView recyclerView;
    @BindView(R.id.btn_cargar_mas) TextView botonCargarMas;
    @BindView(R.id.btn_filtrar) ImageButton btnFiltrar;
    @BindView(R.id.sv_historial) ScrollView svHistorial;
    @BindView(R.id.sv_sin_datos) ScrollView svSinDatos;
    @BindView(R.id.img_no_data) ImageView imgNoData;

    private List<Ejercicio> ejercicioList;
    public HistorialEjerciciosAdapter historial_ejercicios_adapter;

    private String titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_generic);

        ButterKnife.bind(this);

        titulo = obtenerBundle(PACIENTE_NOMBRE,String.class);

        btnFiltrar.setOnClickListener(v -> {
            Intent intent = new Intent(App.getContext(), FiltrarHistorialEjerciciosActivity.class);
            startActivityForResult(intent, TAG_ACTIVITY_FILTRO);
        });

        if(titulo != null && !titulo.equals("")){
            super.inicializar("Ejercicios de " + titulo);
            ocultarBotonDeCarga();
        }else{
            super.inicializar(R.string.menu_historial_ejercicios);
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HistorialEjerciciosActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        ejercicioList = obtenerListaBundle(LISTADO_EJERCICIOS, Ejercicio.class);

        if(!ejercicioList.isEmpty()){
            agregarDatos(null, null, -1, -1, -1);
        } else {
            svSinDatos.setVisibility(View.VISIBLE);
            svHistorial.setVisibility(View.GONE);
            imgNoData.setImageResource(R.drawable.ic_ejercicio100);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == TAG_ACTIVITY_FILTRO) {
            if(resultCode == Activity.RESULT_OK){
                String fechaDesde = data.getStringExtra(FECHA_DESDE);
                String fechaHasta = data.getStringExtra(FECHA_HASTA);
                int cantidadSuperior = data.getIntExtra(CANTIDAD_HASTA, -1);
                int cantidadInferior = data.getIntExtra(CANTIDAD_DESDE, -1);
                int tipoDeEjercicio = data.getIntExtra(TIPO_EJERCICIO, -1);

                agregarDatos(fechaDesde, fechaHasta, cantidadSuperior, cantidadInferior, tipoDeEjercicio);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.e(Util.TAG, "No se pudo filtrar");
            }
        }
    }

    private void agregarDatos(String fechaDesde, String fechaHasta, int cantidadSuperior,
                              int cantidadInferior, int tipoDeEjercicio) {

        ArrayList<HistorialEjercicioAdapterModel> historialEjercicioAdapterModelArrayList = new ArrayList<>();
        if(ejercicioList != null){
            for(Ejercicio ejercicio : ejercicioList){
                HistorialEjercicioAdapterModel view = new HistorialEjercicioAdapterModel(
                        ejercicio.getFechaDeCreacion(),
                        ejercicio.getTipoEjercicio(),
                        String.valueOf(ejercicio.getCantidadMinutos().intValue()));
                historialEjercicioAdapterModelArrayList.add(view);
            }
        }

        historial_ejercicios_adapter = new HistorialEjerciciosAdapter(historialEjercicioAdapterModelArrayList);
        historial_ejercicios_adapter.filter(fechaDesde, fechaHasta, cantidadSuperior, cantidadInferior, tipoDeEjercicio);
        recyclerView.setAdapter(historial_ejercicios_adapter);
    }

    private void ocultarBotonDeCarga() {
        botonCargarMas.setVisibility(View.GONE);
    }
}
