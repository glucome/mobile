package com.glucomeandroidapp.Activities.Ejercicios;

import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.glucomeandroidapp.Activities.Custom.BaseActivity;
import com.glucomeandroidapp.Controladores.EjerciciosControlador;
import com.glucomeandroidapp.CustomComponents.EditText_Numerico;
import com.glucomeandroidapp.POJO.APIClasses.Requests.EjercicioRequest;
import com.glucomeandroidapp.POJO.Ejercicio.Ejercicio;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoEjercicio;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Utils.DataBaseManager;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.glucomeandroidapp.Utils.UtilesFecha;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;

public class IngresoEjercicioActivity extends BaseActivity {

    @BindView(R.id.valor_minutos)
    EditText_Numerico valorMinutos;
    @BindView(R.id.spinner_tipo_ejercicio)
    Spinner tipoEjercicio;
    @BindView(R.id.fecha_de_ejercicio)
    DatePicker fechaEjercicio;
    @BindView(R.id.hora_de_ejercicio)
    TimePicker horaEjercicio;
    @BindView(R.id.observacion_ejercicio)
    TextView txtObservacion;
    @BindView(R.id.btn_ingresar_ejercicio)
    TextView btnGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso_ejercicio);

        ButterKnife.bind(this);

        super.inicializar(R.string.ejercicio);

        // Logica
        btnGuardar.setOnClickListener(v -> {

            String minutos = valorMinutos.getText().toString();
            String observacion = txtObservacion.getText().toString();
            String fechaDeCreacion = UtilesFecha.formatearFecha(fechaEjercicio, horaEjercicio);
            int tipo = tipoEjercicio.getSelectedItemPosition();

            Ejercicio nuevoEjercicio = new Ejercicio(null, null,
                    UUID.randomUUID().toString(),Double.valueOf(minutos),
                    TipoEjercicio.getById(tipo), fechaDeCreacion, observacion);

            DataBaseManager.getDaoSession().getEjercicioDao().insert(nuevoEjercicio);


            EjercicioRequest ejercicioRequest = new EjercicioRequest(
                    nuevoEjercicio.getFechaDeCreacion(),
                    nuevoEjercicio.getCantidadMinutos().intValue(),
                    nuevoEjercicio.getTipoEjercicio().ordinal(),
                    nuevoEjercicio.getObservacion());

            //Envío la medición al backend
            LoadingDialogManager.mostrar(IngresoEjercicioActivity.this);
            EjerciciosControlador.enviarEjercicio(ejercicioRequest, nuevoEjercicio);

            finish();

        });

    }
}
