package com.glucomeandroidapp.Logic;

import com.glucomeandroidapp.POJO.Calibracion.Calibracion;
import com.glucomeandroidapp.POJO.Calibracion.ItemCalibracion;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoRegresion;
import com.glucomeandroidapp.POJO.Regresion;
import com.glucomeandroidapp.Utils.DataBaseManager;

import java.util.ArrayList;
import java.util.List;

public class CalibracionManagement {

    public static void calcularCalibracion(Calibracion calibracion){
        List<Double> datos635 = new ArrayList<Double>();
        List<Double> datos740 = new ArrayList<Double>();
        List<Double> datos850 = new ArrayList<Double>();
        List<Double> datos940 = new ArrayList<Double>();
        List<Double> datosInvasivos = new ArrayList<Double>();
        List<Regresion> regresiones = new ArrayList<Regresion>();

        for(ItemCalibracion itemCalibracion : calibracion.getItemCalibracionList()){
            datos635.add(itemCalibracion.getMedicionDispositivo().getMedicion635());
            datos740.add(itemCalibracion.getMedicionDispositivo().getMedicion740());
            datos850.add(itemCalibracion.getMedicionDispositivo().getMedicion850());
            datos940.add(itemCalibracion.getMedicionDispositivo().getMedicion940());
            datosInvasivos.add(Double.valueOf(itemCalibracion.getMedicionInvasiva()));
        }

        Regresion regresion635 = new Regresion(datos635, datosInvasivos, TipoRegresion.REGRESION_635);
        Regresion regresion740 = new Regresion(datos740, datosInvasivos, TipoRegresion.REGRESION_740);
        Regresion regresion850 = new Regresion(datos850, datosInvasivos, TipoRegresion.REGRESION_850);
        Regresion regresion940 = new Regresion(datos940, datosInvasivos, TipoRegresion.REGRESION_940);

        regresiones.add(regresion635);
        regresiones.add(regresion740);
        regresiones.add(regresion850);
        regresiones.add(regresion940);

        for(Regresion regresion : regresiones){
            DataBaseManager.getDaoSession().getRegresionDao().insertOrReplace(regresion);
            calibracion.getRegresionList().add(regresion);
        }
        DataBaseManager.getDaoSession().getCalibracionDao().insertOrReplace(calibracion);
    }
}
