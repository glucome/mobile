package com.glucomeandroidapp.Logic;

/**
 * Created by ClaudioSaccella on 01/05/2019.
 */

/**
 * Clase Calculadora de Insulina para calcular la insulina a inyectarse
 */
public class CalculadoraInsulinaManagement {

    /**
     * Calcular insulina a inyectar antes de una comida
     *
     * @param cantidadCHO,              cantidad de carbohidratos a ingerir en gramos
     * @param factorCorreccionCHO,      cantidad de carbohidratos consumidos por u de insulina
     * @return cantidad de unidades de insulina
     */
    public static double CalcularInsulinaPreComida(
            double cantidadCHO,
            double factorCorreccionCHO) {
        return cantidadCHO / factorCorreccionCHO;
    }

    /**
     * Calcular insulina a inyectar para corregir el azucar en sangre
     *
     * @param azucarEnSangreActual,   cantidad de azucar actual en mg/dl
     * @param azucarEnSangreObjetivo, cantidad de azucar objetivo en mg/dl
     * @param factorCorreccionAzucar, cantidad de azucar corregido por u de insulina en mg/dl
     * @return cantidad de unidades de insulina
     */
    public static double CalcularInsulinaCorreccionAzucar(
            double azucarEnSangreActual,
            double azucarEnSangreObjetivo,
            double factorCorreccionAzucar) {
        return (azucarEnSangreActual - azucarEnSangreObjetivo) / factorCorreccionAzucar;
    }

    /**
     * Calcular dosis basal/de fondo (40-50% de la dosis de insulina diaria total)
     * Suponemos 50%
     *
     * @param peso, peso de la persona en kg.
     * @return cantidad de unidades de insulina
     */
    public static double CalcularInsulinaBasal(int peso) {
        return 0.5 * CalcularInsulinaDiaria(peso);
    }

    /**
     * Calcular dosis diaria de insulina (0.55 * peso en kilogramos)
     *
     * @param peso, peso de la persona en kg.
     * @return cantidad de unidades de insulina
     */
    public static double CalcularInsulinaDiaria(double peso) {
        return Math.ceil(peso * 0.55);
    }

    /**
     * Calcular cantidad de CHO en gramos descartados por cada unidad de insulina
     * Esto puede calcularse usando la Regla del “500”: Cálculo del bolo de carbohidratos
     * (500 / insulina diaria)
     *
     * @param peso, peso de la persona en kg.
     * @return cantidad de gramos de carbohidratos
     */
    public static double CalcularFactorCHOInsulina(double peso) {
        return Math.ceil(500 / CalcularInsulinaDiaria(peso));
    }

    /**
     * Calcular cantidad de Azucar en mg/dk descartados por cada unidad de insulina
     * Esto puede calcularse usando la Regla del “1800”
     * (1800 / insulina diaria)
     *
     * @param peso, peso de la persona en kg.
     * @return cantidad de mg/dl de azucar
     */
    public static double CalcularFactorAzucarInsulina(double peso) {
        return  Math.ceil(1800 / CalcularInsulinaDiaria(peso));
    }
}
