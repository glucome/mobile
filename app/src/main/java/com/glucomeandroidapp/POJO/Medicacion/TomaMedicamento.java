package com.glucomeandroidapp.POJO.Medicacion;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Transient;

@Entity(nameInDb = "toma_medicamento")
public class TomaMedicamento {

    private static final long serialVersionUID = 7L;

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "id_backend")
    private Long idBackend;

    @Property(nameInDb = "id_medicamento_backend")
    private Long idMedicamentoBackend;

    @NotNull
    @Property(nameInDb = "cantidad")
    private Double cantidad;

    @NotNull
    @Property(nameInDb = "fecha_de_creacion")
    private String fechaDeCreacion;


    @Transient
    private Medicamento medicamento;


    public TomaMedicamento() {
    }

 
    public TomaMedicamento(@NotNull Long idMedicamentoBackend, @NotNull Double cantidadIngerida, @NotNull String fechaDeCreacion) {
        this.idMedicamentoBackend = idMedicamentoBackend;
        this.cantidad = cantidadIngerida;
        this.fechaDeCreacion = fechaDeCreacion;
    }


    @Generated(hash = 2136914612)
    public TomaMedicamento(Long id, Long idBackend, Long idMedicamentoBackend, @NotNull Double cantidad,
            @NotNull String fechaDeCreacion) {
        this.id = id;
        this.idBackend = idBackend;
        this.idMedicamentoBackend = idMedicamentoBackend;
        this.cantidad = cantidad;
        this.fechaDeCreacion = fechaDeCreacion;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdBackend() {
        return idBackend;
    }

    public void setIdBackend(Long idBackend) {
        this.idBackend = idBackend;
    }

    public Long getIdMedicamentoBackend() {
        return idMedicamentoBackend;
    }

    public void setIdMedicamentoBackend(Long idMedicamentoBackend) {
        this.idMedicamentoBackend = idMedicamentoBackend;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public String getFechaDeCreacion() {
        return fechaDeCreacion;
    }

    public void setFechaDeCreacion(String fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }

    @Override
    public String toString() {
        return "TomaMedicamento{" +
                "id=" + id +
                ", idBackend=" + idBackend +
                ", idMedicamentoBackend=" + idMedicamentoBackend +
                ", cantidad=" + cantidad +
                ", fechaDeCreacion='" + fechaDeCreacion + '\'' +
                '}';
    }

    public Medicamento getMedicamento() {
        return medicamento;
    }

    public void setMedicamento(Medicamento medicamento) {
        this.medicamento = medicamento;
    }
}
