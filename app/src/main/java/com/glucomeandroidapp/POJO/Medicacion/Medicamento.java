package com.glucomeandroidapp.POJO.Medicacion;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.io.Serializable;

@Entity(nameInDb = "medicamento")
public class Medicamento implements Serializable {

    private static final long serialVersionUID = 9L;

    @Id(autoincrement = true)
    private Long idAndroid;

    @Property(nameInDb = "id_backend")
    private long id;

    @Property(nameInDb = "droga")
    private String droga;

    @Property(nameInDb = "marca")
    private String marca;

    @Property(nameInDb = "presentacion")
    private String presentacion;

    @Property(nameInDb = "laboratorio")
    private String laboratorio;

    public Medicamento() {
    }

    public String getDroga() {
        return droga;
    }

    public void setDroga(String droga) {
        this.droga = droga;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public String getLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(String laboratorio) {
        this.laboratorio = laboratorio;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getIdAndroid() {
        return this.idAndroid;
    }

    public void setIdAndroid(long idAndroid) {
        this.idAndroid = idAndroid;
    }

    public void setIdAndroid(Long idAndroid) {
        this.idAndroid = idAndroid;
    }

    public Medicamento(long id, String droga, String marca, String presentacion, String laboratorio) {
        this.id = id;
        this.droga = droga;
        this.marca = marca;
        this.presentacion = presentacion;
        this.laboratorio = laboratorio;
    }

    @Generated(hash = 671957938)
    public Medicamento(Long idAndroid, long id, String droga, String marca, String presentacion,
            String laboratorio) {
        this.idAndroid = idAndroid;
        this.id = id;
        this.droga = droga;
        this.marca = marca;
        this.presentacion = presentacion;
        this.laboratorio = laboratorio;
    }
}
