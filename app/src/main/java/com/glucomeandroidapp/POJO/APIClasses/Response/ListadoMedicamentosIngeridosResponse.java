package com.glucomeandroidapp.POJO.APIClasses.Response;

import java.util.List;

public class ListadoMedicamentosIngeridosResponse extends AbstractResponse {

	private List<MedicamentoIngerido> medicamentosIngeridos;

	private boolean ultimaPagina;

	public ListadoMedicamentosIngeridosResponse(List<MedicamentoIngerido> medicamentosIngeridos, boolean ultimaPagina) {
		this.medicamentosIngeridos = medicamentosIngeridos;
		this.ultimaPagina = ultimaPagina;
	}

	public List<MedicamentoIngerido> getMedicamentosIngeridos() {
		return medicamentosIngeridos;
	}

	public void setMedicamentosIngeridos(List<MedicamentoIngerido> medicamentosIngeridos) {
		this.medicamentosIngeridos = medicamentosIngeridos;
	}

	public boolean isUltimaPagina() {
		return ultimaPagina;
	}

	public void setUltimaPagina(boolean ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}
}
