package com.glucomeandroidapp.POJO.APIClasses.Response;

import java.io.Serializable;

public class TomaMedicamentoResponse extends AbstractResponse implements Serializable {

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
