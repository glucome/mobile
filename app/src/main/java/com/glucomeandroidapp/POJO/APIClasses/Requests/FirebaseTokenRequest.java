package com.glucomeandroidapp.POJO.APIClasses.Requests;

/**
 * Creado por MartinArtime el 02 de mayo del 2019
 */
public class FirebaseTokenRequest {

    private String firebaseToken;
    private LoginRequest usuario;

    public FirebaseTokenRequest(String firebaseToken, LoginRequest usuario){
        this.firebaseToken = firebaseToken;
        this.usuario = usuario;
    }


    public LoginRequest getUsuario() {
        return usuario;
    }

    public void setUsuario(LoginRequest usuario) {
        this.usuario = usuario;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }
    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

}
