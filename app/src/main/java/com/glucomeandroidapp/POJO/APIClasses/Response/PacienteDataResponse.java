package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class PacienteDataResponse extends AbstractResponse implements Serializable {


    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("idUsuario")
    @Expose
    private long idUsuario;

    @SerializedName("nombre")
    @Expose
    private String nombre;

    @SerializedName("altura")
    @Expose
    private double altura;

    @SerializedName("fechaNacimiento")
    @Expose
    private String fechaNacimiento;

    @SerializedName("peso")
    @Expose
    private double peso;

    @SerializedName("sexo")
    @Expose
    private String sexo;

    @SerializedName("tipoDiabetes")
    @Expose
    private String tipoDiabetes;

    @SerializedName("medicos")
    @Expose
    private List<MedicoDataResponse> medicos;

    @SerializedName("allegados")
    @Expose
    private List<AllegadoDataResponse> allegados;

    public PacienteDataResponse(String nombre, double altura, String fechaNacimiento,
                                double peso, String sexo, String tipoDiabetes) {
        this.nombre = nombre;
        this.altura = altura;
        this.fechaNacimiento = fechaNacimiento;
        this.peso = peso;
        this.sexo = sexo;
        this.tipoDiabetes = tipoDiabetes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdUsuario() {
        return this.idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.id = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTipoDiabetes() {
        return tipoDiabetes;
    }

    public void setTipoDiabetes(String tipoDiabetes) {
        this.tipoDiabetes = tipoDiabetes;
    }

    public List<MedicoDataResponse> getMedicos() {
        return medicos;
    }

    public void setMedicos(List<MedicoDataResponse> medicos) {
        this.medicos = medicos;
    }

    public List<AllegadoDataResponse> getAllegados() {
        return allegados;
    }

    public void setAllegados(List<AllegadoDataResponse> allegados) {
        this.allegados = allegados;
    }

    @Override
    public String toString() {
        return "PacienteDataResponse{" +
                "id=" + id +
                ", idUsuario=" + idUsuario +
                ", medicos=" + medicos.toString() +
                ", allegados=" + allegados.toString() +
                ", nombre='" + nombre + '\'' +
                ", altura=" + altura +
                ", fechaNacimiento='" + fechaNacimiento + '\'' +
                ", peso=" + peso +
                ", sexo='" + sexo + '\'' +
                ", tipoDiabetes='" + tipoDiabetes + '\'' +
                '}';
    }
}
