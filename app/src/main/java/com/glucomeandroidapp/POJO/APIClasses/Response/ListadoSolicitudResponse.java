package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.SolicitudVinculacionAllegadoPaciente;

import java.util.List;

public class ListadoSolicitudResponse extends AbstractResponse{
    private List<SolicitudVinculacionAllegadoPaciente> solicitudes;

    public ListadoSolicitudResponse(List<SolicitudVinculacionAllegadoPaciente> solicitudes) {
        this.solicitudes = solicitudes;
    }

    public List<SolicitudVinculacionAllegadoPaciente> getSolicitudes() {
        return solicitudes;
    }

    public void setPacientes(List<SolicitudVinculacionAllegadoPaciente> solicitudes) {
        this.solicitudes = solicitudes;
    }
}