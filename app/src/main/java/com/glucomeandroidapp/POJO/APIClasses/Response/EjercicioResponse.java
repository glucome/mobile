package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.Ejercicio.Ejercicio;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoEjercicio;

import java.util.ArrayList;
import java.util.List;

public class EjercicioResponse {

	private long id;

	private String fechaHora;

	private double minutos;

	private int tipo;

	private String observacion;

	public static List<Ejercicio> toEjercicio(List<EjercicioResponse> listado) {
		List<Ejercicio> ejercicios = new ArrayList<>();
		if(ejercicios != null){
			for(EjercicioResponse response : listado) {
				Ejercicio ejercicio = new Ejercicio();
				ejercicio.setFechaDeCreacion(response.getFechaHora());
				ejercicio.setIdBackend(response.getId());
				ejercicio.setCantidadMinutos(response.getMinutos());
				ejercicio.setObservacion(response.getObservacion());
				ejercicio.setTipoEjercicio(TipoEjercicio.getById(response.getTipo()));
				ejercicios.add(ejercicio);
			}
		}
		return ejercicios;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}

	public double getMinutos() {
		return minutos;
	}

	public void setMinutos(double minutos) {
		this.minutos = minutos;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
}
