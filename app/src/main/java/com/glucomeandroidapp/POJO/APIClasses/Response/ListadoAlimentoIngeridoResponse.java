package com.glucomeandroidapp.POJO.APIClasses.Response;

import java.util.List;

public class ListadoAlimentoIngeridoResponse extends AbstractResponse{

	private List<AlimentoResponse> alimentoIngerido;

	private boolean ultimaPagina;

	public ListadoAlimentoIngeridoResponse(List<AlimentoResponse> alimentoIngerido, boolean ultimaPagina) {
		this.alimentoIngerido = alimentoIngerido;
		this.ultimaPagina = ultimaPagina;
	}

	public List<AlimentoResponse> getAlimentoIngerido() {
		return alimentoIngerido;
	}

	public void setAlimentoIngerido(List<AlimentoResponse> alimentoIngerido) {
		this.alimentoIngerido = alimentoIngerido;
	}

	public boolean isUltimaPagina() {
		return ultimaPagina;
	}

	public void setUltimaPagina(boolean ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}
}
