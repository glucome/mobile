package com.glucomeandroidapp.POJO.APIClasses.Response;

import java.util.List;

public class ListadoMedicoResponse extends AbstractResponse{
    private List<MedicoDataResponse> medicos;

    public ListadoMedicoResponse(List<MedicoDataResponse> medicos) {
        this.medicos = medicos;
    }

    public List<MedicoDataResponse> getMedicos() {
        return medicos;
    }

    public void setMedicos(List<MedicoDataResponse> medicos) {
        this.medicos = medicos;
    }
}
