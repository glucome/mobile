package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.Medicacion.Medicamento;
import com.glucomeandroidapp.POJO.Medicacion.TomaMedicamento;

import java.util.ArrayList;
import java.util.List;

public class MedicamentoIngerido {

	private long id;

	private Medicamento medicamento;

	private String fechaHora;

	private double cantidadIngerida;

	public static List<TomaMedicamento> toTomaMedicamento(List<MedicamentoIngerido> listado) {
		List<TomaMedicamento> medicamentos = new ArrayList<>();
		for(MedicamentoIngerido response : listado) {
			TomaMedicamento medicamento = new TomaMedicamento();
			medicamento.setCantidad(response.getCantidadIngerida());
			medicamento.setFechaDeCreacion(response.getFechaHora());
			medicamento.setMedicamento(response.getMedicamento());
			medicamentos.add(medicamento);
		}
		return medicamentos;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Medicamento getMedicamento() {
		return medicamento;
	}

	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}

	public String getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}

	public double getCantidadIngerida() {
		return cantidadIngerida;
	}

	public void setCantidadIngerida(double cantidadIngerida) {
		this.cantidadIngerida = cantidadIngerida;
	}


}
