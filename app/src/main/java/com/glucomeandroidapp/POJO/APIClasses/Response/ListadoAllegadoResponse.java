package com.glucomeandroidapp.POJO.APIClasses.Response;


import java.util.List;

public class ListadoAllegadoResponse extends AbstractResponse{
    private List<AllegadoDataResponse> allegados;

    public ListadoAllegadoResponse(List<AllegadoDataResponse> allegados) {
        this.allegados = allegados;
    }

    public List<AllegadoDataResponse> getAllegados() {
        return allegados;
    }

    public void setAllegados(List<AllegadoDataResponse> allegados) {
        this.allegados = allegados;
    }
}
