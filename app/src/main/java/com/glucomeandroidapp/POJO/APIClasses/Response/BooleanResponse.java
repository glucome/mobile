package com.glucomeandroidapp.POJO.APIClasses.Response;

/*
import com.glucomeandroidapp.POJO.APIClasses.Response.AbstractResponse;
*/

public class BooleanResponse extends AbstractResponse{
	
	private boolean valor;

	public BooleanResponse(boolean valor) {
		this.valor = valor;
	}

	public boolean isValor() {
		return valor;
	}

	public void setValor(boolean valor) {
		this.valor = valor;
	}
}
