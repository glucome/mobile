package com.glucomeandroidapp.POJO.APIClasses.Requests;

public class TomaMedicamentoRequest {

    private long idMedicamento;

    //Formato dd/MM/yyyy HH:mm
    private String fechaHora;

    private double cantidad;

    public TomaMedicamentoRequest() {
        super();
    }

    public TomaMedicamentoRequest(long idMedicamento, String fechaHora, double cantidad) {
        this.idMedicamento = idMedicamento;
        this.fechaHora = fechaHora;
        this.cantidad = cantidad;
    }

    public long getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(long idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }


}
