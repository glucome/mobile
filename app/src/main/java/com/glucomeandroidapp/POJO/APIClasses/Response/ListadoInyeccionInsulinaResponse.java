package com.glucomeandroidapp.POJO.APIClasses.Response;

import java.util.List;

public class ListadoInyeccionInsulinaResponse extends AbstractResponse{

    private List<InyeccionResponse> insulinaInyectadas;

    private boolean ultimaPagina;

    public ListadoInyeccionInsulinaResponse(List<InyeccionResponse> insulinaInyectadas, boolean ultimaPagina) {
        this.insulinaInyectadas = insulinaInyectadas;
        this.ultimaPagina = ultimaPagina;
    }

    public List<InyeccionResponse> getInsulinaInyectadas() {
        return insulinaInyectadas;
    }

    public void setInsulinaInyectadas(List<InyeccionResponse> insulinaInyectadas) {
        this.insulinaInyectadas = insulinaInyectadas;
    }

    public boolean isUltimaPagina() {
        return ultimaPagina;
    }

    public void setUltimaPagina(boolean ultimaPagina) {
        this.ultimaPagina = ultimaPagina;
    }
}
