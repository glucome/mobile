package com.glucomeandroidapp.POJO.APIClasses.Response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RespuestaApi<T extends AbstractResponse> {

	@SerializedName("mensaje")
	@Expose
	private String mensaje;

	@SerializedName("respuesta")
	@Expose
	private T respuesta;

	public RespuestaApi(T respuesta, String mensaje) {
		this.respuesta = respuesta;
		this.mensaje = mensaje;
	}

	public RespuestaApi(){}

	public RespuestaApi(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public T getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(T respuesta) {
		this.respuesta = respuesta;
	}

}
