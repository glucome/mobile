package com.glucomeandroidapp.POJO.APIClasses.Requests;

public class MedicamentoPacienteRequest {

    private long idPaciente;

    private long idMedicamento;

    public long getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public long getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(long idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public MedicamentoPacienteRequest(long idPaciente, long idMedicamento) {
        super();
        this.idPaciente = idPaciente;
        this.idMedicamento = idMedicamento;
    }

    public MedicamentoPacienteRequest() {
        super();
    }

}