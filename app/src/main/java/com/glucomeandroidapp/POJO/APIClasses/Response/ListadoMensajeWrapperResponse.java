package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.Chat.MensajeWrapper;

import java.util.List;

public class ListadoMensajeWrapperResponse extends AbstractResponse{

    private List<MensajeWrapper> mensajes;

    public ListadoMensajeWrapperResponse(List<MensajeWrapper> mensajes) { this.mensajes = mensajes; }

    public List<MensajeWrapper> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<MensajeWrapper> mensajes) {
        this.mensajes = mensajes;
    }
}
