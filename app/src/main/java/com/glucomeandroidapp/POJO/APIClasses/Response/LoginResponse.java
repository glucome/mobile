package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Creado por MartinArtime el 01 de mayo del 2019
 */
public class LoginResponse<T> extends AbstractResponse implements Serializable {

    private static final long serialVersionUID = 2L;

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("tokenFirebase")
    @Expose
    private String tokenFirebase;

    @SerializedName("usuario")
    @Expose
    private String usuario;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("token_fecha_expiracion")
    @Expose
    private String tokenFechaExpiracion;

    @SerializedName("asociado")
    @Expose
    private T asociado;

    @SerializedName("roles")
    @Expose
    private List<String> roles;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTokenFirebase() {
        return tokenFirebase;
    }

    public void setTokenFirebase(String tokenFirebase) {
        this.tokenFirebase = tokenFirebase;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public T getAsociado() {
        return asociado;
    }

    public void setAsociado(T asociado) {
        this.asociado = asociado;
    }

    public String getTokenFechaExpiracion() {
        return tokenFechaExpiracion;
    }

    public void setTokenFechaExpiracion(String tokenFechaExpiracion) {
        this.tokenFechaExpiracion = tokenFechaExpiracion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
