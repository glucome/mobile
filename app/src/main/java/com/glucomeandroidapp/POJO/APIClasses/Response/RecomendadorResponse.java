package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.Alimento;

import java.util.List;

public class RecomendadorResponse extends AbstractResponse {
	
	private List<Alimento> alimentosRecomendados;

	public List<Alimento> getAlimentosRecomendados() {
		return alimentosRecomendados;
	}

	public void setAlimentosRecomendados(List<Alimento> alimentosRecomendados) {
		this.alimentosRecomendados = alimentosRecomendados;
	}
	
}
