package com.glucomeandroidapp.POJO.APIClasses.Response;

public class InfoTarjetaAumentadaResponse extends AbstractResponse {

    public PacienteDataResponse getPaciente() {
        return paciente;
    }

    public void setPaciente(PacienteDataResponse paciente) {
        this.paciente = paciente;
    }

    PacienteDataResponse paciente;

}
