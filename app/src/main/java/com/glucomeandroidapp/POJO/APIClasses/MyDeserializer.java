package com.glucomeandroidapp.POJO.APIClasses;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Creado por MartinArtime el 04 de mayo del 2019
 */
public class MyDeserializer<AbstractResponse> implements JsonDeserializer<AbstractResponse> {
    @Override
    public AbstractResponse deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException {

/* ESTO ES SI SE HACE DISTINTO, LO DEJO POR LAS DUDAS PARA EL FUTURO
        // Get the "content" element from the parsed JSON
        JsonElement content = je.getAsJsonObject().get("respuesta");

        // Deserialize it. You use a new instance of Gson to avoid infinite recursion
        // to this deserializer
        return new Gson().fromJson(content, type);
*/
        return new Gson().fromJson(je.getAsJsonObject(), type);
    }
}
