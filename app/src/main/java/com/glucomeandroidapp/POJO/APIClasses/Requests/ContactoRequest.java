package com.glucomeandroidapp.POJO.APIClasses.Requests;

public class ContactoRequest {

	private String nombre;

	private String telefono;

	public ContactoRequest(String nombre, String telefono) {
		this.nombre = nombre;
		this.telefono = telefono;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
