package com.glucomeandroidapp.POJO.APIClasses.Response;

import java.util.List;

public class ListadoPacienteResponse extends AbstractResponse{
    private List<PacienteDataResponse> pacientes;

    public ListadoPacienteResponse(List<PacienteDataResponse> medicos) {
        this.pacientes = pacientes;
    }

    public List<PacienteDataResponse> getPacientes() {
        return pacientes;
    }

    public void setPacientes(List<PacienteDataResponse> medicos) {
        this.pacientes = medicos;
    }
}
