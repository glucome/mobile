package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.Alimento;
import com.glucomeandroidapp.POJO.Alimentos.IngresoAlimento;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoAlimento;

import java.util.ArrayList;
import java.util.List;


public class AlimentoResponse {

	private long id;

	private String fechaHora;

	private double porciones;

	private Alimento alimento;

	private int momento;

	private String observacion;

	public static List<IngresoAlimento> toIngresoAlimento(List<AlimentoResponse> listado) {
		List<IngresoAlimento> alimentos = new ArrayList<>();
		if(listado != null){
			for(AlimentoResponse response : listado) {
				IngresoAlimento alimento = new IngresoAlimento();
				alimento.setFechaDeCreacion(response.getFechaHora());
				alimento.setCantidad(response.getPorciones());
				alimento.setMomentoAlimento(MomentoAlimento.getById(response.getMomento()));
				alimento.setObservacion(response.getObservacion());
				alimento.setIdAlimentoBackend(response.getAlimento().getId());
				alimento.setNombreAlimento(response.getAlimento().getNombre());
				alimento.setIdBackend(response.getId());
				alimentos.add(alimento);
			}
		}
		return alimentos;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}


	public double getPorciones() {
		return porciones;
	}

	public void setPorciones(double porciones) {
		this.porciones = porciones;
	}

	public String getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Alimento getAlimento() {
		return alimento;
	}

	public void setAlimento(Alimento alimento) {
		this.alimento = alimento;
	}

	public int getMomento() {
		return momento;
	}

	public void setMomento(int momento) {
		this.momento = momento;
	}
}
