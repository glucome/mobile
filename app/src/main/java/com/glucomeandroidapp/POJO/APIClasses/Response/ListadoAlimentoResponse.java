package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.Alimento;

import java.util.List;

public class ListadoAlimentoResponse extends AbstractResponse {
	
	private List<Alimento> alimentos;

	public List<Alimento> getAlimentos() {
		return alimentos;
	}

	public void setAlimentos(List<Alimento> alimentosRecomendados) {
		this.alimentos = alimentosRecomendados;
	}
	
}
