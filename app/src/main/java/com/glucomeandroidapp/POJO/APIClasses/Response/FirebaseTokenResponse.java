package com.glucomeandroidapp.POJO.APIClasses.Response;

/**
 * Creado por MartinArtime el 23 de junio del 2019
 */
public class FirebaseTokenResponse extends AbstractResponse {

    private String token;

    public FirebaseTokenResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
