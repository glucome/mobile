package com.glucomeandroidapp.POJO.APIClasses.Requests;

public class IngresoAlimentoRequest {


    //Formato dd/MM/yyyy HH:mm
    private String fechaHora;

    private int porciones;

    private int momento;

    private long alimento;

    private String observacion;

    public IngresoAlimentoRequest() {
        super();
    }

    public IngresoAlimentoRequest(String fechaHora, int porciones, int momento, long alimento, String observacion) {
        this.fechaHora = fechaHora;
        this.porciones = porciones;
        this.momento = momento;
        this.alimento = alimento;
        this.observacion = observacion;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public int getPorciones() {
        return porciones;
    }

    public void setPorciones(int porciones) {
        this.porciones = porciones;
    }

    public int getMomento() {
        return momento;
    }

    public void setMomento(int momento) {
        this.momento = momento;
    }

    public long getAlimento() {
        return alimento;
    }

    public void setAlimento(long alimento) {
        this.alimento = alimento;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
