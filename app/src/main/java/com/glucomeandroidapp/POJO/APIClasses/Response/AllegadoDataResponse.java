package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class AllegadoDataResponse extends AbstractResponse implements Serializable {


    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("idUsuario")
    @Expose
    private long idUsuario;

    @SerializedName("tokenFirebase")
    @Expose
    private String tokenFirebase;

    @SerializedName("nombre")
    @Expose
    private String nombre;

    public AllegadoDataResponse(String nombre) {
        this.nombre = nombre;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdUsuario() {
        return this.idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.id = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTokenFirebase() {
        return tokenFirebase;
    }

    public void setTokenFirebase(String tokenFirebase) {
        this.tokenFirebase = tokenFirebase;
    }

    @Override
    public String toString() {
        return "MedicoDataResponse{" +
                "id=" + id +
                ", idUsuario=" + idUsuario +
                ", tokenFirebase=" + tokenFirebase +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
