package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.Enumeraciones.TipoInsulina;
import com.glucomeandroidapp.POJO.Inyeccion;

import java.util.ArrayList;
import java.util.List;

public class InyeccionResponse {

    private long id;

    private String fechaHora;

    private double unidades;

    private int tipo;

    public static List<Inyeccion> toInyeccion(List<InyeccionResponse> listado) {
        List<Inyeccion> inyecciones = new ArrayList<>();
        if(listado != null){
            for(InyeccionResponse response : listado) {
                Inyeccion inyeccion = new Inyeccion();
                inyeccion.setFechaDeCreacion(response.getFechaHora());
                inyeccion.setIdBackend(response.getId());
                inyeccion.setTipoInsulina(TipoInsulina.getById(response.getTipo()));
                inyeccion.setValorInyeccion(response.getUnidades());
                inyecciones.add(inyeccion);
            }
        }
        return inyecciones;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public void setUnidades(double unidades) {
        this.unidades = unidades;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public long getId() {
        return id;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public double getUnidades() {
        return unidades;
    }

    public int getTipo() {
        return tipo;
    }
}
