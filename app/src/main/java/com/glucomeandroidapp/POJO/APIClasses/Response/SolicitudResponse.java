package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.SolicitudVinculacion;

public class SolicitudResponse extends AbstractResponse{

    private SolicitudVinculacion solicitud;

    public SolicitudResponse(SolicitudVinculacion solicitud) {
        super();
        this.solicitud = solicitud;
    }

    public SolicitudVinculacion getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(SolicitudVinculacion solicitud) {
        this.solicitud = solicitud;
    }

    public SolicitudResponse() {
    }
}
