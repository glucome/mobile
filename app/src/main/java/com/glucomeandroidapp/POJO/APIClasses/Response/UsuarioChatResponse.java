package com.glucomeandroidapp.POJO.APIClasses.Response;

public class UsuarioChatResponse extends AbstractResponse {

    private Long idUsuario;
    private Long idChat;
    private String nombre;
    private String avatar;
    private String estado;
    private Long cantidadMensajesNoLeidos;

    public UsuarioChatResponse() {
    }

    public UsuarioChatResponse(Long idUsuario, Long idChat, String nombre, String avatar, String estado, Long cantidadMensajesNoLeidos) {
        this.idUsuario = idUsuario;
        this.idChat = idChat;
        this.nombre = nombre;
        this.avatar = avatar;
        this.estado = estado;
        this.cantidadMensajesNoLeidos = cantidadMensajesNoLeidos;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdChat() {
        return idChat;
    }

    public void setIdChat(Long idChat) {
        this.idChat = idChat;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getCantidadMensajesNoLeidos() {
        return cantidadMensajesNoLeidos;
    }

    public void setCantidadMensajesNoLeidos(Long cantidadMensajesNoLeidos) { this.cantidadMensajesNoLeidos = cantidadMensajesNoLeidos; }
}
