package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class MedicoDataResponse extends AbstractResponse implements Serializable {


    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("idUsuario")
    @Expose
    private long idUsuario;

    @SerializedName("tokenFirebase")
    @Expose
    private String tokenFirebase;

    @SerializedName("nombre")
    @Expose
    private String nombre;

    @SerializedName("matricula")
    @Expose
    private String matricula;

    @SerializedName("descripcion")
    @Expose
    private String descripcion;

    @SerializedName("ubicacion")
    @Expose
    private String ubicacion;


    public MedicoDataResponse(String nombre, String matricula, String descripcion, String ubicacion) {
        this.nombre = nombre;
        this.matricula = matricula;
        this.descripcion = descripcion;
        this.ubicacion = ubicacion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdUsuario() {
        return this.idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.id = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getTokenFirebase() {
        return tokenFirebase;
    }

    public void setTokenFirebase(String tokenFirebase) {
        this.tokenFirebase = tokenFirebase;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "MedicoDataResponse{" +
                "id=" + id +
                ", idUsuario=" + idUsuario +
                ", tokenFirebase=" + tokenFirebase +
                ", nombre='" + nombre + '\'' +
                ", matricula='" + matricula + '\'' +
                '}';
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
