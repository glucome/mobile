package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.Contacto;

import java.util.List;

public class ListadoContactoResponse extends AbstractResponse {

	private List<Contacto> contactos;

	public ListadoContactoResponse(List<Contacto> contactos) {
		this.contactos = contactos;
	}

	public List<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(List<Contacto> contactos) {
		this.contactos = contactos;
	}

}
