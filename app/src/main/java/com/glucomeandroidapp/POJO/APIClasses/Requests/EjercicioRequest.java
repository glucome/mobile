package com.glucomeandroidapp.POJO.APIClasses.Requests;

public class EjercicioRequest {

    //Formato dd/MM/yyyy HH:mm
    private String fechaHora;

    private int minutos;

    private int tipo;

    private String observacion;

    public EjercicioRequest() {
        super();
    }

    public EjercicioRequest(String fechaHora, int minutos, int tipo, String observacion) {
        super();
        this.fechaHora = fechaHora;
        this.minutos = minutos;
        this.tipo = tipo;
        this.observacion = observacion;
    }


    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
