package com.glucomeandroidapp.POJO.APIClasses.Requests;

public class RegistroAllegadoRequest {

    private long id;

    private String nombreUsuario;

    private String mail;

    private String password;

    private String nombre;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public RegistroAllegadoRequest(String nombreUsuario, String mail, String password, String nombre) {
        super();
        this.id = id;
        this.nombreUsuario = nombreUsuario;
        this.mail = mail;
        this.password = password;
        this.nombre = nombre;
    }

    public RegistroAllegadoRequest() {
        super();
    }
}
