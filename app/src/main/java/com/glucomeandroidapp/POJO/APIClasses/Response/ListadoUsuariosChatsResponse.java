package com.glucomeandroidapp.POJO.APIClasses.Response;

import java.util.List;

public class ListadoUsuariosChatsResponse extends  AbstractResponse {

    private List<UsuarioChatResponse> usuariosChats;

    public ListadoUsuariosChatsResponse(List<UsuarioChatResponse> usuariosChats) {
        this.usuariosChats = usuariosChats;
    }

    public List<UsuarioChatResponse> getUsuariosChats() {
        return usuariosChats;
    }

    public void setUsuariosChats(List<UsuarioChatResponse> usuariosChats) {
        this.usuariosChats = usuariosChats;
    }
}
