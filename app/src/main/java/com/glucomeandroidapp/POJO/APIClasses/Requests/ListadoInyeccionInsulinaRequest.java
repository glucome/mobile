package com.glucomeandroidapp.POJO.APIClasses.Requests;

public class ListadoInyeccionInsulinaRequest {

	private String fechaHoraDesde;

	private String fechaHoraHasta;

	private Double unidadesDesde;

	private Double unidadesHasta;

	private Integer tipo;

	private Integer numeroPagina;

	private Integer registrosPorPagina;

	public String getFechaHoraDesde() {
		return fechaHoraDesde;
	}

	public void setFechaHoraDesde(String fechaHoraDesde) {
		this.fechaHoraDesde = fechaHoraDesde;
	}

	public String getFechaHoraHasta() {
		return fechaHoraHasta;
	}

	public void setFechaHoraHasta(String fechaHoraHasta) {
		this.fechaHoraHasta = fechaHoraHasta;
	}

	public Double getUnidadesDesde() {
		return unidadesDesde;
	}

	public void setUnidadesDesde(Double unidadesDesde) {
		this.unidadesDesde = unidadesDesde;
	}

	public Double getUnidadesHasta() {
		return unidadesHasta;
	}

	public void setUnidadesHasta(Double unidadesHasta) {
		this.unidadesHasta = unidadesHasta;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public Integer getNumeroPagina() {
		return numeroPagina;
	}

	public void setNumeroPagina(Integer numeroPagina) {
		this.numeroPagina = numeroPagina;
	}

	public Integer getRegistrosPorPagina() {
		return registrosPorPagina;
	}

	public void setRegistrosPorPagina(Integer registrosPorPagina) {
		this.registrosPorPagina = registrosPorPagina;
	}

}
