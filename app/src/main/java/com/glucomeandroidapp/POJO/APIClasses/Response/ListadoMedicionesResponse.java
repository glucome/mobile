package com.glucomeandroidapp.POJO.APIClasses.Response;

import java.util.List;

public class ListadoMedicionesResponse extends AbstractResponse {

    List<MedicionPacienteResponse> mediciones;

    public ListadoMedicionesResponse(List<MedicionPacienteResponse> mediciones) {
        this.mediciones = mediciones;
    }

    public List<MedicionPacienteResponse> getMediciones() {
        return mediciones;
    }

    public void setMediciones(List<MedicionPacienteResponse> mediciones) {
        this.mediciones = mediciones;
    }
}
