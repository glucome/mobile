package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoMedicion;
import com.glucomeandroidapp.POJO.Medicion.Medicion;

import java.util.ArrayList;
import java.util.List;

public class MedicionPacienteResponse {

    private long id;
    private String valor;
    private String tipo;
    private String fecha;

    public MedicionPacienteResponse(Long id, String valor, String tipo, String fecha) {
        super();
        this.id = id;
        this.valor = valor;
        this.tipo = tipo;
        this.fecha = fecha;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public static List<Medicion> getToMedicion(List<MedicionPacienteResponse> responses){
        List<Medicion> mediciones = new ArrayList<Medicion>();
        for(MedicionPacienteResponse response : responses) {
            Medicion medicion = new Medicion();
            medicion.setTipoMedicion(TipoMedicion.INVASIVA);
            medicion.setValorMedicion(Double.valueOf(response.getValor()));
            medicion.setMomento(MomentoMedicion.getById(Integer.parseInt(response.getTipo())));
            medicion.setFechaDeCreacion(response.getFecha());
            medicion.setIdBackend(response.getId());
            mediciones.add(medicion);
        }
        return mediciones;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
