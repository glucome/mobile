package com.glucomeandroidapp.POJO.APIClasses.Response;

import java.util.List;

public class ListadoEjercicioResponse extends AbstractResponse {

	private List<EjercicioResponse> ejercicios;

	private boolean ultimaPagina;

	public ListadoEjercicioResponse(List<EjercicioResponse> ejercicios, boolean ultimaPagina) {
		this.ejercicios = ejercicios;
		this.ultimaPagina = ultimaPagina;
	}

	public List<EjercicioResponse> getEjercicios() {
		return ejercicios;
	}

	public void setEjercicios(List<EjercicioResponse> ejercicios) {
		this.ejercicios = ejercicios;
	}

	public boolean isUltimaPagina() {
		return ultimaPagina;
	}

	public void setUltimaPagina(boolean ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}
}
