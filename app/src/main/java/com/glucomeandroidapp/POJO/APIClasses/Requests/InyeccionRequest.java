package com.glucomeandroidapp.POJO.APIClasses.Requests;

public class InyeccionRequest {

    //Formato dd/MM/yyyy HH:mm
    private String fechaHora;

    private int unidades;

    private int tipo;

    public InyeccionRequest() {
        super();
    }

    public InyeccionRequest(String fechaHora, int unidades, int tipo) {
        super();
        this.fechaHora = fechaHora;
        this.unidades = unidades;
        this.tipo = tipo;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public int getUnidades() {
        return unidades;
    }

    public void setUnidades(int unidades) {
        this.unidades = unidades;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

}
