package com.glucomeandroidapp.POJO.APIClasses.Response;

import com.glucomeandroidapp.POJO.Medicacion.Medicamento;

import java.util.List;

public class ListadoMedicamentoResponse extends AbstractResponse {

    private List<Medicamento> medicamentos;

    public ListadoMedicamentoResponse(List<Medicamento> medicamentos) {
        this.medicamentos = medicamentos;
    }

    public List<Medicamento> getMedicamentos() {
        return medicamentos;
    }

    public void setMedicamentos(List<Medicamento> medicamentos) {
        this.medicamentos = medicamentos;
    }

}
