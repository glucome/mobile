package com.glucomeandroidapp.POJO.Chat;

/**
 * Creado por MartinArtime el 13 de junio del 2019
 */
public class MensajeDeChat {

   private int type;

   private long fromId;

   private long toId;

   private String message;

   private String dateSent;

   private String fechaHora;

   private long chatId;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getFromId() {
        return fromId;
    }

    public void setFromId(long fromId) {
        this.fromId = fromId;
    }

    public long getToId() {
        return toId;
    }

    public void setToId(long toId) {
        this.toId = toId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateSent() {
        return dateSent;
    }

    public void setDateSent(String dateSent) {
        this.dateSent = dateSent;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }

    public MensajeDeChat() {
    }

    public  MensajeDeChat(MensajeWrapper mensajeWrapper, long idChat){
        this.message = mensajeWrapper.getCuerpo();
        this.type = 1;
        this.toId = mensajeWrapper.getDestinatario();
        this.fromId = mensajeWrapper.getAutor();
        this.chatId = idChat;
        this.fechaHora = mensajeWrapper.getFechaHora();
    }


}
