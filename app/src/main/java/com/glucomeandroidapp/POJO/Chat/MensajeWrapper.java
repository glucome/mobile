package com.glucomeandroidapp.POJO.Chat;

public class MensajeWrapper {

    private String cuerpo;
    private Long autor;
    private Long destinatario;
    private String fechaHora;

    public MensajeWrapper(String cuerpo, Long autor, Long destinatario, String fechaHora) {
        this.cuerpo = cuerpo;
        this.autor = autor;
        this.destinatario = destinatario;
        this.fechaHora = fechaHora;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public Long getAutor() {
        return autor;
    }

    public void setAutor(Long autor) {
        this.autor = autor;
    }

    public Long getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(Long destinatario) {
        this.destinatario = destinatario;
    }

    public String getFechaHora() { return fechaHora; }

    public void setFechaHora(String fechaHora) { this.fechaHora = fechaHora; }
}
