package com.glucomeandroidapp.POJO;

import com.glucomeandroidapp.POJO.Enumeraciones.TipoRegresion;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Transient;

import java.util.List;

@Entity(nameInDb = "regresion")
public class Regresion {

    private static final long serialVersionUID = 12L;

    @Id(autoincrement = true)
    private Long id;

    @Transient
    private List<Double> x;

    @Transient
    private List<Double> y;

    @Transient
    private int n;          //número de datos

    @Property(nameInDb = "regresion_id")
    public long idRegresion;

    @Property(nameInDb = "a")
    public Double a = Double.valueOf(0);

    @Property(nameInDb = "b")
    public Double b = Double.valueOf(0);    //pendiente y ordenada en el origen

    @Property(nameInDb = "r")
    public Double r = Double.valueOf(0);

    @NotNull
    @Property(nameInDb = "tipo_regresion")
    @Convert(converter = TipoRegresion.TipoRegresionConverter.class, columnType = String.class)
    private TipoRegresion tipoRegresion;

    public Regresion(List<Double> x, List<Double> y, TipoRegresion tipoRegresion) {
        this.x = x;
        this.y = y;
        n = x.size(); //número de datos
        this.tipoRegresion = tipoRegresion;
        lineal();
        correlacion();
    }

    @Generated(hash = 783454498)
    public Regresion(Long id, long idRegresion, Double a, Double b, Double r,
            @NotNull TipoRegresion tipoRegresion) {
        this.id = id;
        this.idRegresion = idRegresion;
        this.a = a;
        this.b = b;
        this.r = r;
        this.tipoRegresion = tipoRegresion;
    }

    @Generated(hash = 1983643030)
    public Regresion() {
    }

    private void lineal(){
        Double pxy, sx, sy, sx2;
        pxy = sx = sy = sx2 = 0.0;
        for(int i = 0; i < n; i++){
            sx += x.get(i);
            sy += y.get(i);
            sx2 += (x.get(i) * x.get(i));
            pxy += (x.get(i) * y.get(i));
        }
        a = ((n * pxy) - (sx * sy)) / ((n * sx2) - (sx * sx));
        b = ((sx2 * sy) - (sx * pxy)) / ((n * sx2) - (sx * sx));
    }

    public void correlacion(){
        //valores medios
        Double suma = 0.0;
        for(int i = 0; i < n; i++){
            suma += x.get(i);
        }
        Double mediaX = suma / n;

        suma = 0.0;
        for(int i = 0; i < n; i++){
            suma += y.get(i);
        }
        Double mediaY = suma / n;
        //coeficiente de correlación
        Double pxy, sx2, sy2;
        pxy = sx2 = sy2 = 0.0;
        for(int i = 0; i < n; i++){
            pxy += (x.get(i) - mediaX) * (y.get(i) - mediaY);
            sx2 += (x.get(i) - mediaX) * (x.get(i) - mediaX);
            sy2 += (y.get(i) - mediaY) * (y.get(i) - mediaY);
        }
        r = pxy / Math.sqrt(sx2 * sy2);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getA() {
        return this.a;
    }

    public void setA(Double a) {
        this.a = a;
    }

    public Double getB() {
        return this.b;
    }

    public void setB(Double b) {
        this.b = b;
    }

    public Double getR() {
        return this.r;
    }

    public void setR(Double r) {
        this.r = r;
    }

    public long getIdRegresion() {
        return this.idRegresion;
    }

    public void setIdRegresion(long idRegresion) {
        this.idRegresion = idRegresion;
    }

    public TipoRegresion getTipoRegresion() {
        return this.tipoRegresion;
    }

    public void setTipoRegresion(TipoRegresion tipoRegresion) {
        this.tipoRegresion = tipoRegresion;
    }
}
