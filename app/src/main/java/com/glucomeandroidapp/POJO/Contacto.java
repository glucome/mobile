package com.glucomeandroidapp.POJO;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.io.Serializable;

@Entity(nameInDb = "contacto")
public class Contacto implements Serializable {

    private static final long serialVersionUID = 7L;

    @SerializedName("idAndroid")
    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "id_backend")
    @SerializedName("id")
    private Long idBackend;

    @Property(nameInDb = "nombre")
    private String nombre;

    @Property(nameInDb = "telefono")
    private String telefono;

    public Contacto(String nombre, String telefono){
        this.nombre = nombre;
        this.telefono = telefono;
    }

    @Generated(hash = 1623103275)
    public Contacto(Long id, Long idBackend, String nombre, String telefono) {
        this.id = id;
        this.idBackend = idBackend;
        this.nombre = nombre;
        this.telefono = telefono;
    }

    @Generated(hash = 1624484980)
    public Contacto() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdBackend() {
        return this.idBackend;
    }

    public void setIdBackend(Long idBackend) {
        this.idBackend = idBackend;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }







}
