package com.glucomeandroidapp.POJO;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;

@Entity(nameInDb = "notificacion")
public class Notificacion {

    private static final long serialVersionUID = 8L;

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    @Property(nameInDb = "tipo")
    private String tipoNotificacion;

    @Generated(hash = 855842918)
    public Notificacion(Long id, @NotNull String tipoNotificacion) {
        this.id = id;
        this.tipoNotificacion = tipoNotificacion;
    }

    @Generated(hash = 1352146119)
    public Notificacion() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoNotificacion() {
        return this.tipoNotificacion;
    }

    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }
}
