package com.glucomeandroidapp.POJO.Calibracion;

import com.glucomeandroidapp.POJO.DaoSession;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoRegresion;
import com.glucomeandroidapp.POJO.Regresion;
import com.glucomeandroidapp.POJO.RegresionDao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.OrderBy;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;

@Entity(nameInDb = "calibracion")
public class Calibracion {

    private static final long serialVersionUID = 9L;

    @Id(autoincrement = true)
    private Long id;

    @ToMany(referencedJoinProperty = "idCalibracion")
    @OrderBy("id ASC")
    private List<ItemCalibracion> itemCalibracionList;

    @ToMany(referencedJoinProperty = "idRegresion")
    @OrderBy("id ASC")
    private List<Regresion> regresionList;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 556091773)
    private transient CalibracionDao myDao;

    @Generated(hash = 1972147520)
    public Calibracion(Long id) {
        this.id = id;
    }

    @Generated(hash = 1406821130)
    public Calibracion() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1074130120)
    public List<ItemCalibracion> getItemCalibracionList() {
        if (itemCalibracionList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ItemCalibracionDao targetDao = daoSession.getItemCalibracionDao();
            List<ItemCalibracion> itemCalibracionListNew = targetDao
                    ._queryCalibracion_ItemCalibracionList(id);
            synchronized (this) {
                if (itemCalibracionList == null) {
                    itemCalibracionList = itemCalibracionListNew;
                }
            }
        }
        return itemCalibracionList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1534840509)
    public synchronized void resetItemCalibracionList() {
        itemCalibracionList = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    public boolean cargaFinalizada(){
        for(ItemCalibracion itemCalibracion : getItemCalibracionList()){
            if(!itemCalibracion.cargaFinalizada())
                return false;
        }
        return true;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1471452618)
    public List<Regresion> getRegresionList() {
        if (regresionList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            RegresionDao targetDao = daoSession.getRegresionDao();
            List<Regresion> regresionListNew = targetDao._queryCalibracion_RegresionList(id);
            synchronized (this) {
                if (regresionList == null) {
                    regresionList = regresionListNew;
                }
            }
        }
        return regresionList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1985924785)
    public synchronized void resetRegresionList() {
        regresionList = null;
    }

    public Regresion getRegresionPorTipo(TipoRegresion tipoRegresion){
        List<Regresion> regresiones = getRegresionList();
        for(Regresion regresion : regresiones){


        }
        return null;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1917288830)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCalibracionDao() : null;
    }
}
