package com.glucomeandroidapp.POJO.Calibracion;

import com.glucomeandroidapp.POJO.DaoSession;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.POJO.Notificacion;
import com.glucomeandroidapp.POJO.NotificacionDao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToOne;

import java.util.ArrayList;
import java.util.List;

@Entity(nameInDb = "horario_calibracion")
public class HorarioCalibracion {

    private static final long serialVersionUID = 6L;

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    @Property(nameInDb = "momento")
    @Convert(converter = MomentoMedicion.MomentoMedicionConverter.class, columnType = String.class)
    private MomentoMedicion momentoMedicion;

    @NotNull
    @Property(nameInDb = "hora")
    private int hora;

    @NotNull
    @Property(nameInDb = "minuto")
    private int minuto;

    @Property(nameInDb = "notificacion_id")
    private long notificacionId;

    @ToOne(joinProperty = "notificacionId")
    private Notificacion notificacion;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1452279254)
    private transient HorarioCalibracionDao myDao;

    @Generated(hash = 1572209264)
    private transient Long notificacion__resolvedKey;

    @Generated(hash = 740710913)
    public HorarioCalibracion(Long id, @NotNull MomentoMedicion momentoMedicion, int hora, int minuto,
            long notificacionId) {
        this.id = id;
        this.momentoMedicion = momentoMedicion;
        this.hora = hora;
        this.minuto = minuto;
        this.notificacionId = notificacionId;
    }

    @Generated(hash = 1722326017)
    public HorarioCalibracion() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MomentoMedicion getMomentoMedicion() {
        return this.momentoMedicion;
    }

    public void setMomentoMedicion(MomentoMedicion momentoMedicion) {
        this.momentoMedicion = momentoMedicion;
    }

    public int getHora() {
        return this.hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinuto() {
        return this.minuto;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    public static List<HorarioCalibracion> obtenerHorarios(){
        List<HorarioCalibracion> horarios = new ArrayList<HorarioCalibracion>();
        for(MomentoMedicion momento : MomentoMedicion.values()){
            HorarioCalibracion horario = new HorarioCalibracion();
            horario.setMomentoMedicion(momento);
            horario.setHora(0);
            horario.setMinuto(0);
            horarios.add(horario);
        }
        return horarios;
    }

    public long getNotificacionId() {
        return this.notificacionId;
    }

    public void setNotificacionId(long notificacionId) {
        this.notificacionId = notificacionId;
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 443736560)
    public Notificacion getNotificacion() {
        long __key = this.notificacionId;
        if (notificacion__resolvedKey == null || !notificacion__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            NotificacionDao targetDao = daoSession.getNotificacionDao();
            Notificacion notificacionNew = targetDao.load(__key);
            synchronized (this) {
                notificacion = notificacionNew;
                notificacion__resolvedKey = __key;
            }
        }
        return notificacion;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1802442856)
    public void setNotificacion(@NotNull Notificacion notificacion) {
        if (notificacion == null) {
            throw new DaoException(
                    "To-one property 'notificacionId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.notificacion = notificacion;
            notificacionId = notificacion.getId();
            notificacion__resolvedKey = notificacionId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 2077317085)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getHorarioCalibracionDao() : null;
    }
}
