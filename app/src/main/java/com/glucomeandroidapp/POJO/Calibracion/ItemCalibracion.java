package com.glucomeandroidapp.POJO.Calibracion;

import com.glucomeandroidapp.POJO.DaoSession;
import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.POJO.Medicion.MedicionDispositivo;
import com.glucomeandroidapp.POJO.Medicion.MedicionDispositivoDao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToOne;

@Entity(nameInDb = "item_calibracion")
public class ItemCalibracion {

    private static final long serialVersionUID = 10L;

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    @Property(nameInDb = "momento")
    @Convert(converter = MomentoMedicion.MomentoMedicionConverter.class, columnType = String.class)
    private MomentoMedicion momentoMedicion;

    @Property(nameInDb = "medicion_dispositivo_id")
    private long medicionDispositivoId;

    @ToOne(joinProperty = "medicionDispositivoId")
    private MedicionDispositivo medicionDispositivo;

    @NotNull
    @Property(nameInDb = "medicion_invasiva")
    private Integer medicionInvasiva;

    @Property(nameInDb = "calibracion_id")
    private long idCalibracion;

    @NotNull
    @Property(nameInDb = "fecha_medicion_calibracion")
    private String fechaDeMedicionCalibracion;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 833337348)
    private transient ItemCalibracionDao myDao;

    @Generated(hash = 973132659)
    private transient Long medicionDispositivo__resolvedKey;

    @Generated(hash = 1437393256)
    public ItemCalibracion(Long id, @NotNull MomentoMedicion momentoMedicion, long medicionDispositivoId,
            @NotNull Integer medicionInvasiva, long idCalibracion, @NotNull String fechaDeMedicionCalibracion) {
        this.id = id;
        this.momentoMedicion = momentoMedicion;
        this.medicionDispositivoId = medicionDispositivoId;
        this.medicionInvasiva = medicionInvasiva;
        this.idCalibracion = idCalibracion;
        this.fechaDeMedicionCalibracion = fechaDeMedicionCalibracion;
    }

    @Generated(hash = 686616753)
    public ItemCalibracion() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MomentoMedicion getMomentoMedicion() {
        return this.momentoMedicion;
    }

    public void setMomentoMedicion(MomentoMedicion momentoMedicion) {
        this.momentoMedicion = momentoMedicion;
    }

    public Integer getMedicionInvasiva() {
        return this.medicionInvasiva;
    }

    public void setMedicionInvasiva(Integer medicionInvasiva) {
        this.medicionInvasiva = medicionInvasiva;
    }

    public long getIdCalibracion() {
        return this.idCalibracion;
    }

    public void setIdCalibracion(long idCalibracion) {
        this.idCalibracion = idCalibracion;
    }

    public long getMedicionDispositivoId() {
        return this.medicionDispositivoId;
    }

    public void setMedicionDispositivoId(long medicionDispositivoId) {
        this.medicionDispositivoId = medicionDispositivoId;
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 2106300897)
    public MedicionDispositivo getMedicionDispositivo() {
        long __key = this.medicionDispositivoId;
        if (medicionDispositivo__resolvedKey == null
                || !medicionDispositivo__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            MedicionDispositivoDao targetDao = daoSession.getMedicionDispositivoDao();
            MedicionDispositivo medicionDispositivoNew = targetDao.load(__key);
            synchronized (this) {
                medicionDispositivo = medicionDispositivoNew;
                medicionDispositivo__resolvedKey = __key;
            }
        }
        return medicionDispositivo;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 362017176)
    public void setMedicionDispositivo(@NotNull MedicionDispositivo medicionDispositivo) {
        if (medicionDispositivo == null) {
            throw new DaoException(
                    "To-one property 'medicionDispositivoId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.medicionDispositivo = medicionDispositivo;
            medicionDispositivoId = medicionDispositivo.getId();
            medicionDispositivo__resolvedKey = medicionDispositivoId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    public String getFechaDeMedicionCalibracion() {
        return this.fechaDeMedicionCalibracion;
    }

    public void setFechaDeMedicionCalibracion(String fechaDeMedicionCalibracion) {
        this.fechaDeMedicionCalibracion = fechaDeMedicionCalibracion;
    }

    public boolean cargaFinalizada(){
        getMedicionDispositivo();
        return medicionDispositivo != null && medicionDispositivo.cargaFinalizada() && medicionInvasiva != null && medicionInvasiva != 0;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 304821998)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getItemCalibracionDao() : null;
    }
}
