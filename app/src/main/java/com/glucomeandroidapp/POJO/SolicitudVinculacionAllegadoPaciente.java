package com.glucomeandroidapp.POJO;

import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.EstadoSolicitud;

public class SolicitudVinculacionAllegadoPaciente {

    private long id;

    private EstadoSolicitud estado;

    private PacienteDataResponse paciente;

    private AllegadoDataResponse allegado;

    public SolicitudVinculacionAllegadoPaciente(long id, EstadoSolicitud estado, PacienteDataResponse paciente, AllegadoDataResponse allegado) {
        this.id = id;
        this.estado = estado;
        this.paciente = paciente;
        this.allegado = allegado;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public EstadoSolicitud getEstado() {
        return estado;
    }

    public void setEstado(EstadoSolicitud estado) {
        this.estado = estado;
    }

    public PacienteDataResponse getPaciente() {
        return paciente;
    }

    public void setPaciente(PacienteDataResponse paciente) {
        this.paciente = paciente;
    }

    public AllegadoDataResponse getAllegado() {
        return allegado;
    }

    public void setAllegado(AllegadoDataResponse allegado) {
        this.allegado = allegado;
    }
}
