package com.glucomeandroidapp.POJO.Ejercicio;

import com.glucomeandroidapp.POJO.Enumeraciones.TipoEjercicio;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;

@Entity(nameInDb = "ejercicio")
public class Ejercicio {

    private static final long serialVersionUID = 3L;

//    public Ejercicio() {
//    }
//
//    public Ejercicio(Long id, Long idBackend, String uuid, Double cantidadMinutos, TipoEjercicio tipoEjercicio, String fechaDeCreacion, String observacion) {
//        this.id = id;
//        this.idBackend = idBackend;
//        this.uuid = uuid;
//        this.cantidadMinutos = cantidadMinutos;
//        this.tipoEjercicio = tipoEjercicio;
//        this.fechaDeCreacion = fechaDeCreacion;
//        this.observacion = observacion;
//    }

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "id_backend")
    private Long idBackend;

    @Property(nameInDb = "uuid")
    private String uuid;

    @NotNull
    @Property(nameInDb = "cantidad_minutos")
    private Double cantidadMinutos;

    @NotNull
    @Property(nameInDb = "tipo_de_ejercicio")
    @Convert(converter = TipoEjercicio.TipoEjercicioConverter.class, columnType = String.class)
    private TipoEjercicio tipoEjercicio;

    @NotNull
    @Property(nameInDb = "fecha_de_creacion")
    private String fechaDeCreacion;

    @Property(nameInDb = "observacion")
    private String observacion;

    @Generated(hash = 852431704)
    public Ejercicio(Long id, Long idBackend, String uuid, @NotNull Double cantidadMinutos, @NotNull TipoEjercicio tipoEjercicio, @NotNull String fechaDeCreacion,
            String observacion) {
        this.id = id;
        this.idBackend = idBackend;
        this.uuid = uuid;
        this.cantidadMinutos = cantidadMinutos;
        this.tipoEjercicio = tipoEjercicio;
        this.fechaDeCreacion = fechaDeCreacion;
        this.observacion = observacion;
    }

    @Generated(hash = 1647249994)
    public Ejercicio() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdBackend() {
        return idBackend;
    }

    public void setIdBackend(Long idBackend) {
        this.idBackend = idBackend;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Double getCantidadMinutos() {
        return cantidadMinutos;
    }

    public void setCantidadMinutos(Double cantidadMinutos) {
        this.cantidadMinutos = cantidadMinutos;
    }

    public TipoEjercicio getTipoEjercicio() {
        return tipoEjercicio;
    }

    public void setTipoEjercicio(TipoEjercicio tipoEjercicio) {
        this.tipoEjercicio = tipoEjercicio;
    }

    public String getFechaDeCreacion() {
        return fechaDeCreacion;
    }

    public void setFechaDeCreacion(String fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

}
