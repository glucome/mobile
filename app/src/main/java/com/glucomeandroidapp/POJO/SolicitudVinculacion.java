package com.glucomeandroidapp.POJO;

public class SolicitudVinculacion {

    private long id;

    private int estado;

    public long getId() {
        return this.id;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public void setId(long id) {
        this.id = id;
    }
}
