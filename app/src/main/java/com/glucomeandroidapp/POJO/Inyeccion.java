package com.glucomeandroidapp.POJO;

import com.glucomeandroidapp.POJO.Enumeraciones.TipoInsulina;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;

import java.io.Serializable;

/**
 * Creado por ClaudioSaccella e1627 de abril del 2019
 */
@Entity(nameInDb = "inyeccion")
public class Inyeccion implements Serializable {

    private static final long serialVersionUID = 7L;

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "id_backend")
    private Long idBackend;

    @Property(nameInDb = "uuid")
    private String uuid;

    @NotNull
    @Property(nameInDb = "valor_de_inyeccion")
    private Double valorInyeccion;

    @NotNull
    @Property(nameInDb = "tipo_de_insulina")
    @Convert(converter = TipoInsulina.TipoInyeccionConverter.class, columnType = String.class)
    private TipoInsulina tipoInsulina;

    @NotNull
    @Property(nameInDb = "fecha_de_creacion")
    private String fechaDeCreacion;



    @Generated(hash = 1986681375)
    public Inyeccion() {
    }

    @Generated(hash = 1971560325)
    public Inyeccion(Long id, Long idBackend, String uuid, @NotNull Double valorInyeccion,
            @NotNull TipoInsulina tipoInsulina, @NotNull String fechaDeCreacion) {
        this.id = id;
        this.idBackend = idBackend;
        this.uuid = uuid;
        this.valorInyeccion = valorInyeccion;
        this.tipoInsulina = tipoInsulina;
        this.fechaDeCreacion = fechaDeCreacion;
    }

    @Override
    public String toString() {
        return "Medicion{" +
                "id=" + id +
                ", idBackend=" + idBackend +
                ", uuid='" + uuid + '\'' +
                ", valorInyeccion=" + valorInyeccion +
                ", tipoInsulina=" + tipoInsulina +
                ", fechaDeCreacion='" + fechaDeCreacion + '\'' +
                '}';
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Double getValorInyeccion() {
        return this.valorInyeccion;
    }

    public void setValorInyeccion(Double valorInyeccion) {
        this.valorInyeccion = valorInyeccion;
    }

    public String getFechaDeCreacion() {
        return this.fechaDeCreacion;
    }

    public void setFechaDeCreacion(String fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }

    public void setTipoInsulina(TipoInsulina tipoInsulina) {
        this.tipoInsulina = tipoInsulina;
    }

    public TipoInsulina getTipoInsulina() {
        return this.tipoInsulina;
    }

    public Long getIdBackend() {
        return this.idBackend;
    }

    public void setIdBackend(Long idBackend) {
        this.idBackend = idBackend;
    }

}
