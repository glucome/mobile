package com.glucomeandroidapp.POJO.Medicion;

import com.glucomeandroidapp.POJO.DaoSession;
import com.glucomeandroidapp.POJO.Notificacion;
import com.glucomeandroidapp.POJO.NotificacionDao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToOne;

import java.io.Serializable;

@Entity(nameInDb = "medicion_automatica")
public class MedicionAutomatica implements Serializable {

    private static final long serialVersionUID = 5L;

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "uuid")
    private String uuid;

    @NotNull
    @Property(nameInDb = "hora")
    private int hora;

    @NotNull
    @Property(nameInDb = "minuto")
    private int minuto;

    @NotNull
    @Property(nameInDb = "nombre")
    private String nombre;

    @Property(nameInDb = "notificacion_id")
    private long notificacionId;

    @ToOne(joinProperty = "notificacionId")
    private Notificacion notificacion;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 583892394)
    private transient MedicionAutomaticaDao myDao;

    @Generated(hash = 1572209264)
    private transient Long notificacion__resolvedKey;

    @Generated(hash = 1142713551)
    public MedicionAutomatica(Long id, String uuid, int hora, int minuto, @NotNull String nombre, long notificacionId) {
        this.id = id;
        this.uuid = uuid;
        this.hora = hora;
        this.minuto = minuto;
        this.nombre = nombre;
        this.notificacionId = notificacionId;
    }

    @Generated(hash = 105306721)
    public MedicionAutomatica() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) { this.id = id; }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHoraFormateada(){
        return (getHora() < 10 ? ("0" + getHora()) : getHora()) + ":" + (getMinuto() < 10 ? ("0" + getMinuto()) : getMinuto());
    }

    public long getNotificacionId() {
        return this.notificacionId;
    }

    public void setNotificacionId(long notificacionId) {
        this.notificacionId = notificacionId;
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 443736560)
    public Notificacion getNotificacion() {
        long __key = this.notificacionId;
        if (notificacion__resolvedKey == null || !notificacion__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            NotificacionDao targetDao = daoSession.getNotificacionDao();
            Notificacion notificacionNew = targetDao.load(__key);
            synchronized (this) {
                notificacion = notificacionNew;
                notificacion__resolvedKey = __key;
            }
        }
        return notificacion;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1802442856)
    public void setNotificacion(@NotNull Notificacion notificacion) {
        if (notificacion == null) {
            throw new DaoException("To-one property 'notificacionId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.notificacion = notificacion;
            notificacionId = notificacion.getId();
            notificacion__resolvedKey = notificacionId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 974267068)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getMedicionAutomaticaDao() : null;
    }

}
