package com.glucomeandroidapp.POJO.Medicion;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;

import java.util.Random;

@Entity(nameInDb = "medicion_dispositivo")
public class MedicionDispositivo {

    private static final long serialVersionUID = 9L;

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    @Property(nameInDb = "medicion635")
    private Double medicion635;

    @NotNull
    @Property(nameInDb = "medicion740")
    private Double medicion740;

    @NotNull
    @Property(nameInDb = "medicion850")
    private Double medicion850;

    @NotNull
    @Property(nameInDb = "medicion940")
    private Double medicion940;

    @Generated(hash = 1914417151)
    public MedicionDispositivo(Long id, @NotNull Double medicion635,
            @NotNull Double medicion740, @NotNull Double medicion850,
            @NotNull Double medicion940) {
        this.id = id;
        this.medicion635 = medicion635;
        this.medicion740 = medicion740;
        this.medicion850 = medicion850;
        this.medicion940 = medicion940;
    }

    @Generated(hash = 452469595)
    public MedicionDispositivo() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getMedicion635() {
        return this.medicion635;
    }

    public void setMedicion635(Double medicion635) {
        this.medicion635 = medicion635;
    }

    public Double getMedicion740() {
        return this.medicion740;
    }

    public void setMedicion740(Double medicion740) {
        this.medicion740 = medicion740;
    }

    public Double getMedicion850() {
        return this.medicion850;
    }

    public void setMedicion850(Double medicion850) {
        this.medicion850 = medicion850;
    }

    public Double getMedicion940() {
        return this.medicion940;
    }

    public void setMedicion940(Double medicion940) {
        this.medicion940 = medicion940;
    }

    public void obtenerMediciones(){
        this.medicion635 = random();
        this.medicion740 = random();
        this.medicion850 = random();
        this.medicion940 = random();
    }

    public Double random(){
        Random random = new Random();
        Double min = Double.valueOf(50);
        Double max = Double.valueOf(200);
        return min + (max - min) * random.nextDouble();
    }

    public boolean cargaFinalizada(){
        return medicion635 != null && medicion740 != null && medicion850 != null && medicion940 != null
                && medicion635 > 0 && medicion740 > 0 && medicion850 > 0 && medicion940 > 0;
    }
}
