package com.glucomeandroidapp.POJO.Medicion;

import com.glucomeandroidapp.POJO.Enumeraciones.MomentoMedicion;
import com.glucomeandroidapp.POJO.Enumeraciones.TipoMedicion;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;

import java.io.Serializable;

/**
 * Creado por MartinArtime el 27 de abril del 2019
 */
@Entity(nameInDb = "medicion")
public class Medicion implements Serializable {

    private static final long serialVersionUID = 3L;

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "id_backend")
    private Long idBackend;

    @Property(nameInDb = "uuid")
    private String uuid;

    @NotNull
    @Property(nameInDb = "valor_de_medicion")
    private Double valorMedicion;

    @NotNull
    @Property(nameInDb = "momento_de_medicion")
    @Convert(converter = MomentoMedicion.MomentoMedicionConverter.class, columnType = String.class)
    private MomentoMedicion momento;

    @NotNull
    @Property(nameInDb = "tipo_de_medicion")
    @Convert(converter = TipoMedicion.TipoMedicionConverter.class, columnType = String.class)
    private TipoMedicion tipoMedicion;

    @NotNull
    @Property(nameInDb = "fecha_de_creacion")
    private String fechaDeCreacion;



    @Generated(hash = 2117065148)
    public Medicion() {
    }

    @Generated(hash = 1563878020)
    public Medicion(Long id, Long idBackend, String uuid, @NotNull Double valorMedicion,
            @NotNull MomentoMedicion momento, @NotNull TipoMedicion tipoMedicion,
            @NotNull String fechaDeCreacion) {
        this.id = id;
        this.idBackend = idBackend;
        this.uuid = uuid;
        this.valorMedicion = valorMedicion;
        this.momento = momento;
        this.tipoMedicion = tipoMedicion;
        this.fechaDeCreacion = fechaDeCreacion;
    }

    @Override
    public String toString() {
        return "Medicion{" +
                "id=" + id +
                ", idBackend=" + idBackend +
                ", uuid='" + uuid + '\'' +
                ", tituloPerfil=" + valorMedicion +
                ", momento=" + momento +
                ", tipoMedicion=" + tipoMedicion +
                ", fechaDeCreacion='" + fechaDeCreacion + '\'' +
                '}';
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Double getValorMedicion() {
        return this.valorMedicion;
    }

    public void setValorMedicion(Double valorDeMedicion) {
        this.valorMedicion = valorDeMedicion;
    }

    public String getFechaDeCreacion() {
        return this.fechaDeCreacion;
    }

    public void setFechaDeCreacion(String fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }

    public void setTipoMedicion(TipoMedicion tipoMedicion) {
        this.tipoMedicion = tipoMedicion;
    }

    public TipoMedicion getTipoMedicion() {
        return this.tipoMedicion;
    }

    public Long getIdBackend() {
        return this.idBackend;
    }

    public void setIdBackend(Long idBackend) {
        this.idBackend = idBackend;
    }

    public MomentoMedicion getMomento() {
        return this.momento;
    }

    public void setMomento(MomentoMedicion momento) {
        this.momento = momento;
    }
}
