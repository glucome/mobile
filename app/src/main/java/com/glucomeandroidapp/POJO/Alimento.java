package com.glucomeandroidapp.POJO;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.io.Serializable;

/**
 * Creado por MartinArtime el 07 de mayo del 2019
 */
@Entity(nameInDb = "alimento")
public class Alimento implements Serializable {

    private static final long serialVersionUID = 4L;

    @Id(autoincrement = true)
    private long id;

    @Property(nameInDb = "nombre")
    private String nombre;

    @Property(nameInDb = "indice_glucemico")
    private String indiceGlucemico;

    @Property(nameInDb = "descripcion")
    private String descripcion;

    @Property(nameInDb = "unidad_referencia")
    private String unidadReferencia;

    @Property(nameInDb = "carbohidratos_por_unidad")
    private double carbohidratosPorUnidad;



    @Generated(hash = 448947220)
    public Alimento() {
    }

    @Generated(hash = 362202890)
    public Alimento(long id, String nombre, String indiceGlucemico,
            String descripcion, String unidadReferencia,
            double carbohidratosPorUnidad) {
        this.id = id;
        this.nombre = nombre;
        this.indiceGlucemico = indiceGlucemico;
        this.descripcion = descripcion;
        this.unidadReferencia = unidadReferencia;
        this.carbohidratosPorUnidad = carbohidratosPorUnidad;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIndiceGlucemico() {
        return indiceGlucemico;
    }

    public void setIndiceGlucemico(String indiceGlucemico) {
        this.indiceGlucemico = indiceGlucemico;
    }


    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUnidadReferencia() {
        return unidadReferencia;
    }

    public void setUnidadReferencia(String unidadReferencia) {
        this.unidadReferencia = unidadReferencia;
    }

    public double getCarbohidratosPorUnidad() {
        return carbohidratosPorUnidad;
    }

    public void setCarbohidratosPorUnidad(double carbohidratosPorUnidad) {
        this.carbohidratosPorUnidad = carbohidratosPorUnidad;
    }
}

