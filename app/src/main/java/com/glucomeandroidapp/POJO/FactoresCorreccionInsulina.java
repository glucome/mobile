package com.glucomeandroidapp.POJO;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;

@Entity(nameInDb = "factores_correccion_insulina")
public class FactoresCorreccionInsulina {

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "id_backend")
    private Long idBackend;

    @NotNull
    @Property(nameInDb = "factor_correccion_azucar")
    private Double factorCorreccionAzucar;

    @NotNull
    @Property(nameInDb = "factor_correccion_cho")
    private Double factorCorreccionCHO;

    @Generated(hash = 314801448)
    public FactoresCorreccionInsulina(Long id, Long idBackend,
            @NotNull Double factorCorreccionAzucar,
            @NotNull Double factorCorreccionCHO) {
        this.id = id;
        this.idBackend = idBackend;
        this.factorCorreccionAzucar = factorCorreccionAzucar;
        this.factorCorreccionCHO = factorCorreccionCHO;
    }

    @Generated(hash = 2012407551)
    public FactoresCorreccionInsulina() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdBackend() {
        return this.idBackend;
    }

    public void setIdBackend(Long idBackend) {
        this.idBackend = idBackend;
    }

    public Double getFactorCorreccionAzucar() {
        return this.factorCorreccionAzucar;
    }

    public void setFactorCorreccionAzucar(Double factorCorreccionAzucar) {
        this.factorCorreccionAzucar = factorCorreccionAzucar;
    }

    public Double getFactorCorreccionCHO() {
        return this.factorCorreccionCHO;
    }

    public void setFactorCorreccionCHO(Double factorCorreccionCHO) {
        this.factorCorreccionCHO = factorCorreccionCHO;
    }



}
