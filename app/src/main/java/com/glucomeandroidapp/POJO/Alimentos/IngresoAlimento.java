package com.glucomeandroidapp.POJO.Alimentos;


import com.glucomeandroidapp.POJO.Enumeraciones.MomentoAlimento;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;

@Entity(nameInDb = "ingreso_alimento")
public class IngresoAlimento {

    private static final long serialVersionUID = 3L;



    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "id_backend")
    private Long idBackend;

    @Property(nameInDb = "id_alimento_backend")
    private Long idAlimentoBackend;

    @NotNull
    @Property(nameInDb = "cantidad_unidades")
    private Double cantidad;

    @NotNull
    @Property(nameInDb = "momento_de_alimento")
    @Convert(converter = MomentoAlimento.MomentoAlimentoConverter.class, columnType = String.class)
    private MomentoAlimento momentoAlimento;

    @NotNull
    @Property(nameInDb = "fecha_de_creacion")
    private String fechaDeCreacion;

    @Property(nameInDb = "observacion")
    private String observacion;

    @Property(nameInDb = "nombre_alimento")
    private String nombreAlimento;









    @Generated(hash = 1803887418)
    public IngresoAlimento(Long id, Long idBackend, Long idAlimentoBackend, @NotNull Double cantidad,
            @NotNull MomentoAlimento momentoAlimento, @NotNull String fechaDeCreacion,
            String observacion, String nombreAlimento) {
        this.id = id;
        this.idBackend = idBackend;
        this.idAlimentoBackend = idAlimentoBackend;
        this.cantidad = cantidad;
        this.momentoAlimento = momentoAlimento;
        this.fechaDeCreacion = fechaDeCreacion;
        this.observacion = observacion;
        this.nombreAlimento = nombreAlimento;
    }

    @Generated(hash = 1648216411)
    public IngresoAlimento() {
    }









    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdBackend() {
        return idBackend;
    }

    public void setIdBackend(Long idBackend) {
        this.idBackend = idBackend;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public MomentoAlimento getMomentoAlimento() {
        return momentoAlimento;
    }

    public void setMomentoAlimento(MomentoAlimento momentoAlimento) {
        this.momentoAlimento = momentoAlimento;
    }

    public String getFechaDeCreacion() {
        return fechaDeCreacion;
    }

    public void setFechaDeCreacion(String fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Long getIdAlimentoBackend() {
        return idAlimentoBackend;
    }

    public void setIdAlimentoBackend(Long idAlimentoBackend) {
        this.idAlimentoBackend = idAlimentoBackend;
    }

    public String getNombreAlimento() {
        return this.nombreAlimento;
    }

    public void setNombreAlimento(String nombreAlimento) {
        this.nombreAlimento = nombreAlimento;
    }


}
