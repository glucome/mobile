package com.glucomeandroidapp.POJO.Enumeraciones;

import org.greenrobot.greendao.converter.PropertyConverter;

/**
 * Creado por MartinArtime el 07 de mayo del 2019
 */
public enum TipoDiabetes {

    TIPO_1("Tipo 1"),
    TIPO_2("Tipo 2"),
    GESTACIONAL("Gestacional"),
    NA("N/A");

    private final String nombre;

    TipoDiabetes(String nombre) {
        this.nombre = nombre;
    }

    public static String[] getTiposDeDiabetes(){
        return new String[]{TIPO_1.nombre, TIPO_2.nombre, GESTACIONAL.nombre, NA.nombre};
    }

    public static TipoDiabetes getById(int id) {
        for(TipoDiabetes e : values()) {
            if(e.ordinal() == id){
                return e;
            }
        }
        return TipoDiabetes.NA;
    }

    public String getNombre() {
        return nombre;
    }

    public static class TipoDiabetesConverter implements PropertyConverter<TipoDiabetes, String> {
        @Override
        public TipoDiabetes convertToEntityProperty(String databaseValue) {
            return TipoDiabetes.valueOf(databaseValue);
        }

        @Override
        public String convertToDatabaseValue(TipoDiabetes entityProperty) {
            return entityProperty.name();
        }
    }
}
