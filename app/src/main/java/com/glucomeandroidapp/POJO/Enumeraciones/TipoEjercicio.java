package com.glucomeandroidapp.POJO.Enumeraciones;

import org.greenrobot.greendao.converter.PropertyConverter;

public enum TipoEjercicio {
    CORRER("Correr"), CAMINAR("Caminar"), DEPORTE("Deporte"), OTRO("Otro");

    private final String nombre;

    TipoEjercicio(String nombre) {
        this.nombre = nombre;
    }

    public static TipoEjercicio getById(int id) {
        for(TipoEjercicio e : values()) {
            if(e.ordinal() == id){
                return e;
            }
        }
        return TipoEjercicio.OTRO;
    }

    public String getNombre() {
        return nombre;
    }

    public static class TipoEjercicioConverter implements PropertyConverter<TipoEjercicio, String> {
        @Override
        public TipoEjercicio convertToEntityProperty(String databaseValue) {
            return TipoEjercicio.valueOf(databaseValue);
        }

        @Override
        public String convertToDatabaseValue(TipoEjercicio entityProperty) {
            return entityProperty.name();
        }
    }
}
