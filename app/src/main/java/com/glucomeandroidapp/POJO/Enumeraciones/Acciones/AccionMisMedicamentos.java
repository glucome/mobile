package com.glucomeandroidapp.POJO.Enumeraciones.Acciones;

public enum AccionMisMedicamentos {

    TOMAR_MEDICAMENTO("Tomar Medicamento"),
    CONFIGURAR_HORARIO("Configurar Horario"),
    NA("N/A");

    private final String nombre;

    AccionMisMedicamentos(String nombre) {
        this.nombre = nombre;
    }

    public static String[] getAccionMisMedicamentos(){
        return new String[]{TOMAR_MEDICAMENTO.nombre, CONFIGURAR_HORARIO.nombre, NA.nombre};
    }

    public static AccionMisMedicamentos getById(int id) {
        for(AccionMisMedicamentos e : values()) {
            if(e.ordinal() == id){
                return e;
            }
        }
        return AccionMisMedicamentos.NA;
    }

    public String getNombre() {
        return nombre;
    }
}
