package com.glucomeandroidapp.POJO.Enumeraciones;

import org.greenrobot.greendao.converter.PropertyConverter;

/**
 * Creado por MartinArtime el 07 de mayo del 2019
 */
public enum Sexo {

    MASCULINO("Masculino"),
    FEMENINO("Femenino"),
    OTRO("Otro");

    private final String nombre;

    Sexo(String nombre) {
        this.nombre = nombre;
    }

    public static String[] getSexos(){
        return new String[]{MASCULINO.nombre, FEMENINO.nombre, OTRO.nombre};
    }

    public static Sexo getById(int id) {
        for(Sexo e : values()) {
            if(e.ordinal() == id){
                return e;
            }
        }
        return Sexo.OTRO;
    }


    public String getNombre() {
        return nombre;
    }


    public static class SexoConverter implements PropertyConverter<Sexo, String> {
        @Override
        public Sexo convertToEntityProperty(String databaseValue) {
            return Sexo.valueOf(databaseValue);
        }

        @Override
        public String convertToDatabaseValue(Sexo entityProperty) {
            return entityProperty.name();
        }
    }
}
