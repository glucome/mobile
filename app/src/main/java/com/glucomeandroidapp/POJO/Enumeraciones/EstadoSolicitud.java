package com.glucomeandroidapp.POJO.Enumeraciones;

public enum EstadoSolicitud {

    PENDIENTE("Pendiente"), RECHAZADA("Rechazada"), ACEPTADA("Aceptada");

    private String descripcion;

    public static EstadoSolicitud get(int id) {

            for(EstadoSolicitud e : values()) {
                if(e.ordinal() == id){
                    return e;
                }
            }
            return null;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    private EstadoSolicitud(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId() {
        return ordinal();
    }



}
