package com.glucomeandroidapp.POJO.Enumeraciones;

public enum TipoSolicitud {

	MEDICO_PACIENTE, ALLEGADO_PACIENTE;


	public static TipoSolicitud get(int id) {
		for (TipoSolicitud tipo : values()) {
			if (tipo.ordinal() == id) {
				return tipo;
			}
		}

		return MEDICO_PACIENTE;
	}
}
