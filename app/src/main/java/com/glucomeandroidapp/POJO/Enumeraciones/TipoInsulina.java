package com.glucomeandroidapp.POJO.Enumeraciones;

import org.greenrobot.greendao.converter.PropertyConverter;

/**
 * Creado por MartinArtime el 07 de mayo del 2019
 */
public enum TipoInsulina {

    REGULAR_NORMAL("Regular / normal"),
    LYSPRO_ASPART_GLULISINA("Lyspro / Aspart / Glulisina"),
    NPH("NPH"),
    DETEMIR("Detemir"),
    GLARGINA("Glargina"),
    OTRO("Otro");

    private final String nombre;

    TipoInsulina(String nombre) {
        this.nombre = nombre;
    }

    public static TipoInsulina getById(int id) {
        for(TipoInsulina e : values()) {
            if(e.ordinal() == id){
                return e;
            }
        }
        return TipoInsulina.OTRO;
    }

    public String getNombre() {
        return nombre;
    }

    public static class TipoInyeccionConverter implements PropertyConverter<TipoInsulina, String> {
        @Override
        public TipoInsulina convertToEntityProperty(String databaseValue) {
            return TipoInsulina.valueOf(databaseValue);
        }

        @Override
        public String convertToDatabaseValue(TipoInsulina entityProperty) {
            return entityProperty.name();
        }
    }
}