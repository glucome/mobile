package com.glucomeandroidapp.POJO.Enumeraciones;

public enum TipoCalculoCalibracion {

    PROMEDIO("Promedio"),
    PROMEDIO_PONDERADO("Ponderado");

    private String descripcion;

    private TipoCalculoCalibracion(String descripcion){
        this.descripcion = descripcion;
    }
}
