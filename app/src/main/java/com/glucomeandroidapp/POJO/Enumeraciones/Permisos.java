package com.glucomeandroidapp.POJO.Enumeraciones;

import java.util.Arrays;
import java.util.List;

public enum Permisos {

    ALIMENTOS(Rol.PACIENTE_PREMIUM),
    ALLEGADOS(Rol.PACIENTE, Rol.PACIENTE_PREMIUM),
    CALIBRAR_DISPOSITIVO,
    CONTACTOS(Rol.PACIENTE, Rol.PACIENTE_PREMIUM),
    EJERCICIOS(Rol.PACIENTE_PREMIUM),
    INFORMACION(Rol.PACIENTE, Rol.PACIENTE_PREMIUM),
    INSULINA(Rol.PACIENTE_PREMIUM),
    MEDICAMENTOS(Rol.PACIENTE_PREMIUM),
    MEDICIONES(Rol.PACIENTE, Rol.PACIENTE_PREMIUM),
    MEDICOS(Rol.PACIENTE_PREMIUM),
    NOTIFICACIONES(Rol.PACIENTE_PREMIUM),
    SINCRONIZAR(Rol.PACIENTE, Rol.PACIENTE_PREMIUM),
    TARJETA_AUMENTADA(Rol.PACIENTE_PREMIUM),
    VER_SUSCRIPCION(Rol.PACIENTE, Rol.PACIENTE_PREMIUM),
    VER_PERFIL(Rol.PACIENTE, Rol.PACIENTE_PREMIUM);

    private List<Rol> rolesAdmitidos;

    Permisos(Rol ... roles){
        rolesAdmitidos =  Arrays.asList(roles);
    }

    public boolean aceptaRol(Rol rol){
        return rolesAdmitidos.contains(rol);
    }

}
