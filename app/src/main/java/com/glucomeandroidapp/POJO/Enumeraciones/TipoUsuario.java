package com.glucomeandroidapp.POJO.Enumeraciones;

import java.util.ArrayList;
import java.util.List;

public enum TipoUsuario {

    PACIENTE("Paciente", true), MEDICO("Medico", false), ALLEGADO("Allegado", true);

    private String descripcion;
    private boolean esRegistroEnApp;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isEsRegistroEnApp() {
        return esRegistroEnApp;
    }

    public void setEsRegistroEnApp(boolean esRegistroEnApp) {
        this.esRegistroEnApp = esRegistroEnApp;
    }

    private TipoUsuario(String descripcion, boolean esRegistroEnApp) {
        this.descripcion = descripcion;
        this.esRegistroEnApp = esRegistroEnApp;
    }

    public static String[] getTiposUsuarioRegistro(){
        List<String> tiposUsuariosRegistro = new ArrayList<String>();
        for(TipoUsuario tipoUsuario : values()){
            if(tipoUsuario.isEsRegistroEnApp())
                tiposUsuariosRegistro.add(tipoUsuario.getDescripcion());
        }
        return tiposUsuariosRegistro.toArray(new String[tiposUsuariosRegistro.size()]);
    }

    public static TipoUsuario getByDescripcion(String descripcion){
        for(TipoUsuario tipoUsuario : values()){
            if(tipoUsuario.getDescripcion().equals(descripcion))
                return  tipoUsuario;
        }
        return null;
    }

    public static TipoUsuario getByOrdenRegistro(int orden){
        int contador = 0;
        for(TipoUsuario tipoUsuario : values()){
            if(tipoUsuario.isEsRegistroEnApp()){
                if(contador == orden){
                    return tipoUsuario;
                } else {
                    contador++;
                }
            }
        }
        return null;
    }
}

