package com.glucomeandroidapp.POJO.Enumeraciones;

import org.greenrobot.greendao.converter.PropertyConverter;

public enum MomentoMedicion {

	ANTES_DESAYUNO("Antes del desayuno"),
	DESPUES_DESAYUNO("Después del desayuno"),
	ANTES_ALMUERZO("Antes del almuerzo"),
	DESPUES_ALMUERZO("Después del almuerzo"),
	ANTES_MERIENDA("Antes de la merienda"),
	DESPUES_MERIENDA("Después de la merienda"),
	ANTES_CENA("Antes de la cena"),
	DESPUES_CENA("Después de la cena"),
	NA("No aplica");

	private final String nombre;

	MomentoMedicion(String nombre) {
		this.nombre = nombre;
	}

	public static MomentoMedicion getById(int id) {
		for(MomentoMedicion e : values()) {
			if(e.ordinal() == id){
				return e;
			}
		}
		return MomentoMedicion.NA;
	}
	    
	public String getNombre() {
			return nombre;
		}

	public static class MomentoMedicionConverter implements PropertyConverter<MomentoMedicion, String> {
		@Override
		public MomentoMedicion convertToEntityProperty(String databaseValue) {
			return MomentoMedicion.valueOf(databaseValue);
		}

		@Override
		public String convertToDatabaseValue(MomentoMedicion entityProperty) {
			return entityProperty.name();
		}
	}

}
