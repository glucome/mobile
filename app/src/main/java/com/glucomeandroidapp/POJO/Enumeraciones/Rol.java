package com.glucomeandroidapp.POJO.Enumeraciones;

import org.greenrobot.greendao.converter.PropertyConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Creado por MartinArtime el 07 de mayo del 2019
 */
public enum Rol {

    PACIENTE("ROLE_PACIENTE_BASICO", TipoUsuario.PACIENTE, "PACIENTE_BASICO"),
    MEDICO("ROLE_MEDICO_BASICO", TipoUsuario.MEDICO, "MEDICO_BASICO"),
    ALLEGADO("ROLE_ALLEGADO", TipoUsuario.ALLEGADO, "ALLEGADO"),
    PACIENTE_PREMIUM("ROLE_PACIENTE_PREMIUM", TipoUsuario.PACIENTE, "PACIENTE_PREMIUM"),
    MEDICO_PREMIUM("ROLE_MEDICO_PREMIUM", TipoUsuario.MEDICO, "MEDICO_PREMIUM");

    private final String nombre;
    private TipoUsuario tipoUsuario;
    private String nombreParaSecurityConfig;

    Rol(String nombre, TipoUsuario tipoUsuario, String nombreParaSecurityConfig) {
        this.nombre = nombre;
        this.tipoUsuario = tipoUsuario;
        this.nombreParaSecurityConfig = nombreParaSecurityConfig;
    }

    public String getNombre() {
        return nombre;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public String getNombreParaSecurityConfig() {
        return nombreParaSecurityConfig;
    }

    public boolean esPaciente(){
        return  this.equals(Rol.PACIENTE) || this.equals(Rol.PACIENTE_PREMIUM);
    }

    public boolean esAllegado(){
        return  this.equals(Rol.ALLEGADO);
    }

    public boolean esMedico(){
        return  this.equals(Rol.MEDICO) || this.equals(Rol.MEDICO_PREMIUM);
    }

    public static class RolConverter implements PropertyConverter<Rol, String> {
        @Override
        public Rol convertToEntityProperty(String databaseValue) {
            return Rol.valueOf(databaseValue);
        }

        @Override
        public String convertToDatabaseValue(Rol entityProperty) {
            return entityProperty.name();
        }
    }

    public static Rol getBasicoPorTipoUsuario(TipoUsuario tipoUsuario){
        Rol rol;
        switch(tipoUsuario){
            case PACIENTE:
                rol = PACIENTE;
                break;
            case ALLEGADO:
                rol = ALLEGADO;
                break;
            default:
                rol = null;
                break;
        }
        return rol;
    }

    public static List<Rol> get(TipoUsuario tipo) {
        List<Rol> roles = new ArrayList<>();
        for (Rol rol : values()) {
            if (rol.getTipoUsuario().ordinal() == tipo.ordinal()) {
                roles.add(rol);
            }
        }
        return roles;
    }

    public static Rol getPorNombre(String nombre) {
        for (Rol rol : values()) {
            if (rol.getNombre().equals(nombre)) {
                return rol;
            }
        }
        return null;
    }


}
