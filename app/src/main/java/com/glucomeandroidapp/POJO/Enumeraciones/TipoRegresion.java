package com.glucomeandroidapp.POJO.Enumeraciones;

import org.greenrobot.greendao.converter.PropertyConverter;

public enum TipoRegresion {

    REGRESION_635(1, "635"),
    REGRESION_740(2, "740"),
    REGRESION_850(3, "850"),
    REGRESION_940(4, "940");

    private Integer id;
    private String descripcion;

    TipoRegresion(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public static TipoRegresion getById(int id) {
        for(TipoRegresion e : values()) {
            if(e.getId() == id){
                return e;
            }
        }
        return null;
    }

    public Integer getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public static class TipoRegresionConverter implements PropertyConverter<TipoRegresion, String> {
        @Override
        public TipoRegresion convertToEntityProperty(String databaseValue) {
            return TipoRegresion.valueOf(databaseValue);
        }

        @Override
        public String convertToDatabaseValue(TipoRegresion entityProperty) {
            return entityProperty.name();
        }
    }
}
