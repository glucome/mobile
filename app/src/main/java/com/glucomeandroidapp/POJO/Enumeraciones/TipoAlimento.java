package com.glucomeandroidapp.POJO.Enumeraciones;

public enum TipoAlimento {
	
	ALTA("Indice glucemico alto",51,200),
	MEDIA("Indice glucemico medio",41,50),
	BAJA("Indice glucemico bajo",0,40);
	
	private String nombre;
	private int valorMinimo;
	private int valorMaximo;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	TipoAlimento(String nombre, int valorMinimo, int valorMaximo) {
		this.nombre = nombre;
		this.valorMaximo =  valorMaximo;
		this.valorMinimo = valorMinimo;
	}

	public static TipoAlimento getByName(String nombre) {
		for(TipoAlimento tipoAlimento : values()) {
			if(tipoAlimento.nombre.equals(nombre)) {
				return tipoAlimento;
			}
		}
		return null;
	}

	public int getValorMinimo() {
		return valorMinimo;
	}

	public void setValorMinimo(int valorMinimo) {
		this.valorMinimo = valorMinimo;
	}

	public int getValorMaximo() {
		return valorMaximo;
	}

	public void setValorMaximo(int valorMaximo) {
		this.valorMaximo = valorMaximo;
	}
	
}
