package com.glucomeandroidapp.POJO.Enumeraciones;

import org.greenrobot.greendao.converter.PropertyConverter;

/**
 * Creado por MartinArtime el 07 de mayo del 2019
 */
public enum TipoMedicion {

    INVASIVA("Invasiva"),
    NO_INVASIVA("No Invasiva"),
    NA("No aplica");

    private final String nombre;

    TipoMedicion(String nombre) {
        this.nombre = nombre;
    }

    public static TipoMedicion getById(int id) {
        for(TipoMedicion e : values()) {
            if(e.ordinal() == id){
                return e;
            }
        }
        return TipoMedicion.NA;
    }

    public String getNombre() {
        return nombre;
    }

    public static class TipoMedicionConverter implements PropertyConverter<TipoMedicion, String> {
        @Override
        public TipoMedicion convertToEntityProperty(String databaseValue) {
            return TipoMedicion.valueOf(databaseValue);
        }

        @Override
        public String convertToDatabaseValue(TipoMedicion entityProperty) {
            return entityProperty.name();
        }
    }
}
