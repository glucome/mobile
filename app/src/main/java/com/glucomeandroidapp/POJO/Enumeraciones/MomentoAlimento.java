package com.glucomeandroidapp.POJO.Enumeraciones;

import org.greenrobot.greendao.converter.PropertyConverter;

public enum  MomentoAlimento {

    DESAYUNO("Desayuno"),
    ALMUERZO("Almuerzo"),
    MERIENDA("Merienda"),
    CENA("Cena"),
    OTRO("Otro");

    private final String nombre;

    MomentoAlimento(String nombre) {
        this.nombre = nombre;
    }

    public static MomentoAlimento getById(int id) {
        for(MomentoAlimento e : values()) {
            if(e.ordinal() == id){
                return e;
            }
        }
        return MomentoAlimento.OTRO;
    }

    public String getNombre() {
        return nombre;
    }

    public static class MomentoAlimentoConverter implements PropertyConverter<MomentoAlimento, String> {
        @Override
        public MomentoAlimento convertToEntityProperty(String databaseValue) {
            return MomentoAlimento.valueOf(databaseValue);
        }

        @Override
        public String convertToDatabaseValue(MomentoAlimento entityProperty) {
            return entityProperty.name();
        }
    }
}
