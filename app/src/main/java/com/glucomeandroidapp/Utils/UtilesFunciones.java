package com.glucomeandroidapp.Utils;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.glucomeandroidapp.Activities.MenuPrincipalActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.APIService;
import com.glucomeandroidapp.Conexiones.RetrofitClient;
import com.glucomeandroidapp.DAO.HorarioMedicacionDAO;
import com.glucomeandroidapp.DAO.MedicionesAutomaticasDAO;
import com.glucomeandroidapp.POJO.Medicacion.HorarioMedicacion;
import com.glucomeandroidapp.POJO.Medicion.MedicionAutomatica;
import com.glucomeandroidapp.POJO.Notificacion;
import com.glucomeandroidapp.R;
import com.glucomeandroidapp.Receivers.NotificacionesReceiver;

import java.util.Calendar;
import java.util.List;

import static android.content.Context.ALARM_SERVICE;
import static com.glucomeandroidapp.Utils.Util.CANAL_ALERTAS;
import static com.glucomeandroidapp.Utils.Util.CANAL_ALERTAS_DESCRIPCION;
import static com.glucomeandroidapp.Utils.Util.CANAL_ALERTAS_ID;
import static com.glucomeandroidapp.Utils.Util.HORA;
import static com.glucomeandroidapp.Utils.Util.ID_MEDICION;
import static com.glucomeandroidapp.Utils.Util.ID_NOTIFICACION;
import static com.glucomeandroidapp.Utils.Util.MEDICIONES_AUTOMATICAS;
import static com.glucomeandroidapp.Utils.Util.MINUTO;
import static com.glucomeandroidapp.Utils.Util.NUMERO_NOTIFICACION;
import static com.glucomeandroidapp.Utils.Util.TEXTO_NOTIFICACION;
import static com.glucomeandroidapp.Utils.Util.TIPO_NOTIFICACION;
import static com.glucomeandroidapp.Utils.Util.TITULO_NOTIFICACION;
import static com.glucomeandroidapp.Utils.Util.TOMAR_MEDICAMENTOS;
import static com.glucomeandroidapp.Utils.Util.URL_BASE;

/**
 * Creado por MartinArtime el 09 de mayo del 2019
 */
public class UtilesFunciones {

    /**
     * Crea el servicio de Retrofit encargado de hacer la conexion con el servidor web
     * @return APIService para la URL base seteada
     */
    public static APIService getAPIService() {
        return RetrofitClient.getClient(URL_BASE).create(APIService.class);
    }


    /**
     * Método que permite saber si hay conexión a internet
     * @return true si hay conexión, sino false.
     */
    public static boolean hayConexionAInternet() {
        ConnectivityManager connectivityManager = ((ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    /**
     *
     */
    public static void programarNotificacion(Context context, int numeroNotificacion, String titulo,
                                             String texto, int hora, int minuto, int idMedicion, boolean isReprogramar, String tipo){

        // Seteo un calendar con la hora y minutos de la alarma y si es para hoy o el dia siguiente si son anteriores a este momento
        Calendar antes = Calendar.getInstance();
        Calendar calendarAlarma = Calendar.getInstance();

        if(isReprogramar){
            calendarAlarma.set(calendarAlarma.get(Calendar.YEAR),
                    calendarAlarma.get(Calendar.MONTH),
                    calendarAlarma.get(Calendar.DAY_OF_MONTH),
                    hora, minuto, calendarAlarma.get(Calendar.SECOND));

            calendarAlarma.add(Calendar.DATE, 1);

        } else {
            calendarAlarma.set(calendarAlarma.get(Calendar.YEAR),
                    calendarAlarma.get(Calendar.MONTH),
                    calendarAlarma.get(Calendar.DAY_OF_MONTH),
                    hora, minuto, calendarAlarma.get(Calendar.SECOND));

            if(calendarAlarma.before(antes)) {
                calendarAlarma.add(Calendar.DATE, 1);
            }
        }

        Log.e(Util.TAG, "Hora de alarma: "+calendarAlarma.getTime().toString());

        // Armo el intent con todos los datos para poder reprogramarla cuando caiga
        Intent alarmIntent = new Intent(context, NotificacionesReceiver.class);
        alarmIntent.putExtra(ID_NOTIFICACION, numeroNotificacion);
        alarmIntent.putExtra(ID_MEDICION, idMedicion);
        alarmIntent.putExtra(TITULO_NOTIFICACION, titulo);
        alarmIntent.putExtra(TEXTO_NOTIFICACION, texto);
        alarmIntent.putExtra(HORA, hora);
        alarmIntent.putExtra(MINUTO, minuto);
        alarmIntent.putExtra(TIPO_NOTIFICACION, tipo);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, numeroNotificacion,
                alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendarAlarma.getTimeInMillis(), pendingIntent);

        numeroNotificacion++;

        LocalStorageManager.guardar(NUMERO_NOTIFICACION, numeroNotificacion);

    }


    /**
     * Crea una notificación.
     * @param context, desde donde se llama a crear la notificación
     * @param titulo, que tendrá la notificación
     * @param texto, descripción que tendrá la notificación
     */
    public static void agregarNotificacion(Context context, String titulo, String texto,
                                           int idNotificacion, int idHorario, int hora, int minuto, String tipo) {


        // Declaro la acción a hacer al presionar en la notificación
        Intent notificationIntent = new Intent(context, MenuPrincipalActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Creo y genero el canal de notificaciones
        NotificationChannel channel = new NotificationChannel(CANAL_ALERTAS_ID, CANAL_ALERTAS,
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription(CANAL_ALERTAS_DESCRIPCION);
        channel.enableLights(true);
        channel.setLightColor(Color.RED);
        channel.enableVibration(true);
        channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});


        // Creo la notificación
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, MEDICIONES_AUTOMATICAS)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle(titulo)
                .setContentText(texto)
                .setContentIntent(contentIntent)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true)
                .setChannelId(CANAL_ALERTAS_ID)
  //              .addAction(R.drawable.atencion48, PROLONGAR_TEXTO, prolongarPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // Lanzo la notificación
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);

        notificationManager.createNotificationChannel(channel);
        notificationManager.notify(idNotificacion, notification);


        // Renuevo la notificación

        Integer numero = LocalStorageManager.obtener(NUMERO_NOTIFICACION, Integer.class);

        Log.e(Util.TAG, "FUNCIONA!, Numero de notificación: "+idNotificacion +
                " y numero: "+ numero + " y numero de medicion automatica "+idHorario);

        MedicionAutomatica medicion = null;
        HorarioMedicacion horario = null;

        if(tipo.equals(MEDICIONES_AUTOMATICAS)){
            List<MedicionAutomatica> mediciones = MedicionesAutomaticasDAO.getMedicionAutomaticaById(idHorario);
            if(!mediciones.isEmpty() && numero!=null){
                medicion = mediciones.get(0);

                // Creo la notificación y la guardo
                Notificacion notificacion = new Notificacion();
                notificacion.setTipoNotificacion(MEDICIONES_AUTOMATICAS);
                notificacion.setId(Long.valueOf(numero));
                DataBaseManager.getDaoSession().getNotificacionDao().insertOrReplace(notificacion);

                // Actualizo la medicion con los nuevos valores
                medicion.setNotificacionId(numero);
                medicion.setNotificacion(notificacion);
                DataBaseManager.getDaoSession().getMedicionAutomaticaDao().insertOrReplace(medicion);

                // Programo la nueva notificación
                programarNotificacion(context, numero, titulo, texto, hora, minuto,
                        medicion.getId().intValue(), true, MEDICIONES_AUTOMATICAS);
            }
        } else { // Sino son las medicaciones

            List<HorarioMedicacion> horarios = HorarioMedicacionDAO.getHorarioMedicacionById(idHorario);
            if(!horarios.isEmpty() && numero!=null){
                horario = horarios.get(0);

                // Creo la notificación y la guardo
                Notificacion notificacion = new Notificacion();
                notificacion.setTipoNotificacion(TOMAR_MEDICAMENTOS);
                notificacion.setId(Long.valueOf(numero));
                DataBaseManager.getDaoSession().getNotificacionDao().insertOrReplace(notificacion);

                // Actualizo la medicion con los nuevos valores
                horario.setNotificacionId(numero);
                horario.setNotificacion(notificacion);
                DataBaseManager.getDaoSession().getHorarioMedicacionDao().insertOrReplace(horario);

                // Programo la nueva notificación
                programarNotificacion(context, numero, titulo, texto, hora, minuto,
                        horario.getId().intValue(), true, TOMAR_MEDICAMENTOS);
            }
        }
    }


    /**
     *  Permite cancelar una notificacion recurrente que esté vigente
     * @param context, contexto desde el que se llama
     * @param idMedicion, id de identificacion de la alerta
     */
    public static void cancelarNotificacion(Context context, int idMedicion) {

        Intent intent = new Intent(context, NotificacionesReceiver.class);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                idMedicion, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        am.cancel(pendingIntent);
        pendingIntent.cancel();
    }


}
