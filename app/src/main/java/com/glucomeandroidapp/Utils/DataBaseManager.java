package com.glucomeandroidapp.Utils;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.DaoMaster;
import com.glucomeandroidapp.POJO.DaoSession;

import org.greenrobot.greendao.database.Database;

import static com.glucomeandroidapp.Utils.Util.DB_NAME;

public class DataBaseManager {

    private static DaoSession daoSession;

    public static DaoSession getDaoSession() {
        if(daoSession ==null){
            conectarse(DB_NAME + AccountDataManager.obtenerIdUsuario());
        }
        return daoSession;
    }

    public static void conectarse(String dbName) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(App.getContext(), dbName);
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    public static void desconectarse() {
        daoSession = null;
    }
}
