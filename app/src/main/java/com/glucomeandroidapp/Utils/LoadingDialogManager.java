package com.glucomeandroidapp.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.glucomeandroidapp.R;

public class LoadingDialogManager {

    private static AlertDialog alertDialog;

    public static void crearDialogConfirmacion(Activity activity, String titulo, String descripcion){

        AlertDialog dialog = new AlertDialog.Builder(activity)
                .setView(R.layout.dialog_confirmacion)
                .show();

        TextView txtTitulo = dialog.findViewById(R.id.dialog_titulo);
        txtTitulo.setText(titulo);

        TextView txtDescripcion = dialog.findViewById(R.id.dialog_descripcion);
        txtDescripcion.setText(descripcion);

        Button buttonOk = dialog.findViewById(R.id.button_ok);
        buttonOk.setOnClickListener(v -> dialog.dismiss());

    }

    public static AlertDialog prepararDialogSiNo(Activity activity, String titulo, String descripcion){

        AlertDialog dialog = new AlertDialog.Builder(activity)
                .setView(R.layout.dialog_si_no)
                .show();

        TextView txtTitulo = dialog.findViewById(R.id.dialog_titulo);
        txtTitulo.setText(titulo);

        TextView txtDescripcion = dialog.findViewById(R.id.dialog_descripcion);
        txtDescripcion.setText(descripcion);

        return dialog;
    }

    public static void mostrar(Activity activity) {

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                    .setCancelable(false)
                    .setView(R.layout.dialog_loading);

            alertDialog = builder.create();
            alertDialog.show();

        } catch (Exception ex) {
            Log.e("LoadingDialogManager.mostrar()", ex.getMessage());
        }
    }


    public static void ocultar() {
        try {
            alertDialog.dismiss();
        } catch (Exception ex) {
            Log.e("LoadingDialogManager.ocultar()", ex.getMessage());
        }

    }
}
