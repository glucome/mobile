package com.glucomeandroidapp.Utils;

import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Enumeraciones.Rol;

import static com.glucomeandroidapp.Utils.Util.DB_NAME;

public class AccountDataManager {

    private static final String USER_ACCOUNT = "login_response";
    private static final String USER_DATA = "user_data";

    /* Usuario */

    public static LoginResponse obtenerDatosUsuario() {
        return LocalStorageManager.obtener(USER_ACCOUNT, LoginResponse.class);
    }

    public static String obtenerToken() {
        return obtenerDatosUsuario().getToken();
    }

    public static long obtenerIdUsuario() {
        return obtenerDatosUsuario().getId();
    }

    /**
     * Método para obtener el Rol del usuario logueado en formato String --> 'ROLE_PACIENTE' por ej.
     */
    public static String obtenerRol() {
        return obtenerDatosUsuario().getRoles().get(0).toString();
    }

    public static Rol obtenerRolEnum() {
        return Rol.getPorNombre(obtenerRol());
    }

    /* Paciente */

    public static PacienteDataResponse obtenerDatosPaciente() {
        return LocalStorageManager.obtener(USER_DATA, PacienteDataResponse.class);
    }

    public static long obtenerIdPaciente() {
        return obtenerDatosPaciente().getId();
    }

    /* Allegado */

    public static AllegadoDataResponse obtenerDatosAllegado() {
        return LocalStorageManager.obtener(USER_DATA, AllegadoDataResponse.class);
    }

    public static long obtenerIdAllegado() {
        return obtenerDatosAllegado().getId();
    }

    /* Medico */

    public static MedicoDataResponse obtenerDatosMedico() {
        return LocalStorageManager.obtener(USER_DATA, MedicoDataResponse.class);
    }

    public static long obtenerIdMedico() {
        return obtenerDatosMedico().getId();
    }

    /* Logueo y Deslogueo */

    public static void loguearse(LoginResponse usuario) {
        LocalStorageManager.guardar(
                USER_ACCOUNT, usuario);
        DataBaseManager.conectarse(DB_NAME + usuario.getId());
    }

    public static void almacenarDatosUsuario(Object datosUsuario) {
        LocalStorageManager.guardar(
                USER_DATA, datosUsuario);
    }

    public static void actualizarCuentaUsuario(LoginResponse datosUsuario) {
        LocalStorageManager.guardar(
                USER_ACCOUNT, datosUsuario);
    }

    public static void desoleguarse() {
        LocalStorageManager.eliminar(USER_ACCOUNT);
        LocalStorageManager.eliminar(USER_DATA);
        DataBaseManager.desconectarse();
    }

}
