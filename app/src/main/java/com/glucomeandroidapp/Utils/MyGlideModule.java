package com.glucomeandroidapp.Utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;


/**
 * Creado por MartinArtime el 21 de mayo del 2019
 */
@GlideModule
public class MyGlideModule extends AppGlideModule {}

