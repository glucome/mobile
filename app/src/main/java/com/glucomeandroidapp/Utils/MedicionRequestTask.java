package com.glucomeandroidapp.Utils;

import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.util.Log;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.CallbackInterface;

import java.io.IOException;

public class MedicionRequestTask extends AsyncTask<Integer, Double, Double> {

    private static final int CODE_REQUEST_MEDICION = 1;
    private static final int MIN_BYTES_TO_READ = 1;

    private BluetoothSocket bluetoothSocket;
    private CallbackInterface<Double> callbackInterface;

    public MedicionRequestTask(BluetoothSocket bluetoothSocket, CallbackInterface<Double> callbackInterface) {
        super();
        this.bluetoothSocket = bluetoothSocket;
        this.callbackInterface = callbackInterface;
    }

    @Override
    protected Double doInBackground(Integer... integers) {

        Double result = 0.0;

        try {
            Log.i("REQUEST", "MeasurementRequest");
            // Enviar solicitud de medicion
            bluetoothSocket.getOutputStream().write(CODE_REQUEST_MEDICION);

            while (bluetoothSocket.getInputStream().available() < MIN_BYTES_TO_READ)
                Thread.sleep(1000);

            // Aca debería solicitar los datos al arduino
            byte[] buffer = new byte[128];
            int bytes = bluetoothSocket.getInputStream().read(buffer);
            String readMessage = new String(buffer, 0, bytes);

            Log.i("JSON", readMessage);
            Log.i("JSON", "size: " + bytes);

            result = Double.valueOf(readMessage);

            Log.e(Util.TAG, "Valor leido: "+result);

        } catch (Exception ex){
            return 0.0;
        }

        return result;
    }

    @Override
    protected void onPostExecute(Double value) {
        callbackInterface.onSuccess(value);
    }

}
