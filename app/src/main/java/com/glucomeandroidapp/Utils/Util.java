package com.glucomeandroidapp.Utils;

import com.glucomeandroidapp.Conexiones.APIService;
import com.google.gson.Gson;

/**
 * Clase que tiene todas las constantes importantes y algunos métodos estáticos usados en la app
 * Creado por MartinArtime el 25 de abril del 2019
 */
public class Util {



    /**
     * Objetos útiles que es más comodo tenerlos a mano desde Util
     * y no con el poco legible código para llamar a "App".
     * Se inicializan en App de todos modos
     */
    private static Gson gson = new Gson();

    public static Gson getGsonHelper() {
        return gson;
    }

    //    public static Crypto crypto;
    public static APIService apiService;


    ///////////////////// Constantes /////////////////////////

    /**
     * Constantes Generales
     */
    public static final String DB_NAME = "glucoMe";
    public static final String BUNDLE_NAME = "bundle_name";
    public static final String APP_JSON = "application/json";
    public static final String BEARER = "Bearer ";
    public static final String SHARED_PREFERENCES = "com.glucomeandroidapp.shrpref";
    public static final String SI = "SI";
    public static final String NO = "NO";
    public static final String BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
    public static final int MEDICION_MINIMA_ACEPTABLE = 70;
    public static final int MEDICION_MAXIMA_ACEPTABLE = 140;

    /**
     * Constantes para Objetos
     */
    public static final String OBJECT_USUARIO = "usuario";

    public static final String ULTIMO_ENVIO_A_BACKEND = "ultimo_envio_a_backend";

    public static final String ALERTAS_PACIENTE_A_MEDICO_MEDICIONES = "alertas_de_paciente_a_medico_mediciones";
    public static final String ALERTAS_PACIENTE_A_ALLEGADO_MEDICIONES = "alertas_de_paciente_a_allegado_mediciones";
    public static final String ALERTAS_PACIENTE_A_MEDICO_MEDICACIONES = "alertas_de_paciente_a_medico_medicaciones";
    public static final String ALERTAS_PACIENTE_A_ALLEGADO_MEDICACIONES = "alertas_de_paciente_a_allegado_medicaciones";

    public static final String ID_NOTIFICACION = "id_notificacion";
    public static final String ID_MEDICION = "id_medicion";
    public static final String TITULO_NOTIFICACION = "titulo_notificacion";
    public static final String TEXTO_NOTIFICACION = "texto_notificacion";
    public static final String UBICACION_NOTIFICACION = "ubicacion_notificacion";

    public static final String HORA = "HORA";
    public static final String MINUTO = "MINUTO";


    /**
     * Constantes para Firebase
     */
    public static final String FIREBASE_KEY = "key=";
    public static final String FIREBASE_UPSTREAM = "https://fcm.googleapis.com/fcm/send";
    public static final String FIREBASE_SERVIDOR_HEREDADO = "AIzaSyAAUHg_saayyPp4xM1yqlzhKL-zYrCLLQA";
    public static final String FIREBASE_SERVIDOR = "AAAAsgZSL5c:APA91bERYRAoyRzqGkAzGZPmY5DRXOEQAbaCK9_wN8GmSfi7m23KTRBIfjObiVwECgj1tynOCIZVuily5hv6goQAAKf4GSdxA5ugSMEK0aPRdr0YS2V0tNjtAIAMtbSe8rtUxVeArwnE";


    /**
     * Constantes para Tipos de Input
     */
    public static final String INPUT_EMAIL = "email";
    public static final String INPUT_PASSWORD = "password";
    public static final String INPUT_NOMBRE = "nombre";
    public static final String INPUT_EDAD = "edad";

    /**
     * Constantes para LoginResponse (PerfilAdapter)
     */
    public static final String PERFIL_NOMBRE_DE_USUARIO = "Nombre";
    public static final String PERFIL_ALTURA = "Altura";
    public static final String PERFIL_PESO = "Peso";
    public static final String PERFIL_SEXO = "Sexo";
    public static final String PERFIL_FECHA_DE_NACIMIENTO = "Fecha de nacimiento";
    public static final String PERFIL_TIPO_DE_DIABETES = "Tipo de diabetes";
    public static final String PERFIL_MATRICULA = "Matricula";

    /**
     * Nombres de Roles del backend
     */
    public static final String ROLE = "ROLE";


    /**
     * Constantes para mensajes en Toast
     */
    // Generales
    public static final String FALLO_LA_BD = "Hubo un problema con la base de datos";
    public static final String FALTA_DE_PERMISOS = "No tiene los permisos suficientes para la petición";
    public static final String DATOS_USUARIO_NO_ENCONTRADOS = "No se encontraron los datos del usuario";
    public static final String GUARDADO_FALLIDO = "Falló el guardado, intenta de nuevo!";
    public static final String NO_HAY_INTERNET = "No hay conexión a internet";

    // Login
    public static final String LOGIN_EXITOSO = "Login exitoso!";
    public static final String DATOS_INCORRECTOS_LOGIN = "El mail y/o contraseña son incorrectos";
    public static final String FALLO_EL_LOGIN = "Falló el login, intenta de nuevo!";

    // Recuperar Contraseña
    public static final String FALLO_LA_SOLICITUD = "Ocurrió un error, intenta de nuevo!";
    public static final String EMAIL_NO_EXISTE = "El mail no corresponde a un usuario";
    public static final String CAMBIO_PASS_EXITOSO = "Contraseña cambiada exitosamente!";

    // Registro
    public static final String REGISTRO_EXITOSO = "Registro exitoso!";
    public static final String USUARIO_EXISTENTE = "Ya existe un usuario o mail, utiliza otro!";
    public static final String FALLO_EL_REGISTRO = "Falló el registro, intenta de nuevo!";

    //Edición mediciones automáticas
    public static final String GUARDADO_EXITOSO_MEDICION_AUTOMATICA = "Horario de medición guardado";
    public static final String BORRADO_EXITOSO_MEDICION_AUTOMATICA = "Horario de medición borrado";

    //Calibración
    public static final String GUARDADO_EXITOSO_HORARIOS_CALIBRACION = "Horarios de calibración guardados";
    public static final String MEDICION_OBTENIDA_CALIBRACION = "Se obtuvo la medición";
    public static final String FALLO_OBTENCION_MEDICION_CALIBRACION = "Falló la obtención de la medición. Intente nuevamente";
    public static final String GUARDADO_SIN_DATOS_CALIBRACION = "Debe obtener la medición e ingresar el valor invasivo mayor a cero";
    public static final String ITEM_CALIBRACION_NO_EDITABLE = "Ya se grabó esa calibración y no es editable";
    public static final String FINALIZACION_CALIBRACION_INCOMPLETA = "Para finalizar la calibración debe ingresar todas las mediciones";
    public static final String FINALIZACION_CALIBRACION_COMPLETA = "Ha finalizado correctamente la calibración del dispositivo";

    //Recomendador de comidas
    public static final String FALLO_DE_COMIDAS = "No se pudo obtener una recomendación para su estado actual.";
    public static final String COMIDAS_RECOMENDADAS = "comidasRecomendadas";
    public static final String COMIDAS_RECOMENDADAS_BUNDLE = "comidasRecomendadasBundle";
    public static final String INDICE_GLUCEMICO = "Indice Glúcemico: ";
    public static final String PORCIONES_REFERENCIA = "Como referencia se considera que una porción equivale a ";
    public static final String ULTIMA_MEDICION = "ultimaMedicion";
    public static final String RECETAS = "Recetas de ";

    //Busqueda de mediciones
    public static final String MEDICIONES_LIST = "medicionesList";
    public static final String FALLO_BUSQUEDA_MEDICIONES = "Se ha producido un error al obtener las mediciones, intente nuevamente mas tarde";
    public static final String FALLO_INGRESO_MEDICION = "No se pudieron enviar los datos al servidor";

    public static final String BLUETOOTH_DEVICE = "bluetoothDevice";
    public static final String BT_DEVICE_GLUKO_INVASIVE = "glucome";
    public static final String BT_DEVICE_GLUKO_NIN = "noinvasivo";

    public static final String MEDICION = "medicion";

    //Busqueda de médicos
    public static final String BUSQUEDA_VACIA = "Debe ingresar un nombre";
    public static final String FALLO_BUSQUEDA_MEDICOS = "No se pudieron buscar los médicos";
    public static final String MEDICO_SELECCIONADO = "medicoSeleccionado";
    public static final String MIS_MEDICOS_LIST = "misMedicos";
    public static final String MIS_PACIENTES_LIST = "misPacientes";
    public static final String SOLICITUD_MEDICO = "solicitudMedicoSeleccionado";
    public static final String MEDICO_BUSCADO_BUNDLE = "medicoBuscadoBundle";
    public static final String MATRICULA = "Matrícula: ";
    public static final String SOLICITUD_CORRECTA = "Solicitud enviada correctamente";
    public static final String SOLICITUD_CANCELACION_CORRECTA = "Solicitud cancelada correctamente";
    public static final String FALLO_EN_SOLICITUD = "Se ha producido un error al enviar la solicitud, intente nuevamente mas tarde";
    public static final String FALLO_EN_CANCELACION_SOLICITUD = "Se ha producido un error al cancelar la solicitud, intente nuevamente mas tarde";
    public static final String FALLO_EN_OBTENER_SOLICITUD = "Se ha producido un error al obtener la solicitud, intente nuevamente mas tarde";
    public static final String MEDICO_BORRADO_CORRECTAMENTE = "Médico borrado correctamente.";
    public static final String FALLO_EN_BORRAR_MEDICO = "Se ha producido un error al obtener la solicitud, intente nuevamente mas tarde";
    public static final String FALLO_BUSQUEDA_PACIENTES = "Se ha producido un error al obtener los pacientes, intente nuevamente mas tarde";


    //Medicamentos
    public static final String FALLO_BUSQUEDA_MEDICAMENTO = "No se pudieron buscar los medicamentos";
    public static final String MIS_MEDICAMENTOS_LIST = "misMedicamentos";
    public static final String TIPO_ACCION_MIS_MEDICAMENTOS = "tipoAccionMisMedicamentos";
    public static final String BUSQUEDA_VACIA_MEDICAMENTO = "No se encontraron medicamentos";
    public static final String MEDICAMENTO_CORRECTO = "Medicamento guardado correctamente";
    public static final String FALLO_EN_GRABAR_MEDICAMENTO_BACKEND = "No se pudo grabar el medicamento en la nube";
    public static final String MEDICAMENTO = "medicamento";
    public static final String MEDICAMENTO_BUNDLE = "medicamentoBundle";
    public static final String GUARDADO_EXITOSO_HORARIO_MEDICACION = "Horario medicación guardado";
    public static final String HORARIOS_MEDICAMENTOS_LIST = "horariosMedicamentoList";
    public static final String HORARIO_MEDICACION = "horarioMedicacion";
    public static final String HORARIO_MEDICACION_BUNDLE = "horarioMedicacionBundle";

    //ALLEGADOS
    public static final String FALLO_BUSQUEDA_ALLEGADO = "No se pudieron buscar los allegados";
    public static final String ALLEGADO_SELECCIONADO = "allegadoSeleccionado";
    public static final String MIS_ALLEGADOS_LIST = "misAllegados";
    public static final String SOLICITUD_ALLEGADO = "solicitudAllegadoSeleccionado";
    public static final String ALLEGADO_BUSCADO_BUNDLE = "allegadoBuscadoBundle";
    public static final String ALLEGADO_BORRADO_CORRECTAMENTE = "Allegado borrado correctamente.";
    public static final String FALLO_EN_BORRAR_ALLEGADO = "Se ha producido un error al obtener la solicitud, intente nuevamente mas tarde";
    public static final String SOLICITUDES_BUNDLE = "solicitudesBundle";
    public static final String FALLO_BUSQUEDA_SOLICITUDES = "Se ha producido un error al obtener las solicitudes, intente nuevamente mas tarde";
    public static final String SOLICITUD_RECHAZADA_CORRECTA = "Solicitud rechazada correctamente";
    public static final String FALLO_RECHAZO_SOLICITUD = "Se ha producido un error al rechazar la solicitud, intente nuevamente mas tarde";
    public static final String SOLICITUD_ACEPTADA_CORRECTA = "Solicitud aceptada correctamente";
    public static final String FALLO_ACEPTAR_SOLICITUD = "Se ha producido un error al aceptar la solicitud, intente nuevamente mas tarde";

    // INYECCION
    public static final String FALLO_INGRESO_INYECCION = "No se pudieron enviar los datos al servidor";
    public static final String  FALLO_BUSQUEDA_INYECCIONES = "No se pudieron buscar las inyecciones, intente nuevamente más tarde";

    // ALIMENTOS
    public static final String BUSQUEDA_VACIA_ALIMENTOS = "No se encontraron alimentos";
    public static final String FALLO_BUSQUEDA_ALIMENTOS = "No se pudieron buscar los alimentos";
    public static final String FALLO_INGRESO_ALIMENTO = "No se pudieron enviar los datos al servidor";
    public static final String ALIMENTO = "alimento";

    // EJERCICIOS
    public static final String FALLO_INGRESO_EJERCICIO = "No se pudieron enviar los datos al servidor";

    //CHATS
    public static final String MIS_CHAT_LIST = "misChatsList";
    public static final String MENSAJES_LIST = "MensajesList";
    public static final String FALLO_MARCANDO_COMO_LEIDO = "No se pudieron marcar los mensajes como leidos";
    public static final String FALLO_EN_CAMBIO_DE_ESTADO = "No se pudo cambiar su estado de conexión";

    public static final String FALLO_ACTUALIZACION_FACTORES = "No se han podido actualizar los factores de corrección";
    //Contactos
    public static final String FALLO_ENVIO_CONTACTO = "No se pudo enviar a la nube el contacto, se intentará mas tarde.";
    public static final String FALLO_DATOS_CONTACTO = "El nombre y el télefono no pueden ser vacíos, completelos e intente nuevamente.";
    public static final String CONTACTOS_LIST = "listadoDeContactos";

    //SUSCRIPCION
    public static final String EXITO_CAMBIO_SUSCRIPCION = "Se realizó el cambio de suscripción con éxito!";
    public static final String FALLO_CAMBIO_SUSCRIPCION = "No se pudo realizar el cambio de suscripción, intente nuevamente más tarde";

    // FOTO
    public static final String FALLO_ENVIO_FOTO = "No se pudieron enviar los datos al servidor";

    // TARJETA AUMENTADA
    public static final String PACIENTE_ID_BUNDLE = "paciente_id";
    public static final String PACIENTE_DATA_BUNDLE = "paciente_data";
    public static final String NOMBRE_ARCHIVO_PDF = "/tarjeta_aumentada.pdf";




    /**
     * MENSAJES GENERICOS SEGUN CODIGO DE RESPUESTA
     */
    public static final String MENSAJE_BAD_REQUEST = "Error al contactar el servidor";
    public static final String MENSAJE_RESPUESTA_VACIA = "No se han obtenido resultados";

    /**
     * Códigos de respuesta posibles del backend
     */
    public static final int OK_REQUEST = 200;
    public static final int BAD_REQUEST = 400;
    public static final int TOKEN_EXPIRADO = 401;
    public static final int SIN_CONEXION = 999;
    public static final int NOT_ACCEPTABLE = 406;
    public static final int EXCEPCION = -1;


    /**
     * Mensajes de respuesta del backend
     */
    public static final String OK = "ok";
    public static final String NOT_OK = "not_ok";

    /**
     * Constante TAG para debug al hacer pruebas en distintas partes de la app
     * Así se puede filtrar en el Logcat para ver los logs que uno quiere
     */
    public static final String TAG = "com.glucomeandroidapp";

    /**
     * URLs - Estructura de todas las rutas es:  URL_BASE + "COSA A HACER" + "/"
     * La URL_BASE hay que cambiar la IP en base a la que tenga asignada la pc en ese momento
     * cuando se corre el backend sino obviamente no anda.
     */
    //public static final String URL_BASE = "https://www.glucoMe.com.ar/";
    //public static final String URL_EXTRA = "/test";
    public static final String IP = "190.178.208.163";
    //public static final String IP = "3.14.81.255"; // IP del servidor Amazon
    public static final String URL_BASE = "http://" + IP + ":8080/api/v1/";
    public static final String URL_WEB_SOCKET = "ws://" + IP + ":8080/socket";
    public static final String URL_INICIALIZAR = "inicializar";
    public static final String URL_PACIENTE_LOGIN = "auth/signin/";
    public static final String URL_CAMBIAR_PASS = "auth/cambiarPass/";
    public static final String URL_EXISTE_MAIL = "existe_mail/{mail}/";
    public static final String URL_ENVIAR_USUARIO_TOKEN_FIREBASE = "firebase/token/";
    public static final String URL_ENVIAR_NOTIFICACION_MEDICION_USUARIO_FIREBASE = "firebase/enviarNotificacionMedicion/{idPaciente}/{idAvisado}/{medicion}/{ubicacion}/";
    public static final String URL_ENVIAR_NOTIFICACION_MEDICACION_USUARIO_FIREBASE = "firebase/enviarNotificacionMedicacion/{idPaciente}/{idAvisado}/{horaMedicacion}/";
    public static final String URL_OBTENER_USUARIO_TOKEN_FIREBASE = "firebase/token/{idUsuario}/";
    public static final String URL_PACIENTE_REGISTRO = "paciente/";
    public static final String URL_PACIENTE_MEDICION_ENVIO = "paciente/{id}/medicion/";
    public static final String URL_PACIENTE_INYECCION_ENVIO = "paciente/{id}/insulina/";
    public static final String URL_RECOMENDADOR = "recomendador/{id}/";
    public static final String URL_IMAGEN_COMIDA = "imagen/alimento/";
    public static final String URL_IMAGEN_USUARIO = "imagen/usuario/";
    public static final String URL_BUSQUEDA_MEDICO = "medico/";
    public static final String URL_SOLICITUD_MEDICO_PACIENTE = "solicitud/";
    public static final String URL_BUSQUEDA_SOLICITUD = "solicitud/{idPaciente}/{idMedico}/";
    public static final String URL_CANCELACION_SOLICITUD = "solicitud/{idSolicitud}/";
    public static final String URL_OBTENER_MIS_MEDICOS = "paciente/{idPaciente}/medicos/";
    public static final String URL_ENVIAR_FOTO_PERFIL = "imagen/{idUsuario}/";
    public static final String URL_OBTENER_MIS_MEDICAMENTOS = "paciente/{idPaciente}/medicamentos/";
    public static final String URL_BUSQUEDA_MEDICAMENTO = "medicamento/";
    public static final String URL_AGREGAR_MEDICAMENTO = "medicamento/";
    public static final String URL_BORRAR_MEDICO = "paciente/medico/";
    public static final String URL_ALLEGADO_REGISTRO = "allegado/";
    public static final String URL_OBTENER_MIS_PACIENTES = "medico/{idMedico}/pacientes/";
    public static final String URL_OBTENER_MIS_PACIENTES_ALLEGADO = "allegado/{idAllegado}/pacientes/";
    public static final String URL_BUSQUEDA_ALLEGADO = "allegado/";
    public static final String URL_OBTENER_MIS_ALLEGADOS = "paciente/{idPaciente}/allegados/";
    public static final String URL_PACIENTE_TOMA_MEDICAMENTO = "paciente/{id}/medicamento/";
    public static final String URL_OBTENER_MEDICIONES_PACIENTES = "paciente/{id}/medicion/listadoSinFiltro/";
    public static final String URL_BORRAR_ALLEGADO = "paciente/allegado/";
    public static final String URL_IMAGEN_ALLEGADO = "imagen/usuario/";
    public static final String URL_SOLICITUD_ALLEGADO_PACIENTE = "solicitud/allegado/";
    public static final String URL_GET_SOLICITUDES_ALLEGADO = "allegado/{idAllegado}/solicitudes/";
    public static final String URL_ACEPTACION_SOLICITUD = "solicitud/{idSolicitud}/";
    public static final String URL_PACIENTE_EJERCICIO_ENVIO = "paciente/{id}/ejercicio/";
    public static final String URL_BUSQUEDA_ALIMENTO = "alimento/";
    public static final String URL_INGRESO_ALIMENTO = "paciente/{id}/alimento_ingerido/";
    public static final String URL_OBTENER_CHAT_PACIENTE = "chat/medicos/";
    public static final String URL_OBTENER_MENSAJES_CHAT = "chat/mensajes/";
    public static final String URL_MARCAR_CHAT_COMO_LEIDO = "chat/marcar_mensajes_como_leidos/";
    public static final String URL_CAMBIAR_ESTADO_CHAT = "chat/cambiar_estado/";
    public static final String URL_PACIENTE_FACTORES_CORRECCION = "paciente/{id}/factor_correccion_insulina/";
    public static final String URL_AGREGAR_CONTACTO = "paciente/{id}/contacto/";
    public static final String URL_OBTENER_CONTACTOS = "paciente/{id}/contacto/";
    public static final String URL_BORRAR_CONTACTO = "contacto/{id}/";
    public static final String URL_PACIENTE_INFO_TARJETA_AUMENTADA = "paciente/{id}/info_tarjeta/";
    public static final String URL_CAMBIAR_SUSCRIPCION = "suscripcion/paciente/{id}/";
    public static final String URL_MEDICAMENTOS_TOMADOS = "paciente/{idPaciente}/medicamento/listado/";
    public static final String URL_INYECCIONES_INSULINA = "paciente/{idPaciente}/insulina/listado/";
    public static final String URL_ALIMENTOS = "paciente/{idPaciente}/alimento_ingerido/listado/";
    public static final String URL_EJERCICIOS = "paciente/{idPaciente}/ejercicio/listado/";
    public static final String URL_DESCARGA_TARJETA = URL_BASE + "descargar_tarjeta_aumentada/";

    /**
     * Dialog
     */
    public static final String AGREGAR_MEDICAMENTO_TITULO = "Agregar Medicamento";
    public static final String AGREGAR_MEDICAMENTO_DESCRIPCION = "¿Está seguro de agregar este medicamento?";
    public static final String BORRAR_ALLEGADO_TITULO = "Borrar allegado";
    public static final String BORRAR_ALLEGADO_DESCRIPCION = "¿Está seguro de borrar este allegado?";
    public static final String BORRAR_MEDICO_TITULO = "Borrar medico";
    public static final String BORRAR_MEDICO_DESCRIPCION = "¿Está seguro de borrar este medico?";
    public static final String BORRAR_CONTACTO_TITULO = "Borrar contacto";
    public static final String BORRAR_CONTACTO_DESCRIPCION = "¿Está seguro de borrar este contacto?";
    public static final String BORRAR_MEDICION_AUTOMATICA_TITULO = "Borrar horario de medición";
    public static final String BORRAR_MEDICION_AUTOMATICA_DESCRIPCION = "¿Está seguro de borrar este horario de medición?";
    public static final String CONFIRMACION_INSULINA_TITULO = "Insulina a Inyectarse";
    public static final String FALTAN_VALORES_INSULINA_TITULO = "No puede ingresar aún";
    public static final String FALTAN_VALORES_INSULINA_DESCRIPCION = "Debe configurar los factores de corrección antes de poder realizar recomendaciones.";
    public static final String MENU_CALIBRACION_TITULO = "Calibración";
    public static final String MENU_CALIBRACION_DESCRIPCION = "Ya tiene una calibración activa, si ingresa nuevamente borrará la calibración antigua y deberá realizarla nuevamente. ¿Desea continuar?";
    public static final String BORRAR_HORARIO_MEDICACION_TITULO = "Borrar horario de medicación";
    public static final String BORRAR_HORARIO_MEDICACION_DESCRIPCION = "¿Está seguro de borrar este horario?";
    public static final String CAMBIAR_SUSCRIPCION_BOTON = "Suscribirse a Gluko Premium";
    public static final String CAMBIAR_SUSCRIPCION_TITULO = "Suscribirse";
    public static final String CAMBIAR_SUSCRIPCION_DESCRIPCION = "¿Confirma la suscripción a Gluko Premium?";
    public static final String CANCELAR_SUSCRIPCION_BOTON = "Cancelar Suscripción a Gluko Premium";
    public static final String CANCELAR_SUSCRIPCION_TITULO = "Cancelar Suscripción";
    public static final String CANCELAR_SUSCRIPCION_DESCRIPCION = "¿Está seguro que desea cancelar la suscripción a Gluko Premium?";


    /**
     * Canales de notificaciones
     */
    public static final String NUMERO_NOTIFICACION = "NUMERO_NOTIFICACION";
    public static final String TIPO_NOTIFICACION = "TIPO_NOTIFICACION";

    public static final String MEDICIONES_AUTOMATICAS = "000";
    public static final String TOMAR_MEDICAMENTOS = "001";
    public static final String HORARIOS_CALIBRACION = "002";


    public static final String PROLONGAR = "00A";
    public static final String PROLONGAR_TEXTO = "Prolongar";

    // Canal Alertas
    public static final String CANAL_ALERTAS = "Alerta";
    public static final String CANAL_ALERTAS_ID = "01A";
    public static final String CANAL_ALERTAS_DESCRIPCION = "Para alertas";

    //NotificacionesDAO
    public static final String TITULO_MEDICION_AUTOMATICA = "Horario de medición";
    public static final String TEXTO_MEDICION_AUTOMATICA = "Es hora de hacerse una medición! ";
    public static final String TITULO_CALIBRACION = "Calibración de Dispositivo";
    public static final String TEXTO_CALIBRACION = "! Es hora de calibrar!";
    public static final String TITULO_MEDICACION = "¡Es hora de tomarse la medicación!";

    public static final Integer ALARMA_DIARIA_MEDICIONES_AUTOMATICAS = 100;
    public static final Integer ALARMA_DIARIA_TOMAR_MEDICACIONES = 200;

    // Refresh Token
    public static final Integer REFRESH_TOKEN = 999;
    public static final String REFRESH_TOKEN_ACTION = "999";
    public static final String REFRESH_TOKEN_MILLIS = "Millis Token";

    // Envío de datos automaticos al backend
    public static final Integer ENVIO_AUTOMATICO = 888;
    public static final String ENVIO_AUTOMATICO_ACTION = "888";
    public static final String ENVIO_AUTOMATICO_MILLIS = "Millis Envio Automatico";

    // Verificacion si paciente tomo medicación
    public static final Integer VERIFICAR_PACIENTE_TOMO_MEDICACION = 777;
    public static final String VERIFICAR_PACIENTE_TOMO_MEDICACION_ACTION = "777";
    public static final String VERIFICAR_PACIENTE_TOMO_MEDICACION_MILLIS = "Millis Paciente Tomo Medicacion";


    /**
     * Crypto
     */
    public static final String KEY = "fqJfdzGDvfwbedsKSUGty3VZ9taXxMVw";
    public static final String VECTOR = "disVect0riztNo1z";

    /**
     * Filtros
     */
    public static final String FECHA_DESDE = "fechaDesde";
    public static final String FECHA_HASTA = "fechaHasta";
    public static final String VALOR_MEDICION_SUPERIOR = "valorDeMedicionSuperior";
    public static final String VALOR_MEDICION_INFERIOR = "valorDeMedicionInferior";
    public static final String MOMENTO_MEDICION = "momentoMedicion";
    public static final String MOMENTO_ALIMENTO = "momentoAlimento";
    public static final String NOMBRE = "nombre";
    public static final String EDAD = "edad";
    public static final String TIPO_INSULINA = "tipoInsulina";
    public static final String TIPO_EJERCICIO = "tipoEjercicio";
    public static final String TIPO_DIABETES = "tipoDiabetes";
    public static final String CANTIDAD_DESDE = "cantidadDesde";
    public static final String CANTIDAD_HASTA = "cantidadHasta";
    public static final String EDAD_DESDE = "edadDesde";
    public static final String EDAD_HASTA = "edadHasta";

    public static final Integer TAG_ACTIVITY_FILTRO = 165;

    /**
     * Otras
     */
    public static final String TIPO_DE_ADAPTER_CON_IMAGEN = "Imagen y datos";
    public static final String TIPO_DE_ADAPTER_SOLO_TEXTO = "Solo texto";
    public static final String FRAGMENT_MENU_PRINCIPAL = "Menu principal";
    public static final String FRAGMENT_RESUMEN = "Resumen";
    public static final String FRAGMENT_CONFIGURACION = "Configuracion";
    public static final String MEDICION_AUTOMATICA_ID = "idMedicionAutomatica";
    public static final String PACIENTE_ID = "idPaciente";
    public static final String PACIENTE_NOMBRE = "nombrePaciente";
    public static final String LISTADO_TOMA_MEDICAMENTO= "listadoTomaMedicamentos";
    public static final String LISTADO_INYECCIONES = "listadoInyecciones";
    public static final String LISTADO_ALIMENTOS = "listadoAlimentos";
    public static final String LISTADO_EJERCICIOS = "listadoEjercicios";
    public static final String ITEM_CALIBRACION_ID = "idItemCalibracion";
    public static final String FRAGMENT_AL_QUE_VOLVER = "FragmentAlQueVolver";
    public static final String ENVIANDO_DATOS = "Enviando datos!";
    public static final String DIALOG_CARGANDO = "Cargando";
    public static final String DIALOG_CON_CONFIRMACION = "Confirmacion";
    public static final String DIALOG_CON_SI_NO = "SiNo";

}
