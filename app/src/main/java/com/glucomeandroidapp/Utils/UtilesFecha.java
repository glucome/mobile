package com.glucomeandroidapp.Utils;

import android.widget.DatePicker;
import android.widget.TimePicker;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Calendar;

/**
 * @author Pablo
 *
 */
public class UtilesFecha {

	private static final DateTimeFormatter FORMATTER_FECHA = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	private static final DateTimeFormatter FORMATTER_FECHA_HORA =  DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

	public static LocalDateTime millsToLocalDateTime(long millis) {
		Instant instant = Instant.ofEpochMilli(millis);
		return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static Long getTimeInMillisFromLocalDateTime(LocalDateTime local) {
		Calendar antes = Calendar.getInstance();
		Calendar calendarAlarma = Calendar.getInstance();
		calendarAlarma.set(calendarAlarma.get(Calendar.YEAR),
				calendarAlarma.get(Calendar.MONTH),
				calendarAlarma.get(Calendar.DAY_OF_MONTH),
				local.getHour(), local.getMinute(),
				calendarAlarma.get(Calendar.SECOND));

		if(calendarAlarma.before(antes)) {
			calendarAlarma.add(Calendar.DATE, 1);
		}

		return calendarAlarma.getTimeInMillis();
	}

	private static LocalDate getLocalDateFromString(String fecha) throws DateTimeParseException{
		return LocalDate.parse(fecha, FORMATTER_FECHA);
	}
	
	public static String getStringFromLocalDate(LocalDate fecha) throws DateTimeParseException{
		if(fecha == null)
			return null;
		return fecha.format(FORMATTER_FECHA);
	}

	public static String getStringFromLocalDateTime(LocalDateTime fecha) throws DateTimeParseException{
		if(fecha == null)
			return null;
		return fecha.format(FORMATTER_FECHA_HORA);
	}

	public static LocalDateTime getLocalDateTimeFromString(String fechaHora) throws DateTimeParseException{
		return LocalDateTime.parse(fechaHora, FORMATTER_FECHA_HORA);
	}

	public static String getEdadFromFechaDeNacimiento(LocalDate fechaDeNacimiento){
		return String.valueOf(Period.between(fechaDeNacimiento, LocalDate.now()).getYears());
	}

	public static String obtenerEdad(String dateString){
		LocalDate date = getLocalDateFromString(dateString);
		return obtenerEdad(date.getYear(), date.getMonthValue(), date.getDayOfMonth());
	}

	private static String obtenerEdad(int year, int month, int day){
		LocalDate dob = LocalDate.of(year, month, day);
		LocalDate today = LocalDate.now();

		int age = today.getYear() - dob.getYear();

		if (today.getDayOfYear() < dob.getDayOfYear()){
			age--;
		}

		return Integer.toString(age);
	}

	public static String formatearFecha(LocalDateTime dateTime){
		int dia = dateTime.getDayOfMonth();
		int mes = dateTime.getMonth().getValue();
		int anio = dateTime.getYear();
		int hora = dateTime.getHour();
		int minutos = dateTime.getMinute();
		return formatearFecha(dia, mes, anio, hora, minutos);
	}

	public static String formatearFecha(DatePicker datePicker, TimePicker timePicker){
		int dia = datePicker.getDayOfMonth();
		int mes = datePicker.getMonth() + 1;
		int anio = datePicker.getYear();
		int hora = timePicker.getHour();
		int minutos = timePicker.getMinute();
		return formatearFecha(dia, mes, anio, hora, minutos);
	}

	public static String formatearSoloDate(DatePicker datePicker){
		int dia = datePicker.getDayOfMonth();
		int mes = datePicker.getMonth() + 1;
		int anio = datePicker.getYear();
		return formatearFecha(dia, mes, anio, 0, 0);
	}

	private static String formatearFecha(int dia, int mes, int anio, int hora, int minutos){
		String strMes = mes < 10 ? "0" + mes
				: String.valueOf(mes);
		String strDia = dia < 10 ? "0" + dia
				: String.valueOf(dia);
		String strHora = hora < 10 ? "0" + hora
				: String.valueOf(hora);
		String strMinutos = minutos < 10 ? "0" + minutos
				: String.valueOf(minutos);

		return strDia + "/" + strMes + "/" +
				anio + " " + strHora + ":" + strMinutos;
	}

}
