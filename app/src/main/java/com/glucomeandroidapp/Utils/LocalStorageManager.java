package com.glucomeandroidapp.Utils;

import android.content.SharedPreferences;

import com.glucomeandroidapp.App;

import java.lang.reflect.Type;

import static android.content.Context.MODE_PRIVATE;
import static com.glucomeandroidapp.Utils.Crypto.decrypt;
import static com.glucomeandroidapp.Utils.Crypto.encrypt;
import static com.glucomeandroidapp.Utils.Util.SHARED_PREFERENCES;

public class LocalStorageManager {
    /**
     * Método para convertir un objeto en JSON, el objeto es encriptado antes de la conversion
     * @param objeto a convertir
     * @return un String con formato JSON
     */
    private static String encriptar(Object objeto){
        return encrypt(Util.getGsonHelper().toJson(objeto));
    }

    /**
     * Método para obtener un objeto, el cual es desencriptado y desearializado
     * @param json guardado
     * @param classType, tipo de dato a devolver
     * @return un GenericClass para ser casteado luego
     */
    private static <T> T desencriptar(String json, Type classType){
        String decrypt = decrypt(json);
        if(decrypt != null && !decrypt.equals("{}")){
            return Util.getGsonHelper().fromJson(decrypt, classType);
        }
        return null;
    }

    /**
     * Método que se encarga de guardar un dato como <key, value> exclusivo para uso de la app
     * Es genérico y guarda en json, el objeto tiene que implementar Serializable
     */
    public static void guardar(String key, Object element){
        SharedPreferences preferences = App.getInstance().getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        String json = encriptar(element);
        prefsEditor.putString(key, json);
        prefsEditor.apply();
    }

    /**
     * Método que se encarga de obtener un dato guardado como <key, value> de la app
     * Genérico, hay que castearlo al objeto original a la salida del método.
     */
    public static <T> T obtener(
            String key, Type clase) {
        SharedPreferences preferences = App.getInstance().getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        if (preferences.contains(key)) {
            String cosaRecuperada = preferences.getString(key, null);
            if(cosaRecuperada == null ||  cosaRecuperada.equals("")){
                return null;
            }

            return desencriptar(cosaRecuperada, clase);
        }
        return null;
    }


    /**
     * Método que se encarga de eliminar un dato como <key, value> exclusivo para uso de la app
     */
    public static void eliminar(String key){
        SharedPreferences preferences = App.getInstance().getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        preferences.edit().remove(key).apply();
    }


}
