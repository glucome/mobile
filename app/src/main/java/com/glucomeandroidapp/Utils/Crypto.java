package com.glucomeandroidapp.Utils;

import android.util.Base64;
import android.util.Log;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static com.glucomeandroidapp.Utils.Util.KEY;
import static com.glucomeandroidapp.Utils.Util.TAG;
import static com.glucomeandroidapp.Utils.Util.VECTOR;

/**
 * Clase tipo-Util donde se encuentran las funciones criptográficas para agregar seguridad
 * a los datos dentro de la app y también para la autenticación de usuarios
 *
 * Creado por MartinArtime (hace tiempo ya)
 */
public class Crypto {

    private static String mKey = KEY;
    private static String mInitVector = VECTOR;

    /**
     * Constructor con datos estáticos que estan en la clase Util
     */
//    public Crypto() {
//        mKey = KEY;
//        mInitVector = VECTOR;
//    }

    /**
     * Método para encriptar un string dado por medio de AES (Advanced Encryption Standard)
     * @param value, String a ser encriptado
     * @return un String encriptado
     */
    public static String encrypt(String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(
                    mInitVector.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec smKeySpec = new SecretKeySpec(
                    mKey.getBytes(StandardCharsets.UTF_8), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, smKeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());

            return Base64.encodeToString(encrypted, 0);
        } catch (Exception ex) {
            Log.e(TAG, "Error encriptando. Mensaje: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Método para desencriptar un string dado por medio de AES (Advanced Encryption Standard)
     * @param encrypted, String encriptado
     * @return un String desencriptado
     */
    public static String decrypt(String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(
                    mInitVector.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec smKeySpec = new SecretKeySpec(
                    mKey.getBytes(StandardCharsets.UTF_8), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, smKeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decode(encrypted, 0));

            String s = new String(original);

            return s;
        } catch (Exception ex) {
            Log.e(TAG, "Error desencriptando. Mensaje: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Método usado para el cambiarPass de usuarios
     * @param input, contraseña del usuario intentando ingresar
     * @return el hash que devuelve la funcion dada la contraseña
     *
     * TODO ESTE METODO DESPUÉS HAY QUE CAMBIARLO, LO USE UNA VEZ EN EL LABURO Y SERVÍA AHORA
     * HAY QUE VER PARA USAR EL MISMO MÉTODO DE HASHEO EN TODO EL SISTEMA ASÍ ES CONSISTENTE
     * Y SOLO NOS MANEJAMOS CON HASHES
     */
    public static String md5(String input) {
        if (input != null) {
            StringBuilder result = new StringBuilder(input);
            MessageDigest md;
            try {
                md = MessageDigest.getInstance("MD5");
                md.update(input.getBytes());
                BigInteger hash = new BigInteger(1, md.digest());
                result = new StringBuilder(hash.toString(16));
                while (result.length() < 32) { //40 for SHA-1
                    result.insert(0, "0");
                }
            } catch (NoSuchAlgorithmException e) {
                Log.e(TAG, "Problema al obtener instancia de MD5");
            }
            return result.toString();
        }
        return null;
    }
}