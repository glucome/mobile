package com.glucomeandroidapp.Utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Controladores.VariosControlador;
import com.glucomeandroidapp.POJO.APIClasses.Response.AllegadoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.MedicoDataResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.PacienteDataResponse;
import com.glucomeandroidapp.POJO.Contacto;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import static com.glucomeandroidapp.Utils.Util.ALERTAS_PACIENTE_A_ALLEGADO_MEDICIONES;
import static com.glucomeandroidapp.Utils.Util.ALERTAS_PACIENTE_A_MEDICO_MEDICIONES;
import static com.glucomeandroidapp.Utils.Util.MEDICION_MAXIMA_ACEPTABLE;
import static com.glucomeandroidapp.Utils.Util.MEDICION_MINIMA_ACEPTABLE;

/**
 * Creado por MartinArtime el 05 de septiembre del 2019
 */
public class AlertasAOtrosUsuariosManager {

    public static void enviarAlertasAAsociadosSiCorresponde(Activity activity, String medicion){

        String avisarAAllegado = LocalStorageManager.obtener(
                ALERTAS_PACIENTE_A_ALLEGADO_MEDICIONES, String.class);

        String avisarAMedico = LocalStorageManager.obtener(
                ALERTAS_PACIENTE_A_MEDICO_MEDICIONES, String.class);

        LoginResponse loginResponse = AccountDataManager.obtenerDatosUsuario();

        PacienteDataResponse usuario = AccountDataManager.obtenerDatosPaciente();

        if (usuario != null) {
            // Si se dan ambas condiciones, se envía un mensaje por Firebase al los medicos asociados
            if (Double.valueOf(medicion) < MEDICION_MINIMA_ACEPTABLE || Double.valueOf(medicion) > MEDICION_MAXIMA_ACEPTABLE) {

                Dexter.withActivity(activity)
                        .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {

                                    LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
/*
                                    // Create a criteria object to retrieve provider
                                    Criteria criteria = new Criteria();

                                    // Get the name of the best provider
                                    String provider = locationManager.getBestProvider(criteria, true);
*/
                                    Location myLocation = getLastKnownLocation(locationManager);

                                    if(myLocation!=null){
                                        //latitude of location
                                        double myLatitude = myLocation.getLatitude();

                                        //longitude og location
                                        double myLongitude = myLocation.getLongitude();

                                        String ubicacion = myLatitude + "," + myLongitude;

                                        Log.e(Util.TAG, "Ubicacion obtenida: "+ubicacion);

                                        //if (avisarAMedico != null && avisarAMedico.equals("true")) {
                                            if (loginResponse != null && usuario.getMedicos() != null) {
                                                for (MedicoDataResponse medico : usuario.getMedicos()) {

                                                    App.getInstance().mostrarMensajeFlotante("Enviando alerta de fuera de rango a medicos");
                                                    VariosControlador.enviarNotificacionMedicionAUsuario(loginResponse, medico.getIdUsuario(), medicion, ubicacion);
                                                }
                                            }
                                        //}
                                        //if (avisarAAllegado != null && avisarAAllegado.equals("true")) {
                                            if (loginResponse != null && usuario.getAllegados() != null) {
                                                for (AllegadoDataResponse allegado : usuario.getAllegados()) {

                                                    App.getInstance().mostrarMensajeFlotante("Enviando alerta de fuera de rango a allegados");
                                                    VariosControlador.enviarNotificacionMedicionAUsuario(loginResponse, allegado.getIdUsuario(), medicion, ubicacion);
                                                }
                                            }
                                        //}

                                    }
                                    List<Contacto> contactoList = DataBaseManager.getDaoSession().getContactoDao().loadAll();
                                    if (contactoList != null && !contactoList.isEmpty()) {
                                        VariosControlador.pidePermisoSms(activity);
                                        //if (VariosControlador.pidePermisoSms(activity.getParent())) {
                                        if(VariosControlador.tienePermisoSms(activity)){
                                            for (Contacto contacto : contactoList) {
                                                VariosControlador.enviarSmsDeNotificacionAContactos(contacto, medicion);
                                            }
                                        }
                                    }


                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();

            }
        }

    }

    private static Location getLastKnownLocation(LocationManager locationManager) {

        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            @SuppressLint("MissingPermission")
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }
}
