package com.glucomeandroidapp.Utils;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import com.glucomeandroidapp.Activities.Mediciones.MedicionAutomaticaActivity;
import com.glucomeandroidapp.App;
import com.glucomeandroidapp.Conexiones.CallbackInterface;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import static com.glucomeandroidapp.Utils.Util.BLUETOOTH_DEVICE;
import static com.glucomeandroidapp.Utils.Util.BT_DEVICE_GLUKO_INVASIVE;
import static com.glucomeandroidapp.Utils.Util.BT_DEVICE_GLUKO_NIN;
import static com.glucomeandroidapp.Utils.Util.DIALOG_CARGANDO;


public class BluetoothDeviceManager {

    public static final int REQUEST_ENABLE_BT = 101;

    private BluetoothAdapter mBluetoothAdapter;
    private BroadcastReceiver mReceiver;

    //private Activity activity;
    private boolean deviceFound;

    public BluetoothDeviceManager(CallbackInterface<BluetoothDevice> callbackInterface) {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null)
            App.getInstance().mostrarMensajeFlotante("Este dispositivo no soporta Bluetooth");

        //
        //activity = fromActivity;

        mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                if (BluetoothDevice.ACTION_FOUND.equals(action)) {

                    BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    if(bluetoothDevice != null){
                        try{
                            Log.e("BT", bluetoothDevice.getName());

                            if (bluetoothDevice.getName().toLowerCase().equals(BT_DEVICE_GLUKO_INVASIVE) ||
                                    bluetoothDevice.getName().toLowerCase().equals(BT_DEVICE_GLUKO_NIN)) {
                                mBluetoothAdapter.cancelDiscovery();
                                deviceFound = true;
                                callbackInterface.onSuccess(bluetoothDevice);
//                        Intent i = new Intent(activity, MedicionAutomaticaActivity.class);
//                        i.putExtra(BLUETOOTH_DEVICE, bluetoothDevice);
//                        activity.startActivity(i);
                            }
                        }catch (Exception ex){
                            Log.e("BT", "name null");
                            ex.printStackTrace();
                        }

                    }else{
                        Log.e("BT", "null");
                    }



                } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    if(!deviceFound){
                        App.getInstance().mostrarMensajeFlotante("No se encontró el dispositivo.");
                        callbackInterface.onSuccess(null);
                       //LoadingDialogManager.ocultar();
                    }
                }
            }
        };

        // Register broadcasts
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        App.getContext().registerReceiver(mReceiver, filter);
    }

    public void searchDevice(Activity fromActivity, boolean forceBtConection) {

        deviceFound = false;

        // Check permission
        Dexter.withActivity(fromActivity)
                .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (mBluetoothAdapter.isEnabled()){
                            LoadingDialogManager.mostrar(fromActivity);
                            mBluetoothAdapter.startDiscovery();
                        }else if(forceBtConection){
                            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            fromActivity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        App.getInstance().mostrarMensajeFlotante("Se necesita acceso para poder realizar la medición.");
                    }
                })
                .check();
    }

    public void onDestroy() {
        try{
            App.getContext().unregisterReceiver(mReceiver);
            if (mBluetoothAdapter != null) {
                mBluetoothAdapter.disable();
            }
        }catch (Exception ex){
            ex.printStackTrace();
            App.getInstance().mostrarMensajeFlotante("Error al intentar cerrar la conexión bluetooth.");
        }
    }


}
