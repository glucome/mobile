package com.glucomeandroidapp.CustomComponents;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.glucomeandroidapp.R;

public class EditText_Numerico extends LinearLayout {

    ImageView imgAdd;
    ImageView imgRemove;
    TextView txtValue;

    public EditText_Numerico(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.component_input_numerico, this);

        int[] sets = {R.attr.value, R.attr.step, R.attr.permitirNegativos};
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.EditText_Numerico);
        Integer valor = typedArray.getInt(R.styleable.EditText_Numerico_value, 0);
        Integer step = typedArray.getInt(R.styleable.EditText_Numerico_step, 1);
        Boolean permitirNegativos = typedArray.getBoolean(R.styleable.EditText_Numerico_permitirNegativos, false);
        Boolean permitirCero = typedArray.getBoolean(R.styleable.EditText_Numerico_permitirCero, true);
        typedArray.recycle();

        initComponents();

        if(!permitirNegativos && valor < 0 && !permitirCero){
            setText(1);
        } else if(!permitirNegativos && valor < 0){
            setText(0);
        } else {
            setText(valor);
        }

        imgAdd.setOnClickListener(view -> setText(getText() + step));
        imgRemove.setOnClickListener(view -> {
            if (!permitirCero && (getText() - step == 0))
                return;
            if (!permitirNegativos && (getText() - step < 0))
                return;
            setText(getText() - step);
        });

        imgAdd.setOnLongClickListener(v -> {
            setText(getText() + step*10);
            return true;
        });
        imgRemove.setOnLongClickListener(v -> {
            if (!permitirCero && (getText() - step*10 == 0))
                return true;
            if (!permitirNegativos && (getText() - step*10 < 0))
                return true;
            setText(getText() - step*10);
            return true;
        });
    }

    private void initComponents() {
        txtValue = findViewById(R.id.txt_valor_input_numerico);

        imgAdd = findViewById(R.id.img_add_input_numerico);

        imgRemove = findViewById(R.id.img_remove_input_numerico);
    }

    public Integer getText() {
        return Integer.valueOf(txtValue.getText().toString());
    }

    public void setText(Integer value) {
        String stringValue = value.toString();
        txtValue.setText(stringValue);
    }
}
