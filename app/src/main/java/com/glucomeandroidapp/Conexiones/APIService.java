package com.glucomeandroidapp.Conexiones;

import com.glucomeandroidapp.POJO.APIClasses.Requests.CambiarEstadoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ContactoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.EjercicioRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.FactorCorreccionInsulinaRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.FirebaseTokenRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.InyeccionRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ListadoAlimentoIngeridoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ListadoEjercicioRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ListadoInyeccionInsulinaRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.ListadoMedicamentoTomadosRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.LoginRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.MarcarMensajesComoLeidosRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.MedicamentoPacienteRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.MedicionRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.RegistroAllegadoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.RegistroPacienteRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.SolicitudRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.SuscripcionPlanRequest;
import com.glucomeandroidapp.POJO.APIClasses.Requests.TomaMedicamentoRequest;
import com.glucomeandroidapp.POJO.APIClasses.Response.BooleanResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.EmptyResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.FirebaseTokenResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.IdResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.InfoTarjetaAumentadaResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoAlimentoIngeridoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoAlimentoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoAllegadoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoContactoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoEjercicioResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoInyeccionInsulinaResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMedicamentoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMedicamentosIngeridosResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMedicionesResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMedicoResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoMensajeWrapperResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoPacienteResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoSolicitudResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.ListadoUsuariosChatsResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.LoginResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RecomendadorResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.POJO.APIClasses.Response.SolicitudResponse;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

import static com.glucomeandroidapp.Utils.Util.URL_ACEPTACION_SOLICITUD;
import static com.glucomeandroidapp.Utils.Util.URL_AGREGAR_CONTACTO;
import static com.glucomeandroidapp.Utils.Util.URL_AGREGAR_MEDICAMENTO;
import static com.glucomeandroidapp.Utils.Util.URL_ALIMENTOS;
import static com.glucomeandroidapp.Utils.Util.URL_ALLEGADO_REGISTRO;
import static com.glucomeandroidapp.Utils.Util.URL_BORRAR_ALLEGADO;
import static com.glucomeandroidapp.Utils.Util.URL_BORRAR_CONTACTO;
import static com.glucomeandroidapp.Utils.Util.URL_BORRAR_MEDICO;
import static com.glucomeandroidapp.Utils.Util.URL_BUSQUEDA_ALIMENTO;
import static com.glucomeandroidapp.Utils.Util.URL_BUSQUEDA_ALLEGADO;
import static com.glucomeandroidapp.Utils.Util.URL_BUSQUEDA_MEDICAMENTO;
import static com.glucomeandroidapp.Utils.Util.URL_BUSQUEDA_MEDICO;
import static com.glucomeandroidapp.Utils.Util.URL_BUSQUEDA_SOLICITUD;
import static com.glucomeandroidapp.Utils.Util.URL_CAMBIAR_ESTADO_CHAT;
import static com.glucomeandroidapp.Utils.Util.URL_CAMBIAR_PASS;
import static com.glucomeandroidapp.Utils.Util.URL_CAMBIAR_SUSCRIPCION;
import static com.glucomeandroidapp.Utils.Util.URL_CANCELACION_SOLICITUD;
import static com.glucomeandroidapp.Utils.Util.URL_EJERCICIOS;
import static com.glucomeandroidapp.Utils.Util.URL_ENVIAR_FOTO_PERFIL;
import static com.glucomeandroidapp.Utils.Util.URL_ENVIAR_NOTIFICACION_MEDICACION_USUARIO_FIREBASE;
import static com.glucomeandroidapp.Utils.Util.URL_ENVIAR_NOTIFICACION_MEDICION_USUARIO_FIREBASE;
import static com.glucomeandroidapp.Utils.Util.URL_ENVIAR_USUARIO_TOKEN_FIREBASE;
import static com.glucomeandroidapp.Utils.Util.URL_EXISTE_MAIL;
import static com.glucomeandroidapp.Utils.Util.URL_GET_SOLICITUDES_ALLEGADO;
import static com.glucomeandroidapp.Utils.Util.URL_INGRESO_ALIMENTO;
import static com.glucomeandroidapp.Utils.Util.URL_INICIALIZAR;
import static com.glucomeandroidapp.Utils.Util.URL_INYECCIONES_INSULINA;
import static com.glucomeandroidapp.Utils.Util.URL_MARCAR_CHAT_COMO_LEIDO;
import static com.glucomeandroidapp.Utils.Util.URL_MEDICAMENTOS_TOMADOS;
import static com.glucomeandroidapp.Utils.Util.URL_OBTENER_CHAT_PACIENTE;
import static com.glucomeandroidapp.Utils.Util.URL_OBTENER_CONTACTOS;
import static com.glucomeandroidapp.Utils.Util.URL_OBTENER_MEDICIONES_PACIENTES;
import static com.glucomeandroidapp.Utils.Util.URL_OBTENER_MENSAJES_CHAT;
import static com.glucomeandroidapp.Utils.Util.URL_OBTENER_MIS_ALLEGADOS;
import static com.glucomeandroidapp.Utils.Util.URL_OBTENER_MIS_MEDICAMENTOS;
import static com.glucomeandroidapp.Utils.Util.URL_OBTENER_MIS_MEDICOS;
import static com.glucomeandroidapp.Utils.Util.URL_OBTENER_MIS_PACIENTES;
import static com.glucomeandroidapp.Utils.Util.URL_OBTENER_MIS_PACIENTES_ALLEGADO;
import static com.glucomeandroidapp.Utils.Util.URL_OBTENER_USUARIO_TOKEN_FIREBASE;
import static com.glucomeandroidapp.Utils.Util.URL_PACIENTE_EJERCICIO_ENVIO;
import static com.glucomeandroidapp.Utils.Util.URL_PACIENTE_FACTORES_CORRECCION;
import static com.glucomeandroidapp.Utils.Util.URL_PACIENTE_INFO_TARJETA_AUMENTADA;
import static com.glucomeandroidapp.Utils.Util.URL_PACIENTE_INYECCION_ENVIO;
import static com.glucomeandroidapp.Utils.Util.URL_PACIENTE_LOGIN;
import static com.glucomeandroidapp.Utils.Util.URL_PACIENTE_MEDICION_ENVIO;
import static com.glucomeandroidapp.Utils.Util.URL_PACIENTE_REGISTRO;
import static com.glucomeandroidapp.Utils.Util.URL_PACIENTE_TOMA_MEDICAMENTO;
import static com.glucomeandroidapp.Utils.Util.URL_RECOMENDADOR;
import static com.glucomeandroidapp.Utils.Util.URL_SOLICITUD_ALLEGADO_PACIENTE;
import static com.glucomeandroidapp.Utils.Util.URL_SOLICITUD_MEDICO_PACIENTE;

/**
 * Interfaz que tiene las firmas a las acciones posibles para transferencia con el servidor web
 *
 * Creado por MartinArtime el 24 de abril del 2019
 */
public interface APIService {

    /**
     * Firma para el método que envia el registro de un paciente
     */
    @POST(URL_PACIENTE_REGISTRO)
    Call<RespuestaApi<LoginResponse>> registrarPaciente(@Header("Content-Type") String content_type,
                                                        @Body RegistroPacienteRequest nuevoUsuario);

    /**
     * Firma para el método que verifica si el mail corresponde a un usuario
     */
    @GET(URL_EXISTE_MAIL)
    Call<RespuestaApi<BooleanResponse>> existeMail(@Header("Content-Type") String content_type,
                                                   @Path("mail") String email);

    /**
     * Firma para el método que envia el cambiarPass de un paciente
     */
    @POST(URL_PACIENTE_LOGIN)
    Call<RespuestaApi<LoginResponse>> loginPaciente(@Header("Content-Type") String content_type,
                                                                      @Body LoginRequest usuarioLogin);

    /**
     * Firma para el método que envia el cambio de pass de un usuario
     */
    @POST(URL_CAMBIAR_PASS)
    Call<RespuestaApi<BooleanResponse>> cambiarPass(@Header("Content-Type") String content_type,
                                                                      @Body LoginRequest usuarioLogin);

    /**
     * Firma para el método que envia una medicion al backend
     */
    @POST(URL_PACIENTE_MEDICION_ENVIO)
    Call<RespuestaApi<IdResponse>> enviarMedicion(@Header("Content-Type") String content_type,
                                                        @Path("id") long idLoginResponse,
                                                        @Body MedicionRequest medicion,
                                                        @Header("Authorization") String authHeader);

    /**
     * Firma para el método que envia una inyeccion al backend
     */
    @POST(URL_PACIENTE_INYECCION_ENVIO)
    Call<RespuestaApi<IdResponse>> enviarInyeccion(@Header("Content-Type") String content_type,
                                                          @Path("id") long idLoginResponse,
                                                          @Body InyeccionRequest inyeccionRequest,
                                                          @Header("Authorization") String authHeader);

    /**
     * Firma para el método que envia una inyeccion al backend
     */
    @POST(URL_PACIENTE_EJERCICIO_ENVIO)
    Call<RespuestaApi<IdResponse>> enviarIngresoEjercicio(@Header("Content-Type") String content_type,
                                                                 @Path("id") long idLoginResponse,
                                                                 @Body EjercicioRequest ejercicioRequest,
                                                                 @Header("Authorization") String authHeader);

    /**
     * Firma para el método que inicializa la BD en el backend
     */
    @GET(URL_INICIALIZAR)
    Call<ResponseBody> inicializarBDBackEnd();

    @GET(URL_RECOMENDADOR)
    Call<RespuestaApi<RecomendadorResponse>> getRecomendacion(@Header("Content-Type") String content_type,
                                                              @Path("id") long idLoginResponse,
                                                              @Header("Authorization") String authHeader);

    @GET(URL_BUSQUEDA_MEDICO)
    Call<RespuestaApi<ListadoMedicoResponse>> getListadoMedico(@Header("Content-Type") String appJson,
                                                               @QueryMap Map<String, String> params,
                                                               @Header("Authorization") String authHeader);

    @POST(URL_SOLICITUD_MEDICO_PACIENTE)
    Call<RespuestaApi<SolicitudResponse>> enviarSolicitud(@Header("Content-Type") String content_type,
                                                          @Body SolicitudRequest request,
                                                          @Header("Authorization") String authHeader);

    @GET(URL_BUSQUEDA_SOLICITUD)
    Call<RespuestaApi<SolicitudResponse>> getSolicitud(@Header("Content-Type") String content_type,
                                                       @Path("idPaciente") long idPaciente,
                                                       @Path("idMedico") long idMedico,
                                                       @QueryMap Map<String, String> params,
                                                       @Header("Authorization") String authHeader);

    @DELETE(URL_CANCELACION_SOLICITUD)
    Call<RespuestaApi<SolicitudResponse>> cancelarSolicitud(@Header("Content-Type") String content_type,
                                                            @Path("idSolicitud") long id,
                                                            @Header("Authorization") String authHeader);

    @GET(URL_OBTENER_MIS_MEDICOS)
    Call<RespuestaApi<ListadoMedicoResponse>> getMedicosDelPaciente(@Header("Content-Type") String appJson,
                                                                    @Path("idPaciente") long id,
                                                                    @Header("Authorization") String authHeader);

    @Multipart
    @POST(URL_ENVIAR_FOTO_PERFIL)
    Call<RespuestaApi<LoginResponse>> enviarFotoDePerfil(@Path("idUsuario") long idPaciente,
                                                         @Part MultipartBody.Part image,
                                                         @Header("Authorization") String authHeader);

    @GET(URL_OBTENER_MIS_MEDICAMENTOS)
    Call<RespuestaApi<ListadoMedicamentoResponse>> getMedicamentosDelPaciente(@Header("Content-Type") String appJson,
                                                                              @Path("idPaciente") long id,
                                                                              @Header("Authorization") String authHeader);

    @GET(URL_BUSQUEDA_MEDICAMENTO)
    Call<RespuestaApi<ListadoMedicamentoResponse>> getListadoMedicamentos(@Header("Content-Type") String appJson,
                                                                          @QueryMap Map<String, String> params,
                                                                          @Header("Authorization") String authHeader);

    @POST(URL_AGREGAR_MEDICAMENTO)
    Call<RespuestaApi<BooleanResponse>> agregarMedicamento(@Header("Content-Type")String appJson,@Body MedicamentoPacienteRequest medicamentoPacienteRequest,@Header("Authorization") String s);


    @DELETE(URL_BORRAR_MEDICO)
    Call<RespuestaApi<BooleanResponse>> borrarMedico(@Header("Content-Type") String appJson,
                                                     @QueryMap Map<String, String> params,
                                                     @Header("Authorization") String authHeader);

    /**
     * Firma para el método que envia el registro de un allegado
     */
    @POST(URL_ALLEGADO_REGISTRO)
    Call<RespuestaApi<LoginResponse>> registrarAllegado(@Header("Content-Type") String content_type,
                                                        @Body RegistroAllegadoRequest nuevoUsuario);

    @GET(URL_OBTENER_MIS_PACIENTES)
    Call<RespuestaApi<ListadoPacienteResponse>> getPacientesDelMedico(@Header("Content-Type") String appJson,
                                                                      @Path("idMedico") long id,
                                                                      @Header("Authorization") String authHeader);


    /**
     * Firma para el método que envia una toma de medicamentos
     */
    @POST(URL_PACIENTE_TOMA_MEDICAMENTO)
    Call<RespuestaApi<IdResponse>> enviarTomaMedicamento(@Header("Content-Type") String content_type,
                                                                @Path("id") long idLoginResponse,
                                                                @Body TomaMedicamentoRequest tomaMedicamentoRequest,
                                                                @Header("Authorization") String authHeader);

    @GET(URL_BUSQUEDA_ALLEGADO)
    Call<RespuestaApi<ListadoAllegadoResponse>> getListadoAllegado(@Header("Content-Type") String appJson,
                                                                   @QueryMap Map<String, String> params,
                                                                   @Header("Authorization") String authHeader);
    @GET(URL_OBTENER_MIS_ALLEGADOS)
    Call<RespuestaApi<ListadoAllegadoResponse>> getAllegadosDelPaciente(@Header("Content-Type") String appJson,
                                                                        @Path("idPaciente") long id,
                                                                        @Header("Authorization") String authHeader);
    @DELETE(URL_BORRAR_ALLEGADO)
    Call<RespuestaApi<BooleanResponse>> borrarAllegado(@Header("Content-Type") String appJson,
                                                       @QueryMap Map<String, String> params,
                                                       @Header("Authorization") String authHeader);
    @POST(URL_SOLICITUD_ALLEGADO_PACIENTE)
    Call<RespuestaApi<SolicitudResponse>> enviarSolicitudAllegado(@Header("Content-Type") String content_type,
                                                                  @Body SolicitudRequest request,
                                                                  @Header("Authorization") String authHeader);
    @GET(URL_GET_SOLICITUDES_ALLEGADO)
    Call<RespuestaApi<ListadoSolicitudResponse>> getSolicitudesDelAllegado(@Header("Content-Type") String appJson,
                                                                                    @Path("idAllegado") long id,
                                                                                    @Header("Authorization") String authHeader);
    @PUT(URL_ACEPTACION_SOLICITUD)
    Call<RespuestaApi<SolicitudResponse>> aceptarSolicitud(@Header("Content-Type") String content_type,
                                                           @Path("idSolicitud") long id,
                                                           @Header("Authorization") String authHeader);

    @GET(URL_ENVIAR_NOTIFICACION_MEDICION_USUARIO_FIREBASE)
    Call<JSONObject> enviarNotificacionMedicionFirebaseAUsuario(@Header("Content-Type") String content_type,
                                                       @Path("idPaciente") long idPaciente,
                                                       @Path("idAvisado") long idAvisado,
                                                       @Path("medicion") String medicion,
                                                       @Path("ubicacion") String ubicacion,
                                                       @Header("Authorization") String authHeader);

    @GET(URL_ENVIAR_NOTIFICACION_MEDICACION_USUARIO_FIREBASE)
    Call<JSONObject> enviarNotificacionMedicacionFirebaseAUsuario(@Header("Content-Type") String content_type,
                                                                  @Path("idPaciente") long idPaciente,
                                                                  @Path("idAvisado") long idAvisado,
                                                                  @Path("horaMedicacion") long horaMedicacion,
                                                                  @Header("Authorization") String authHeader);

    @GET(URL_OBTENER_MIS_PACIENTES_ALLEGADO)
    Call<RespuestaApi<ListadoPacienteResponse>> getPacientesDelAllegado(@Header("Content-Type") String content_type,
                                                                        @Path("idAllegado") long id,
                                                                        @Header("Authorization") String authHeader);

    @POST(URL_OBTENER_MEDICIONES_PACIENTES)
    Call<RespuestaApi<ListadoMedicionesResponse>> getMedicionesPaciente(@Header("Content-Type") String content_type,
                                                                        @Path("id") long id,
                                                                        @Body long idPaciente,
                                                                        @Header("Authorization") String authHeader);

    @GET(URL_BUSQUEDA_ALIMENTO)
    Call<RespuestaApi<ListadoAlimentoResponse>> getListadoAlimentos(@Header("Content-Type") String appJson,
                                                                       @QueryMap Map<String, String> params,
                                                                       @Header("Authorization") String authHeader);

    @Multipart
    @POST(URL_INGRESO_ALIMENTO)
    Call<RespuestaApi<IdResponse>> enviarIngresoAlimento(@Path("id") long idLoginResponse,
                                                         @Part MultipartBody.Part file,
                                                         @Part("data") RequestBody data,
                                                         @Header("Authorization") String authHeader);
    @POST(URL_ENVIAR_USUARIO_TOKEN_FIREBASE)
    Call<RespuestaApi<LoginResponse>> enviarTokenFirebaseDeUsuario(@Header("Content-Type") String content_type,
                                                                      @Body FirebaseTokenRequest ftr,
                                                                      @Header("Authorization") String authHeader);

    @GET(URL_OBTENER_USUARIO_TOKEN_FIREBASE)
    Call<RespuestaApi<FirebaseTokenResponse>> obtenerTokenFirebaseDeUsuario(@Header("Content-Type") String content_type,
                                                                            @Path("idUsuario") long id,
                                                                            @Header("Authorization") String authHeader);
    @GET(URL_OBTENER_CHAT_PACIENTE)
    Call<RespuestaApi<ListadoUsuariosChatsResponse>> getChatsDelPaciente(@Header("Content-Type") String content_type,
                                                                         @Header("Authorization") String authHeader,
                                                                         @QueryMap Map<String, String> params);
    @GET(URL_OBTENER_MENSAJES_CHAT)
    Call<RespuestaApi<ListadoMensajeWrapperResponse>> getMensajesDelChat(@Header("Content-Type") String content_type,
                                                                         @Header("Authorization") String authHeader,
                                                                         @QueryMap Map<String, String> params);

    @POST(URL_MARCAR_CHAT_COMO_LEIDO)
    Call<RespuestaApi<EmptyResponse>> marcarMensajesComoLeidos(@Header("Content-Type") String content_type,
                                                               @Header("Authorization") String authHeader,
                                                               @Body  MarcarMensajesComoLeidosRequest request);

    @POST(URL_CAMBIAR_ESTADO_CHAT)
    Call<RespuestaApi<EmptyResponse>> actualizarEstado(@Header("Content-Type") String content_type,
                                                       @Header("Authorization") String authHeader,
                                                       @Body  CambiarEstadoRequest request);


    @POST(URL_PACIENTE_FACTORES_CORRECCION)
    Call<RespuestaApi<IdResponse>> enviarFactoresCorreccion(@Header("Content-Type") String content_type,
                                                   @Path("id") long idLoginResponse,
                                                   @Body FactorCorreccionInsulinaRequest data,
                                                   @Header("Authorization") String authHeader);
    @POST(URL_AGREGAR_CONTACTO)
    Call<RespuestaApi<IdResponse>> enviarContacto(@Header("Content-Type") String content_type,
                                                  @Header("Authorization") String authHeader,
                                                  @Body  ContactoRequest contactoRequest,
                                                  @Path("id") long idPaciente);

    @GET(URL_OBTENER_CONTACTOS)
    Call<RespuestaApi<ListadoContactoResponse>> getContactos(@Header("Content-Type") String appJson,
                                                             @Path("id") long idPaciente,
                                                             @Header("Authorization")  String token);
    @DELETE(URL_BORRAR_CONTACTO)
    Call<RespuestaApi<IdResponse>> borrarContacto(@Header("Content-Type") String appJson,
                                                  @Path("id") Long idBackend,
                                                  @Header("Authorization") String token);

    @GET(URL_PACIENTE_INFO_TARJETA_AUMENTADA)
    Call<RespuestaApi<InfoTarjetaAumentadaResponse>> getInfotarjetaAumentada(@Header("Content-Type") String content_type,
                                                                             @Path("id") long id,
                                                                             @Header("Authorization") String authHeader);

    @POST(URL_CAMBIAR_SUSCRIPCION)
    Call<RespuestaApi<BooleanResponse>> enviarCambioSuscripcion(@Header("Content-Type") String content_type,
                                                  @Header("Authorization") String authHeader,
                                                  @Body SuscripcionPlanRequest suscripcionPlanRequest,
                                                  @Path("id") long idPaciente);

    @POST(URL_MEDICAMENTOS_TOMADOS)
    Call<RespuestaApi<ListadoMedicamentosIngeridosResponse>> getMedicamentosTomadosPaciente(@Header("Content-Type") String content_type,
                                                                                            @Path("idPaciente") long id,
                                                                                            @Header("Authorization") String authHeader,
                                                                                            @Body ListadoMedicamentoTomadosRequest listadoMedicamentoTomadosRequest);
    @POST(URL_INYECCIONES_INSULINA)
    Call<RespuestaApi<ListadoInyeccionInsulinaResponse>> getInsulinaInyectadaPaciente(@Header("Content-Type") String content_type,
                                                                                      @Path("idPaciente") long id,
                                                                                      @Header("Authorization") String authHeader,
                                                                                      @Body ListadoInyeccionInsulinaRequest listadoInyeccionInsulinaRequest);
    @POST(URL_ALIMENTOS)
    Call<RespuestaApi<ListadoAlimentoIngeridoResponse>> getAlimentosPaciente(@Header("Content-Type") String content_type,
                                                                             @Path("idPaciente") long id,
                                                                             @Header("Authorization") String authHeader,
                                                                             @Body  ListadoAlimentoIngeridoRequest listadoAlimentoIngeridoRequest);
    @POST(URL_EJERCICIOS)
    Call<RespuestaApi<ListadoEjercicioResponse>> getEjerciciosPaciente(@Header("Content-Type") String content_type,
                                                                       @Path("idPaciente") long id,
                                                                       @Header("Authorization") String authHeader,
                                                                       @Body ListadoEjercicioRequest listadoEjercicioRequest);
}

