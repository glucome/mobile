package com.glucomeandroidapp.Conexiones;

import android.util.Log;

import androidx.annotation.NonNull;

import com.glucomeandroidapp.App;
import com.glucomeandroidapp.POJO.APIClasses.Response.AbstractResponse;
import com.glucomeandroidapp.POJO.APIClasses.Response.RespuestaApi;
import com.glucomeandroidapp.Receivers.RefreshTokenReceiver;
import com.glucomeandroidapp.Utils.LoadingDialogManager;
import com.glucomeandroidapp.Utils.Util;
import com.glucomeandroidapp.Utils.UtilesFunciones;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.glucomeandroidapp.Utils.Util.BAD_REQUEST;
import static com.glucomeandroidapp.Utils.Util.OK;
import static com.glucomeandroidapp.Utils.Util.OK_REQUEST;
import static com.glucomeandroidapp.Utils.Util.TAG;
import static com.glucomeandroidapp.Utils.Util.TOKEN_EXPIRADO;

public class APIManager {
    public static <T extends AbstractResponse> void obtenerRespuesta(Call<RespuestaApi<T>> request, final CallListener<T> listener) {
        if(UtilesFunciones.hayConexionAInternet()){
            request.enqueue(new Callback<RespuestaApi<T>>() {
                @Override
                public void onResponse(@NonNull Call<RespuestaApi<T>> call,
                                       @NonNull Response<RespuestaApi<T>> response) {
                    try {
                        // Si el código de respuesta es 200
                        if (response.isSuccessful() && response.code() == OK_REQUEST) {
                            RespuestaApi res = response.body();
                            if (res != null) {
                                if (res.getMensaje().equals(OK)) {
                                    T respuesta = (T) res.getRespuesta();
                                    if (respuesta != null)
                                        listener.procesarRespuesta(respuesta);
                                }
                            } else {
                                listener.procesarError(response.code());
                            }
                        } else {
                            // En caso de que el código de respuesta no sea exitoso
                            listener.procesarError(response.code());
                            switch (response.code()) {
                                case BAD_REQUEST:
                                    Log.e(TAG, "BAD REQUEST " + response.code());
                                    break;
                                case TOKEN_EXPIRADO:
                                    Log.e(TAG, "TOKEN EXPIRADO " + response.code());
                                    RefreshTokenReceiver.setAlarm(App.getContext(),
                                            LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
                                    break;
                                default:
                                    Log.e(TAG, "CÓDIGO DE ERROR: " + response.code());
                                    break;
                            }
                        }
                    } catch (Exception e) {
                        listener.procesarError(Util.EXCEPCION);
                    }
                    LoadingDialogManager.ocultar();
                }

                @Override
                public void onFailure(@NonNull Call<RespuestaApi<T>> call,
                                      @NonNull Throwable t) {
                    Log.e(TAG, "FAILURE: " + t.getMessage());
                    listener.procesarError(Util.EXCEPCION);
                    LoadingDialogManager.ocultar();
                }
            });
        }else{
            listener.procesarError(Util.SIN_CONEXION);
            LoadingDialogManager.ocultar();
        }
    }
}
