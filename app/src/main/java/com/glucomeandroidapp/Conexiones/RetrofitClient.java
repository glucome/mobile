package com.glucomeandroidapp.Conexiones;


/**
 * Created by MartinArtime on 08/01/2018.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Clase cliente de la API de Retrofit para realizar la conexion con el servidor web
 */
public class RetrofitClient {

    private static Retrofit sRetrofit = null;

    /**
     * Obtener el cliente dada una url base, se le agrega el convertidor de gson para clases POJO
     * @param baseUrl, URL base
     * @return un objeto sRetrofit singleton
     */
    public static Retrofit getClient(String baseUrl) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        /**
         *  Descomentar lo de abajo si se quiere ver logueada la respuesta del servidor
         */

     /*   httpClient.addInterceptor(chain -> {
            Request request =
                    chain.request().newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response response = chain.proceed(request);


            if (response.body() != null) {
                for (int i=0; i<response.headers().size()-1; i++) {
                    Log.d("APP", response.headers().name(i) + response.headers().value(i));
                }
                Log.d("APP", Objects.requireNonNull(response.body().contentType()).toString());
                Log.d("APP", response.message());
                Log.d("APP", response.body().string());
            }

            return response;
        });*/

        Gson gson = new GsonBuilder()
                // Crear otra linea como la de abajo por cada clase a que hereda de "Response"
                // en el backend y no esté incluida acá, sino no la va a poder convertir
                // gson al objeto "nativo" local.
                //.registerTypeAdapter(LoginResponse.class, new MyDeserializer<LoginResponse>())
                //.registerTypeAdapter(LoginResponse.class, new MyDeserializer<MedicionResponse>())
                .create();


        if (sRetrofit==null) {
            sRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return sRetrofit;
    }
}