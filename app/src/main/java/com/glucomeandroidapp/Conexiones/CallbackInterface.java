package com.glucomeandroidapp.Conexiones;

public interface CallbackInterface<T> {
    void onSuccess(T respuesta);
}
