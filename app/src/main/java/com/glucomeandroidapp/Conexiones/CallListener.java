package com.glucomeandroidapp.Conexiones;

import com.glucomeandroidapp.POJO.APIClasses.Response.AbstractResponse;

public interface CallListener<T extends AbstractResponse> {
    void procesarRespuesta(T respuesta);
    void procesarError(int codigoError);
}
