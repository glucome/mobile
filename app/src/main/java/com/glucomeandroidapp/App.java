package com.glucomeandroidapp;

import android.app.Application;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import com.glucomeandroidapp.Utils.Util;

import java.util.HashMap;
import java.util.Map;

import static com.glucomeandroidapp.Utils.Util.BUNDLE_NAME;
import static com.glucomeandroidapp.Utils.Util.INPUT_EDAD;
import static com.glucomeandroidapp.Utils.Util.INPUT_EMAIL;
import static com.glucomeandroidapp.Utils.Util.INPUT_NOMBRE;
import static com.glucomeandroidapp.Utils.Util.INPUT_PASSWORD;
import static com.glucomeandroidapp.Utils.UtilesFunciones.getAPIService;

/**
 * Instancia de la aplicación que corre antes que las activities e inicializa
 *
 * Creado por MartinArtime el 25 de abril del 2019
 */
public class App extends Application {

    /**
     *  Objetos útiles para usar en toda la app
     */

    private SharedPreferences preferences = null;

    /* Obtener instancia desde cualquier lado */
    private static App instance;

    public static App getInstance() {
        return instance;
    }

    public static Context getContext(){
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();

        // Inicializo objetos importantes en el funcionamiento general de la app
//        Util.crypto = new Crypto();
        //Util.gson = new Gson();
        Util.apiService = getAPIService();

        //TODO Base de datos SQLite DE PRUEBA, PARA PROD SE HACE DISTINTO, HAY QUE
        //     PONER VERSION DE LA BD SIENDO USADA PARA CONSISTENCIA.
//        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, DB_NAME);
//        Database db = helper.getWritableDb();
//        //DaoMaster.dropAllTables(db, true);
//        Util.DataBaseManager.getDaoSession() = new DaoMaster(db).newSession();

        // Para borrar todas las sharedPreferences - SOLO PARA TEST
        // SharedPreferences preferences = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        // preferences.edit().clear().apply();
    }


    ////////////////// MÉTODOS ÚTILES EXTRA //////////////////

    /**
     * Método que se encarga de iniciar una pestaña web con búsqueda
     */
    public void iniciarBusquedaWeb(String query){
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, query);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * Método que se encarga de iniciar una Activity a partir de otra
     *
     * @param claseAcitivy, que Activity abrir con el método
      */
    public void iniciarActividad(Class claseAcitivy){
        Intent intent = new Intent(App.getContext(), claseAcitivy);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //intent.putExtra(BUNDLE_NAME, null);
        /* Voy a comentar esto porque jode más de lo que ayuda, podemos mandar directamente un Bundle por ahora
        if(b!=null && b.length>0){
            if(b[0] instanceof Bundle){
                Bundle bundle = (Bundle) b[0];
                intent.putExtra(bundle.getString(BUNDLE_NAME), bundle);
            }
        }*/
        startActivity(intent);
    }

    /**
     * Método que se encarga de iniciar una Activity a partir de otra
     *
     * @param claseAcitivy, que Activity abrir con el método
     * @param bundle, con los datos a enviar
     */
    public void iniciarActividad(Class claseAcitivy, Bundle bundle){
        Intent intent = new Intent(App.getContext(), claseAcitivy);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(BUNDLE_NAME, bundle);
        /* Voy a comentar esto porque jode más de lo que ayuda, podemos mandar directamente un Bundle por ahora
        if(b!=null && b.length>0){
            if(b[0] instanceof Bundle){
                Bundle bundle = (Bundle) b[0];
                intent.putExtra(bundle.getString(BUNDLE_NAME), bundle);
            }
        }*/
        startActivity(intent);
    }

    /**
     * Método que se encarga de iniciar una Activity a partir de si misma
     *
     * @param intent, intent de la activity
     */
    public void iniciarActividad(Intent intent){
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //intent.putExtra(BUNDLE_NAME, null);
        /* Voy a comentar esto porque jode más de lo que ayuda, podemos mandar directamente un Bundle por ahora
        if(b!=null && b.length>0){
            if(b[0] instanceof Bundle){
                Bundle bundle = (Bundle) b[0];
                intent.putExtra(bundle.getString(BUNDLE_NAME), bundle);
            }
        }*/
        startActivity(intent);
    }



    /**
     * Muestra un toast en la vista con un mensaje
     * @param mensaje que debe ser mostrado en el Toast
     */
    public void mostrarMensajeFlotante(String mensaje){
        Toast.makeText(getBaseContext(), mensaje, Toast.LENGTH_LONG).show();
    }

    /**
     *  Método "genérico" que valida el input de distintas pantallas
     *  @param input, un hashmap con el nombre del input y el valor
     *  @return un hashmap con el nombre del input y la respuesta
     */
    public boolean validarInput(HashMap<String, String> input, HashMap<String, String> respuesta) {
        boolean valido = true;

        for(Map.Entry<String, String> entry : input.entrySet()) {
            String vista = entry.getKey();
            String dato = entry.getValue();

            switch (vista) {
                case INPUT_EMAIL:
                    if (dato.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(dato).matches()) {
                        respuesta.put(INPUT_EMAIL, "Ingresar un email válido");
                        valido = false;
                    } else {
                        respuesta.put(INPUT_EMAIL, null);
                    }
                    break;
                case INPUT_PASSWORD:
                    if (dato.isEmpty() || dato.length() < 4) {
                        respuesta.put(INPUT_PASSWORD, "Ingresar al menos 4 caracteres");
                        valido = false;
                    } else {
                        respuesta.put(INPUT_PASSWORD, null);
                    }
                    break;
                case INPUT_NOMBRE:
                    if (dato.isEmpty() || dato.length() < 3) {
                        respuesta.put(INPUT_NOMBRE, "Ingresar al menos 3 caracteres");
                        valido = false;
                    } else {
                        respuesta.put(INPUT_NOMBRE, null);
                    }
                    break;
                case INPUT_EDAD:
                    if(!dato.isEmpty()){
                        Integer edad = Integer.valueOf(dato);
                        if (edad < 1 || edad > 150) {
                            respuesta.put(INPUT_EDAD, "Ingresar una edad válida");
                            valido = false;
                        } else {
                            respuesta.put(INPUT_EDAD, null);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        return valido;
    }

}