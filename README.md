# Mobile

*  Abrir Android Studio
*  Seleccionar la opción "Import Project (Gradle, Eclipse ADT, etc)"
*  Seleccionar la carpeta raíz donde se clono este repositorio (mobile)

# Firma de la APP
En Android Studio:
* Ir a "File -> Project Structure -> Modules -> Signing Configs".
* Agregar dos configuraciones para firmar la APP con "Add Signing Config (+)", una con el archivo "debug.keystore" y otra con "release.keystore".
* Ir a la pestaña "Default Config" dentro de "Modules" .
* En Signing Config seleccionar la configuración a utilizar para firmar la APP.